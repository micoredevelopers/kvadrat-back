<?php


use App\Http\Controllers\TrialModalController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;


    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => ['localizationRedirect'],
        ], function () {
        Route::group(
            [
                'prefix' => CityHelper::getCityPrefix()
            ], function () {

        Route::get('/', [HomeController::class, 'index'])->name('home');

        Route::get('/level-up-course', [PageController::class, 'levelUp'])->name('levelUp');
        Route::get('/community-course', [PageController::class, 'community'])->name('community');
        Route::get('/square-introductory-course', [PageController::class, 'intro'])->name('intro');
        Route::get('/franchise-page', [PageController::class, 'franch'])->name('franch');
        Route::get('/trial-lesson', [PageController::class, 'trial'])->name('trial');

        Route::post('trial-modal', [TrialModalController::class, 'saveModal'])->name('trial.modal');
        Route::post('franch-modal', [\App\Http\Controllers\FranchModalController::class, 'saveModal'])->name('franch.modal');
//    Route::post('feedback/send', [FeedbackController::class, 'send'])->name('feedback.send');
//    Route::post('feedback/report', [FeedbackController::class, 'report'])->name('feedback.report');


    });
});