<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CityController;

Route::as('api.')->group(function () {

	Route::get('cities', [CityController::class, 'cities']);
	Route::prefix('categories')->group(function () {
		Route::get('/', [CategoryController::class, 'categories'])->name('');
		Route::get('/sub', [CategoryController::class, 'subcategories'])->name('categories.subcategories');
		Route::get('/{category}', [CategoryController::class, 'category']);
	});
});
