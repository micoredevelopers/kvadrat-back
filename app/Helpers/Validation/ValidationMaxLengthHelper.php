<?php


namespace App\Helpers\Validation;


class ValidationMaxLengthHelper
{

   public const TINYINT = 127;
   public const TINYINT_UNSIGNED = 255;
   public const SMALLINT = 32767;
   public const MEDIUMINT = 8388607;
   public const MEDIUMINT_UNSIGNED = 16777215;
   public const SMALLINT_UNSIGNED = 65535;
   public const INT = 2147483647;
   public const INT_UNSIGNED = 4294967295;
   public const BIGINT = 9223372036854775807;
   public const BIGINT_UNSIGNED = 18446744073709551615;
   //
   public const CHAR = 256;
   public const VARCHAR = 65535;
   public const TINYTEXT = 256;
   public const TEXT = 65535;
   public const MEDIUMTEXT = 16777215;
   public const LONGTEXT = 4294967295;


}