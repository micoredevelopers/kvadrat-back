<?php

use App\Models\Translate\Translate;

if (!function_exists('getTranslate')) {
	function getTranslate($key, $defaultText = '', $asObject = false)
	{
		\App\Helpers\Sidebar\PageUsedTranslateHelper::addTranslate($key);
		//todo remove crutch
		$translate = Translate::getTranslate($key, $asObject);
		if (isLocalEnv() && null === $translate){
			debugInfo(sprintf('Отсутствует локализация - %s',$key));
		}
		$text = $translate ?? $defaultText ?: $key;
		if (!$asObject && isAdmin() && request()->has('kek')){
			$text = $key . ' '  . $text;
		}

		return $text;
	}
}

if (!function_exists('translateFormat')) {
	function translateFormat($key, array $values)
	{
		/** @var $translate Translate */
		$translate = getTranslate($key, false, true);
		if ($translate instanceof Translate) {
			return str_replace(array_keys($values), array_values($values), $translate->value);
		}

		return '';
	}
}

function btnTranslateEdit($key)
{
	return sprintf(
		'<a target="_blank" href="%s?search=%s" class="badge badge-danger">%s</a>',
		 route('translate.index'),
		$key,
		\Illuminate\Support\Str::limit(getTranslate($key), 20)
	);
}

if (!function_exists('translateYesNo')) {
	function translateYesNo($condition)
	{
		return $condition ? getTranslate('global.yes') : getTranslate('global.no');
	}
}