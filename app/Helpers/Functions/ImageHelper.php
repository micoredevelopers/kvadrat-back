<?php

	use Illuminate\Support\Facades\Storage;
	use Illuminate\Support\Str;

	function imgPathOriginal($pathToImage)
	{
		return Str::replaceLast('_s.', '.', (string)$pathToImage);
	}

	function imgPathThumbnail($pathToImage)
	{
		return Str::replaceLast('.', '_s.', (string)$pathToImage);
	}


	if (!function_exists('getPathToImage')) {
		function getPathToImage(?string $src = null, $default = 'images/default.png', $disk = null)
		{
			if (isExternalFile($src)) {
				return $src;
			}
			return storageFileExists($src, $disk) ? getStorageFilePath($src, $disk) : asset($default);
		}
	}

	function getPathToOrderImage(?string $src = null, $default = 'images/default-order.svg', $disk = null)
	{
		return getPathToImage($src, $default);
	}

	if (!function_exists('isExternalFile')) {
		function isExternalFile($src)
		{
			return isStringUrl($src);
		}
	}

	function defaultImage()
	{
		return asset('images/staff/default.png');
	}