<?php


namespace App\Helpers\Date;


use App\Helpers\Date\DaysName\DaysNameHelper;
use App\Helpers\Date\HoursName\HoursNameHelper;
use App\Helpers\Date\MinutesName\MinutesNameHelper;
use App\Helpers\Date\MonthName\MonthNameHelper;
use App\Helpers\Date\YearName\YearNameHelper;
use Illuminate\Support\Carbon;

class DateNameHelper
{
	/**
	 * @var Carbon|null
	 */
	private $date;

	private $withAgo = false;
	/**
	 * @var MinutesNameHelper
	 */
	private $minutesNameHelper;
	/**
	 * @var DaysNameHelper
	 */
	private $daysNameHelper;
	/**
	 * @var HoursNameHelper
	 */
	private $hoursNameHelper;
	/**
	 * @var MonthNameHelper
	 */
	private $monthNameHelper;
	/**
	 * @var YearNameHelper
	 */
	private $yearNameHelper;

	public function __construct(
		?Carbon $date = null,
		YearNameHelper $yearNameHelper,
		MonthNameHelper $monthNameHelper,
		DaysNameHelper $daysNameHelper,
		HoursNameHelper $hoursNameHelper,
		MinutesNameHelper $minutesNameHelper
	)
	{
		$this->date = $date;
		$this->daysNameHelper = $daysNameHelper;
		$this->hoursNameHelper = $hoursNameHelper;
		$this->minutesNameHelper = $minutesNameHelper;
		$this->monthNameHelper = $monthNameHelper;
		$this->yearNameHelper = $yearNameHelper;
	}

	public static function make(?Carbon $date = null): self
	{
		return app(__CLASS__, compact('date'));
	}

	public function default()
	{
		if (null === $this->date) {
			return '';
		}
		$dateTarget = $this->date;
		$now = now();
		$days = $now->diffInDays($dateTarget);
		$diffHours = $now->diffInHours($dateTarget);
		$yearDiff = $now->diffInYears($dateTarget);
		$diffMinutes = $now->diffInMinutes($dateTarget);
		if ($days > 1) {
			//6 декабря, в 11:58
			return $yearDiff ? $dateTarget->formatLocalized('%e %B, %Y') : $dateTarget->formatLocalized('%e %B, ' . __('date.in') . ' %R');
		}
		if (1 === $days) {
			//Вчера в 11:58
			return $dateTarget->formatLocalized(__('date.yesterday-at') . ' %R');
		}
		if ($diffHours > 4) {
			return $dateTarget->formatLocalized(__('date.today-at') . ' %R');
		}
		if ($diffHours) {
			$matches = [
				1 => __('date.hours.one'),
				2 => __('date.hours.two'),
				3 => __('date.hours.three'),
				4 => __('date.hours.four'),
			];
			if (!array_key_exists($diffHours, $matches)) {
				return '';
			}
			return $this->addAgo($matches[$diffHours]);
		}
		if ($diffMinutes) {
			return $this->addAgo($diffMinutes . ' ' . $this->minutesNameHelper->getName($diffMinutes));
		}
		return __('date.now');
	}

	/**
	 * @return bool
	 */
	public function isWithAgo(): bool
	{
		return $this->withAgo;
	}

	public function setWithAgo(bool $withAgo): self
	{
		$this->withAgo = $withAgo;
		return $this;
	}

	protected function addAgo($date): string
	{
		if (!$this->isWithAgo()) {
			return $date;
		}
		return sprintf('%s %s', $date, __('date.ago'));
	}

	public function doubleValue($glue = null): string
	{
		$glue = $glue ?: __('deal.date.glue');
		$diff = now()->diffAsCarbonInterval($this->date);
		$years = $diff->y;
		$months = $diff->m;
		$days = $diff->d;
		$hours = $diff->h;
		$minutes = $diff->i;

		if ($years) {
			return $this->addAgo($this->concat($glue, [$this->textYears($years), $this->textMonths($months)]));
		}
		if ($months) {
			return $this->addAgo($this->concat($glue, [$this->textMonths($months), $this->textDays($days)]));
		}
		if ($days) {
			return $this->addAgo($this->concat($glue, [$this->textDays($days), $this->textHours($hours)]));
		}
		if ($hours) {
			return $this->addAgo($this->concat($glue, [$this->textHours($hours), $this->textMinutes($minutes)]));
		}
		return $minutes ? $this->addAgo($this->textMinutes($minutes)) : '';

	}

	public function textYears($num): string
	{
		if (!$num) {
			return '';
		}
		return $num . ' ' . $this->yearNameHelper->getName($num);
	}

	public function textMonths($num): string
	{
		if (!$num) {
			return '';
		}
		return $num . ' ' . $this->monthNameHelper->getName($num);
	}

	public function textDays($num): string
	{
		if (!$num) {
			return '';
		}
		return $num . ' ' . $this->daysNameHelper->getName($num);
	}

	public function textHours($num): string
	{
		if (!$num) {
			return '';
		}
		return $num . ' ' . $this->hoursNameHelper->getName($num);
	}

	public function textMinutes($num): string
	{
		if (!$num) {
			return '';
		}
		return $num . ' ' . $this->minutesNameHelper->getName($num);
	}

	private function concat($glue, array $dates)
	{
		return implode($glue, array_filter($dates, 'strlen'));
	}
}