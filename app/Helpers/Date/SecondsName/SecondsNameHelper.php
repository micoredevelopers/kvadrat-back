<?php

namespace App\Helpers\Date\SecondsName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;
use App\Helpers\Date\MinutesName\MinutesNameHelper;

class SecondsNameHelper
{
	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;
	/**
	 * @var bool
	 * Применять именительный падеж?
	 */
	private $isNominative = false;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getNameBySeconds(int $hours): string
	{
		$hours = $this->numbersExtractor->extractTenth($hours);
		$hours = $this->numbersExtractor->getForMatch($hours);
		$matches = $this->getMatches();
		return $matches[$hours] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => ($this->isNominative ? 'секунда' :'секунду'),
			'2-4'  => 'секунды',
			'5-20' => 'секунд',
		];
		return $this->rangeGenerator->generate($range);
	}

	/**
	 * @param bool $isNominative
	 * @return SecondsNameHelper
	 */
	public function setIsNominative(bool $isNominative): self
	{
		$this->isNominative = $isNominative;
		return $this;
	}
}