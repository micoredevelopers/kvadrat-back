<?php


namespace App\Helpers\Date\YearName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;
use Illuminate\Support\Str;

class YearNameHelper
{

	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getName(int $year): string
	{
		$year = $this->numbersExtractor->extractTenth($year);
		$year = $this->numbersExtractor->getForMatch($year);
		$matches = $this->getMatches();
		return $matches[$year] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => 'год',
			'2-4'  => 'года',
			'5-20' => 'лет',
		];
		return  $this->rangeGenerator->generate($range);
	}
}