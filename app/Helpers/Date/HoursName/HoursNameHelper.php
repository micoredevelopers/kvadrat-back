<?php

namespace App\Helpers\Date\HoursName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;
use Illuminate\Support\Str;

class HoursNameHelper
{
	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getName(int $hour): string
	{
		$hour = $this->numbersExtractor->extractTenth($hour);
		$hour = $this->numbersExtractor->getForMatch($hour);
		$matches = $this->getMatches();
		return $matches[$hour] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => 'час',
			'2-4'  => 'часа',
			'5-20' => 'часов',
		];
		return $this->rangeGenerator->generate($range);
	}
}