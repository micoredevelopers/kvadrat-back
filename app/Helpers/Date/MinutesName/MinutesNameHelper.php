<?php

namespace App\Helpers\Date\MinutesName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;

class MinutesNameHelper
{
	/**
	 * @var bool
	 * Применять именительный падеж?
	 */
	private $isNominative = false;
	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getName(int $hours): string
	{
		$hours = $this->numbersExtractor->extractTenth($hours);
		$hours = $this->numbersExtractor->getForMatch($hours);
		$matches = $this->getMatches();
		return $matches[$hours] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => ($this->isNominative ? 'минута' : 'минуту'),
			'2-4'  => 'минуты',
			'5-20' => 'минут',
		];
		return $this->rangeGenerator->generate($range);
	}

	/**
	 * @param bool $isNominative
	 * @return MinutesNameHelper
	 */
	public function setIsNominative(bool $isNominative): self
	{
		$this->isNominative = $isNominative;
		return $this;
	}
}