<?php


namespace App\Helpers\Date\MonthName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;
use Illuminate\Support\Str;

class MonthNameHelper
{

	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getName(int $month): string
	{
		$month = $this->numbersExtractor->extractTenth($month);
		$month = $this->numbersExtractor->getForMatch($month);
		$matches = $this->getMatches();
		return $matches[$month] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => 'месяц',
			'2-4'  => 'месяца',
			'5-14' => 'месяцев',
		];
		return  $this->rangeGenerator->generate($range);
	}
}