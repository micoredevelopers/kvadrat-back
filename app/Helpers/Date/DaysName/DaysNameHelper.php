<?php

namespace App\Helpers\Date\DaysName;


use App\Helpers\Date\DateNamesHelpers\NumbersExtractor;
use App\Helpers\Date\DateNamesHelpers\RangeGenerator;
use Illuminate\Support\Str;

class DaysNameHelper
{
	/**
	 * @var NumbersExtractor
	 */
	private $numbersExtractor;
	/**
	 * @var RangeGenerator
	 */
	private $rangeGenerator;

	public function __construct(NumbersExtractor $numbersExtractor, RangeGenerator $rangeGenerator)
	{
		$this->numbersExtractor = $numbersExtractor;
		$this->rangeGenerator = $rangeGenerator;
	}

	public function getName(int $day): string
	{
		$day = $this->numbersExtractor->extractTenth($day);
		$day = $this->numbersExtractor->getForMatch($day);
		$matches = $this->getMatches();
		return $matches[$day] ?? '';
	}

	private function getMatches(): array
	{
		$range = [
			'1'    => 'день',
			'2-4'  => 'дня',
			'5-14' => 'дней',
		];
		return  $this->rangeGenerator->generate($range);
	}
}