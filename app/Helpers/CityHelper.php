<?php

namespace App\Helpers;

use App\DataContainers\City\CityRouteData;
use App\Enum\CitiesEnums;

/**
 * Class CityHelper
 * @package App\Helpers
 * @method static string getCityPrefix()
 * @method static CitiesEnums getCity()
 */
class CityHelper
{
    /**
     * @var CityRouteData
     */
    public static $cityRouteData;

    public static function __callStatic($name, $arguments)
    {
        if (!self::$cityRouteData instanceof CityRouteData) {
            throw new \InvalidArgumentException();
        }

        if (method_exists(self::$cityRouteData, $name)) {
            return self::$cityRouteData->{$name}(...$arguments);
        }

        throw new \RuntimeException('City not defined');
    }

}