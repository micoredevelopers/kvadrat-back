<?php


namespace App\Helpers\Dev;


class BladeLoopAutocomplete
{
	public $iteration = 1;
	public $index = 0;
	public $remaining = 0;
	public $count = 1;
	public $parent = null;
	public $first = true;
	public $last = true;
	public $odd = true;
	public $even = false;
	public $depth = 1;

}