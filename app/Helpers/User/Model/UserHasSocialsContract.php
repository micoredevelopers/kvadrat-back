<?php


namespace App\Helpers\User\Model;


interface UserHasSocialsContract
{
	public function getViber(): string;

	public function getTelegram(): string;

	public function getFacebook(): string;

	public function getInstagram(): string;

	public function getSite(): string;

	public function getSocialByKey(string $key): string;

}