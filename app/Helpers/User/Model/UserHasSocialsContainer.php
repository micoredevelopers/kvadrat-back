<?php


namespace App\Helpers\User\Model;


use Illuminate\Database\Eloquent\Model;

class UserHasSocialsContainer implements UserHasSocialsContract
{

	/**
	 * @var Model
	 */
	private $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	private function get($social): string
	{
		return (string)$this->model->getAttribute($social);
	}

	public function getViber(): string
	{
		return $this->get('viber');
	}

	public function getTelegram(): string
	{
		return $this->get('telegram');
	}

	public function getFacebook(): string
	{
		return $this->get('facebook');
	}

	public function getInstagram(): string
	{
		return $this->get('instagram');
	}

	public function getSite(): string
	{
		return $this->get('site');
	}

	public function getSocialByKey(string $key): string
	{
		return $this->get($key);
	}
}