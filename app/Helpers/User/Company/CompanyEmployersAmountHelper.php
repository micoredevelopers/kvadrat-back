<?php


namespace App\Helpers\User\Company;


use Illuminate\Support\Arr;

class CompanyEmployersAmountHelper
{
	public static function getAmounts(): array
	{
		return ['less-10' => 'Меньше 10 сотрудников', '10-50' => '10 - 50 сотрудников', '50-250' => '50 - 250 сотрудников',];
	}

	public static function getKeys(): array
	{
		return array_keys(self::getAmounts());
	}

	public static function getValues(): array
	{
		return array_values(self::getAmounts());
	}

	public static function amountExists(string $type): bool
	{
		return Arr::has(self::getAmounts(), $type);
	}

	public static function getByKey($key): string
	{
		return self::getAmounts()[$key] ?? '';
	}

}