<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\AbstractRequest;
use App\Rules\Feedback\FeedbackThrottle;

class FileFeedbackRequest extends AbstractRequest
{
	protected $fillableFields = ['type', 'ip'];

	public function rules()
	{
		return [
			'name'    => $this->getRuleRequiredChar(),
			'phone'   => $this->getBasePhoneRule(),
            'city',
		];
	}

	protected function mergeRequestValues()
	{
		$this->merge(['ip' => $this->ip()]);
	}

	public function messages()
	{
		return [
			'files.max' => 'Максимум :max файлов',
		];
	}
}
