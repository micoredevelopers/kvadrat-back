<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\AbstractRequest;
use App\Rules\Feedback\FeedbackThrottle;

class FranchFeedbackRequest extends AbstractRequest
{
	protected $fillableFields = ['type', 'ip'];

	public function rules()
	{
		return [
			'name'    => $this->getRuleRequiredChar(),
			'phone'   => $this->getBasePhoneRule(),
		];
	}

}
