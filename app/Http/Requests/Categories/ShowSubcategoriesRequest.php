<?php declare(strict_types=1);

namespace App\Http\Requests\Categories;

use App\Http\Requests\AbstractRequest;

class ShowSubcategoriesRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'category' => ['required', 'exists:categories,id']
		];
	}
}
