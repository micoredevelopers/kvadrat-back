<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;

class FaqRequest extends AbstractRequest implements RequestParameterModelable
{

	protected $requestKey = 'faq';

	protected $toBooleans = ['active'];

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'question' => 'required|max:1000',
			'answer'   => 'required|max:' . ValidationMaxLengthHelper::TEXT,
			'active'   => 'nullable',
		];
		return $rules;
	}

	protected function mergeRequestValues()
	{
	}


}
