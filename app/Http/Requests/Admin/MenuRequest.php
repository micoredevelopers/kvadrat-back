<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Http\Requests\AbstractRequest;

class MenuRequest extends AbstractRequest implements RequestParameterModelable
{
    protected $toBooleans = ['active'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => 'required',
            'url'           => 'required',
            'menu_group_id' => ['required', 'exists:menu_groups,id'],
        ];
        if ($this->get('page_id')) {
            $rules['page_id'] = ['exists:pages,id'];
        }
        return $rules;
    }

    protected function mergeRequestValues()
    {
//        $this->mergeUrlFromName();
        $this->merge([
            'parent_id' => (int)$this->get('parent_id'),
        ]);
    }
}
