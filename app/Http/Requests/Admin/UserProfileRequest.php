<?php

namespace App\Http\Requests\Admin;

use App\Traits\Requests\User\IsPasswordWasSend;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserProfileRequest extends AbstractRequest
{
	use IsPasswordWasSend;

	public function rules()
	{
		$localesIn = array_keys(\LaravelLocalization::getSupportedLocales());
		$rules = [
			'name' => 'required|min:2|alpha_num',
			'locale' => ['required', Rule::in($localesIn)],
		];
		if ($this->isPasswordsWasSend()) {
			$rules += [
				'password'                  => 'required|min:6',
				'password_new'              => 'required|confirmed|min:6|different:password',
				'password_new_confirmation' => 'required|min:6',
			];
		}
		return $rules;
	}

}
