<?php

	namespace App\Http\Requests\Admin;

	use App\Contracts\Requests\RequestParameterModelable;
	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;
	use App\Traits\Requests\Helpers\GetActionModel;

	class CityRequest extends AbstractRequest implements RequestParameterModelable
	{
		use GetActionModel;

		protected $toBooleans = ['active'];

		protected $requestKey = 'service';

		public function rules()
		{
			$rules = [
				'name'      => ['required', 'string', 'max:255'],
				'region_id' => ['required', 'exists:regions,id'],
			];

			return $rules;
		}
	}
