<?php

	namespace App\Http\Requests\User\Register;

	use App\Http\Requests\User\AbstractUserRequest;
	use App\Services\Phone\PhoneUkraineFormatter;
	use Illuminate\Support\Arr;

	class UserRegisterPasswordRequest extends AbstractUserRequest
	{
		public function rules()
		{
			return $this->getPasswordCredentials();
		}

		protected function mergeRequestValues()
		{
			$this->merge([
				'password'         => trim($this->get('password')),
				'password_confirm' => trim($this->get('password_confirm')),
			]);
		}

	}
