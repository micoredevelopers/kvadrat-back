<?php

	namespace App\Http\Requests\User\Register;

	use App\Helpers\User\Company\CompanyEmployersAmountHelper;
	use Illuminate\Validation\Rule;

	class UserRegisterTypeCompanyRequest extends AbstractUserRegisterTypeRequest
	{
		public function rules()
		{
			return $this->withDefaultTypeRules([
				'name'    => $this->getNameRule(),
				'city_id'         => $this->getCityRule(),
				'company_workers' => ['required', Rule::in(CompanyEmployersAmountHelper::getKeys())],
				'password'        => $this->getCurrentPasswordRule(),
			]);
		}

	}
