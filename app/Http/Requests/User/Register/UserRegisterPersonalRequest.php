<?php

	namespace App\Http\Requests\User\Register;

	use App\Http\Requests\User\AbstractUserRequest;

	class UserRegisterPersonalRequest extends AbstractUserRequest
	{
		public function rules()
		{
			return [
				'phone'    => ['required', 'max:255', 'phone:UA,mobile', 'unique:users,phone'],
				'email'    => $this->getRegisterEmailRule(),
//				'password' => $this->getPasswordCredentials()['password'],
			];
		}

		protected function mergeRequestValues()
		{
			$this->mergeFormatPhone();
		}

		public function getExistedFields(): array
		{
			return array_keys($this->rules());
		}

	}
