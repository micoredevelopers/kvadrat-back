<?php

	namespace App\Http\Requests\User\Register;

	use App\Helpers\User\Company\CompanyEmployersAmountHelper;
	use Illuminate\Validation\Rule;

	class UserRegisterTypeCustomerRequest extends AbstractUserRegisterTypeRequest
	{
		public function rules()
		{
			return $this->withDefaultTypeRules([
				'name'     => $this->getNameRule(),
				'surname'  => $this->getSurnameRule(),
				'city_id'         => $this->getCityRule(),
				'password'        => $this->getCurrentPasswordRule(),
			]);
		}

	}
