<?php

namespace App\Http\Requests\User\Register;

use App\Http\Requests\User\AbstractUserRequest;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\Type\PlatformUserTypes;
use Illuminate\Validation\Rule;

abstract class AbstractUserRegisterTypeRequest extends AbstractUserRequest
{


	public function getAgreementRule(): array
	{
		return ['required'];
	}

	public function messages()
	{
		return [
			'agreement.required' => getTranslate('messagies.must.agree.terms', 'Вы должны согласиться с условиями'),
		];
	}

	protected function withDefaultTypeRules(array $rules = [])
	{
		$in = [
			UserTypeContract::TYPE_CUSTOMER,
			UserTypeContract::TYPE_PERFORMER,
			UserTypeContract::TYPE_PERFORMER_COMPANY,
		];
		return array_merge($rules, [
			'agreement' => $this->getAgreementRule(),
			'type'      => ['required', Rule::in($in)],
			'avatar'    => $this->getImageRule(),
		]);
	}

	public function getFillableFields( $except = []): array
	{
		$except[] = 'agreement';
		return parent::getFillableFields($except);
	}

}
