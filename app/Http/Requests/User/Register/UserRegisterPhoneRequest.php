<?php

namespace App\Http\Requests\User\Register;

use App\Http\Requests\User\AbstractUserRequest;

class UserRegisterPhoneRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'phone' => ['required', 'max:255', 'phone:UA,mobile', 'unique:users,phone'],
		];
	}

	protected function mergeRequestValues()
	{
		$this->mergeFormatPhone();
	}

	public function getExistedFields(): array
	{
		return array_keys($this->rules());
	}

}
