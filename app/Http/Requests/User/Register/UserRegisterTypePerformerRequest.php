<?php

	namespace App\Http\Requests\User\Register;

	use App\Http\Requests\User\AbstractUserRequest;

	class UserRegisterTypePerformerRequest extends AbstractUserRegisterTypeRequest
	{
		public function rules()
		{
			return $this->withDefaultTypeRules([
				'name'     => $this->getNameRule(),
				'surname'  => $this->getSurnameRule(),
				'city_id'  => $this->getCityRule(),
				'password' => $this->getCurrentPasswordRule(),
			]);
		}

	}
