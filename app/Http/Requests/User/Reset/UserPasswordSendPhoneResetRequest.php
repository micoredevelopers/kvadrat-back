<?php

	namespace App\Http\Requests\User\Reset;

	use App\Http\Requests\User\AbstractUserRequest;
	use App\Services\Phone\PhoneUkraineFormatter;

	class UserPasswordSendPhoneResetRequest extends AbstractUserRequest
	{
		public function rules()
		{
			return $this->getLoginPhoneRule();
		}

		protected function mergeRequestValues()
		{
			$this->mergeFormatPhone();
		}

	}
