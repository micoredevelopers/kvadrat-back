<?php


namespace App\Http\Requests\User\Cabinet\Portfolio;


use App\Http\Requests\User\AbstractUserRequest;

class PerformerPortfolioRequest extends AbstractUserRequest
{

	public function rules()
	{
		return [
			'images'      => ['required'],
			'images.*'    => array_merge($this->getImageRule(['max', 'nullable']), ['max' => 'max:' . 4 * 1024]),
			'category_id' => ['required', 'exists:skills,category_id'],
			'description' => ['required', 'string', 'max:1100'],
		];
	}

	public function messages()
	{
		return [
			'category_id.exists' => getTranslate('messagies.you.have.not.category', 'Выбранная категория не отмечена в ваших навыках'),
			'images.required'    => getTranslate('messagies.must.load.picture', 'Изображение обязательно для загрузки'),
		];
	}

}