<?php

	namespace App\Http\Requests\User\Cabinet;

	use App\Http\Requests\User\AbstractUserRequest;

	class ChangePasswordRequest extends AbstractUserRequest
	{
		public function rules()
		{
			return $this->getChangePasswordRules();
		}
	}
