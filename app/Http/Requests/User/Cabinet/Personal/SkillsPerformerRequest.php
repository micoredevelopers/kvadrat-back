<?php

	namespace App\Http\Requests\User\Cabinet\Personal;

	use App\Http\Requests\User\AbstractUserRequest;

	class SkillsPerformerRequest extends AbstractUserRequest
	{
		public function rules()
		{
			return [
				'skills.*' => ['nullable', 'string', 'max:255', 'exists:categories,id'],
			];
		}

	}
