<?php


namespace App\Http\Requests\User\Cabinet\Personal;


use App\Http\Requests\User\AbstractUserRequest;

class AbstractPerformerRequest extends AbstractUserRequest
{
	protected function getBothPerformerRules(): array
	{
		return [
			'city_id'      => $this->getCityRule(),
			'email'        => $this->getRegisterEmailRule(),
			'phone_second' => $this->getBasePhoneRule(['required']),
			'viber'        => ['nullable', 'string', 'max:255'],
			'telegram'     => ['nullable', 'string', 'max:255'],
			'instagram'    => ['nullable', 'string', 'max:255'],
			'facebook'     => ['nullable', 'string', 'max:255'],
			'site'         => ['nullable', 'string', 'max:255'],
			'profession'   => ['nullable', 'string', 'max:255'],
			'description'  => ['nullable', 'string', 'max:5000'],
			'avatar'       => $this->getImageRule(),
		];
	}

	public function dateBirth()
	{
		return ['required', 'date', 'before:' . now()->toDateString()];
	}
}