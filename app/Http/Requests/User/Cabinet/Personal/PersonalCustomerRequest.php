<?php

namespace App\Http\Requests\User\Cabinet\Personal;

use App\Platform\User\AuthenticatedUserResolver;

class PersonalCustomerRequest extends AbstractPerformerRequest
{

	/**
	 * @var AuthenticatedUserResolver
	 */
	private $authenticatedUserResolver;

	public function __construct(AuthenticatedUserResolver $authenticatedUserResolver)
	{
		parent::__construct();
		$this->authenticatedUserResolver = $authenticatedUserResolver;
	}

	public function rules()
	{
		$rules = $this->addSometimesToRules(array_merge(
			$this->getBothPerformerRules(),
			[
				'email'        => $this->getRegisterEmailRule(),
				'about'      => ['string', 'max:3000'],
			]));
		$rules['email']['unique'] .= ',' . $this->authenticatedUserResolver->getUser()->getKey();
		return $rules;

	}

	protected function mergeRequestValues()
	{
		if ($this->has('date_birth')) {
			$this->merge(['date_birth' => getDateCarbon($this->get('date_birth'))->toDateString()]);
		}
		if ($this->has('about')) {
			$this->merge(['about' => strip_tags($this->get('about'))]);
		}
		if ($this->has('phone_second')) {
			$this->mergeFormatPhone('phone_second');
		}
	}
}
