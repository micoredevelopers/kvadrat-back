<?php

namespace App\Http\Requests\User\Cabinet\Personal;

use App\Contracts\Requests\RequestParameterModelable;
use App\Platform\User\AuthenticatedUserResolver;

class PersonalPerformerCompanyRequest extends AbstractPerformerRequest implements RequestParameterModelable
{
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $authenticatedUserResolver;

	public function __construct(AuthenticatedUserResolver $authenticatedUserResolver)
	{
		parent::__construct();
		$this->authenticatedUserResolver = $authenticatedUserResolver;
	}

	public function rules()
	{
		$rules = $this->addSometimesToRules(array_merge(
				$this->getPersonal(),
				$this->getBothPerformerRules(),
				[
					'date_birth'   => $this->dateBirth(),
				]
			)
		);
		$rules['email']['unique'] .= $this->authenticatedUserResolver->getUser()->getKey();
		return $rules;
	}

	protected function mergeRequestValues()
	{
		if ($this->has('date_birth')) {
			$this->merge(['date_birth' => getDateCarbon($this->get('date_birth'))->toDateString()]);
		}
		if ($this->has('phone_second')) {
			$this->mergeFormatPhone('phone_second');
		}
	}
}
