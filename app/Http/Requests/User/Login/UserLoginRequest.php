<?php

namespace App\Http\Requests\User\Login;

use App\Http\Requests\User\AbstractUserRequest;
use App\Services\Phone\PhoneUkraineFormatter;

class UserLoginRequest extends AbstractUserRequest
{
	public function rules()
	{
		$rules = [
			'password' => $this->getCurrentPasswordRule(),
		];
		$rules['login'] = $this->isLoginTypePhone() ? $this->getLoginPhoneRule() : $this->getLoginEmailRule();
		return $rules;
	}

	protected function mergeRequestValues()
	{
		$login = (string)$this->get('login');
		if ($this->isLoginTypePhone()) {
			$phone = PhoneUkraineFormatter::formatPhone($login);
			$this->merge([
				'phone' => $phone,
				'login' => $phone,
			]);
		}
		if ($this->isLoginTypeEmail()) {
			$this->merge(['email' => $login]);
		}
	}

	public function isLoginTypeEmail(): bool
	{
		return filter_var($this->get('login'), FILTER_VALIDATE_EMAIL);
	}

	public function isLoginTypePhone(): bool
	{
		return !$this->isLoginTypeEmail();
	}

	public function getLoginField(): string
	{
		if ($this->isLoginTypePhone()) {
			return 'phone';
		}
		return 'email';
	}

	public function messages()
	{
		return [
			'login.exists' => getTranslate('messagies.mail.phone.not.registreted', 'Номер телефона или E-mail не зарегистрирован'),
		];
	}

}
