<?php

	namespace App\Http\Requests\Order;

	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\Order\AbstractOrderRequest;
	use App\Rules\Uploads\NotDangerExecutable;
	use Illuminate\Http\UploadedFile;
	use Illuminate\Support\Carbon;

	class OrderCreateRequest extends AbstractOrderRequest
	{
		protected $fillableFields = ['closes_at'];
		protected $toIntegers = ['is_urgent', 'show_phone'];

		public function rules()
		{
			return [
				'category_id' => ['required', 'integer', 'exists:categories,id'],
				'city_id'     => ['required', 'integer', 'exists:cities,id'],
				'title'       => ['required', 'string', 'max:1000'],
				'description' => ['required', 'string', 'max:3000'],
				'budget'      => ['nullable', 'integer', 'max:' . ValidationMaxLengthHelper::BIGINT],
				'address'     => ['nullable', 'string', 'max:1000'],
				'show_phone'  => ['nullable'],
				'files.*'     => $this->getFilesRule(),
			];
		}

		protected function mergeRequestValues()
		{
			$dateTime = now()->addDays(14);
			$description = strip_tags($this->get('description'), '<b><i><u><a><ul><ol><li><br><strong>');

			if (isDateValid(null)) {
				$date = $this->get('date');
				$time = isDateValid($this->get('time')) ? getDateCarbon($this->get('date'))->toTimeString() : null;
				$dateTime = $date . ' ' . $time;
			}
			$this->merge([
				'budget'      => abs((int)$this->get('budget')),
				'description' => $description,
				'closes_at'   => $dateTime,
				'date'        => getDateCarbon($this->get('date'))->toDateString(),
				'time'        => getDateCarbon($this->get('date'))->toTimeString(),
			]);
		}

		public function messages()
		{
			$messages = [];
			$files = $this->file('files');
			if ($files) {
				foreach ($files as $i => $file) {
					/** @var $file UploadedFile */
					$sizeKb = ((int)$file->getSize() / 1024);
					$messages[ 'files.' . $i . '.max' ] = sprintf('Размер файла %s не может быть более :max Кбайт. Размер файла: %d Кбайт', $file->getClientOriginalName(), $sizeKb);
//				$messages[ 'files.' . $i . '.mimetypes' ] = sprintf('Формат файла %s(%s) не разрешено к загрузке, разрешенные к загрузке, изображения, текстовые файлы, видео и аудио, pdf, xlsx .', $file->getClientOriginalName(), $file->getExtension());
				}
			}
			return $messages;
		}
	}
