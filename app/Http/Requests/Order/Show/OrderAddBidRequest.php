<?php

namespace App\Http\Requests\Order\Show;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Rules\Order\OrderExistsRule;

class OrderAddBidRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'text'     => ['required', 'string', 'max:1000'],
			'days'     => ['required','integer', 'min:1', 'max:' . ValidationMaxLengthHelper::SMALLINT_UNSIGNED],
			'bid'      => ['required', 'integer', 'min:1', 'max:' . ValidationMaxLengthHelper::INT_UNSIGNED],
		];
	}

	protected function mergeRequestValues()
	{
		$this->merge(['text' => strip_tags($this->get('text'))]);
	}
}
