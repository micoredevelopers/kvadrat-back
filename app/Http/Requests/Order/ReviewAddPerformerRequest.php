<?php

	namespace App\Http\Requests\Order;

	use App\Http\Requests\AbstractRequest;

	class ReviewAddPerformerRequest extends AbstractRequest
	{
		public function rules()
		{
			return [
				'adequate_rating'     => ['required', 'integer', 'min:1', 'max:5'],
				'legibility_rating'  => ['required', 'integer', 'min:1', 'max:5'],
				'suggestions_reacts' => ['required', 'integer', 'min:1', 'max:5'],
				'punctuality_rating'    => ['required', 'integer', 'min:1', 'max:5'],
				'recommend_rating'   => ['required', 'integer', 'min:1', 'max:5'],
				'comment'            => ['required', 'string', 'max:5000'],
				'images.*'           => array_merge($this->getImageRule(['max']), ['max' => 'max:' . 4 * 1024]),
			];
		}

		protected function mergeRequestValues()
		{
			$this->merge([
				'comment' => strip_tags($this->get('comment')),
			]);
		}
	}
