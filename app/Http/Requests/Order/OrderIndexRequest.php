<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\AbstractRequest;

class OrderIndexRequest extends AbstractRequest
{
	public function rules()
	{
		return [];
	}

	protected function mergeRequestValues()
	{
		if ($this->has('category')) {
			$categories = (array)$this->get('category');
			$categories = array_map(function ($item) {
				return (int)$item;
			}, array_filter($categories, 'is_numeric'));
			$this->merge(['category' => $categories]);
		}
	}
}
