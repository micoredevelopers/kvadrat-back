<?php


	namespace App\Http\Requests\Phone;


	use App\Http\Requests\AbstractRequest;
	use App\Http\Requests\Order\AbstractOrderRequest;
	use App\Rules\Verification\SendCodeEquals;

	class BaseSendConfirmPhoneRequest extends AbstractRequest
	{
		public function rules()
		{
			return [
				'code' => ['required', 'integer', app(SendCodeEquals::class)],
			];
		}

		protected function mergeRequestValues()
		{
			if (is_array($codeArr = $this->get('code'))) {
				$codes = array_filter($codeArr, 'is_string');
				$this->merge([
					'code' => implode($codes),
				]);
			}
		}

	}