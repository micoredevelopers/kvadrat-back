<?php

	namespace App\Http\Requests\Phone;

	use App\Http\Requests\AbstractRequest;
	use App\Rules\Verification\ResendCodeAvailable;

	class ResendCodeRequest extends AbstractRequest
	{
		public function rules()
		{
			return [
				'resend' => ['required', app(ResendCodeAvailable::class)]
			];
		}
	}
