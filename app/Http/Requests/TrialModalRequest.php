<?php

namespace App\Http\Requests;

class TrialModalRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'name'    => $this->getRuleRequiredChar(),
            'phone'   => $this->getBasePhoneRule(),
            'city' => 'required',
        ];
    }
}
