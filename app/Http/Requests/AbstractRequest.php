<?php


namespace App\Http\Requests;


use App\Rules\Uploads\NotDangerExecutable;
use App\Traits\Requests\Helpers\IsAction;
use App\Traits\Requests\Helpers\RequestAttributesCaster;
use Illuminate\Foundation\Http\FormRequest;

class AbstractRequest extends FormRequest
{
	protected $toBooleans = [];

	use RequestAttributesCaster;

	use IsAction;

	protected $requestKey = '';
	protected $fillableFields = [];

	public function authorize()
	{
		return true;
	}

	/**
	 * @param $fields = [ 'email' => ['required', 'email', '...'] ]
	 * @param array $except = ['password', 'email', '...']
	 * @return array
	 */
	protected function exceptFields($fields, $except = []): array
	{
		return array_diff_key($fields, array_flip($except));
	}


	public function getRequestKey(): string
	{
		return $this->requestKey;
	}

	protected function prepareForValidation()
	{
		$this->mergeRequestValues();
		$this->mapCasts();
	}

	protected function mergeRequestValues() { }

	protected function castCheckboxes(array $checkboxes)
	{
		foreach ($checkboxes as $checkbox) {
			$this->merge([$checkbox => $this->toCheckbox($this->get($checkbox))]);
		}
	}

	protected function toCheckbox($value)
	{
		return (int)$value;
	}

	protected function mergeUrlFromName()
	{
		$this->mergeUrlFromField();
	}

	protected function mergeUrlFromTitle()
	{
		$this->mergeUrlFromField('title');
	}

	protected function mergeUrlFromField($field = 'name')
	{
		if (!$this->get('url')) {
			$this->merge(['url' => $this->get($field)]);
		}
		$this->merge([
			'url' => \Str::slug($this->get('url')),
		]);
	}

	/**
	 * @param array | string | int $except
	 * @return array
	 */
	public function getFillableFields($except = []): array
	{
		$except = (array)$except;
		$fn = static function ($key) {
			$key = rtrim($key, '.*');
			return $key;
		};
		$keys = array_merge(
			array_keys($this->exceptFields($this->rules(), $except)),
			$this->fillableFields,
		);
		$keys = array_map($fn, $keys);
		return $keys;
	}

	public function addSometimesToRules(array $rules, array $exceptAdd = []): array
	{
		$fn = static function (&$rule, $keyRule, $exceptAdd) {
			if (!in_array($keyRule, $exceptAdd, true)) {
				$rule['sometimes'] = 'sometimes';
			}
		};
		array_walk($rules, $fn, $exceptAdd);
		return $rules;
	}

	public function getImageRule(array $except = []): array
	{
		$fields = ['nullable' => 'nullable', 'image' => 'image', 'mimes' => 'mimes:jpg,jpeg,png', 'max' => 'max:' . (0.5 * 1024)];
		return $this->exceptFields($fields, $except);
	}

	public function getFilesRule(): array
	{
		return ['nullable'   => 'nullable',
				'file'       => 'file',
//				'mimetypes:image/*,text/*,font/*,audio/*,video/*,application/pdfs,application/vnd.oasis.opendocument.text,application/zip,application/xml,application/x-rar-compressed,application/x-7z-compressed,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,',
				'not_danger' => app(NotDangerExecutable::class),
				'max'        => 'max:' . (5 * 1024),
		];
	}

	public function getRuleNullableChar(): array
	{
		return ['nullable', 'max:255', 'string'];
	}

	public function getRuleRequiredChar(): array
	{
		return ['required', 'max' => 'max:255', 'string'];
	}

	public function getBaseEmailRule(): array
	{
		return [
			'required' => 'required', 'max' => 'max:255', 'email' => 'email:rfc,dns',
		];
	}

    public function getBasePhoneRule(): array
    {
        return [
            'required' => 'required', 'max' => 'max:19', 'min' => 'min:10'
        ];
    }
}
