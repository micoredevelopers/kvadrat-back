<?php

namespace App\Http\Requests;

class FranchModalRequest extends AbstractRequest
{

    public function rules()
    {
        return [
            'name'    => $this->getRuleRequiredChar(),
            'phone'   => $this->getBasePhoneRule(),
        ];
    }
}
