<?php


	namespace App\Http\Controllers;


	use App\Http\Requests\Phone\ResendCodeRequest;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use App\Services\Phone\PhoneVerificationService;
	use App\Services\SMS\SMSSenderService;

	class ResendCodeController extends SiteController
	{
		/**
		 * @var PhoneVerificationService
		 */
		private $phoneVerificationService;
		/**
		 * @var PhoneVerificationDataKeeper
		 */
		private $dataKeeper;
		/**
		 * @var SMSSenderService
		 */
		private $SMSSenderService;

		public function __construct(
			PhoneVerificationService $phoneVerificationService
			, PhoneVerificationDataKeeper $dataKeeper
			,SMSSenderService $SMSSenderService
		)
		{
		    parent::__construct();
			$this->phoneVerificationService = $phoneVerificationService;
			$this->dataKeeper = $dataKeeper;
			$this->SMSSenderService = $SMSSenderService;
		}

		public function resend(ResendCodeRequest $request)
		{
			$phone = $this->dataKeeper->getPhone();
			$this->SMSSenderService->send($phone)->getLastVerification();
			$this->setSuccessMessage(translateFormat('sms.code-resend-sended', ['%phone%' => $phone,]));
			return redirect()->back()->with($this->getResponseMessage());
		}

	}