<?php

namespace App\Http\Controllers;

use App\Repositories\FaqRepository;
use App\Repositories\MenuRepository;

class PageController extends SiteController
{

	public function default($page)
	{
		$view = 'public.pages.dev.' . $page;

		$data = [];
		if (view()->exists($view)) {
			$data['content'] = view($view);
		}

		return $this->main($data);
	}

    public function levelUp()
    {
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.pages.course.level-up')->with($with);
        return $this->main($data);
    }
    public function community()
    {
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.pages.course.community')->with($with);
        return $this->main($data);
    }
    public function intro()
    {
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.pages.course.introduction')->with($with);
        return $this->main($data);
    }

    public function franch()
    {
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.pages.franchise.index')->with($with);
        return $this->main($data);
    }

    public function trial()
    {
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.pages.course.trial')->with($with);
        return $this->main($data);
    }


}
