<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\TrialModalRequest;
use App\Mail\FeedbackMailer;
use App\Models\TrialModal;


class TrialModalController extends SiteController
{

    public function saveModal(TrialModalRequest $request, TrialModal $trial)
    {

        $trial->setName($request->name);
        $trial->setPhone($request->phone);
        $trial->setCity($request->city);

        $trial->save();


        try {
            \Mail::queue(new FeedbackMailer([
                'name' => $request->get('name'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
            ]));
        } catch (\Throwable $e){
            logger($e);
        }


        if ($request->expectsJson()){
            return $this->setSuccessMessage('Success send')->getResponseMessageForJson();
        }
        return back()->with($this->getMessage());
    }
}
