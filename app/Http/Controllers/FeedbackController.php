<?php

namespace App\Http\Controllers;


use App\Events\Feedback\FeedbackCreated;
use App\Http\Requests\Feedback\FileFeedbackRequest;
use App\Mail\FeedbackMailer;
use App\Models\Feedback\Feedback;
use App\Models\Feedback\FeedbackContract;
use App\Repositories\FeedbackRepository;
use App\Http\Requests\AbstractRequest as Request;
use App\Uploaders\Feedback\FeedbackFilesUploader;
use Mail;
use stdClass;

class FeedbackController extends SiteController
{
	/**
	 * @var FeedbackRepository
	 */
	private $repository;

	public function __construct(FeedbackRepository $repository)
	{
		parent::__construct();
		$this->repository = $repository;
	}

	public function report(FileFeedbackRequest $request)
	{
		$request->merge(['type' => FeedbackContract::TYPE_REPORT]);
		return $this->feedback($request);
	}

	public function idea(FileFeedbackRequest $request)
	{
		$request->merge(['type' => FeedbackContract::TYPE_IDEA]);
		return $this->feedback($request);
	}
    public function send(FileFeedbackRequest $request)
    {
        $request->rules();

        $data = new stdClass();
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->city = $request->city;
        Mail::to('kiraknax@gmail.com')->queue(new FeedbackMailer((array)$data));
        return redirect()->route('home')
            ->with('success', 'Ваше сообщение успешно отправлено');
    }

    public function sendFranch(FileFeedbackRequest $request)
    {
        $request->rules();

        $data = new stdClass();
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->franch = ['ФРАНШИЗА'];
        Mail::to('kiraknax@gmail.com')->queue(new FeedbackMailer((array)$data));
        return redirect()->route('home')
            ->with('success', 'Ваше сообщение успешно отправлено');
    }

	protected function feedback(Request $request)
	{
		$this->setFailMessage(getTranslate('messagies.aplication.not.save', 'Заявка не была отправлена'));
		$input = $request->only($request->getFillableFields('files'));

		/** @var  $feedback Feedback*/
		if ($feedback = $this->repository->create($input)) {
			if ($request->hasFile('files')) {
				$uploader = app(FeedbackFilesUploader::class)->appendToPath($feedback->getKey());
				foreach ($request->file('files') as $file) {
					$paths[] = $uploader->upload($file);
				}
				$feedback->setFiles($paths ?? []);
				$this->repository->update([], $feedback);
			}
			event(app(FeedbackCreated::class, compact('feedback')));
			$this->setSuccessMessage(getTranslate('messagies.aplication.is.save', 'Ваша заявка успешно отправлена'));
		}
		return $this->getResponseMessageForJson();
	}
}
