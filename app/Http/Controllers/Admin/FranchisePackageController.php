<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Events\Admin\CategoriesChanged;
use App\Repositories\FranchisePackageRepository;
use App\Traits\Authorizable;
use Composer\Package\Package;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class FranchisePackageController extends AdminController
{
    use Authorizable;


    protected $routeKey = 'admin.packages';

    protected $permissionKey = 'packages';

    protected $key = 'package';

    protected $name = 'Пакеты';

    /**
     * @var FranchisePackageRepository
     */
    private $packageRepository;

    public function __construct(FranchisePackageRepository $packageRepository)
    {
        parent::__construct();
        $this->packageRepository = $packageRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));

        $this->shareViewModuleData();

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $this->addScripts('libs/nestable2/jquery.nestable.min.js');
        $this->addCss('libs/nestable2/jquery.nestable.min.css');
        $list = $this->packageRepository->getListAdmin();
        $data['content'] = view('admin.packages.index', compact('list'));
        return $this->main($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $vars['categories'] = $this->packageRepository->getListAdmin();
        $data['content'] = view('admin.packages.create')->with($vars);

        return $this->main($data);
    }

    public function store(CategoryRequest $request)
    {
        if ($category = $this->packageRepository->create($request->only($request->getFillableFields()))) {
            $this->setSuccessStore();
            event(new CategoriesChanged());
        }
        $this->saveImage($request, $category);

        return $this->redirectOnCreated($category);
    }

    public function edit(FranchisePackages $package)
    {
        $vars['edit'] = $package;
        if (!$package) {
            $this->setMessage(__('modules._.record-not-finded'));
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        $packages = $this->packageRepository;
        /** @var  $categoryParent FranchisePackages */

        $title = $this->titleEdit($packages);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.package.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(CategoryRequest $request, FranchisePackages $category)
    {

        return $this->redirectOnUpdated($category);
    }

    public function destroy(FranchisePackages $category, Photo $photo)
    {

        return back()->with($this->getResponseMessage());
    }


}
