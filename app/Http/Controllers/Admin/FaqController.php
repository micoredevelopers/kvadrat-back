<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FaqRequest;
use App\Models\Faq\Faq;
use App\Repositories\FaqRepository;

class FaqController extends AdminController
{
	protected $routeKey = 'admin.faq';

	protected $permissionKey = 'faq';

	protected $key = 'faq';

	private $name = 'FAQ';
	/**
	 * @var FaqRepository
	 */
	private $repository;

	public function __construct(FaqRepository $repository)
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index()
	{
		$this->setTitle($this->name);
		$vars['list'] = $this->repository->getListAdmin();
		$data['content'] = view('admin.faq.index', $vars);

		return $this->main($data);
	}

	public function create()
	{
		$data['content'] = view('admin.faq.create');
		return $this->main($data);
	}


	public function store(FaqRequest $request)
	{
		$input = $request->only($request->getFillableFields());
		if ($faq = $this->repository->create($input)) {
			$this->setSuccessStore();
			$this->fireEvents();
		}

		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $faq->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	public function edit($faqId)
	{
		/** @var  $faq Faq */
		$faq = $this->repository->find($faqId);
		$vars['edit'] = $faq;
		$title = $this->titleEdit($faq);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.faq.edit', $vars);

		return $this->main($data);
	}

	public function update(FaqRequest $request, $faqId)
	{
		$faq = $this->repository->find($faqId);
		$input = $request->only($request->getFillableFields());
		if ($this->repository->update($input, $faq)) {
			$this->fireEvents();
			$this->setSuccessUpdate();
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}


	public function destroy($faq)
	{
		if ($this->repository->delete($faq)) {
			$this->setSuccessDestroy();
			$this->fireEvents();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}


	private function fireEvents()
	{
	}
}
