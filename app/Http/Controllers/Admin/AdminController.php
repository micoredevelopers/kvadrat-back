<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Contracts\HasImagesContract;
use App\Http\Controllers\BaseController;
use App\Models\Admin\Photo;
use App\Models\Language;
use App\Models\Model;
use App\Traits\Controllers\ResourceControllerHelpers;
use App\Traits\Controllers\ResourceControllerPreActions;
use App\Traits\Controllers\ResourceControllerReturnMessages;
use Illuminate\Http\Request;

abstract class AdminController extends BaseController
{
	use ResourceControllerReturnMessages;
	use ResourceControllerHelpers;
	use ResourceControllerPreActions;

	protected $langAdmin = 1;

	protected $data = [];

	/**
	 * @var string
	 * For module route prefix
	 */
	protected $routeKey;
	/**
	 * @var string
	 * For check permissions in Authorizable trait, and to share to views key for check permissions
	 */
	protected $permissionKey;

	/**
	 * @var string
	 * "Module" key, for methods like getThumbnailSizes()
	 */
	protected $key;

	/**
	 * @var Photo
	 */
	protected $photoModel;

	protected $languagesList;

	public function __construct()
	{
		parent::__construct();

		$this->languagesList = Language::all();
		$this->checkLanguage();
		if (\Auth::guard('admin')->check()) {
			app()->setLocale(\Auth::guard('admin')->user()->getAttribute('locale'));
		}
		$this->photoModel = new Photo();
		$this->addBreadCrumb(__('generic.dashboard'), route('admin.index'));
	}

	public function main($data)
	{
		\Arr::set($data, 'cardTitle', \Arr::get($data, 'cardTitle', $this->getTitle()));
		$this->setTitle(' - ' . getSetting('global.sitename'));
		$data['menu'] = app(AdminMenuRepositoryContract::class)->getNestedMenu();
		$data['styles'] = $this->getStylesString();
		$data['scripts'] = $this->getScriptsString($this->getScripts());
		$data['scriptsDefer'] = $this->getScriptsString($this->getScripts(true), 'defer');
		$data['sections'] = [];
		$data['languages'] = $this->languagesList->whereActive();
		\Arr::set($data, 'breadcrumbs', \Arr::get($data, 'breadcrumbs', $this->getBreadCrumbs()));
		//Получаем секции дочерних шаблонов, поскольку шаблоны не наследуют друг друга
		$data['sections'] = (\Arr::get($data, 'content') AND ($data['content'] instanceof \Illuminate\View\View)) ? $data['content']->renderSections() : [];

		return view('admin.layouts.app-admin', $data);
	}

	protected function checkLanguage()
	{
		$this->langAdmin = getCurrentLangId();
	}

	/**
	 * @param HasImagesContract $belongToModel
	 * @param Request $request
	 */
	protected function saveAdditionalImages(HasImagesContract $belongToModel, Request $request): void
	{
		$imagesCollection = $this->photoModel->saveAdditionPhotos($belongToModel, $request);
		event(new \App\Events\Admin\Image\MultipleImageUploaded($belongToModel, $imagesCollection, $request));
	}

	public function callAction($method, $parameters)
	{
		switch ($method) {
			case 'index' :
				$this->beforeIndex($parameters);
				break;
			case 'create' :
				$this->beforeCreate($parameters);
				break;
			case 'store' :
				$this->beforeStore($parameters);
				break;
			case 'edit' :
				$this->beforeEdit($parameters);
				break;
			case 'update' :
				$this->beforeUpdate($parameters);
				break;
			case 'show' :
				$this->beforeShow($parameters);
				break;
			case 'destroy' :
				$this->beforeDestroy($parameters);
				break;
		}
		$this->onCallAction($method, $parameters);

		return parent::callAction($method, $parameters);
	}

	protected function shareViewModuleData()
	{
		\View::share('routeKey', ($this->routeKey ?? null));
		\View::share('permissionKey', ($this->permissionKey ?? null));
		\View::share('key', ($this->key ?? null));
	}

	protected function linkByModel(Model $model)
	{
		return CRUDLinkByModel($model);
	}

	protected function onCallAction($method, $parameters): void
	{
	}

	protected function redirectOnCreated(Model $model)
	{
		if (\request()->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $model->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	protected function redirectOnUpdated(Model $model)
	{
        if (\request()->expectsJson()) {
            return $this->getResponseMessageForJson();
        }
		if (\request()->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('edit', $model->getKey()))->with($this->getResponseMessage());
	}
}
