<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Repositories\CityRepository;
use App\Traits\Authorizable;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class CityController extends AdminController
{

    use Authorizable;

    private $name;

    protected $key = 'cities';

    protected $routeKey = 'admin.cities';

    protected $permissionKey = 'cities';
    /**
     * @var CityRepository
     */
    private $repository;

    public function __construct(CityRepository $repository)
    {
        parent::__construct();
        $this->name = __('modules.cities.title');
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
        $this->repository = $repository;
    }

    public function index()
    {
        $this->setTitle($this->name);
        $vars['list'] = $this->repository->getListAdmin();
        $data['content'] = view('admin.cities.index', $vars);
        return $this->main($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $data['content'] = view('admin.cities.create');
        return $this->main($data);
    }

    public function store(Request $request)
    {
        $input = $request->except('image');
        if ($city = $this->repository->create($input)) {
            $this->setSuccessStore();
        }
        return $this->redirectOnCreated($city);
    }

    public function edit(City $city)
    {
        $edit = $city;
        $title = $this->titleEdit($edit);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.cities.edit', compact(
            'edit'
        ));
        return $this->main($data);
    }

    public function update(Request $request, City $city)
    {
        $input = $request->except('image');

        if ($this->repository->update($input, $city)) {
            $this->setSuccessUpdate();
        }

        return $this->redirectOnUpdated($city);
    }

    /**
     * @param City $city
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(City $city)
    {
        if (!$city->canDelete()) {
            $this->setFailMessage('Удаление этой записи не доступно');
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }
        if ($this->repository->delete($city->id)) {
            $this->setSuccessDestroy();
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

}
