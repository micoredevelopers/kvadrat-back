<?php

	namespace App\Http\Controllers\Admin;


	use App\Widgets\Chart\OrdersChart;
	use App\Widgets\OrderTotalsWidget;

	class IndexController extends AdminController
	{
		public function index()
		{
			$this->setTitle(__('modules._.dashboard'));
			$charts = collect([]);
			$widgets = collect([]);
			$data['content'] = view('admin.index.dashboard')->with(compact('charts', 'widgets'));
			return $this->main($data);
		}

		public function clearCache()
		{
			$this->setMessage('Cache Cleared!')->setStatus(true);
			\Artisan::call('cache:clear');
			return redirect()->back()->with($this->getResponseMessage());
		}

		public function clearView()
		{
			\Artisan::call('view:clear');
			$this->setMessage('Cache views cleared!')->setStatus(true);
			return redirect()->back()->with($this->getResponseMessage());
		}

		/**
		 * @throws \Illuminate\Auth\Access\AuthorizationException
		 */
		public function storageLink()
		{
			$this->authorize('superadmin');
			\Artisan::call('storage:link');
			return redirect()->back()->with($this->getResponseMessage());
		}


		public function refreshDb()
		{
			$this->authorize('superadmin');
			if (isLocalEnv()){
				\Artisan::call('migrate:fresh --seed');
				\Artisan::call('cache:clear');
			}
			return redirect()->back()->with($this->getResponseMessage());
		}

	}
