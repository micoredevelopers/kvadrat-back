<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\MenusChanged;
use App\Helpers\Media\ImageRemover;
use App\Helpers\ResponseHelper;
use App\Http\Requests\Admin\MenuRequest;
use App\Models\Admin\Photo;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\MenuLang;
use App\Repositories\MenuRepository;
use App\Repositories\PageRepository;
use App\Scopes\SortOrderScope;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use App\Traits\Controllers\ThumbnailSizes;
use Illuminate\Http\Request;

class MenuController extends AdminController
{
    use ThumbnailSizes;
    use SaveImageTrait;
    use Authorizable;

    public $thumbnailWidth = false;
    public $thumbnailHeight = false;


    protected $routeKey = 'admin.menu';

    protected $permissionKey = 'menu';

    protected $menuGroup;

    private $name = 'Меню';

    protected $key = 'menu';
	/**
	 * @var MenuRepository
	 */
	private $repository;

	public function __construct(Request $request, MenuRepository $repository)
    {
        parent::__construct();
        $this->name = __('modules.menu.title');
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->menuGroup = MenuGroup::find((int)$request->get('group'));
        if ($this->menuGroup) {
            $this->addBreadCrumb($this->menuGroup->name, $this->routeWithGroup($this->resourceRoute('index')));
        }
        $this->shareViewModuleData();
        \View::share('group', $this->menuGroup);
		$this->repository = $repository;
	}

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function checkGroup()
    {
        if (!$this->menuGroup) {
            return redirect()->back()->with('error', 'group menu not selected')->send();
        }
    }

    private function routeWithGroup($url)
    {
        return $url . '?group=' . $this->menuGroup->id;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $vars['groups'] = MenuGroup::all();
        $this->setTitle($this->name);
        $this->addScripts('libs/nestable2/jquery.nestable.min.js');
        $this->addCss('libs/nestable2/jquery.nestable.min.css');

        $query = Menu::parentMenu()->withGlobalScope('sort', new SortOrderScope())->with('lang', 'allMenus');

        $view = 'admin.menu.index-group';
        if ($this->menuGroup) {
            $view = 'admin.menu.index';
            $query->where('menu_group_id', $this->menuGroup->id);
        }
        $vars['list'] = $query->get();
        $data['content'] = view($view, $vars);
        return $this->main($data);
    }

    /**
     * @param PageRepository $pageRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(PageRepository $pageRepository)
    {
        $this->checkGroup();
        $vars['pages'] = $pageRepository->getForAdmin();
        $vars['menus'] = Menu::getForDisplayEdit();
        $data['content'] = view('admin.menu.create')->with($vars);

        return $this->main($data);
    }


    public function store(MenuRequest $request)
    {
        $this->checkGroup();

        $input = $request->except('image');
        if ($menu = $this->repository->createRelated($input, $this->menuGroup)) {
            $this->setSuccessStore();
            $this->fireEvents();
        }
        $this->saveImage($request, $menu);
        if ($request->has('createOpen')) {
            return redirect($this->routeWithGroup($this->resourceRoute('edit', $menu->id)))->with($this->getResponseMessage());
        }

        return redirect($this->routeWithGroup($this->resourceRoute('index')))->with($this->getResponseMessage());
    }


    public function edit($id)
    {
        $this->checkGroup();
        $vars['groups'] = MenuGroup::all();
        $vars['edit'] = Menu::with('lang')->findOrFail($id);
        $title = $this->titleEdit($vars['edit']);
        $this->addBreadCrumb($title)->setTitle($title);

        $vars['menus'] = Menu::getForDisplayEdit();
        $data['content'] = view('admin.menu.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(MenuRequest $request, $id)
    {
    	/** @var  $menu  Menu*/
    	$menu = $this->repository->find($id);
        $this->checkGroup();

        $input = $request->all();
        if ($this->repository->update($input, $menu)) {
            $this->setSuccessUpdate();
            $this->saveImage($request, $menu);
            $this->fireEvents();
        }
        if ($request->has('saveClose')) {
            return redirect($this->routeWithGroup($this->resourceRoute('index')))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }


    public function destroy($id, ImageRemover $imageRemover)
    {
		$menu = $this->repository->find($id);
        if ($menu->delete()) {
			$imageRemover->removeImage($menu->getImage());
            $this->setSuccessDestroy();
            $this->fireEvents();
        }

        return back()->with($this->getResponseMessage());
    }

	public function nesting(Request $request)
	{
		$requestData = $request->get('menus');
		$lastJson = json_encode($requestData);
		$cacheKey = 'menus.nested.order';
		if (!\Cache::get($cacheKey) || $lastJson !== \Cache::get($cacheKey)){
			Menu::nestable($requestData);
			$this->setSuccessUpdate();
			$this->fireEvents();
		}
		\Cache::set($cacheKey, $lastJson);

		return $this->getResponseMessageForJson();
	}

    protected function fireEvents()
    {
        event(new MenusChanged());
    }
}
