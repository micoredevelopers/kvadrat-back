<?php

	namespace App\Http\Controllers\Admin;

	use App\Http\Requests\Admin\PageRequest;
	use App\Models\Admin\Photo;
	use App\Models\Page\Page;
	use App\Repositories\PageRepository;
	use App\Repositories\ServiceRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;

	class PageController extends AdminController
	{
		use SaveImageTrait;
		use Authorizable;

		private $name;

		protected $key = 'pages';

		protected $permissionKey = 'pages';

		protected $routeKey = 'admin.pages';
		/**
		 * @var PageRepository
		 */
		private $repository;

		public function __construct(PageRepository $repository)
		{
			parent::__construct();
			$this->name = __('modules.pages.title');
			$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
			view()->share([
				'pageTypes', Page::getPageTypes(),
			]);
		}

		public function index(Request $request, PageRepository $pageRepository)
		{
			$this->setTitle($this->name);
			$vars['list'] = $pageRepository->getListAdmin($request);
			$vars['request'] = $request;

			$data['content'] = view('admin.pages.index', $vars);
			return $this->main($data);
		}

		/**
		 * @param PageRepository $pageRepository
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function create(PageRepository $pageRepository)
		{
			$pageTypes = Page::getPageTypes();
			$list = $pageRepository->getForAdmin();
			$data['content'] = view('admin.pages.create')->with(compact('pageTypes', 'list'));
			return $this->main($data);
		}

		/**
		 * @param PageRequest $request
		 * @param Page        $page
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function store(PageRequest $request)
		{
			$input = $request->except('image');
			if ($page = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->saveImage($request, $page);
			}

			return $this->redirectOnCreated($page);
		}

		public function edit(Page $page)
		{
			$title = $this->titleEdit($page->lang, 'title');
			$this->addBreadCrumb($title)->setTitle($title);
			view()->share([
				'edit'       => $page,
				'photosList' => $page->images,
			]);

			$data['content'] = $this->getViewByPage($page);
			return $this->main($data);
		}

		private function getViewByPage(Page $page)
		{
			return view('admin.pages.edit');
		}

		/**
		 * @param PageRequest $request
		 * @param Page        $page
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function update(PageRequest $request, Page $page)
		{
			$input = $request->except('image', 'sub_image');
			if ($this->repository->update($input, $page)) {
				$this->setSuccessUpdate();
			}

			$this->saveImage($request, $page);
			$this->saveImage($request, $page, 'sub_image');

			return $this->redirectOnUpdated($page);
		}

		/**
		 * @param Page  $page
		 * @param Photo $photo
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 * @throws \Exception
		 */
		public function destroy(Page $page, Photo $photo)
		{
			if ($page->delete()) {
				$photo->deleteImageStorage($page->getImage());
				$photo->deleteImageStorage($page->getAttribute('sub_image'));
				$this->setSuccessDestroy();
			}
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

	}
