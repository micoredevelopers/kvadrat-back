<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SliderRequest;
use App\Models\Admin\Photo;
use App\Models\Image;
use App\Models\Slider\Slider;
use App\Models\Slider\SliderItem;
use App\Models\Slider\SliderItemLang;
use App\Repositories\SliderRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class SliderController extends AdminController
{
	use Authorizable;

	protected $name = 'Слайдеры';

	protected $key = 'sliders';

	protected $permissionKey = 'sliders';

	protected $routeKey = 'admin.sliders';

	public function __construct(SliderItem $sliderItem, SliderItemLang $sliderItemLang)
	{
		parent::__construct();
		$this->name = __('modules.sliders.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
//		\View::share('sliderItemEmpty', $sliderItem);
//		\View::share('sliderItemLangEmpty', $sliderItemLang);
	}

	/**
	 * @param SliderRepository $sliderRepository
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(SliderRepository $sliderRepository)
	{
		$title = $this->name;
		$this->setTitle($title);
		$vars['list'] = $sliderRepository->getListForAdmin();
		$data['content'] = view('admin.sliders.index', $vars);

		return $this->main($data);
	}

	/**
	 * @param Slider $slider
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Slider $slider)
	{
		$data['content'] = view('admin.sliders.create')->with(['edit' => $slider,]);

		return $this->main($data);
	}

	/**
	 * @param SliderRequest $request
	 * @param Slider $slider
	 * @param SliderRepository $sliderRepository
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(SliderRequest $request, Slider $slider, SliderRepository $sliderRepository)
	{
		if ($sliderRepository->save($request, $slider)) {
			$this->setSuccessStore();
		}
		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $slider->id))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	/**
	 * @param Slider $slider
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Slider $slider)
	{
		$slider->load('items.lang');
		$vars['edit'] = $slider;
		$title = $this->titleEdit($vars['edit'], 'comment');
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.sliders.edit', $vars);

		return $this->main($data);
	}

	/**
	 * @param SliderRequest $request
	 * @param Slider $slider
	 * @param SliderRepository $sliderRepository
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(SliderRequest $request, Slider $slider, SliderRepository $sliderRepository)
	{
		if ($sliderRepository->save($request, $slider)) {
			$this->setSuccessUpdate();
		}

		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	/**
	 * @param Slider $slider
	 * @param Photo $photo
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Slider $slider, Photo $photo)
	{
		if ($slider->delete()) {
			$photo->deleteImageStorage($slider->getImage());
			if ($slider->images) {
				foreach ($slider->images as $image) {
					/** @var $image Image */
					$photo->deleteImageStorage($image->getImage());
				}
			}
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	public function createSliderItem(Request $request, Slider $slider, SliderRepository $sliderRepository)
	{
		$this->authorize('add_sliders');
		$this->setFailStore();
		if ($sliderRepository->storeSlideItem($request,$slider)){
			$this->setSuccessStore();
		}
		return $this->getResponseMessageForJson();

	}

}
