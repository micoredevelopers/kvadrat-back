<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\MenuLang;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use App\Traits\Controllers\ThumbnailSizes;
use Illuminate\Http\Request;

class MenuGroupController extends AdminController
{
    use Authorizable;

    private $name = 'Группы меню';

    protected $tb = 'menu-group';
    protected $key = 'menu-group';

    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb('Меню', route('admin.menu.index'));
        $this->addBreadCrumb($this->name, route($this->tb . '.index'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = [];
        $vars['key'] = $this->tb;
        $data['cardTitle'] = $this->name;
        $this->setTitle($this->name);
        $vars['list'] = MenuGroup::all();
        if (view()->exists('admin.' . $this->tb . '.index')) {
            $data['content'] = view('admin.menu-group.index', $vars);
        }

        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = [];
        if (view()->exists('admin.menu-group.create')) {
            $data['content'] = view('admin.menu-group.create');
        }

        return $this->main($data);
    }

    /**
     * @param Request $request
     * @param MenuGroup $menuGroup
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, MenuGroup $menuGroup)
    {
        $this->setSuccessStore();
        $input = $request->all();

        $menuGroup->fill($input)->save();
        if ($request->has('createOpen')) {
            return redirect(route('menu-group.edit', $menuGroup->id))->with($this->getResponseMessage());
        }

        return redirect(route('menu-group.index'))->with($this->getResponseMessage());
    }


    public function update(Request $request, MenuGroup $menuGroup)
    {

        $input = $request->all();
        $menuGroup->fill($input);
        if ($menuGroup->save()) {
            $this->setMessage(__('generic.successfully_updated'));
        }
        if ($request->has('saveClose')) {
            return redirect(route($this->tb . '.index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param MenuGroup $menuGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(MenuGroup $menuGroup)
    {
        $data = [];
        $vars['edit'] = $menuGroup;
        $groupMenuTranslate = getTranslate('main.group.menu', 'Группа меню');
        $data['cardTitle'] = $title = "$groupMenuTranslate" . $vars['edit']['name'];
        $this->addBreadCrumb($title)->setTitle($title);

        if (view()->exists('admin.menu-group.edit')) {
            $data['content'] = view('admin.menu-group.edit', $vars);
        }

        return $this->main($data);
    }

    /**
     * @param Menu $menu
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Menu $menu, Photo $photo)
    {
        if ($menu->delete()) {
            $this->setSuccessDestroy();
        }

        return redirect(route('galleries.index'))->with($this->getResponseMessage());
    }
}
