<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Category;

use App\Events\Admin\CategoriesChanged;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\Category\CategoryRequest;
use App\Models\Admin\Photo;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends AdminController
{
    use Authorizable;

    use SaveImageTrait;

    protected $routeKey = 'admin.categories';

    protected $permissionKey = 'categories';

    protected $key = 'category';

    protected $name = 'Категории';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));

        $this->shareViewModuleData();

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $this->addScripts('libs/nestable2/jquery.nestable.min.js');
        $this->addCss('libs/nestable2/jquery.nestable.min.css');
        $list = $this->categoryRepository->getListAdmin();
        $data['content'] = view('admin.category.index', compact('list'));
        return $this->main($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $vars['categories'] = $this->categoryRepository->getListAdmin();
        $data['content'] = view('admin.category.create')->with($vars);

        return $this->main($data);
    }

    public function store(CategoryRequest $request)
    {
        if ($category = $this->categoryRepository->create($request->only($request->getFillableFields()))) {
            $this->setSuccessStore();
            event(new CategoriesChanged());
        }
        $this->saveImage($request, $category);

        return $this->redirectOnCreated($category);
    }

    public function edit(Category $category)
    {
        $vars['edit'] = $category;
        if (!$category) {
            $this->setMessage(__('modules._.record-not-finded'));
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        $categories = $this->categoryRepository->getCategoryParents($category, false);
        /** @var  $categoryParent Category */
        foreach ($categories as $categoryParent) {
            $this->addBreadCrumb($categoryParent->getNameDisplay(), route(routeKey('category', 'edit'), $categoryParent->getKey()));
        }

        $title = $this->titleEdit($category);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.category.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        if ($this->categoryRepository->update($request->only($request->getFillableFields()), $category)) {
            $this->setSuccessUpdate();
            $this->saveImage($request, $category);

            event(new CategoriesChanged());
        }

        return $this->redirectOnUpdated($category);
    }

    public function destroy(Category $category, Photo $photo)
    {
        if ($category->categories()->count()) {
            $this->setMessage('Нельзя удалить категорию имеющую дочерние категории');
        } else if ($category->delete()) {
            $photo->deleteImageStorage($category->image);
            $this->setSuccessDestroy();
            event(new CategoriesChanged());
        }

        return back()->with($this->getResponseMessage());
    }

    public function nesting(Request $request): array
    {
        $requestData = $request->get('categories');
        $this->categoryRepository->nestable($requestData);
        $this->setSuccessUpdate();

        event(new CategoriesChanged());

        return $this->getResponseMessageForJson();
    }

}
