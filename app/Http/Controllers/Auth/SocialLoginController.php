<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\SocialProvider;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\UserRepository;
use App\Services\Phone\PhoneUkraineFormatter;
use App\Services\User\Login\UserLoginService;
use App\Services\User\Registration\UserRegistrationStepsRedirectsService;
use App\Services\User\Registration\UserRegistrationStepsService;
use App\Models\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Contracts\User as SocialUser;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SocialLoginController extends LoginController
{


    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserRegistrationStepsService
     */
    private $registrationStepsService;

    public function __construct(UserRepository $userRepository, UserRegistrationStepsService $registrationStepsService, UserLoginService $userLoginService)
    {
        parent::__construct($userLoginService);
        $this->redirectTo = route('registration.phone.check');
        $this->userRepository = $userRepository;
        $this->registrationStepsService = $registrationStepsService;
    }

    public function handleAppleProviderCallback()
    {
        //todo apple login
    }

    public function redirectToAppleProvider()
    {
        $this->setFailMessage(getTranslate('messagies.error.unvailable.social', 'Данная соцсеть временно не доступна для регистрации'));
        return redirect(back())->with($this->getResponseMessage());
    }

    public function redirectToGoogleProvider(): RedirectResponse
    {
        return $this->redirectToProvider('google');
    }

    public function handleGoogleProviderCallback()
    {
        try {
            $driver = 'google';
            $user = Socialite::driver($driver)->user();
            return $this->onUserRetrieved($user, $driver);
        } catch (Exception $e) {
            return $this->getFailResponse($driver);
        }
    }

    public function redirectToFacebookProvider(): RedirectResponse
    {
        return $this->redirectToProvider('facebook');
    }

    public function handleFacebookProviderCallback()
    {
        try {
            $driver = 'facebook';
            $user = Socialite::driver($driver)->user();
            return $this->onUserRetrieved($user, $driver);
        } catch (Exception $e) {
            return $this->getFailResponse($driver);
        }
    }

    protected function getFailResponse(string $driver)
    {
        $this->setFailMessage(translateFormat('auth.social.auth.fail', ['social' => ucfirst($driver)]));
        return redirect('/')->with($this->getResponseMessage());
    }

    protected function redirectToProvider(string $provider): RedirectResponse
    {
        return Socialite::driver($provider)->redirect();
    }

    protected function onUserRetrieved(SocialUser $socialUser, string $provider)
    {
        $socialId = $socialUser->getId();
        $socialProvider = SocialProvider::where('provider_id', $socialId)->where('provider', $provider)->first();
        $user = ($socialProvider) ? $socialProvider->getUser() : $this->onProviderNotFound($socialUser, $provider);
        // If user already registered (match email or phone), we dont need send user through registration steps
        //and straight authorize
        if (!$user->wasRecentlyCreated) {
            auth()->login($user);
        } else {
            return $this->onRegistered($user);
        }

        $this->registrationStepsService->setUserId($user->id);

        return $this->onAuthorized($user);
    }

    protected function onProviderNotFound(SocialUser $socialUser, string $provider = ''): User
    {
        $fio = explode(' ', $socialUser->getName());
        $name = $fio[0] ?? '';
        $surname = $fio[1] ?? '';
        $email = $socialUser->getEmail();
        $avatar = $socialUser->getAvatar();
        $data = Arr::except(compact(array_keys(get_defined_vars())), ['fio']);

        $socialId = $socialUser->getId();

        $phone = trim($socialUser->phone ?? '');
        if ($phone) {
            $phone = PhoneUkraineFormatter::formatPhone($phone);
        }
        $createProviderData = ['provider' => $provider, 'provider_id' => $socialId];
        $user = $this->userRepository->where('email', $email)
            ->when($phone, function ($query, $phone) {
                return $query->orWhere('phone', $phone);
            })->first();
        if ($user) {
            $user->socialProviders()->create($createProviderData);
            return $user;
        }
        if ($phone) {
            $data['phone'] = $phone;
        }
        event(new Registered($user = $this->create($data)));
        $user->socialProviders()->create($createProviderData);
        return $user;
    }

    protected function create(array $data)
    {
        //todo remove duplication with RegisterController

        return User::create([
            'email' => $data['email'] ?? '',
            'name' => $data['name'] ?? '',
            'surname' => $data['surname'] ?? '',
            'phone' => $data['phone'] ?? null,
            'avatar' => $data['avatar'] ?? null,
            'customer_avatar' => $data['avatar'] ?? null,
            'password' => Hash::make($data['password'] ?? Str::random()),
        ]);
    }

    protected function onRegistered(User $user)
    {
        app(UserRegistrationStepsService::class)->personalDataSubmitted()->setSocialRegister(true);
        return redirect(UserRegistrationStepsRedirectsService::getByStep($this->registrationStepsService->getStep()))->with($this->getResponseMessage());
    }

    protected function onAuthorized(User $user)
    {
        $redirectTo = $this->redirectPath();
        $this->userLoginService->dropRedirect();
        return redirect($redirectTo)->with($this->getResponseMessage());
    }


    public function deletionFacebookCallback(Request $request)
    {
        $signed_request = $request->only('signed_request');
        $data = $this->parse_signed_request($signed_request);
        $user_id = $data['user_id'];

// Start data deletion


        $status_url = route('deletion.facebook',$user_id); // URL to track the deletion
        $confirmation_code = $user_id; // unique code for the deletion request

        $data = array(
            'url' => $status_url,
            'confirmation_code' => $confirmation_code
        );
        return json_encode($data);
    }

    function parse_signed_request($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        $secret = env('GOOGLE_CLIENT_SECRET'); // Use your app secret here

        // decode the data
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode($this->base64_url_decode($payload), true);

        // confirm the signature
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    public function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

}
