<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\SiteController;
use App\Http\Requests\User\Register\UserRegisterPersonalRequest;
use App\Providers\RouteServiceProvider;
use App\Repositories\UserRepository;
use App\Services\User\Registration\UserRegistrationStepsRedirectsService;
use App\Services\User\Registration\UserRegistrationStepsService;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;

class RegisterController extends SiteController
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;

	public function __construct(UserRepository $userRepository, UserRegistrationStepsService $registrationStepsService)
	{
		parent::__construct();
		$this->middleware('guest');
		$this->redirectTo = route('cabinet.password');
		$this->userRepository = $userRepository;
		$this->registrationStepsService = $registrationStepsService;
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'test' => ['nullable'],
		]);
	}

	protected function create(array $data)
	{
		return $this->userRepository->create([
			'email'    => $data['email'] ?? null,
			'fio'      => $data['fio'] ?? null,
			'phone'    => $data['phone'] ?? null,
			'password' => ($data['password'] ?? false) ? Hash::make($data['password']) : '',
		]);
	}

	public function showRegistrationForm()
	{
		return redirect('/');
		$data['content'] = view('auth.registration.register');
		return $this->main($data);
	}

	public function register(UserRegisterPersonalRequest $request)
	{
		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->only($request->getExistedFields()))));

		if ($response = $this->registered($request, $user)) {
			return $response;
		}
		return $request->expectsJson()
			? new Response(['redirect' => $this->redirectPath()], 201)
			: redirect($this->redirectPath());
	}

	protected function registered(Request $request, $user)
	{
		UserRegistrationStepsService::resolveSelf()->personalDataSubmitted();
		return null;
	}

	public function redirectTo(): string
	{
		return UserRegistrationStepsRedirectsService::getByStep($this->registrationStepsService->getStep());
	}


}
