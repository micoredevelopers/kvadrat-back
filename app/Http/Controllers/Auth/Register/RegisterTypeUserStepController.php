<?php


namespace App\Http\Controllers\Auth\Register;


use App\Events\User\Registration\UserSelectTypeSelectedEvent;
use App\Helpers\Media\ImageSaver;
use App\Helpers\User\Company\CompanyEmployersAmountHelper;
use App\Helpers\User\View\PhoneDisplay;
use App\Http\Controllers\SiteController;
use App\Http\Requests\AbstractRequest as Request;
use App\Http\Requests\User\Register\UserRegisterTypeCompanyRequest;
use App\Http\Requests\User\Register\UserRegisterTypeCustomerRequest;
use App\Http\Requests\User\Register\UserRegisterTypePerformerRequest;
use App\Platform\Cabinet\Route\CabinetDestinationLinks;
use App\Platform\Cabinet\Route\LinkCabinetByUserType;
use App\Platform\Model\UserFields;
use App\Platform\Order\Helpers\TempOrderHelper;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\CityRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\UserRepository;
use App\Services\User\Registration\UserRegistrationStepsService;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Exceptions\ValidatorException;

class RegisterTypeUserStepController extends SiteController
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;
	/**
	 * @var TempOrderHelper
	 */
	private $tempOrderHelper;

	public function __construct(
		UserRegistrationStepsService $registrationStepsService,
		UserRepository               $userRepository,
		AuthenticatedUserType        $authenticatedUserType,
		TempOrderHelper              $tempOrderHelper
	)
	{
		parent::__construct();
		$this->registrationStepsService = $registrationStepsService;
		$this->userRepository = $userRepository;
		$this->authenticatedUserType = $authenticatedUserType;
		$this->tempOrderHelper = $tempOrderHelper;
	}

	public function showForm(CityRepository  $cityRepository,
							 OrderRepository $orderRepository)
	{
		$this->setTitle(getTranslate('registration.create.questionnaire', 'Заполнение анкеты'));
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		$cities = $cityRepository->getListPublic();
		$userName = $user->getName();
		$userSurname = $user->getSurname();
		$userPhone = PhoneDisplay::display($user->getPhone());
		$userEmail = $user->getEmail();
		$userAvatar = $user->getAvatar();
		if ($this->tempOrderHelper->hasUnfinishedOrder() && ($order = $orderRepository->find($this->tempOrderHelper->getOrderId()))) {
			$userCity = $order->city_id ?? null;
		}
		$employersQuantites = CompanyEmployersAmountHelper::getAmounts();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = ($this->tempOrderHelper->hasUnfinishedOrder()
			? view('auth.registration.order-type-customer')
			: view('auth.registration.select-type'))->with($with);

		return $this->main($data);
	}

	public function sendFormCustomer(UserRegisterTypeCustomerRequest $request)
	{
		return $this->onSubmitted($request);
	}

	public function sendFormCompany(UserRegisterTypeCompanyRequest $request)
	{
		return $this->onSubmitted($request);
	}

	public function sendFormPerformer(UserRegisterTypePerformerRequest $request)
	{
		return $this->onSubmitted($request);
	}

	private function onSubmitted(Request $request)
	{
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		try {
			$fields = $request->only($request->getFillableFields());
			if ($request->hasFile('avatar')) {
				$avatarField = UserFields::get(UserTypeHelper::getType($request->get('type')), 'avatar');
				$imageSaver = app(ImageSaver::class, ['request' => $request, 'nameKey' => 'avatar']);
				$imageSaver->setFolderName($user->getTable() . DIRECTORY_SEPARATOR . $request->get('type'));
				$fields[$avatarField] = $imageSaver->saveFromRequest();
			}
			if ($request->has('password')) {
				$fields['password'] = Hash::make($request->get('password'));
			}
			$this->userRepository->update($fields, $user);
		} catch (ValidatorException $e) {
			return $this->redirect($request, url()->previous());
		}

		auth()->login($user);
		event(new UserSelectTypeSelectedEvent($user, $request));

		return $this->redirect($request, $this->getRedirectPath($this->registrationStepsService));
	}

	private function redirect(\Illuminate\Http\Request $request, $path)
	{
		return $request->expectsJson() ? ['redirect' => $path] : redirect($path);
	}

	private function getRedirectPath(UserRegistrationStepsService $stepsService): string
	{
		if ($to = $stepsService->getRedirectTo()) {
			return $to;
		}
		$userType = $this->authenticatedUserType->getUserType();

		return LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PERSONAL);
	}

}