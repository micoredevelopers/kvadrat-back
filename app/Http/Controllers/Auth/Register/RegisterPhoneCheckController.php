<?php


namespace App\Http\Controllers\Auth\Register;


use App\Http\Controllers\SiteController;
use App\Http\Requests\User\Register\UserRegisterPhoneRequest;
use App\Repositories\UserRepository;
use App\Services\Phone\PhoneUkraineFormatter;
use App\Services\User\Registration\UserRegistrationStepsRedirectsService;
use App\Services\User\Registration\UserRegistrationStepsService;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;

class RegisterPhoneCheckController extends SiteController
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRegistrationStepsService $registrationStepsService, UserRepository $userRepository)
	{
		parent::__construct();
		$this->registrationStepsService = $registrationStepsService;
		$this->userRepository = $userRepository;
	}

	public function showForm()
	{
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		$validator = Validator::make(['phone' => PhoneUkraineFormatter::formatPhone($user->getPhone())], [
			'phone' => ['required','phone:UA,mobile'],
		]);
		if ($validator->passes()) {
			app(UserRegistrationStepsService::class)->phoneFormatValidated();
			return redirect(UserRegistrationStepsRedirectsService::getByStep($this->registrationStepsService->getStep()));
		}
		$with = compact('user');
		$data['content'] = view('auth.registration.check-phone')->with($with);
		return $this->main($data);
	}

	public function sendForm(UserRegisterPhoneRequest $request)
	{
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		try {
			$this->userRepository->update(['phone' => $request->get('phone')], $user);
		} catch (ValidatorException $e) {
		}
		$to = UserRegistrationStepsRedirectsService::getByStep($this->registrationStepsService->getStep());
		if ($request->expectsJson()) {
			$this->setResponseData(['redirect' => $to]);
			return $this->getResponseMessageForJson();
		}
		return redirect($to);
	}

}