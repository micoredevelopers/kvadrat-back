<?php


namespace App\Http\Controllers\Auth\Register;


use App\Events\Phone\PhoneVerifiedEvent;
use App\Events\User\Registration\PhoneVerifyStepSuccessCompletedEvent;
use App\Http\Controllers\SiteController;
use App\Http\Requests\Phone\BaseSendConfirmPhoneRequest;
use App\Listeners\User\Registration\UserSelectTypeSelectedListener;
use App\Repositories\UserRepository;
use App\Services\Phone\PhoneVerificationDataKeeper;
use App\Services\Phone\PhoneVerificationService;
use App\Services\User\Registration\UserRegistrationStepsRedirectsService;
use App\Services\User\Registration\UserRegistrationStepsService;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterPhoneConfirmController extends SiteController
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRegistrationStepsService $registrationStepsService, UserRepository $userRepository)
	{
		parent::__construct();
		$this->registrationStepsService = $registrationStepsService;
		$this->userRepository = $userRepository;
	}

	public function showForm(PhoneVerificationService $phoneVerificationService, Request $request)
	{
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		if ($user->getAttribute('phone_verified_at')) {
			return $this->onCompletedStep($request, $user);
		}
		$phone = $user->getPhone();
		$verification = $phoneVerificationService->getVerification($phone);
		$with = compact(
			'phone'
			, 'user'
			, 'verification'
		);
		$data['content'] = view('auth.registration.confirm')->with($with);
		return $this->main($data);
	}

	public function sendForm(
		BaseSendConfirmPhoneRequest $request
		, PhoneVerificationService $phoneVerificationService
		, PhoneVerificationDataKeeper $phoneVerificationDataKeeper
	)
	{
		if ($verification = $phoneVerificationService->getVerification($phoneVerificationDataKeeper->getPhone())) {
			event(app(PhoneVerifiedEvent::class, compact('verification')));
		}
		$user = $this->userRepository->find($this->registrationStepsService->getUserId());
		event(app(PhoneVerifyStepSuccessCompletedEvent::class, compact('user')));
		return $this->onCompletedStep($request, $user);
	}

	protected function onCompletedStep(Request $request, User $user)
	{
		$redirectTo = UserRegistrationStepsRedirectsService::getByStep($this->registrationStepsService->getStep());
		if ($request->expectsJson()) {
			return ['redirect' => $redirectTo];
		}
		return redirect($redirectTo);

	}


}