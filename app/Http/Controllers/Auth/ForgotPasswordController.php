<?php

	namespace App\Http\Controllers\Auth;

	use App\Http\Controllers\SiteController;
	use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
	use Illuminate\Http\JsonResponse;
	use Illuminate\Http\Request;
	use Illuminate\Validation\ValidationException;

	class ForgotPasswordController extends SiteController
	{
		use SendsPasswordResetEmails;

		/*
		|--------------------------------------------------------------------------
		| Password Reset Controller
		|--------------------------------------------------------------------------
		|
		| This controller is responsible for handling password reset emails and
		| includes a trait which assists in sending these notifications from
		| your application to your users. Feel free to explore this trait.
		|
		*/

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
			$this->middleware('guest');
		}

		public function showLinkRequestForm()
		{
			$data['content'] =  view('auth.passwords.email');
			return $this->main($data);
		}

		protected function sendResetLinkResponse(Request $request, $response)
		{
			$this->setSuccessMessage(trans($response));
			return $request->expectsJson()
				? new JsonResponse($this->getResponseMessageForJson(), 200)
				: back()->with($this->getResponseMessage() );
		}

		protected function sendResetLinkFailedResponse(Request $request, $response)
		{
			if ($request->expectsJson()) {
				throw ValidationException::withMessages([
					'email' => [trans($response)],
				]);
			}

			return back()
				->withInput($request->only('email'))
				->withErrors(['email' => trans($response)]);
		}

	}
