<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SiteController;
use App\Http\Requests\User\Login\UserLoginRequest;
use App\Platform\Route\PlatformRoutes;
use App\Providers\RouteServiceProvider;
use App\Services\User\Login\UserLoginService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends SiteController
{
	protected $username = 'email';
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;
	/**
	 * @var UserLoginService
	 */
	protected $userLoginService;

	/**
	 * Create a new controller instance.
	 *
	 * @param UserLoginService $userLoginService
	 */
	public function __construct(UserLoginService $userLoginService)
	{
		parent::__construct();
		$this->middleware('guest')->except('logout');
		$this->userLoginService = $userLoginService;
	}

	public function username()
	{
		return $this->username;
	}

	public function showLoginForm()
	{
		return redirect('/#login');
	}

	protected function credentials(Request $request)
	{
		return $request->only($this->username(), 'password');
	}

	public function login(UserLoginRequest $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if (method_exists($this, 'hasTooManyLoginAttempts') &&
			$this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);

			$this->sendLockoutResponse($request);
		}
		$this->setUserNameByRequest($request);


		if ($this->attemptLogin($request)) {
			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	protected function sendLoginResponse(Request $request)
	{
		$request->session()->regenerate();

		$this->clearLoginAttempts($request);

		if ($response = $this->authenticated($request, $this->guard()->user())) {
			return $response;
		}

		$redirectTo = $this->redirectPath();
		$this->userLoginService->dropRedirect();
		$this->setSuccessMessage(getTranslate('messagies.succes.auth', 'Вы авторизированы'))->setResponseData(['redirect' => $redirectTo]);
		return $request->expectsJson()
			? $this->getResponseMessageForJson()
			: redirect()->intended($redirectTo);
	}

	protected function attemptLogin(Request $request)
	{
		$credentials = $this->credentials($request);
		$credentials['active'] = 1;
		return Auth::attempt($credentials);
	}

	protected function validateLogin(Request $request)
	{
		//no need validate here, because we have custom request
	}

	protected function setUserNameByRequest(UserLoginRequest $request)
	{
		$this->username = $request->getLoginField();
	}

	protected function guard()
	{
		return Auth::guard('web');
	}

	public function redirectPath()
	{
		return $this->userLoginService->getRedirectTo();
	}
}
