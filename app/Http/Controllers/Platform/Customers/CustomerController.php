<?php

namespace App\Http\Controllers\Platform\Customers;

use App\Helpers\User\Model\UserHasSocialsContract;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Cabinet\Display\UserRegistrationDuration;
use App\Platform\Reviews\ReviewDisplayContainer;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\PerformerSkillsRepository;
use App\Repositories\PortfolioRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class CustomerController extends AbstractPlatformController
{
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var PortfolioRepository
	 */
	private $portfolioRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	/**
	 * PerformersController constructor.
	 * @param UserRepository $userRepository
	 * @param ReviewRepository $reviewRepository
	 * @param PortfolioRepository $portfolioRepository
	 * @param AuthenticatedUserResolver $userResolver
	 */
	public function __construct(
		UserRepository $userRepository,
		ReviewRepository $reviewRepository,
		PortfolioRepository $portfolioRepository,
		AuthenticatedUserResolver $userResolver
	)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
		$this->userRepository = $userRepository;
		$this->portfolioRepository = $portfolioRepository;
		$this->userResolver = $userResolver;
	}

	public function index()
	{
		d(__METHOD__);
		return [];
	}

	public function show($customerId)
	{
		$this->setTitle(getTranslate('header.customer.profile', 'Профиль заказчика'));
		$customer = $this->userRepository->findCustomer($customerId);
		$customerContainer = app(DisplayCustomerContainer::class, ['user' => $customer]);
		$linkBack = url()->previous('/');
		if ($customerContainer->isProfileEnabled()) {
			$userContainer = $customerContainer;
			$currentUser = $this->userResolver->getUser();
			$userLastActivity = app(UserLastActivity::class, ['user' => $customer]);
			$userRegistrationDuration = app(UserRegistrationDuration::class, ['user' => $customer]);
			$customer->load('city');
			$userSocials = app(UserHasSocialsContract::class, ['model' => $customer]);
			$reviews = $this->reviewRepository->getAboutCustomerPaginated($customer);
			$reviews->load('reviewFrom', 'order', 'images');
			$reviews->getCollection()->transform(function ($review) {
				return app(ReviewDisplayContainer::class, compact('review'));
			})
			;
			$reviews->setPath(route('platform.customers.reviews.load', $customerId));

		}
		$with = compact(array_keys(get_defined_vars()));
		if ($customerContainer->isProfileEnabled()) {
			$data['content'] = view('public.cabinet.customer.public-profile.public-profile')->with($with);
		} else {
			$data['content'] = view('public.cabinet.customer.public-profile.profile-disabled')->with($with);
		}

		return $this->main($data);
	}
}
