<?php


namespace App\Http\Controllers\Platform\Customers;


use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use App\Platform\Reviews\ReviewDisplayContainer;
use Illuminate\Http\Request;

class CustomerReviewsLoadController extends AbstractPlatformController
{
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(
		UserRepository $userRepository,
		ReviewRepository $reviewRepository
	)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
		$this->userRepository = $userRepository;
	}

	public function loadMore($customerId, Request $request): array
	{
		$customer = $this->userRepository->findCustomer($customerId);
		$reviews = $this->reviewRepository->getAboutCustomerPaginated($customer);
		$reviews->load('reviewFrom', 'order', 'images');
		$reviews->getCollection()->transform(function ($review) {
			return app(ReviewDisplayContainer::class, compact('review'));
		})
		;
		$reviews->setPath(route('platform.customers.reviews.load', $customerId));
		$with = compact(array_keys(get_defined_vars()));
		$view = view('public.cabinet.customer.public-profile.includes.reviews')->with($with)->render();
		return [
			'view'      => $view,
			'next_page' => $reviews->nextPageUrl(),
		];
	}

}