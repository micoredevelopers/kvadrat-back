<?php


namespace App\Http\Controllers\Platform\Performers;


use App\DataContainers\Platform\User\SearchPerformerDataContainer;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Http\Requests\User\PerformersIndexRequest;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Cabinet\Display\UserRegistrationDuration;
use App\Platform\Query\DataContainers\CategoryTransformer;
use App\Platform\Query\OnPageHelper;
use App\Platform\Query\PaginationOnPage;
use App\Platform\Reviews\ReviewDisplayContainer;
use App\Platform\Route\PlatformRoutes;
use App\Platform\Search\SearchPerformersSavedFilter;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\PerformerRepository;
use App\Repositories\PerformerSkillsRepository;
use App\Repositories\PortfolioRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class PerformersController extends AbstractPlatformController
{
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var PortfolioRepository
	 */
	private $portfolioRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var PerformerSkillsRepository
	 */
	private $skillsRepository;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;
	/**
	 * @var CityRepository
	 */
	private $cityRepository;

	public function __construct(
		PerformerRepository $userRepository,
		ReviewRepository $reviewRepository,
		PortfolioRepository $portfolioRepository,
		AuthenticatedUserResolver $userResolver,
		CategoryRepository $categoryRepository,
		CityRepository $cityRepository
	)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
		$this->userRepository = $userRepository;
		$this->portfolioRepository = $portfolioRepository;
		$this->userResolver = $userResolver;
		$this->categoryRepository = $categoryRepository;
		$this->cityRepository = $cityRepository;
	}

	public function index(SearchPerformerDataContainer $container, PerformersIndexRequest $request, PaginationOnPage $paginationOnPage)
	{
		$currentUser = $this->userResolver->getUser();
		if ($currentUser) {
			SearchPerformersSavedFilter::make($currentUser)->syncWithRequest($request);
		}
		$this->setTitle(getTranslate('performers.title', 'Исполнители'));
		$container->fillFromRequest($request);
		$onPageItems = app(OnPageHelper::class, ['items' => $paginationOnPage->getItems(), 'activeItem' => $container->getOnPage()]);
		$performers = $this->userRepository->getPerformersPaginated($container);
		$performers->setPath(route(PlatformRoutes::PERFORMERS_LIST))->withQueryString();
		$performers->getCollection()->load('metas')->transform(function ($performer) {
			return app(DisplayPerformerContainer::class, ['user' => $performer]);
		})
		;
		$selectedCategories = $this->categoryRepository->find($container->getCategoryIds());
		$cities = $this->cityRepository->getListSelect();
		$selectedCity = $container->getCityId();
		$search = $container->getSearch();
		$categories = app(CategoryTransformer::class,
			['categories' => $this->categoryRepository->getTreePublic(), 'selected' => $container->getCategoryIds()])->transform();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('public.platform.performers.list.list')->with($with);
		return $this->main($data);
	}

	public function show($performerId, PerformerSkillsRepository $skillsRepository)
	{
		$performer = $this->userRepository->find($performerId);
		$performerContainer = app(DisplayPerformerContainer::class, ['user' => $performer]);
		$this->setTitle(getTranslate('header.public.profile', 'Публичный профиль') . $performerContainer->getName());
		$linkBack = url()->previous('/');
		if ($performerContainer->isProfileEnabled()) {
			$userContainer = $performerContainer;
			$currentUser = $this->userResolver->getUser();
			$canEmployee = (false && ($currentUser && $currentUser->getKey() !== $performer->getKey()));
			$userLastActivity = app(UserLastActivity::class, ['user' => $performer]);
			$userRegistrationDuration = app(UserRegistrationDuration::class, ['user' => $performer]);
			$performer->load('city');
			$userSocials = app(UserHasSocialsContract::class, ['model' => $performer]);
			$reviews = $this->reviewRepository->getAboutPerformerPaginated($performer);

			$reviews->load('reviewFrom', 'order', 'images');
			$reviews->getCollection()->transform(function ($review) {
				return app(ReviewDisplayContainer::class, compact('review'));
			});
			$reviews->setPath(route('platform.performers.reviews.load', $performerId));
			$skillsCategories = $skillsRepository->getTreePublic($performer);
			$portfolios = $this->portfolioRepository->getByUser($performer);
		}
		$with = compact(array_keys(get_defined_vars()));
		if ($performerContainer->isProfileEnabled()) {
			$data['content'] = view('public.platform.performers.public-profile.public-profile')->with($with);
		} else {
			$data['content'] = view('public.platform.performers.public-profile.profile-disabled')->with($with);
		}

		return $this->main($data);
	}
}