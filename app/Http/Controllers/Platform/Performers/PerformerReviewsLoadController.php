<?php


namespace App\Http\Controllers\Platform\Performers;


use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Repositories\PerformerRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use App\Platform\Reviews\ReviewDisplayContainer;
use Illuminate\Http\Request;

class PerformerReviewsLoadController extends AbstractPlatformController
{
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(
		PerformerRepository $userRepository,
		ReviewRepository $reviewRepository
	)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
		$this->userRepository = $userRepository;
	}

	public function loadMore($performerId, Request $request): array
	{
		$performer = $this->userRepository->find($performerId);
		$reviews = $this->reviewRepository->getAboutPerformerPaginated($performer);
		$reviews->load('reviewFrom', 'order', 'images');
		$reviews->getCollection()->transform(function ($review) {
			return app(ReviewDisplayContainer::class, compact('review'));
		})
		;
		$reviews->setPath(route('platform.performers.reviews.load', $performerId));
		$with = compact(array_keys(get_defined_vars()));
		$view = view('public.platform.performers.public-profile.includes.reviews')->with($with)->render();
		return [
			'view'      => $view,
			'next_page' => $reviews->nextPageUrl(),
		];
	}

}