<?php

namespace App\Http\Controllers\Platform\Order;

use App\Events\Order\BeforeOrderClosedEvent;
use App\Events\Order\OrderClosedEvent;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Containers\Show\DealPermissionsContainer;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\Order\OrderRepository;

class OrderCloseController extends AbstractPlatformController
{
	/**
	 * @var OrderRepository
	 */
	private $repository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	public function __construct(
		OrderRepository $orderRepository,
		AuthenticatedUserResolver $userResolver
	)
	{
		parent::__construct();
		$this->repository = $orderRepository;
		$this->userResolver = $userResolver;
	}

	public function close($orderId)
	{
		$order = $this->repository->findForActions($orderId);
		if (!$order) {
			return $this->setFailMessage(getTranslate('registration.deal.not.found', 'Сделка не найдена'))->getResponseMessageForJson();
		}
		$container = $order->getContainer();
		/** @var  $currentUser User */
		$currentUser = $this->userResolver->getUser();
		$permissions = app(DealPermissionsContainer::class, ['performer' => $currentUser, 'order' => $order, 'customer' => $order->getContainer()->getCustomer()]);
		if (!$permissions->isUserOwnerDeal()) {
			return $this->setFailMessage(getTranslate('registration.close.only.author', 'Завершить сделку может только автор'))->getResponseMessageForJson();
		}
		if ($container->isClosed()) {
			return $this->setFailMessage(getTranslate('registration.deal.is.closed', 'Сделка уже закрыта'))->getResponseMessageForJson();
		}

		$this->setFailMessage(getTranslate('messagies.error.deal.not.closed', 'Ошибка, сделка не была закрыта'));
		event(app(BeforeOrderClosedEvent::class, compact('order')));
		$order->getSetter()->close();
		if ($this->repository->update([], $order)) {
			event(app(OrderClosedEvent::class, compact('order')));
			$this->setSuccessMessage(getTranslate('messagies.deal.is.now.closed', 'Сделка закрыта'));
		}

		return $this->getResponseMessageForJson();

	}
}
