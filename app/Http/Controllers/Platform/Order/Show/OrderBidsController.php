<?php


namespace App\Http\Controllers\Platform\Order\Show;


use App\Events\Order\Show\Bids\BidBeforeDisabledEvent;
use App\Events\Order\Show\Bids\BidCreatedEvent;
use App\Events\Order\Show\Bids\BidDisabledEvent;
use App\Events\Order\Show\Bids\BidEditedEvent;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Http\Requests\Order\Show\OrderAddBidRequest;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Containers\Show\DealPermissionsContainer;
use App\Platform\Order\Services\IsUserOwnerService;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Rules\Order\OrderExistsRule;

class OrderBidsController extends AbstractPlatformController
{
	/**
	 * @var OrderRepository
	 */
	private $repository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;

	public function __construct(
		OrderRepository $repository,
		OrderBidRepository $orderBidRepository,
		AuthenticatedUserResolver $userResolver
	)
	{
		parent::__construct();
		$this->repository = $repository;
		$this->userResolver = $userResolver;
		$this->orderBidRepository = $orderBidRepository;
	}

	public function addBid($orderId, OrderAddBidRequest $request)
	{
		/** @var  $order Order */
		$order = $this->repository->findForActions($orderId);
		$container = $order->getContainer();
		if ($check = $this->checkCan($order)) {
			return $check;
		}
		/** @var  $currentUser User */
		$currentUser = $this->userResolver->getUser();
		if ($container->hasPerformer()) {
			return $this->setFailMessage(getTranslate('registration.performer.is.customer', 'Исполнитель уже выбран заказчиком'))->getResponseMessageForJson();
		}
		$permissions = app(DealPermissionsContainer::class, ['performer' => $currentUser, 'order' => $order, 'customer' => $container->getCustomer()]);
		if (app(IsUserOwnerService::class)->check($container->getCustomer(), $currentUser)) {
			return $this->setFailMessage(getTranslate('registration.not.request.to.you.deals', 'Нельзя оставить заявку к своей сделке'))->getResponseMessageForJson();
		}
		$this->setFailMessage(getTranslate('registration.only.one.request', 'Можно оставлять только одну заявку к сделке'));
		if ($permissions->canAddBid()) {
			$input = $request->only($request->getFillableFields());
			$input[$order->getForeignKey()] = $order->getKey();
			if ($bid = $this->orderBidRepository->createRelated($input, $currentUser)) {
				debugInfo($bid);
				event(app(BidCreatedEvent::class, ['orderBid' => $bid, 'order' => $order]));
				$this->setSuccessMessage(getTranslate('registration.succes.deal', 'Заявка успешно добавлена'))->setResponseData(['bid' => $bid->toArray()]);
			}
		}
		return $this->getResponseMessageForJson();
	}


	public function editBid($orderId, OrderAddBidRequest $request)
	{
		/** @var  $order Order */
		$order = $this->repository->findForActions($orderId);
		if ($check = $this->checkCan($order)) {
			return $check;
		}
		/** @var  $currentUser User */
		$currentUser = $this->userResolver->getUser();
		$permissions = app(DealPermissionsContainer::class, ['performer' => $currentUser, 'order' => $order, 'customer' => $order->getContainer()->getCustomer()]);
		$bid = $this->orderBidRepository->getByOrderAndUser($order, $currentUser);
		if (is_null($bid)) {
			return $this->setFailMessage(getTranslate('registration.deals.not.found', 'Заявка не найдена'))->getResponseMessageForJson();
		}
		$this->setFailMessage(getTranslate('registration.deals.editing.not.available', 'Редактирование заявки не доступно'));
		if ($permissions->canEditBid($bid)) {
			$input = $request->only($request->getFillableFields());
			$this->orderBidRepository->update($input, $bid);
			event(app(BidEditedEvent::class, ['orderBid' => $bid, 'order' => $order]));
			$this->setSuccessMessage(getTranslate('registration.succes.editing.deal', 'Заявка успешно отредактирована'))->setResponseData(['bid' => $bid->toArray()]);
		}
		return $this->getResponseMessageForJson();
	}

	public function cancelBid($orderId)
	{
		/** @var  $order Order */
		$order = $this->repository->findForActions($orderId);
		if ($check = $this->checkCan($order)) {
			return $check;
		}
		/** @var  $currentUser User */
		$currentUser = $this->userResolver->getUser();
		$orderBid = $this->orderBidRepository->getByOrderAndUser($order, $currentUser);
		if (is_null($orderBid)) {
			return $this->setFailMessage(getTranslate('registration.deals.not.found', 'Заявка не найдена'))->getResponseMessageForJson();
		}

		$permissions = app(DealPermissionsContainer::class, ['performer' => $currentUser, 'order' => $order, 'customer' => $order->getContainer()->getCustomer()]);
		if (!$permissions->isCanCancelBid()) {
			return $this->setFailMessage(getTranslate('registration.deals.no.deleting', 'Нельзя отозвать заявку'))->getResponseMessageForJson();
		}
		$orderBid->getSetter()->disable();
		event(app(BidBeforeDisabledEvent::class, compact('orderBid','order')));
		if ($this->orderBidRepository->update([], $orderBid)) {
			event(app(BidDisabledEvent::class, compact('orderBid','order')));
		}
		return $this->setSuccessMessage(getTranslate('registration.bid.is.deleting', 'Ставка отозвана'))->getResponseMessageForJson();
	}

	private function checkCan(Order $order)
	{
		if (!$order) {
			return $this->setFailMessage(getTranslate('registration.deal.not.found', 'Сделка не найдена'))->getResponseMessageForJson();
		}
		if (!$this->userResolver->getUser()) {
			return $this->setFailMessage(getTranslate('registration.you.must.auth', 'Для создания завки необходимо авторизироваться'))->getResponseMessageForJson();
		}
		return null;
	}
}