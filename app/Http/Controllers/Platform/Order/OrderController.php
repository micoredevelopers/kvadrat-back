<?php

namespace App\Http\Controllers\Platform\Order;

use App\DataContainers\Platform\Order\SearchOrderDataContainer;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Http\Requests\Order\OrderIndexRequest;
use App\Models\Order\Order;
use App\Platform\Order\Services\IsUserCanViewOrderService;
use App\Platform\Query\DataContainers\CategoryTransformer;
use App\Platform\Query\OnPageHelper;
use App\Platform\Query\PaginationOnPage;
use App\Platform\Route\PlatformRoutes;
use App\Platform\Search\SearchDealsSavedFilter;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends AbstractPlatformController
{
	/**
	 * @var PaginationOnPage
	 */
	private $paginationOnPage;
	/**
	 * @var OrderRepository
	 */
	private $repository;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;
	/**
	 * @var CityRepository
	 */
	private $cityRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	public function __construct(
		PaginationOnPage $paginationOnPage,
		OrderRepository $repository,
		CategoryRepository $categoryRepository,
		CityRepository $cityRepository,
		AuthenticatedUserResolver $userResolver
	)
	{
		parent::__construct();
		$this->paginationOnPage = $paginationOnPage;
		$this->repository = $repository;
		$this->categoryRepository = $categoryRepository;
		$this->cityRepository = $cityRepository;
		$this->userResolver = $userResolver;
	}

	public function index(SearchOrderDataContainer $container, OrderIndexRequest $request, IsUserCanViewOrderService $viewOrderService)
	{
		$currentUser = $this->userResolver->getUser();
		if ($currentUser) {
			SearchDealsSavedFilter::make($currentUser)->syncWithRequest($request);
		}
		$container->fillFromRequest($request);
		$this->setTitle(getTranslate('order.list', 'Сделки'))->addBreadCrumb(getTranslate('order.list', 'Сделки'));

		$onPageItems = app(OnPageHelper::class, ['items' => $this->paginationOnPage->getItems(), 'activeItem' => $container->getOnPage()]);

		$orders = $this->repository->getOrdersPaginated($container);
		$orders->load('city',);
		$orders->setPath(route(PlatformRoutes::ORDER_LIST))->withQueryString();

		$orders->getCollection()->transform(function (Order $order) use ($currentUser, $viewOrderService) {
				$container = $order->getContainer();
				$container->setCanViewOrder($viewOrderService->check($currentUser, $order));
				return $container;
		})
		;
		$selectedCategories = $this->categoryRepository->find($container->getCategoryIds());
		$selectedCity = $container->getCityId();
		$cities = $this->cityRepository->getListSelect();
		$search = $container->getSearch();
		$categories = app(CategoryTransformer::class,
			['categories' => $this->categoryRepository->getTreePublic(), 'selected' => $container->getCategoryIds()])->transform();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('public.deals.list.index')->with($with);

		return $this->main($data);
	}
}
