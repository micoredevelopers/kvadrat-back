<?php


namespace App\Http\Controllers\Platform\Order;


use App\Events\Order\Reviews\AddCustomerReviewEvent;
use App\Events\Order\Reviews\AddPerformerReviewEvent;
use App\Events\Order\Reviews\BeforeAddCustomerReviewEvent;
use App\Events\Order\Reviews\BeforeAddPerformerReviewEvent;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Http\Requests\Order\ReviewAddPerformerRequest;
use App\Http\Requests\Order\ReviewAddCustomerRequest;
use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Order\Containers\ReviewsDataContainer;
use App\Platform\Order\Containers\Show\DealPermissionsContainer;
use App\Platform\Order\Services\IsUserOwnerService;
use App\Platform\Order\Services\Performer\IsUserSelectedAsPerformerInOrder;
use App\Platform\Order\Services\Reviews\IsDateReviewNotExpired;
use App\Platform\Order\Uploads\ReviewImagesUploader;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\Order\OrderRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class OrderReviewsController extends AbstractPlatformController
{
	private $orderRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(
		OrderRepository $orderRepository,
		UserRepository $userRepository,
		AuthenticatedUserResolver $userResolver,
		ReviewRepository $reviewRepository
	)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
		$this->userResolver = $userResolver;
		$this->userRepository = $userRepository;
		$this->reviewRepository = $reviewRepository;
	}

	public function performer($orderId, ReviewAddPerformerRequest $request)
	{
		$order = $this->orderRepository->findForActions($orderId);
		if (!is_null($check = $this->check($order))) {
			return $check;
		}
		$container = $order->getContainer();
		$performer = $this->getCurrentUser();
		$customer = $container->getCustomer();
		$reviewsContainer = app(ReviewsDataContainer::class, compact('order'));
		if (!app(IsUserSelectedAsPerformerInOrder::class)->check($performer, $order)) {
			return $this->setFailMessage(getTranslate('messagies.you.not.performer.there', 'Вы не можете оставить отзыв где вы не выбраны исполнителем'))->getResponseMessageForJson();
		}
		if ($reviewsContainer->hasPerformerReview()) {
			return $this->setFailMessage(getTranslate('messagies.you.save.comment', 'Вы уже оставили отзыв к сделке'))->getResponseMessageForJson();
		}

		$review = $this->reviewRepository->makeModel();
		$review->fill($request->only($request->getFillableFields()));

		$review->getSetter()->setFrom($performer)->setAbout(UserTypeContract::TYPE_CUSTOMER)->setTo($customer)->setOrder($order);
		event(app(BeforeAddPerformerReviewEvent::class, compact('order', 'review', 'performer')));
		if ($review = $this->reviewRepository->create($review->toArray())) {
			$this->uploadImages($request, $review);
			$this->setSuccessMessage(getTranslate('messagies.save.comment', 'Ваш отзыв успешно добавлен'));
			event(app(AddPerformerReviewEvent::class, compact('order', 'review', 'performer')));
		}

		return $this->getResponseMessageForJson();
	}

	public function customer($orderId, ReviewAddCustomerRequest $request)
	{
		/** @var  $order Order */
		$order = $this->orderRepository->findForActions($orderId);
		if (!is_null($check = $this->check($order))) {
			return $check;
		}
		$container = $order->getContainer();
		$customer = $this->getCurrentUser();
		$performer = $container->getPerformer();
		$reviewsContainer = app(ReviewsDataContainer::class, compact('order'));
		if (!app(IsUserOwnerService::class)->check($container->getCustomer(), $customer)) {
			return $this->setFailMessage(getTranslate('messagies.can.save.only.author', 'Отзыв о сделке может оставить только автор сделки'))->getResponseMessageForJson();
		}
		if ($reviewsContainer->hasCustomerReview()) {
			return $this->setFailMessage(getTranslate('messagies.you.save.comment', 'Вы уже оставили отзыв к сделке'))->getResponseMessageForJson();
		}
		if (!$performer) {
			return $this->setFailMessage(getTranslate('messagies.not.selected.performer', 'Нельзя оставить отзыв без выбранного исполнителя'))->getResponseMessageForJson();
		}
		$review = $this->reviewRepository->makeModel();
		$review->fill($request->only($request->getFillableFields()));
		$review->getSetter()->setFrom($customer)->setAbout(UserTypeContract::TYPE_PERFORMER)->setTo($performer)->setOrder($order);
		event(app(BeforeAddCustomerReviewEvent::class, compact('order', 'review', 'customer')));
			if ($review = $this->reviewRepository->create($review->toArray())) {
			$this->uploadImages($request, $review);
			$this->setSuccessMessage(getTranslate('messagies.save.comment', 'Ваш отзыв успешно добавлен'));
			event(app(AddCustomerReviewEvent::class, compact('order', 'review', 'customer')));
		}

		return $this->getResponseMessageForJson();
	}

	private function uploadImages(Request $request, Review $review): void
	{
		if ($request->hasFile('images')) {
			$images = app(ReviewImagesUploader::class, ['uploadedFiles' => $request->file('images')])
				->setFolderName('deals/reviews/' . $review->getOrderKey())->upload()
			;
			foreach ($images as $path) {
				$review->images()->create(['image' => $path]);
			}
		}
	}

	private function check(?Order $order)
	{
		if (!$order) {
			return $this->setFailMessage(getTranslate('registration.deal.not.found', 'Сделка не найдена'))->getResponseMessageForJson();
		}
		$container = $order->getContainer();

		if (!app(IsDateReviewNotExpired::class)->check($container->getDateClose())) {
			return $this->setFailMessage(getTranslate('messagies.time.to.comment.finish', 'Период добавления отзыва истек'))->getResponseMessageForJson();
		}

		if ($container->isOpened()) {
			return $this->setFailMessage(getTranslate('messagies.reviews.after.close.deal', 'Добавление отзыва будет доступно после закрытия сделки'))->getResponseMessageForJson();
		}

		return null;
	}

	private function getCurrentUser(): User
	{
		return $this->userResolver->getUser();
	}
}