<?php


	namespace App\Http\Controllers\Platform\Order;


	use App\Events\Order\Show\OrderViewedEvent;
	use App\Http\Controllers\Platform\AbstractPlatformController;
	use App\Models\Order\Order;
	use App\Models\Order\OrderBid;
	use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
	use App\Platform\Cabinet\Display\UserLastActivity;
	use App\Platform\Order\Containers\ReviewsDataContainer;
	use App\Platform\Order\Containers\Show\DealPermissionsContainer;
	use App\Platform\Order\Services\IsUserCanViewOrderService;
	use App\Platform\Order\Services\Performer\IsUserSelectedAsPerformerInOrder;
	use App\Platform\User\AuthenticatedUserResolver;
	use App\Platform\User\AuthenticatedUserType;
	use App\Platform\User\Type\UserTypeHelper;
	use App\Repositories\Order\OrderBidRepository;
	use App\Repositories\Order\OrderRepository;

	class OrderShowController extends AbstractPlatformController
	{

		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var AuthenticatedUserResolver
		 */
		private $userResolver;
		/**
		 * @var IsUserCanViewOrderService
		 */
		private $viewOrderService;
		/**
		 * @var OrderBidRepository
		 */
		private $orderBidRepository;
		/**
		 * @var AuthenticatedUserType
		 */
		private $authenticatedUserType;

		public function __construct(
			OrderRepository $orderRepository,
			AuthenticatedUserResolver $userResolver,
			IsUserCanViewOrderService $viewOrderService,
			OrderBidRepository $orderBidRepository,
			AuthenticatedUserType $authenticatedUserType
		)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->userResolver = $userResolver;
			$this->viewOrderService = $viewOrderService;
			$this->orderBidRepository = $orderBidRepository;
			$this->authenticatedUserType = $authenticatedUserType;
		}

		public function show($slug)
		{
			/** @var  $order Order */
			$order = $this->orderRepository->addPublicCriteriaToQuery()->findOneByField('slug', $slug);
			if (!$order) {
				abort(404);
			}
			$order->load('customer', 'performer');
			$container = $order->getContainer();
			$this->setTitle($container->getTitle());
			return $this->showDeal($order);
		}

		protected function showDeal(Order $order)
		{
			$container = $order->getContainer();
			$currentUser = $this->userResolver->getUser();
			$container->setCanViewOrder($this->viewOrderService->check($currentUser, $order));
			$customer = app(DisplayCustomerContainer::class, ['user' => $container->getCustomer()]);
			$permissions = app(DealPermissionsContainer::class, ['performer' => $currentUser, 'order' => $order, 'customer' => $container->getCustomer()]);
			$userLastActivity = app(UserLastActivity::class, ['user' => $container->getCustomer()]);
			event(app(OrderViewedEvent::class, compact('order', 'currentUser')));
			if ($currentUser && !$permissions->isUserOwnerDeal() && $permissions->isHasBid()) {
				$performerBid = $this->orderBidRepository->getByOrderAndUser($order, $currentUser)->getContainer();
			}
			if ($container->isClosed()) {
				$reviewsContainer = app(ReviewsDataContainer::class, compact('order'));
			}

			$bids = $this->orderBidRepository->getListPublic($order)
				->load('user')
				->transform(function (OrderBid $orderBid) use ($order) {
					$flag = app(IsUserSelectedAsPerformerInOrder::class)->check($orderBid->getContainer()->getPerformer(), $order);
					return $orderBid->getContainer()->setChoosedAsPerformer($flag);
				})
			;
			if ($currentUser && !UserTypeHelper::isCustomer($this->authenticatedUserType->getUserType())) {
				$otherOrders = $this->orderRepository->getRecommendDeals($order);
			}

			$with = compact(array_keys(get_defined_vars()));
			$data['content'] = view('public.deals.show.on-deal-opened')->with($with);
			return $this->main($data);
		}

	}