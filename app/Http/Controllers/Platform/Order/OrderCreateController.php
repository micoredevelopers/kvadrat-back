<?php

namespace App\Http\Controllers\Platform\Order;

use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderCreatedEvent;
use App\Events\Order\OrderCreateFilesUploadedEvent;
use App\Events\Order\OrderPublished;
use App\Helpers\Debug\LoggerHelper;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Http\Requests\Order\OrderCreateRequest;
use App\Models\Category\Category;
use App\Models\Category\CategoryLang;
use App\Platform\Order\Helpers\TempOrderHelper;
use App\Platform\Order\Services\CreateOrderCategoriesService;
use App\Platform\Order\Uploads\OrderCreateFilesStoreJob;
use App\Platform\Route\PlatformRoutes;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderCreateController extends AbstractPlatformController
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CityRepository
     */
    private $cityRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        OrderRepository    $orderRepository,
        CityRepository     $cityRepository
    )
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
        $this->orderRepository = $orderRepository;
        $this->cityRepository = $cityRepository;
    }

    public function create(
    	Request $request,
		AuthenticatedUserResolver $userResolver,
		CreateOrderCategoriesService $orderCategoriesService
	)
    {
        $user = $userResolver->getUser();
        $this->setTitle(getTranslate('order.create', 'Создание сделки'));
        $displayTip = !isLocalEnv();
        $orderCategoriesData = $orderCategoriesService->generateCreateOrderCategoriesData();
        $cities = $this->cityRepository->getListSelect();
        $title = $request->has('title') ? $request->get('title') : getTestField('title');
        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('public.deals.create.create')->with($with);
        return $this->main($data);
    }

    public function store(
        OrderCreateRequest        $request,
        TempOrderHelper           $tempOrderHelper,
        AuthenticatedUserResolver $userResolver
    )
    {
        $redirectTo = url()->previous() ?: route(PlatformRoutes::ORDER_CREATE);
        $input = $request->only($request->getFillableFields('files'));
        if ($user = $userResolver->getUser()) {
            $input['customer_id'] = $user->getKey();
        }
        try {
            DB::beginTransaction();
            if ($order = $this->orderRepository->create($input)) {
                event(app(OrderCreatedEvent::class, compact('order')));
                $files = $request->file('files');
                if ($files) {
                    $orderUploads = app(OrderCreateFilesStoreJob::class, compact('files', 'order'))->run()->getOrderUploads();
                    event(app(OrderCreateFilesUploadedEvent::class, compact('order', 'orderUploads', 'files')));
                }
                $redirectTo = route(PlatformRoutes::ORDER_SHOW, $order->getSlug());
                $this->setSuccessMessage(getTranslate('order.create.success', 'Сделка успешно создана'));
            }
            if ($user) {
                $this->setResponseData(['redirect' => $redirectTo]);
                event(app(OrderBeforePublished::class, compact('order')));
                event(app(OrderPublished::class, compact('order')));
            } else {
                $tempOrderHelper->setUnfinishedOrder($order->getKey());
                $this->setResponseData(['authenticate' => true]);
                $this->setSuccessMessage(getTranslate('order.create.need-auth', 'Ваша сделка будет опубликована после регистрации(или авторизации если вы уже зарегистрированы)'));
            }
            DB::commit();
        } catch (\Throwable $e) {
            app(LoggerHelper::class)->error($e);
            throwIfDev($e);
            $this->setFailMessage(getTranslate('order.create.fail', 'Ошибка при создании сделки'));
            DB::rollBack();
        }

        return $request->expectsJson() ? $this->getResponseMessageForJson() : redirect($redirectTo);
    }

}
