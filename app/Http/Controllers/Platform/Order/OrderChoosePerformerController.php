<?php

namespace App\Http\Controllers\Platform\Order;

use App\Events\Order\Show\Bids\BeforeChoosePerformer;
use App\Events\Order\Show\Bids\ChoosedPerformer;
use App\Http\Controllers\Platform\AbstractPlatformController;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Order\Containers\Show\DealPermissionsContainer;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\UserRepository;

class OrderChoosePerformerController extends AbstractPlatformController
{
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var UserRepository
	 */
	private $userRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;

	public function __construct(
		OrderRepository $orderRepository,
		AuthenticatedUserResolver $userResolver,
		UserRepository $userRepository,
		OrderBidRepository $orderBidRepository
	)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
		$this->userResolver = $userResolver;
		$this->userRepository = $userRepository;
		$this->orderBidRepository = $orderBidRepository;
	}

	public function choose($orderId, $performerId)
	{
		/** @var  $order Order */
		$order = $this->orderRepository->findForActions($orderId);
		if (!$order) {
			return $this->setFailMessage(getTranslate('registration.deal.not.found', 'Сделка не найдена'))->getResponseMessageForJson();
		}
		/** @var  $currentUser User */
		$currentUser = $this->userResolver->getUser();
		/** @var  $performer User */
		try {
			$performer = $this->userRepository->addPublicCriteriaToQuery()->find($performerId);
			$performerName = app(DisplayPerformerContainer::class, ['user' => $performer])->getFullName();
		} catch (\Throwable $e) {
			return $this->setFailMessage(getTranslate('registration.not.found.performer', 'Упс, нам не удалось найти такого исполнителя'))->getResponseMessageForJson();
		}
		//todo check is selectable bid belongs to current deal

		if (!$this->orderBidRepository->getByOrderAndUser($order, $performer)) {
			return $this->setFailMessage(translateFormat('registration.not.select.by.performer', ['%performerName%' => $performerName, ]))->getResponseMessageForJson();
		}
		if ($order->getContainer()->hasPerformer()) {
			return $this->setFailMessage(translateFormat('registration.selected.performer', ['%performerName%' => $performerName, ]))->getResponseMessageForJson();
		}
		$permissions = app(DealPermissionsContainer::class,
			['performer' => $performer, 'order' => $order, 'customer' => $order->getContainer()->getCustomer()]);
		if (!$permissions->isUserOwnerDeal($currentUser)) {
			return $this->setFailMessage(getTranslate('registration.can.select.author', 'Выбрать исполнителя может только автор'))->getResponseMessageForJson();
		}
		if ($order->getContainer()->isClosed()) {
			return $this->setFailMessage(getTranslate('registration.not.select.performer.close.deal', 'Нельзя выбрать исполнителя при закрытой сделке'))->getResponseMessageForJson();
		}
		$this->setFailMessage(getTranslate('registration.error.not.select.performer', 'Произлошла ошибка, нам не удалось выбрать исполнителя'));
		$order->getSetter()->setPerformerId($performer->getKey());
		event(app(BeforeChoosePerformer::class, compact('order', 'performer')));
		if ($this->orderRepository->update([], $order)) {
			event(app(ChoosedPerformer::class, compact('order', 'performer')));
			$this->setSuccessMessage(translateFormat('registration.selected.now.performer', ['%performerName%' => $performerName, ]));
		}
		return $this->getResponseMessageForJson();
	}
}
