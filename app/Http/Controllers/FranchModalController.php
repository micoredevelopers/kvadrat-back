<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\FranchModalRequest;
use App\Mail\FeedbackMailer;
use App\Models\FranchModal;
use App\Models\TrialModal;


class FranchModalController extends SiteController
{

    /**
     * @var FranchModal
     */
    private $franchModal;
    /**
     * @var \Request
     */
    private $request;

    /**
     * FranchModalController constructor.
     * @param FranchModal $franchModal
     * @param FranchModalRequest $request
     */
    public function __construct(FranchModal $franchModal, FranchModalRequest $request)
    {
        parent::__construct();

        $this->franchModal = $franchModal;
        $this->request = $request;
    }


    public function saveModal()
    {
        $franch = $this->franchModal;

        $franch->setName($this->request->name);
        $franch->setPhone($this->request->phone);

        $franch->save();


        try {
            \Mail::queue(new FeedbackMailer([
                'name' => $this->request->get('name'),
                'phone' => $this->request->get('phone'),
                'franch' => 'ФРАНШИЗА',
            ]));
        } catch (\Throwable $e) {
            logger($e);
        }

        if ($this->request->expectsJson()) {
            return $this->setSuccessMessage('Success send')->getResponseMessageForJson();
        }
        return back()->with($this->getMessage());
    }

}
