<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\DataContainers\Category\SearchCategoryRequestData;
use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\ShowSubcategoriesRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{

	/**
	 * @var CategoryRepository
	 */
	private $repository;

	public function __construct(CategoryRepository $repository)
	{
		$this->repository = $repository;
	}

	public function categories()
	{
		$categories = $this->repository->getTreePublic();
		return CategoryResource::collection($categories);
	}

	public function category(Category $category)
	{
		return CategoryResource::make(
			$this->repository->find($category->getKey())
		);
	}

	public function subcategories(ShowSubcategoriesRequest $request)
	{
		$category = $this->repository->find($request->get('category'));

		return CategoryResource::collection(
			$this->repository->getSubCategories($category, SearchCategoryRequestData::createForPublic())
		);
	}

}
