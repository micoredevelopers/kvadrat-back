<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Repositories\CityRepository;

class CityController extends Controller
{
	/**
	 * @var CityRepository
	 */
	private $cityRepository;

	public function __construct(CityRepository $cityRepository)
	{
		$this->cityRepository = $cityRepository;
	}

	public function cities()
	{
		$cities = $this->cityRepository->getListPublic();
		return CityResource::collection($cities);
	}

}
