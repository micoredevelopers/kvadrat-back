<?php

namespace App\Http\Controllers;

use App\Repositories\Order\OrderRepository;

class HomeController extends SiteController
{

	public function index()
	{
		$this->setTitle(getTranslate('main.main.title', 'Квадрат'));

		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('public.home.home')->with($with);
		return $this->main($data);
	}



}
