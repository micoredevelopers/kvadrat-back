<?php


namespace App\Http\Controllers\Cabinet\Performer;

use App\Helpers\Media\ImageSaver;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Http\Controllers\Cabinet\AbstractPersonalDataController;
use App\Http\Requests\User\Cabinet\Personal\PersonalPerformerCompanyRequest;
use App\Http\Requests\User\Cabinet\Personal\PersonalPerformerPhysicalRequest;
use App\Http\Requests\User\Cabinet\Personal\SkillsPerformerRequest;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Display\UserMetaContainer;
use App\Platform\Cabinet\Service\PerformerTypeService;
use App\Platform\Cabinet\Uploads\Avatar\AvatarUploader;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\UserMetaRepository;
use Illuminate\Http\Request;

class PersonalDataController extends AbstractPersonalDataController
{
	/**
	 * @var CityRepository
	 */
	private $cityRepository;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	public function __construct(CityRepository $cityRepository, UserMetaRepository $userMetaRepository)
	{
		parent::__construct();
		$this->cityRepository = $cityRepository;
		$this->userMetaRepository = $userMetaRepository;
	}

	public function index(CategoryRepository $categoryRepository)
	{
		$this->setTitle(getTranslate('messagies.personal.data.performer', 'Личные данные исполнителя'));
		$user = $this->getUser();
		$categories = $categoryRepository->getTreePublic();
		$cities = $this->cityRepository->getListPublic();
		$displayTip = !($this->userMetaRepository->getByUserAndKey($user, UserMetaActions::PERFORMER_TIP_WATCHED));
		$userSocials = app(UserHasSocialsContract::class, ['model' => $user]);
		$userContainer = $performerContainer = app(DisplayPerformerContainer::class, ['user' => $user]);
		$userMeta = app(UserMetaContainer::class, ['userContainer' => $performerContainer]);
		$deleteAvatarUrl = storageFileExists($user->getAvatar()) ? route(PlatformRoutes::CABINET_PERFORMER_DELETE_AVATAR) : null;
		$with = compact(array_keys(get_defined_vars()));

		$data['content'] = view('public.cabinet.performer.personal.personal-data')->with($with);
		return $this->main($data);
	}

	public function submit()
	{
		$user = $this->getUser();
		$this->setSuccessMessage(getTranslate('messagies.data.updated.succes', 'Данные успешно обновлены'));
		$performerTypeService = app(PerformerTypeService::class, ['user' => $user]);
		$request = app($performerTypeService->isCompany() ? PersonalPerformerCompanyRequest::class : PersonalPerformerPhysicalRequest::class);
		$input = $request->only($request->getFillableFields());

		if ($request->hasFile('avatar')) {
			$input['avatar'] = app(AvatarUploader::class, ['file' => $request->file('avatar')])->uploadPerformer();
			$this->setResponseData(['avatar' => getPathToImage($input['avatar'])]);
		}
		if ($input) {
			$this->repository->update($input, $user);
		}
		return $request->expectsJson() ? $this->getResponseMessageForJson() : back()->with($this->getResponseMessage());
	}

	public function tipWatched()
	{
		$user = $this->getUser();
		if ($this->userMetaRepository->getByUserAndKey($user, UserMetaActions::PERFORMER_TIP_WATCHED)) {
			return false;
		}
		$this->userMetaRepository->getMetaBuilder()->setUser($user)->setKey(UserMetaActions::PERFORMER_TIP_WATCHED)->setValue(1)->build();
		return true;
	}

	public function avatar()
	{
		$user = $this->getUser();
		return $this->baseAvatarDelete(UserTypeContract::TYPE_PERFORMER, $user);
	}

	public function profileToggle()
	{
		return $this->baseProfileToggle(UserTypeContract::TYPE_PERFORMER);
	}

	public function skills(SkillsPerformerRequest $request)
	{
		$user = $this->getUser();
		$categories = array_values($request->get('skills', []));
		$categories ? $user->skills()->sync($categories) : $user->skills()->detach();
		$this->setSuccessMessage(getTranslate('messagies.list.skills.updated', 'Список навыков обновлен'));
		return $this->getResponseMessageForJson();
	}
}