<?php


namespace App\Http\Controllers\Cabinet\Performer;

use App\Http\Controllers\Cabinet\AbstractCabinetController;
use App\Http\Requests\User\Cabinet\Portfolio\PerformerPortfolioRequest;
use App\Models\User\Portfolio;
use App\Platform\Cabinet\Uploads\PerformerPortfolioImagesDeleter;
use App\Repositories\CityRepository;
use App\Repositories\PerformerSkillsRepository;
use App\Repositories\PortfolioRepository;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Uploads\PerformerPortfolioImagesUploader;
use Illuminate\Http\Client\Request;

class PortfolioController extends AbstractCabinetController
{
    /**
     * @var CityRepository
     */
    private $cityRepository;
    /**
     * @var PortfolioRepository
     */
    private $portfolioRepository;

    public function __construct(CityRepository $cityRepository, PortfolioRepository $portfolioRepository)
    {
        parent::__construct();
        $this->cityRepository = $cityRepository;
        $this->portfolioRepository = $portfolioRepository;
    }

    public function index(PerformerSkillsRepository $skillsRepository)
    {
        $this->setTitle(getTranslate('forms.portfolio-title', 'Портфолио'));
        $user = $this->getUser();
        $skillsCategories = $skillsRepository->getTreePublic($user);
        $cities = $this->cityRepository->getListPublic();
        $portfolios = $this->portfolioRepository->getByUser($user);
        $performerContainer = app(DisplayPerformerContainer::class, ['user' => $user]);
        $with = compact(array_keys(get_defined_vars()));

        $data['content'] = view('public.cabinet.performer.personal.portfolio.performer-portfolio')->with($with);
        return $this->main($data);
    }

    public function sendForm(PerformerPortfolioRequest $request)
    {
        $user = $this->getUser();
        $this->setFailMessage(getTranslate('messagies.portfolio.not.added', 'Портфолио не было добавлено, обратитесь к администратору'));
        $input = $request->only($request->getFillableFields());

        if ($input) {
            $this->setSuccessMessage(getTranslate('messagies.portfolio.added', 'Портфолио добавлено'));
            /** @var  $portfolio Portfolio */
            $portfolio = $this->portfolioRepository->createRelated($input, $user);
            if ($request->hasFile('images')) {
                $uploader = app(PerformerPortfolioImagesUploader::class, ['uploadedFiles' => $request->file('images'),]);
                foreach ($uploader->upload() as $path) {
                    $portfolio->images()->create(['image' => $path]);
                }
            }
        }
        return $request->expectsJson() ? $this->getResponseMessageForJson() : back()->with($this->getResponseMessage());
    }

    /**
     * @param $portfoliosId
     */
    public function deletePortfolio($portfoliosId)
    {
        $this->setSuccessMessage('success');

        /** @var  $portfolio Portfolio */
        $userId = $this->getUser()->getKey();
        $portfolio = $this->portfolioRepository->findByUser($userId, $portfoliosId);
        PerformerPortfolioImagesDeleter::deleteImgCollection($portfolio->getImages());
        $portfolio->delete();
        return $this->getResponseMessageForJson();
    }

    /**
     * @param $portfoliosId
     * @param $imageId
     * @param $userId
     * @throws \Exception
     */
    public function delImgInPortfolio($portfoliosId, $imageId, $userId)
    {
        /** @var  $portfolio Portfolio */
        $portfolio = $this->portfolioRepository->findByUser($userId, $portfoliosId);
        $portfolio->images()->where('id', $imageId)->first()->delete();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $portfoliosId
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function sendEditingForm(\Illuminate\Http\Request $request, $portfoliosId)
    {
        $userId = $this->getUser()->getKey();
        $this->setFailMessage(getTranslate('messagies.error.portfolio.to.admin', 'Портфолио не было редактированно, обратитесь к администратору'));
        $input = $request->input();

        if ($input) {
            $this->setSuccessMessage(getTranslate('messagies.portfolio.editing', 'Портфолио измененно'));
            /** @var  $portfolio Portfolio */

            foreach ($input['deletedImages'] as $delImg) {
                $this->delImgInPortfolio($portfoliosId, $delImg,$userId);
            }

            $portfolio = $this->portfolioRepository->findByUser($userId, $portfoliosId);
            $portfolio->update($request->only(['category_id', 'description']));

            if ($request->hasFile('images')) {
                $uploader = app(PerformerPortfolioImagesUploader::class, ['uploadedFiles' => $request->file('images'),]);
                foreach ($uploader->upload() as $path) {
                    $portfolio->images()->create(['image' => $path]);
                }
            }
        }
        return $request->expectsJson() ? $this->getResponseMessageForJson() : back()->with($this->getResponseMessage());
    }
}