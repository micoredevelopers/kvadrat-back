<?php
namespace App\Http\Controllers\Cabinet\Performer;

use App\Http\Controllers\Cabinet\AbstractCabinetController;
use App\Repositories\PerformerSkillsRepository;
use App\Repositories\ReviewRepository;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Reviews\ReviewDisplayContainer;

class ReviewsController extends AbstractCabinetController
{
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(ReviewRepository $reviewRepository)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
	}

	public function index(PerformerSkillsRepository $skillsRepository)
	{
		$this->setTitle(getTranslate('cabinet.comments', 'Отзывы'));
		$user = $this->getUser();
		$reviews = $this->reviewRepository->getAboutPerformer($user);
		$reviews->load('reviewFrom', 'order', 'images');
		$reviews = $reviews->map(function ($review){
			return app(ReviewDisplayContainer::class, compact('review'));
		});
		$skillsCategories = $skillsRepository->getTreePublic($user);
		$performerContainer = app(DisplayPerformerContainer::class, ['user' => $user]);
		$with = compact(array_keys(get_defined_vars()));

		$data['content'] = view('public.cabinet.performer.personal.reviews.performer-reviews')->with($with);
		return $this->main($data);
	}


}