<?php


namespace App\Http\Controllers\Cabinet;


use App\Events\Platform\Cabinet\UserTypeSwitched;
use App\Platform\Cabinet\Display\AuthenticatedUserTypeContainerResolver;
use App\Platform\Cabinet\Route\SwitchUserType;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\Type\UserTypeHelper;

class SwitchUserTypeController extends AbstractCabinetController
{
	/**
	 * @var AuthenticatedUserType
	 */
	private $userType;
	/**
	 * @var AuthenticatedUserTypeContainerResolver
	 */
	private $userResolver;
	/**
	 * @var SwitchUserType
	 */
	private $switchUserType;

	public function __construct(
		AuthenticatedUserType $userType,
		AuthenticatedUserTypeContainerResolver $userResolver,
		SwitchUserType $switchUserType
	)
	{
		parent::__construct();
		$this->userType = $userType;
		$this->userResolver = $userResolver;
		$this->switchUserType = $switchUserType;
	}

	public function switch()
	{
		$currentType = $this->userType->getUserType();
		$user = $this->userResolver->create()->getUser();
		$newType = UserTypeHelper::revert($currentType);
		$this->userType->setUserType($newType)->setSettedBy($this);
		event(new UserTypeSwitched($newType, $user));
		$redirectTo = $this->switchUserType->getSwitchLink($currentType, $newType);
		return redirect($redirectTo ?: url()->previous());
	}
}