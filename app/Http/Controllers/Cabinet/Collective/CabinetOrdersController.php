<?php


	namespace App\Http\Controllers\Cabinet\Collective;


	use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
	use App\Http\Controllers\Cabinet\AbstractCabinetController;
	use App\Models\Order\Order;
	use App\Platform\Query\OnPageHelper;
	use App\Platform\Query\PaginationOnPage;
	use App\Platform\User\AuthenticatedUserType;
	use App\Platform\User\Type\UserTypeHelper;
	use App\Repositories\Order\OrderRepository;
	use Illuminate\Http\Request;

	class CabinetOrdersController extends AbstractCabinetController
	{
		/**
		 * @var OrderRepository
		 */
		protected $orderRepository;
		/**
		 * @var AuthenticatedUserType
		 */
		private $authenticatedUserType;

		public function __construct(AuthenticatedUserType $authenticatedUserType)
		{
			parent::__construct();
			$this->orderRepository = app(OrderRepository::class);
			$this->authenticatedUserType = $authenticatedUserType;
		}

		public function orders(Request $request)
		{
			$this->setTitle(getTranslate('header.my-deals', 'Мои заказы'));
			$searchContainer = $this->makeSearchContainerFromRequest($request);
			$search = $searchContainer->getSearch();
			$this->loadOnpage($searchContainer);
			$user = $this->getUser();
			$load = ['city'];
			($isPerformer = UserTypeHelper::isPerformer($this->authenticatedUserType->getUserType()))
				? $searchContainer->setPerformerId($user->getKey())
				: $searchContainer->setCustomerId($user->getKey());
			$searchContainer->setWhereClosed(false)->setSearch('');
			$activeOrders = $this->orderRepository->getOrdersCabinet($searchContainer);
			$activeOrders->load($load);
			//
			$searchContainer->setWhereClosed(true)->setPaginate(true)->setSearch($search);
			$closedOrders = $this->orderRepository->getOrdersCabinet($searchContainer);
			$closedOrders->load($load);
			$with = compact(array_keys(get_defined_vars()));
			$data['content'] = view('public.cabinet.collective.my-orders')->with($with);
			return $this->main($data);
		}

		private function loadOnpage($container)
		{
			$paginationOnPage = app(PaginationOnPage::class);
			$onPageItems = app(OnPageHelper::class, ['items' => $paginationOnPage->getItems(), 'activeItem' => $container->getOnPage()]);
			view()->share(compact('onPageItems'));
		}


		protected function makeSearchContainerFromRequest(Request $request): SearchCabinetOrdersContainer
		{
			return app(SearchCabinetOrdersContainer::class)->fillFromRequest($request);
		}
	}