<?php


namespace App\Http\Controllers\Cabinet\Customer;

use App\Helpers\User\Model\UserHasSocialsContract;
use App\Http\Controllers\Cabinet\AbstractPersonalDataController;
use App\Http\Requests\User\Cabinet\Personal\PersonalPerformerPhysicalRequest;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\UserMetaContainer;
use App\Platform\Cabinet\Uploads\Avatar\AvatarUploader;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Model\UserFields;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\CityRepository;
use App\Repositories\UserMetaRepository;

class PersonalDataController extends AbstractPersonalDataController
{
	/**
	 * @var CityRepository
	 */
	private $cityRepository;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	public function __construct(CityRepository $cityRepository, UserMetaRepository $userMetaRepository)
	{
		parent::__construct();
		$this->cityRepository = $cityRepository;
		$this->userMetaRepository = $userMetaRepository;
	}

	public function index()
	{
		$this->setTitle(getTranslate('messagies.personal.data.customer', 'Личные данные заказчика'));
		$customer = $this->getUser();
		$cities = $this->cityRepository->getListPublic();
		$displayTip = !($this->userMetaRepository->getByUserAndKey($customer, UserMetaActions::CUSTOMER_TIP_WATCHED));
		$userSocials = app(UserHasSocialsContract::class, ['model' => $customer]);
		$userContainer = $customerContainer = app(DisplayCustomerContainer::class, ['user' => $customer]);
		$userMeta = app(UserMetaContainer::class, ['userContainer' => $customerContainer]);
		$deleteAvatarUrl = storageFileExists($customer->getAvatar(UserFields::getCustomer('avatar'))) ? route(PlatformRoutes::CABINET_CUSTOMER_DELETE_AVATAR) : null;
		$with = compact(array_keys(get_defined_vars()));

		$data['content'] = view('public.cabinet.customer.personal-data')->with($with);
		return $this->main($data);
	}

	public function submit(PersonalPerformerPhysicalRequest $request)
	{
		$user = $this->getUser();
		$this->setSuccessMessage(getTranslate('messagies.data.updated.succes', 'Данные успешно обновлены'));
		$input = $request->only($request->getFillableFields());
		foreach ($input as $key => $item) {
			$newKey = UserFields::getCustomer($key);
			$input[$newKey] = $item;
			if ($newKey !== $key) {
				unset($input[$key]);
			}
		}
		if ($request->hasFile('avatar')) {
			$field = UserFields::getCustomer('avatar');
			$input[$field] = app(AvatarUploader::class, ['file' => $request->file('avatar')])->uploadCustomer();
			$this->setResponseData(['avatar' => getPathToImage($input[$field])]);
		}
		if ($input) {
			$this->repository->update($input, $user);
		}
		return $request->expectsJson() ? $this->getResponseMessageForJson() : back()->with($this->getResponseMessage());
	}

	public function tipWatched()
	{
		$user = $this->getUser();
		if ($this->userMetaRepository->getByUserAndKey($user, UserMetaActions::CUSTOMER_TIP_WATCHED)) {
			return false;
		}
		$this->userMetaRepository->getMetaBuilder()->setUser($user)->setKey(UserMetaActions::CUSTOMER_TIP_WATCHED)->setValue(1)->build();
		return true;
	}

	public function avatar()
	{
		$user = $this->getUser();
		return $this->baseAvatarDelete(UserTypeContract::TYPE_CUSTOMER, $user);
	}


	public function profileToggle()
	{
		return $this->baseProfileToggle(UserTypeContract::TYPE_CUSTOMER);
	}

}