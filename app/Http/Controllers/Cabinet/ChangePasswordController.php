<?php


namespace App\Http\Controllers\Cabinet;

use App\Http\Requests\User\Cabinet\ChangePasswordRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends AbstractCabinetController
{

	public function index()
	{
		$this->setTitle(getTranslate('auth.edit.password', 'Изменение пароля'));
		$user = $this->getUser();
		$with = compact('user');
		$data['content'] = view('public.cabinet.shared.change-password')->with($with);
		return $this->main($data);
	}

	public function submit(ChangePasswordRequest $request)
	{
		$user = $this->getUser();
		$this->setFailMessage(getTranslate('users.current-password-invalid'));
		$passwordsMatches = \Hash::check($request->get('password'), $user->password);
		if ($passwordsMatches) {
			$newPassword = Hash::make($request->get('password_new'));
			$this->repository->update(['password' => $newPassword], $user->id);
			Auth::login($user);
			$this->setSuccessMessage(getTranslate('users.credentials-updated'));
		} else if ($request->expectsJson()) {
			return \Illuminate\Support\Facades\Response::json(
				['errors' => ['password' => [getTranslate('users.current-password-invalid')]]],
				Response::HTTP_UNPROCESSABLE_ENTITY
			);
		}
		if ($request->expectsJson()) {
			$this->setResponseData(['redirect' => route('home') .'#login']);
			return $this->getResponseMessageForJson();
		}

		return redirect()->back()->with($this->getResponseMessage());
	}
}