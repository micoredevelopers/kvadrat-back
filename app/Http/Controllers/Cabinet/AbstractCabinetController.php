<?php


	namespace App\Http\Controllers\Cabinet;

	use App\Http\Controllers\SiteController;
	use App\Repositories\UserRepository;
	use App\Models\User;
	use Illuminate\Http\Request;

	class AbstractCabinetController extends SiteController
	{
		/**
		 * @var UserRepository
		 */
		protected $repository;

		public function __construct()
		{
			parent::__construct();
			$this->repository = app(UserRepository::class);
		}

		public function getUser(): User
		{
			return $user = $this->repository->find(\Auth::id());
		}
	}