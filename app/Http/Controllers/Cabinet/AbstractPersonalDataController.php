<?php


namespace App\Http\Controllers\Cabinet;

use App\Models\User;
use App\Platform\Cabinet\Display\DisabledProfileText;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\DisplayUserTypeContainerContract;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Display\UserMetaContainer;
use App\Platform\Cabinet\Uploads\Avatar\AvatarDeleter;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Model\UserFields;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\UserMetaRepository;
use Illuminate\Http\Response;

abstract class AbstractPersonalDataController extends AbstractCabinetController
{
	protected function baseProfileToggle($type): array
	{
		$disabledProfileText = app(DisabledProfileText::class);
		$userMetaRepository = app(UserMetaRepository::class);
		$user = $this->getUser();
		$userContainer = $this->getUserContainer($type, $user);

		$this->repository->update([UserFields::get($type, 'profile_enabled') => (!$userContainer->isProfileEnabled())], $user);
		$this->setSuccessMessage($disabledProfileText->getProfileIsActive($userContainer->isProfileEnabled()));
		$userMeta = app(UserMetaContainer::class, ['userContainer' => $userContainer]);
		if (!$userContainer->isProfileEnabled()) {
			if (!$meta = $userMetaRepository->getByUserAndKey($user, $this->getMetaKey($type))) {
				$userMetaRepository->getMetaBuilder()->setUser($user)->setKey($this->getMetaKey($type))->setValue(now())->build();
			} else {
				$meta->setValue(now());
				$userMetaRepository->update([], $meta);
			}
			$this->setResponseData([
				'disabled_at' => $userMeta->getProfileDisabledAtText(),
			]);
		}
		$this->setResponseData([
			'title'     => $userMeta->getToggleTextHeadTitle(),
			'subtitle'  => $userMeta->getToggleTextConfirm(),
			'toggle'    => $userMeta->getToggleTextBtn(),
			'active'    => (int)$userContainer->isProfileEnabled(),
			'btnStatus' => $userContainer->isProfileEnabled() ? $userMeta->getToggleTextBtn() : $userMeta->getProfileDisabledAtText(),
		]);
		return $this->getResponseMessageForJson();
	}

	protected function baseAvatarDelete($type, User $user)
	{
		$field = UserFields::get($type, 'avatar');
		if (!AvatarDeleter::supports($user->getAvatar($field))) {
			$this->setFailMessage(getTranslate('messagies.not.photo.profile', 'Фото профиля отсутствует'));
		    return new Response($this->getResponseMessageJsonToErrorValidation('avatar'), 404);
		}
		AvatarDeleter::delete($user->getAvatar($field));
		$this->setSuccessMessage(getTranslate('messagies.profile.photo.added', 'Фото профиля было удалено'));
		$this->setResponseData(['avatar' => asset('images/ProfileDefault.svg')]);
		return $this->getResponseMessageForJson();
	}

	protected function getUserContainer($type, User $user): DisplayUserTypeContainerContract
	{
		return app((UserTypeHelper::isPerformer($type) ? DisplayPerformerContainer::class : DisplayCustomerContainer::class), ['user' => $user]);
	}

	protected function getMetaKey($type): string
	{
		return UserTypeHelper::isPerformer($type) ? UserMetaActions::PERFORMER_DISABLED_AT : UserMetaActions::CUSTOMER_DISABLED_AT;
	}
}