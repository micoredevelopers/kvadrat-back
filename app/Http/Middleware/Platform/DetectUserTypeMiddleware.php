<?php

namespace App\Http\Middleware\Platform;

use App\Platform\User\AuthenticatedUserResolver;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\FallbackUserTypeDetector;
use Closure;

class DetectUserTypeMiddleware
{
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;

	public function __construct(
		AuthenticatedUserResolver $userResolver,
		AuthenticatedUserType $authenticatedUserType
	)
	{
		$this->userResolver = $userResolver;
		$this->authenticatedUserType = $authenticatedUserType;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->supports()) {
			$fallbackUserTypeDetector = app(FallbackUserTypeDetector::class, ['user' => $this->userResolver->getUser()]);
			$userType = $fallbackUserTypeDetector->getUserType();
			if ($userType) {
				$this->authenticatedUserType->setUserType($userType)->setSettedBy($this);
			}
		}
		return $next($request);
	}

	private function supports(): bool
	{
		return (bool)$this->userResolver->getUser() && !$this->authenticatedUserType->getUserType();
	}
}
