<?php

namespace App\Http\Middleware\Platform;

use App\Events\Platform\User\Analytics\UserLastSeedUpdated;
use App\Models\User;
use App\Platform\Analytics\UserLastSeenAt;
use App\Platform\User\AuthenticatedUserResolver;
use Closure;
use Illuminate\Support\Carbon;

class LastSeen
{
	/**
	 * @var UserLastSeenAt
	 */
	private $lastSeen;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	public function __construct(UserLastSeenAt $lastSeen, AuthenticatedUserResolver $userResolver)
	{
		$this->lastSeen = $lastSeen;
		$this->userResolver = $userResolver;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (($user = $this->userResolver->getUser())) {
			$lastSeen = now();
			$this->lastSeen->setLastSeen($user->getKey(), $lastSeen);
			if ($this->needsToUpdateLastVisited($this->lastSeen->getLastSeen($user->getKey()), $user)){
				event(app(UserLastSeedUpdated::class, ['user' => $user, 'dateLastSeen' => $lastSeen,]));
			}
		}
		return $next($request);
	}

	private function needsToUpdateLastVisited(?Carbon $lastVisitDate, User $user): bool
	{
		if (!$lastVisitDate || !$user->getDateLastSeen()) {
			// no have cached last date, need update
			return true;
		}
		return $lastVisitDate->diffInMinutes($user->getDateLastSeen()) >= 10;
	}
}
