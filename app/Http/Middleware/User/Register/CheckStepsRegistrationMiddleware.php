<?php

	namespace App\Http\Middleware\User\Register;

	use App\Services\User\Registration\UserRegistrationSteps;
	use App\Services\User\Registration\UserRegistrationStepsRedirectsService;
	use App\Services\User\Registration\UserRegistrationStepsService;
	use Closure;
	use Illuminate\Http\Request;
	use Illuminate\Support\Str;

	class CheckStepsRegistrationMiddleware
	{
		private $registrationHelper;

		public function __construct(UserRegistrationStepsService $registrationHelper)
		{
			$this->registrationHelper = $registrationHelper;
		}

		/**
		 * Handle an incoming request.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param \Closure                 $next
		 * @return mixed
		 */
		public function handle($request, Closure $next)
		{
			if ($this->supports($request)) {
				debugInfo('redirect from ' . __CLASS__);
				return redirect(UserRegistrationStepsRedirectsService::getByStep($this->registrationHelper->getStep()));
			}
			return $next($request);
		}

		private function supports(Request $request): bool
		{
			$uri = url()->current();
			if (Str::startsWith($request->getPathInfo(), '/register')) {
				if ($request->isMethod('GET')) {
					return UserRegistrationStepsRedirectsService::getByStep($this->registrationHelper->getStep()) !== $uri;
				}
				if ($request->isMethod('POST')) {
					return !in_array($uri, UserRegistrationStepsRedirectsService::getSubmits(), true);
				}
			}
			return false;
		}

	}
