<?php

namespace App\Http\Middleware;

use App\Enum\CitiesEnums;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\URL;

class CityRouteParameterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle( Request $request, Closure $next)
    {
        /** @var  $enums CitiesEnums[]*/
        $enums = CitiesEnums::getEnums();
        $matched = false;
        $path = trim(parse_url(getNonLocaledUrl(), PHP_URL_PATH), '/');
        foreach ($enums as $enum) {
            if (Str::startsWith($path, $enum->getUrl())){
                $matched = true;
                break;
            }
        }
        if (!$matched){
            $to = sprintf('/%s/%s', $enums[0]->getUrl(), $path);
            return redirect(langUrl($to));
        }
        return $next($request);
    }
}
