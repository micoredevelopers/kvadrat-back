<?php

namespace App\Http\Middleware;

use App\Helpers\LanguageHelper;
use Closure;

class Language
{
	/**
	 * @param $request
	 * @param Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$languageList = \App\Models\Language::active(1)->get();
		if ($locale = getCurrentLocale()) {
			$language = $languageList->where('key', $locale)->first();
			if ($language) {
				LanguageHelper::setLanguageId((int)$language->id);
			}
		}
		if (!getCurrentLangId() && !($language = $languageList->where('default', 1)->first())) {
			abort(404);
		}
		$language = $languageList->where('id', getCurrentLangId())->first();
		LanguageHelper::setLanguageId((int)$language->id);

		return $next($request);
	}
}
