<?php

namespace App\Http\Middleware;

class AdminAuthenticated extends Authenticate
{
	protected function redirectTo($request): string
	{
		if (!$request->expectsJson()) {
			return route('admin.login');
		}
		abort(503);
		return '';
	}
}
