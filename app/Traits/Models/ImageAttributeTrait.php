<?php

namespace App\Traits\Models;


trait ImageAttributeTrait
{
	public function setImage($name)
	{
		if (is_string($name)) {
			$column = 'image';
			$this->attributes[$column] = $name;
		}
		return $this;
	}

	public function getImage()
	{
		$column = 'image';
		$name = \Arr::get($this->attributes, $column);
		return $name;
	}

}