<?php

	namespace App\Traits\Models\User;


	use App\Platform\Order\Model\UserSetter;

	trait UserHelpersTrait
	{
		private $setter;

		public function getSetter(): UserSetter
		{
			if (null === $this->setter) {
				$this->setter = app(UserSetter::class, ['user' => $this]);
			}
			return $this->setter;
		}
	}
