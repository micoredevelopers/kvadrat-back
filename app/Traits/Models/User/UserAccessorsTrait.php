<?php

namespace App\Traits\Models\User;

use App\Helpers\User\Model\UserHasSocialsContainer;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Models\Category\Category;
use App\Models\City;
use App\Models\User\Portfolio;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

trait UserAccessorsTrait
{

	public function getUserSocials(): UserHasSocialsContract
	{
		return app(UserHasSocialsContainer::class, ['model' => $this]);
	}

	/**
	 * @return Collection | Category[]
	 */
	public function getSkills(): Collection
	{
		return $this->skills;
	}

	/**
	 * @return Collection | Portfolio[]
	 */
	public function getPortfolios(): Collection
	{
		return $this->portfolios;
	}

	public function getPerformerCity(): ?City
	{
		return $this->city;
	}

	public function getCustomerCity(): ?City
	{
		return $this->city;
	}


}
