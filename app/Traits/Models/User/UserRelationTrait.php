<?php

namespace App\Traits\Models\User;


use App\Models\Category\Category;
use App\Models\City;
use App\Models\Order\Review;
use App\Models\User\Portfolio;
use App\Models\User\SocialProvider;
use App\Models\User\UserMeta;
use App\Platform\Contract\UserTypeContract;
use App\Traits\Models\Relations\DeletedByUserIdTrait;
use App\Traits\Models\Relations\ModifiedByUserIdTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserRelationTrait
{

	public function socialProviders(): HasMany
	{
		return $this->hasMany(SocialProvider::class);
	}

	public function metas(): HasMany
	{
		return $this->hasMany(UserMeta::class);
	}

	public function skills(): BelongsToMany
	{
		return $this->belongsToMany(Category::class, 'skills', null, 'category_id');
	}

	public function portfolios(): HasMany
	{
		return $this->hasMany(Portfolio::class);
	}

	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}

	public function reviewsPerformer(): HasMany
	{
		return $this->hasMany(Review::class, 'to_id')->where('about', UserTypeContract::TYPE_PERFORMER);
	}

	public function reviewsCustomer(): HasMany
	{
		return $this->hasMany(Review::class, 'to_id')->where('about', UserTypeContract::TYPE_CUSTOMER);
	}

}
