<?php

namespace App\Traits\Models\User;



use Illuminate\Support\Carbon;

trait UserSettersTrait
{
	public function setDateLastSeen(Carbon $dateTime): self
	{
		$this->setAttribute('last_seen_at', $dateTime->toDateTimeString());
		return $this;
	}
}
