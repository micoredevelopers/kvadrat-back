<?php

namespace App\Traits\Models;


use App\Contracts\HasLocalized;

trait MainOrLangExceptAttributeTrait
{

    /**
     * @param string $column
     * @return mixed|string
     */
    public function getExceptAttribute()
    {
    	$column = 'excerpt';
        return $this->getLangColumn($column);
    }

}