<?php



namespace App\Traits\Observers;
use App\Models\User;

trait AddModifiedBy
{

	private function addModifiedBy(\App\Contracts\Models\HasModifiedBy $modifiedModel, \Illuminate\Contracts\Auth\Authenticatable $authenticatable = null)
	{
		/** @var  $authUser User*/
		try{
			$authUser = $authenticatable ?? \Auth::guard('admin')->user();
			if ($authUser){
				if (!$modifiedModel->modifiable){
					$modifiedModel->modifiable()->create([$authUser->getForeignKey() => $authUser->getKey()]);
				} else{
					$modifiedModel->modifiable->fill([$authUser->getForeignKey() => $authUser->getKey()])->save();
				}
			}
		} catch (\Exception $e){

		}
	}

}