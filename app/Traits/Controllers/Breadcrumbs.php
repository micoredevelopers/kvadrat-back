<?php


namespace App\Traits\Controllers;

trait Breadcrumbs
{
    public function addBreadCrumb($breadName, $breadUrl = '')
    {
        \Breadcrumbs::addBreadCrumb($breadName, $breadUrl);

        return $this;
    }

    public function dropLastBreadCrumb()
    {
        \Breadcrumbs::dropLastBreadCrumb();
    }

    public function dropAllBreadCrumbs()
    {
        \Breadcrumbs::dropAllBreadCrumbs();
    }

    /**
     * @param bool $name
     * @return mixed
     */
    public function getBreadCrumbs($name = false)
    {
        return \Breadcrumbs::getBreadCrumbs($name);
    }
}