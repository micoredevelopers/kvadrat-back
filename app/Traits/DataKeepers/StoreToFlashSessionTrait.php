<?php


namespace App\Traits\DataKeepers;


trait StoreToFlashSessionTrait
{
	use StoreToSessionTrait;

	private function storeData(string $key, $data): void
	{
		$prefix = $this->getPrefix();
		session()->flash($prefix . $key, $data);
	}
}