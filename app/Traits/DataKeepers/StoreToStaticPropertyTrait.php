<?php


namespace App\Traits\DataKeepers;


use Illuminate\Support\Arr;

trait StoreToStaticPropertyTrait
{

	protected static $__dataStatic;
	private function storeData(string $key, $data): void
	{
		self::$__dataStatic[$this->getPrefix($key)] = $data;
	}

	private function getData(string $key)
	{
		return  self::$__dataStatic[$this->getPrefix($key)] ?? '';
	}

	private function getPrefix($key = ''): string
	{
		return ($this->prefix ?? false) ? ($this->prefix ?? '') . '.' . $key : '';
	}
}