<?php

namespace App\Traits;


use App\Contracts\HasLocalized;
use App\Helpers\Debug\LoggerHelper;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

trait EloquentExtend
{

	protected static $schema = [];

	public function getTableColumns()
	{
		if (!Arr::has(static::$schema, $this->getTable())) {
			$columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
			Arr::set(static::$schema, $this->getTable(), $columns);
		}
		return Arr::get(static::$schema, $this->getTable());
	}

	public static function findByUrl(string $url): ?self
	{
		return self::whereUrl($url)->first();
	}

	public function getLangColumn(string $column)
	{
		try {
			if (classImplementsInterface($this, HasLocalized::class) && $this->lang) {
				return $this->lang->{$column} ?? '';
			}
			if (Arr::has($this->attributes, $column)) {
				return Arr::get($this->attributes, $column);
			}
		} catch (\Exception $e) {
			app(LoggerHelper::class)->error($e);
		}
		return '';
	}

	public function getCreatedAt(): ?Carbon
	{
		return ($date = $this->getAttribute($this->getCreatedAtColumn())) ? getDateCarbon($date) : null;
	}

	public function getUpdatedAt(): ?Carbon
	{
		return ($date = $this->getAttribute($this->getUpdatedAtColumn())) ? getDateCarbon($date) : null;
	}

	protected function serializeDate(\DateTimeInterface $date)
	{
		return $date->format('Y-m-d H:i:s');
	}

	public function setBelongsTo(Model $model): self
	{
		$this->setAttribute($model->getForeignKey(), $model->getKey());
		return $this;
	}

	public function getBelongsKey($relation)
	{
		return $this->getAttribute($this->{$relation}()->getForeignKeyName());
	}

	/**
	 * @return int
	 */
	public function getKey()
	{
		return (int)parent::getKey();
	}

	/**
	 * @param $id
	 * @return Model
	 */
	public static function createReference($id)
	{
		$self = new static();
		$self->{$self->getKeyName()} = $id;

		return $self;
	}

}

