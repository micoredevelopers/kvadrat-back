<?php

namespace App\Repositories;

use App\DataContainers\Admin\User\SearchDataContainer;
use App\DataContainers\Platform\User\SearchPerformerDataContainer;
use App\Criteria\ActiveCriteria;
use App\Criteria\User\PerformerActiveAccountCriteria;
use App\Criteria\User\PerformerFilterCriteria;
use App\Criteria\User\UserFilterCriteria;
use App\Models\User;
use Dotenv\Repository\RepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;


class PerformerRepository extends UserRepository
{


	public function addPublicCriteriaToQuery(): PerformerRepository
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		$this->pushCriteria(PerformerActiveAccountCriteria::class)
		;

		return $this;
	}

	public function performersQueryPaginate(SearchPerformerDataContainer $container)
	{
		$repo = $this->app->make(self::class);
		$repo->addPublicCriteriaToQuery();
		$select = ['id','name', 'surname', 'avatar', 'profession', 'about', 'rating'];
		$repo->select($select);
		if ($container) {
			$repo->pushCriteria($this->app->make(PerformerFilterCriteria::class, ['container' => $container]));
		}
		return $repo;
	}


	public function getPerformersPaginated(SearchPerformerDataContainer $container): LengthAwarePaginator
	{
		$perPage = $container->getOnPage();
		$page = $container->getPage();
		$cacheKey = 'performers-paginate.' . md5(serialize($container ? $container->toKey() : []));
		$items = $this->performersQueryPaginate($container)
			->forPage($page, $perPage)
			->orderByDesc('rating')
			->orderByDesc('last_seen_at')
			->get()
		;

		$total = Cache::get($cacheKey, function () use ($cacheKey, $container) {
			$q = $this->performersQueryPaginate($container);
			$count = $q->count();
			Cache::set($cacheKey, $count, now()->addMinutes(10));
			return $count ?? 0;
		});
		/** @var  $paginator */
		$paginator = new LengthAwarePaginator($items, $total, $perPage, $page);
		return $paginator;
	}
}
