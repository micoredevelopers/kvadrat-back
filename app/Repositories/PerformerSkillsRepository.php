<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Criteria\SortNameCriteria;
use App\DataContainers\Category\SearchCategoryRequestData;
use App\Models\Category\Category;
use App\Models\User\Skill;
use App\Models\User;
use App\Traits\Repositories\Criteria\addWhereUserCriteria;
use Illuminate\Support\Collection;


class PerformerSkillsRepository extends AbstractRepository
{
	use addWhereUserCriteria;

	public function model()
	{
		return Skill::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
//		$this->pushCriteria($this->app->make(SortCriteria::class));
//		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortNameCriteria::class));
	}

	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		return $this->paginate();
	}

	public function getListSelect()
	{
		$this->addAdminCriteriaToQuery();
		return $this->get();
	}

	public function getListPublic()
	{
		$this->addPublicCriteriaToQuery();
		return $this->get();
	}

	/**
	 * @param \App\Models\User $user
	 * @return Collection | Category[]
	 * @throws \Illuminate\Contracts\Container\BindingResolutionException
	 */
	public function getTreePublic(User $user): Collection
	{
		//todo refactor
		//shit code не учитывается включенность категорий и сортировка родителей
		$categoryRepo = $this->app->make(CategoryRepository::class);
		$ids = $user->skills()->pluck('category_id')->toArray();
		$categories = $categoryRepo->findMany($ids, SearchCategoryRequestData::createForPublic());
		$categories->load('parent');
		$list = $categories->map(function (Category $category) {
			return $category->parent;
		})
			->unique()
			->whereNotNull()
			->map(function ( $category) use ($categories) {
				$category->setSubcategories($categories->filter(function (Category $subCategory) use ($category) {
					$subCategory->setRelation('parent', null);
					return $category->getKey() === (int)$subCategory->getAttribute('parent_id');
				}));
				return $category;
			})
		;
		return $list;
	}

}

