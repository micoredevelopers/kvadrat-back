<?php declare(strict_types=1);

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCategoryCriteria;
use App\Criteria\SortNameCriteria;
use App\DataContainers\Category\SearchCategoryRequestData;
use App\Models\Category\Category;
use App\Models\Category\CategoryLang;
use App\Scopes\SortOrderScope;
use App\Scopes\WhereActiveScope;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class CategoryRepository extends AbstractRepository
{
	private $cacheKey = 'public.categories.tree';

	public function model()
	{
		return Category::class;
	}

	public function modelLang()
	{
		return CategoryLang::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
		$this->pushCriteria($this->app->make(SortCategoryCriteria::class));
		$this->pushCriteria($this->app->make(ActiveCriteria::class))
			->applyCriteria()
			->resetCriteria()
		;

		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortNameCriteria::class));
	}

	public function getListAdmin()
	{
		return Category::with(['lang', 'allCategories' => function ($q) {
			return $q->withGlobalScope('sort', $this->app->make(SortOrderScope::class));
		},
		])
			->withGlobalScope('sort', $this->app->make(SortOrderScope::class))
			->parentMenu()
			->get()
		;
	}

	public function getTreePublic()
	{
		$this->addPublicCriteriaToQuery();
		try {
			if (!$categories = Cache::get($this->cacheKey)) {
				$this->model = $this->joinLang()->with(['lang', 'categories' => function (HasMany $q) {
					$q
						->withGlobalScope('active', $this->app->make(WhereActiveScope::class))
						->withGlobalScope('sort', $this->app->make(SortOrderScope::class))
					;
				},
				])
					->parentMenu();
				$categories = $this->get();
				Cache::set($this->getCacheKey(), $categories, now()->addHour());

				return $categories;
			}
		} catch (Exception $e) {
			$categories = collect([]);
		}

		return $categories;
	}

	public function findMany(array $ids, SearchCategoryRequestData $data)
	{
		$this->addCriteriaByRequestData($data);
		$this->model = $this->joinLang();

		return $this->findWhereIn('id', $ids);
	}

	public function getParent(Category $category, SearchCategoryRequestData $data)
	{
		$this->addCriteriaByRequestData($data);
		$this->model = $this->joinLang();
		$this->model = $this->model->where($category->getKeyName(), $category->parent()->getParentKey());

		return $this->first();
	}

	public function dropCacheTree(): void
	{
		Cache::forget($this->cacheKey);
	}

	public function rebuildCategoriesTree(): void
	{
		$this->dropCacheTree();
		$this->getTreePublic();
	}

	public function getCategoryParents(Category $category, $onlyActive = true): Collection
	{
		/** @var  $parent Category */
		$parents = collect([]);
		if ($category->parent) {
			$parent = $category->parent;
			$parents->push($parent);
			while ($parent->parent !== null) {
				$parents->push($parent->parent);
				$parent = $parent->parent;
			}
			if ($onlyActive) {
				$parents = $parents->filter(static function (Category $category) {
					return (int)$category->getAttribute('active');
				});
			}
		}

		return $parents->reverse();
	}

	public function findForNestable($id)
	{
		static $categoriesNestable;
		if ($categoriesNestable === null) {
			$categoriesNestable = self::all()->keyBy('id');
		}

		return \Arr::get($categoriesNestable, $id, []);
	}

	public function nestable(array $categories, $parent_id = 0)
	{
		/** @var $findCategory Category */
		foreach ($categories as $num => $category) {
			if ($findCategory = $this->findForNestable(\Arr::get($category, 'id'))) {
				$data = [
					'sort' => $num,
					'parent_id' => (int)$parent_id,
				];
				$findCategory->fill($data)->save();
				if (isset($category['children'])) {
					self::nestable($category['children'], $category['id']);
				}
			}
		}
	}

	/**
	 * @param SearchCategoryRequestData $data
	 * @return Collection|array<Category>
	 */
	public function getMainCategories(SearchCategoryRequestData $data): Collection
	{
		$this->addCriteriaByRequestData($data);
		$this->model = $this->model->parentMenu();
		$this->model = $this->joinLang();

		return $this->get();
	}

	/**
	 * @param Category $parent
	 * @param SearchCategoryRequestData $data
	 * @return Collection
	 */
	public function getSubCategories(Category $parent, SearchCategoryRequestData $data): Collection
	{
		$this->addCriteriaByRequestData($data);
		$this->model = $this->joinLang();
		$this->model = $this->model->where($parent->categories()->getForeignKeyName(), $parent->getKey());

		return $this->get();
	}

	private function addCriteriaByRequestData(SearchCategoryRequestData $data): void
	{
		$data->isPublic() ? $this->addPublicCriteriaToQuery() : $this->addAdminCriteriaToQuery();
	}

	private function getCacheKey(): string
	{
		return sprintf('%s.%s', $this->cacheKey, getCurrentLocale());
	}

}

