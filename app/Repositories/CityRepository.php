<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortNameCriteria;
use App\Models\City;
use App\Models\CityLang;


class CityRepository extends AbstractRepository
{
    public function model()
    {
        return City::class;
    }

    public function modelLang()
    {
        return CityLang::class;
    }

    public function addPublicCriteriaToQuery(): self
    {
        $this->pushCriteria($this->app->make(SortNameCriteria::class));
        $this->pushCriteria($this->app->make(ActiveCriteria::class));
        return $this;
    }

    public function addAdminCriteriaToQuery()
    {
        $this->pushCriteria($this->app->make(SortNameCriteria::class))->applyCriteria()->resetCriteria();
    }

    public function getListAdmin()
    {
        $this->addAdminCriteriaToQuery();
        $this->model = $this->joinLang();
        return $this->paginate();
    }

    public function getListSelect()
    {
        $this->addAdminCriteriaToQuery();
        $this->model = $this->joinLang();
        return $this->get();
    }

    public function getListPublic()
    {
        $this->addPublicCriteriaToQuery();
        $this->model = $this->joinLang();
        return $this->get();
    }

}

