<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Models\FranchisePackages;
use App\Models\FranchisePackagesLang;

class FranchisePackageRepository extends AbstractRepository
{

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return FranchisePackages::class;
    }

    public function modelLang()
    {
        return FranchisePackagesLang::class;
    }

    public function addAdminCriteriaToQuery()
    {
        $this->pushCriteria(SortCriteria::class);
    }

    public function addPublicCriteriaToQuery()
    {
        $this->pushCriteria(SortCriteria::class);
        $this->pushCriteria(ActiveCriteria::class);
    }

    public function getListAdmin()
    {
        $this->addAdminCriteriaToQuery();
        return $this->get();
    }

    public function getListPublic()
    {
        $this->addPublicCriteriaToQuery();
        return $this->with('lang')->get();
    }


}
