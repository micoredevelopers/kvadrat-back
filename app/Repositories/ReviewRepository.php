<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\IsPublishedCriteria;
use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ReviewRepository extends AbstractRepository
{

	public const PER_PAGE = 5;

	/**
	 * @return Review
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 */
	public function makeModel()
	{
		return parent::makeModel();
	}

	public function paginate($limit = null, $columns = ['*'], $method = "paginate")
	{
		$limit = $limit ?: self::PER_PAGE;
		return parent::paginate($limit, $columns, $method);
	}

	public function model()
	{
		return Review::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class)->setTable('reviews'));
		$this->pushCriteria($this->app->make(IsPublishedCriteria::class)->setTable('reviews'));

		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
	}

	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		return $this->paginate();
	}

	public function getListPublic()
	{
		$this->addPublicCriteriaToQuery();
		return $this->get();
	}

	public function toPerformerQuery(Authenticatable $user): Builder
	{
		return $this
			->where('reviews.to_id', $user->getAuthIdentifier())
			->where('reviews.about', UserTypeContract::TYPE_PERFORMER)
			;
	}

	public function fromPerformerQuery(Authenticatable $user): Builder
	{
		return $this
			->where('reviews.from_id', $user->getAuthIdentifier())
			->where('reviews.about', UserTypeContract::TYPE_CUSTOMER)
			;
	}

	public function toCustomerQuery(Authenticatable $user): Builder
	{
		return $this
			->where('reviews.to_id', $user->getAuthIdentifier())
			->where('reviews.about', UserTypeContract::TYPE_CUSTOMER)
			;
	}

	public function fromCustomerQuery(Authenticatable $user): Builder
	{
		return $this
			->where('reviews.from_id', $user->getAuthIdentifier())
			->where('reviews.about', UserTypeContract::TYPE_PERFORMER)
			;
	}

	/**
	 * @param Authenticatable $user
	 * @return Collection | Review[]
	 */
	public function getAboutPerformer(Authenticatable $user): Collection
	{
		$this->addPublicCriteriaToQuery();
		/** @var  $reviews */
		$reviews = $this->toPerformerQuery($user)->select('reviews.*')->latest()->get();
		return $reviews;
	}


	public function getAboutPerformerPaginated(Authenticatable $user): LengthAwarePaginator
	{
		/** @var  $reviews */
		$reviews = $this->addPublicCriteriaToQuery()->toPerformerQuery($user)->select('reviews.*')->latest()->paginate();
		return $reviews;
	}

	public function getAboutCustomerPaginated(Authenticatable $user): LengthAwarePaginator
	{
		$this->addPublicCriteriaToQuery();
		/** @var  $reviews */
		$reviews = $this->toCustomerQuery($user)->select('reviews.*')->latest()->paginate();
		return $reviews;
	}

	/**
	 * @param Authenticatable $user
	 * @return Collection | Review[]
	 */
	public function getAboutCustomer(Authenticatable $user): Collection
	{
		$this->addPublicCriteriaToQuery();
		/** @var  $reviews */
		$reviews = $this->toCustomerQuery($user)->select('reviews.*')->latest()->get();
		return $reviews;
	}

	/**
	 * @param Authenticatable $user
	 * @param Order $order
	 * @return Review|null
	 */
	public function customerReviewByOrder(Authenticatable $user, Order $order): ?Review
	{
		/** @var  $review Review | null*/
		$review = $this->fromCustomerQuery($user)
			->leftJoin('orders', 'orders.id', 'reviews.order_id')
			->select('reviews.*')->latest()
			->where('orders.' . $order->getKeyName(), $order->getKey())
			->first()
			;
		return $review;
	}

	public function performerReviewByOrder(Authenticatable $user, Order $order): ?Review
	{
		return $this->fromPerformerQuery($user)
			->leftJoin('orders', 'orders.id', 'reviews.order_id')
			->select('reviews.*')->latest()
			->where('orders.' . $order->getKeyName(), $order->getKey())->first()
			;
	}

	public function getAboutUser($user)
	{
		$id = ($user instanceof Model) ? $user->getKey() : $user;
		return $this->model->where('to_id', $id)->get();
	}


	public function findUnpublishedReviewsToClosedDeals(): Collection
	{
		return $this->model
			->leftJoin('orders', 'orders.id', 'reviews.order_id')
			->where('reviews.is_published', 0)
			->whereNull('reviews.published_at')
			->where('reviews.active', 1)
			->where('orders.is_closed', 1)
			->where('orders.closes_at', '<=', now()->addDays(config('deals.reviews.days_to_publish')))
			->get('reviews.*');
	}

}

