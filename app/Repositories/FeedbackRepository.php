<?php

namespace App\Repositories;

use App\DataContainers\Admin\Feedback\SearchDataContainer;
use App\Criteria\Feedback\FeedbackFilterCriteria;
use App\Models\Feedback\Feedback;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class FeedbackRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class FeedbackRepository extends AbstractRepository
{
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model()
	{
		return Feedback::class;
	}

	public function addAdminCriteriaToQuery()
	{

	}

	public function getListAdmin(?SearchDataContainer $searchDataContainer = null)
	{
		$this->addAdminCriteriaToQuery();

		if ($searchDataContainer) {
			$this->applyFilter($searchDataContainer);
		}

		$this->latest();
		return $this->paginate();
	}

	public function applyFilter(SearchDataContainer $searchDataContainer): self
	{
		$this->pushCriteria($this->app->make(FeedbackFilterCriteria::class, compact('searchDataContainer')));

		return $this;
	}


}
