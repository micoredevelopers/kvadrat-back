<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Criteria\SortNameCriteria;
use App\Models\Category\Category;
use App\Models\User\Portfolio;
use App\Models\User\Skill;
use App\Models\User;
use App\Traits\Repositories\Criteria\addWhereUserCriteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;


class PortfolioRepository extends AbstractRepository
{
	use addWhereUserCriteria;

	public function model()
	{
		return Portfolio::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
		$this->pushCriteria($this->app->make(SortCriteria::class));
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortNameCriteria::class));
	}

	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		return $this->paginate();
	}

	public function getListPublic()
	{
		$this->addPublicCriteriaToQuery();
		return $this->get();
	}

	/**
	 * @param User $user
	 * @return Collection | Portfolio[]
	 */
	public function getByUser(User $user): Collection
	{
		$this->addPublicCriteriaToQuery();
		$port = $this->addUserCriteria($user)->get();
		$port->load('images', 'category');
		return $port;
	}

	public function findByUser($userId,$portfolioId){
        return $this->where('user_id', $userId)->where('id', $portfolioId)->first();
    }
}

