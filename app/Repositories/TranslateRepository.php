<?php

	namespace App\Repositories;

	use App\Models\Translate\Translate;
	use App\Models\Translate\TranslateLang;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Http\Request;
	use Illuminate\Support\Collection;

	class TranslateRepository extends AbstractRepository
	{

		public function model()
		{
			return Translate::class;
		}

		public function modelLang()
		{
			return TranslateLang::class;
		}

		/**
		 * @param Request $request
		 * @return Collection
		 */
		public function getForAdminDisplay(Request $request): Collection
		{
			$query = Translate::with('lang')->latest();
			if ($search = $request->get('search')) {
				$query->join('translate_langs', 'translate_id', 'id')
					->where(static function (Builder $builder) use ($search) {
						$builder->where('key', 'like', '%' . $search . '%')
							->orWhere('value', 'like', '%' . $search . '%')
							->orWhere('group', 'like', '%' . $search . '%')
						;
					})
					->where('translate_langs.language_id', getCurrentLangId())
				;
			}
			/** @var  $translates */
			$translates = $query->get();
			return $translates;
		}
	}
