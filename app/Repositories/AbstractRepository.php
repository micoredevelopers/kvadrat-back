<?php


namespace App\Repositories;


use App\Builders\Model\UserMetaBuilder;
use App\Contracts\HasLocalized;
use App\Models\Language;
use App\Models\Model;
use App\Models\ModelLang;
use App\Repositories\Admin\LanguageRepository;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Throwable;

abstract class AbstractRepository extends BaseRepository
{
	public function findOneByField($field, $value = null, $columns = ['*']): ?\Illuminate\Database\Eloquent\Model
	{
		$this->applyCriteria();
		$this->applyScope();
		$model = $this->model->where($field, '=', $value)->first($columns);
		$this->resetModel();

		return $this->parserResult($model);
	}

	protected function getLanguagesList(): Collection
	{
		return Language::all();
	}

	public function WhereIsPublished($column = 'published_at')
	{
		return $this->where($column, now(), '<');
	}

	public function whereUrl(string $url): Builder
	{
		return $this->where('url', $url);
	}

	public function findByUrl(string $url)
	{
		return $this->whereUrl($url)->first();
	}

	public function makeModelLang(): ?Model
	{
		return $this->app->make($this->modelLang());
	}

	public function update(array $attributes, $id)
	{
		try {
			DB::beginTransaction();
			$isModel = ($id instanceof \Illuminate\Database\Eloquent\Model);

			$model = ($isModel) ? $id : $this->find($id);
			if (!$model) {
				return null;
			}
			/** @var  $model \Illuminate\Database\Eloquent\Model*/
			$model->fill($attributes)->save();
			/** @var  $entityLang ModelLang */
			if (classImplementsInterface($model, HasLocalized::class) && $entityLang = $model->lang) {
				$entityLang->fill($attributes)->save();
			}
			DB::commit();
		} catch (Throwable $e) {
			DB::rollBack();
			throw $e;
		}
		return $model;
	}

	public function create(array $attributes)
	{
		try {
			($entity = $this->makeModel())->fill($attributes);
			if (!$entity) {
				return null;
			}
			$entity->save();
			if (classImplementsInterface($entity, HasLocalized::class)) {
				$languages = $this->app->make(LanguageRepository::class)->getForCreateEntity();
				foreach ($languages as $language) {
					$attributes[$entity->getForeignKey()] = $entity->getKey();
					$entityLang = $this->makeModelLang();
					if (!$entityLang) {
						continue;
					}
					$entityLang->fill($attributes, true)
						->language()->associate($language)
					;
					$entityLang->save();
				}
			}
		} catch (Throwable $e) {
			throwIfDev($e);
			return null;
		}

		return $entity;
	}

	public function createRelated(array $attributes, \Illuminate\Database\Eloquent\Model $model)
	{
		$attributes[$model->getForeignKey()] = $model->getKey();
		return $this->create($attributes);
	}

	/**
	 * @param \Illuminate\Database\Eloquent\Model $model
	 * @return Builder
	 */
	public function whereModel(\Illuminate\Database\Eloquent\Model $model)
	{
		return $this->where($model->getForeignKey(), $model->getKey());
	}

	/**
	 * @return Builder
	 */
	public function getBuilder()
	{
		/** @var  $builder Builder*/
		$builder = $this->model;
		return $builder;
	}


	public function joinLang($builder = null): Builder
	{
		$builder = $builder ?: $this->model;
		$m = $this->makeModel();
		$lm = $this->makeModelLang();
		return $builder->leftJoin($lm->getTable(), function ($join) use ($m, $lm) {
			$table = $m->getTable();
			$langTable = $lm->getTable();
			$join->on($table . '.' . $m->getKeyName(), $langTable . '.' . $m->getForeignKey())
				->on($langTable . '.language_id', '=', DB::raw(getCurrentLangId()))
			;
		});
	}
}
