<?php

namespace App\Repositories;

use App\Builders\Model\UserMetaBuilder;
use App\DataContainers\Admin\User\SearchDataContainer;
use App\Criteria\ActiveCriteria;
use App\Criteria\User\UserFilterCriteria;
use App\Models\User\UserMeta;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


class UserMetaRepository extends AbstractRepository
{
	private static $cachedMeta = [];

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return UserMeta::class;
	}

	/**
	 * @param          $id
	 * @param string[] $columns
	 * @return UserMeta
	 */
	public function find($id, $columns = ['*'])
	{
		return parent::find($id, $columns);
	}

	/**
	 * @param $field
	 * @param null $value
	 * @param string[] $columns
	 * @return UserMeta | null
	 */
	public function findOneByField($field, $value = null, $columns = ['*']): ?Model
	{
		return parent::findOneByField($field, $value, $columns);
	}

	/**
	 * @param \App\Models\User $user
	 * @return Collection | UserMeta[]
	 */
	public function getByUser(User $user): Collection
	{
		$meta = $user->metas;
		foreach ($meta as $item) {
			$this->setCachedMeta($this->getKeyCache($item->key, $user), $item);
		}
		$meta = $meta->keyBy('key');
		return $meta;
	}

	public function getByUserAndKey(User $user, $key): ?UserMeta
	{
		/** @var  $meta UserMeta | null */
		if(null === ($meta = $this->getCachedMeta($this->getKeyCache($key, $user)))){
			$meta = $user->metas()->where('key', $key)->first();
			$this->setCachedMeta($this->getKeyCache($key, $user), $meta);
		}
		return $meta;
	}

	private function setCachedMeta(string $key, ?UserMeta $meta): void
	{
		static::$cachedMeta[$key] = $meta;
	}

	private function getCachedMeta(string $key)
	{
		return static::$cachedMeta[$key] ?? null;
	}

	private function getKeyCache(string $key, ?User $user = null): string
	{
		return $key . ($user ? $user->getKey() : '');
	}

	public function getMetaBuilder(?string $key = null): UserMetaBuilder
	{
		$builder = $this->app->make(UserMetaBuilder::class);
		$builder->setRepository($this);
		if ($key) {
			$builder->setKey($key);
		}
		return $builder;
	}

}
