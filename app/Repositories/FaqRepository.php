<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Models\Faq\Faq;
use App\Models\Faq\FaqLang;

class FaqRepository extends AbstractRepository
{

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return Faq::class;
	}

	public function modelLang()
	{
		return FaqLang::class;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria(SortCriteria::class);
	}

	public function addPublicCriteriaToQuery()
	{
		$this->pushCriteria(SortCriteria::class);
		$this->pushCriteria(ActiveCriteria::class);
	}

	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		return $this->get();
	}

	public function getListPublic()
	{
		$this->addPublicCriteriaToQuery();
		return $this->with('lang')->get();
	}


}
