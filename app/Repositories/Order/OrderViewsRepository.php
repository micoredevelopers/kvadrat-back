<?php

namespace App\Repositories\Order;

use App\Models\Order\Order;
use App\Models\Order\OrderView;
use App\Models\User;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Collection;


class OrderViewsRepository extends AbstractRepository
{
	public function model()
	{
		return OrderView::class;
	}

	public function getByUser(User $user): Collection
	{
		return $this->model->where($user->getForeignKey(), $user->getKey())->get();
	}

	public function getByOrder(Order $order): Collection
	{
		return $this->model->where($order->getForeignKey(), $order->getKey())->get();
	}

	public function isUserHasView(User $user, Order $order): bool
	{
		return (bool)$this->model
			->where($order->getForeignKey(), $order->getKey())
			->where($user->getForeignKey(), $user->getKey())
			->count()
			;
	}

	public function createView(Order $order, User $user)
	{
		return $this->create([
			$order->getForeignKey() => $order->getKey(),
			$user->getForeignKey()  => $user->getKey(),
		]);
	}
}