<?php

namespace App\Repositories\Order;

use App\Criteria\ActiveCriteria;
use App\Models\Order\Order;
use App\Models\Order\OrderUpload;
use App\Models\User;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Collection;


class OrderUploadsRepository extends AbstractRepository
{
	public function model()
	{
		return OrderUpload::class;
	}

	public function getByOrder($order): Collection
	{
		$order = !is_object($order) ?: $order->getKey();
		return $this->where('order_id', $order)->get();
	}

}

