<?php


	namespace App\Services\User\Reset;


	use App\Traits\ResolveSelf;

	class UserResetStepsService
	{
		private $prefix = 'reset';

		use ResolveSelf;

		public function setUserId(int $id)
		{
			$this->storeData('user_id', $id);
		}

		public function getUserId(): ?int
		{
			return $this->getData('user_id');
		}

		public function phoneConfirmed()
		{
			$this->storeData('confirmed', true);
		}

		public function isPhoneConfirmed(): bool
		{
			return (bool)$this->getData('confirmed');
		}

		private function storeData(string $key, $data)
		{
			$prefix = $this->getPrefix();
			session()->put($prefix . $key, $data);
		}

		private function getData(string $key)
		{
			$prefix = $this->getPrefix();
			return session($prefix . $key);
		}

		private function removeData(string $key)
		{
			$prefix = $this->getPrefix();
			session()->remove($prefix . $key);
		}

		private function getPrefix()
		{
			return $this->prefix . '.';
		}
	}