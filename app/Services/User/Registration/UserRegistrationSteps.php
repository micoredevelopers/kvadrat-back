<?php


namespace App\Services\User\Registration;


use App\Traits\ResolveSelf;

interface UserRegistrationSteps
{
	public const STEP_PERSONAL_DATA = 1;
	public const STEP_CHECK_PHONE = 2;
	public const STEP_CONFIRM_PHONE = 3;
	public const STEP_TYPE_USER = 4;

	public const LAST_STEP = self::STEP_TYPE_USER;
}