<?php


namespace App\Services\User\Registration;


use App\Traits\DataKeepers\StoreToSessionTrait;
use App\Traits\ResolveSelf;

class UserRegistrationStepsService
{
	use ResolveSelf;
	use StoreToSessionTrait;

	private $prefix = 'registration';

	private $redirectTo;

	/**
	 * @var RegistrationStepsIncrementor
	 */
	private $stepsIncrementor;

	public function __construct(RegistrationStepsIncrementor $stepsIncrementor)
	{
		$this->stepsIncrementor = $stepsIncrementor;
		$this->redirectTo = $this->getData('redirectTo');
	}

	protected function setStep($step)
	{
		$this->storeData($this->getStepKey(), $step);
		return $this;
	}

	private function incrementStep(): self
	{
		$this->setStep($this->stepsIncrementor->incrementStep($this->getStep()));
		return $this;
	}

	public function hasSteps(): bool
	{
		return (bool)$this->getStep();
	}

	public function finishRegistration(): void
	{
		session()->forget($this->prefix);
	}

	public function setUserId(int $id): self
	{
		$this->storeData('user_id', $id);
		return $this;
	}

	public function getUserId(): ?int
	{
		return $this->getData('user_id');
	}

	public function personalDataSubmitted(): self
	{
		$this->setStep(UserRegistrationSteps::STEP_PERSONAL_DATA);
		return $this->incrementStep();
	}

	public function phoneFormatValidated(): self
	{
		return $this->incrementStep();
	}

	public function phoneVerified(): self
	{
		return $this->incrementStep();
	}

	public function typeUserSelected(): self
	{
//		$this->incrementStep();
		return $this;
	}

	//
	public function isStepPersonalData(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_PERSONAL_DATA);
	}

	public function isStepConfirmPhone(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_CONFIRM_PHONE);
	}

	public function isStepCheckPhone(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_CHECK_PHONE);
	}

	public function isStepTypeUser(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_TYPE_USER);
	}

	//

	private function getStepKey(): string
	{
		return 'step';
	}

	public function getStep()
	{
		return (int)($this->getData($this->getStepKey()) ?? null);
	}

	/**
	 * @return bool
	 */

	private function getSocialKey()
	{
		return 'is-socials';
	}

	public function isSocialRegister(): bool
	{
		return (int)$this->getData($this->getSocialKey());
	}

	public function setSocialRegister(bool $socialRegister): self
	{
		$this->storeData($this->getSocialKey(), (int)$socialRegister);
		return $this;
	}

	/**
	 * @return null | string
	 */
	public function getRedirectTo(): ?string
	{
		return $this->redirectTo;
	}

	public function setRedirectTo(string $redirectTo): self
	{
		$this->storeData('redirectTo', $redirectTo);
		$this->redirectTo = $redirectTo;
		return $this;
	}
}