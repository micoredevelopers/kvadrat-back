<?php


	namespace App\Services\User\Registration;


	use App\Traits\ResolveSelf;

	class UserRegistrationStepsRedirectsService
	{
		public static function getByStep($step): string
		{
			$matches = [
				UserRegistrationSteps::STEP_PERSONAL_DATA => route('register'),
				UserRegistrationSteps::STEP_CHECK_PHONE   => route('registration.phone.check'),
				UserRegistrationSteps::STEP_CONFIRM_PHONE => route('registration.confirm'),
				UserRegistrationSteps::STEP_TYPE_USER     => route('registration.type'),
			];
			return $matches[ $step ] ?? route(\App\Platform\Route\PlatformRoutes::CABINET_PERFORMER_PERSONAL);
		}

		public static function getSubmits(): array
		{
			return [
				route('register'),
				route('registration.phone.check'),
				route('registration.confirm'),
				route('registration.type.customer.send'),
				route('registration.type.company.send'),
				route('registration.type.performer.send'),
			];
		}
	}