<?php


namespace App\Services\User\Login;


use App\Platform\Route\PlatformRoutes;
use App\Traits\DataKeepers\StoreToStaticPropertyTrait;

class UserLoginService
{
	protected $key = 'redirectTo';
	use StoreToStaticPropertyTrait;

	/**
	 * @return null | string
	 */
	public function getRedirectTo(): ?string
	{
		return $this->getData($this->key) ?: route(PlatformRoutes::CABINET_PERFORMER_PERSONAL);
	}

	public function setRedirectTo(string $redirectTo): self
	{
		$this->storeData($this->key, $redirectTo);
		return $this;
	}

	public function dropRedirect()
	{
		$this->setRedirectTo('');
	}

}