<?php


	namespace App\Services\Phone;


	use App\Events\Phone\VerificationGettedEvent;
	use App\Models\Verification;
	use App\Repositories\VerificationRepository;

	class PhoneVerificationService
	{

		/**
		 * @var VerificationRepository
		 */
		private $repository;

		public function __construct(VerificationRepository $repository)
		{
			$this->repository = $repository;
		}

		public function getVerification($phone): Verification
		{
			if (!$verification = $this->repository->findByPhone($phone)) {
				$code = $this->generateCode();
				$data = [
					'code'  => $code,
					'phone' => $phone,
				];
				$verification = $this->repository->create($data);
			}
			event(app(VerificationGettedEvent::class, ['verification' => $verification]));

			return $verification;
		}

		public function refreshVerification($phone)
		{
			$verification = $this->getVerification($phone);
			if (!$verification->wasRecentlyCreated) {
				$this->repository->update(['code' => $this->generateCode()], $verification);
			}
			return $verification;
		}

		private function generateCode(): int
		{
			return mt_rand(1001, 9999);
		}

	}