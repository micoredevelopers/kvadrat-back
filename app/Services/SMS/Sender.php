<?php


	namespace App\Services\SMS;


	class Sender
	{
		/**
		 * @var SendDriver
		 */
		private $driver;

		public function __construct(SendDriver $driver)
		{
			$this->driver = $driver;
		}

		private $message = '';

		private $phoneNumber;

		public function sendMessage()
		{
			$this->driver->send($this);
		}

		/**
		 * @return mixed
		 */
		public function getPhoneNumber()
		{
			return $this->phoneNumber;
		}

		/**
		 * @param mixed $phoneNumber
		 * @return Sender
		 */
		public function setPhoneNumber($phoneNumber): self
		{
			$this->phoneNumber = extractDigits($phoneNumber);
			return $this;
		}

		/**
		 * @return string
		 */
		public function getMessage(): string
		{
			return $this->message;
		}

		/**
		 * @param string $message
		 */
		public function setMessage(string $message): self
		{
			$this->message = $message;
			return $this;
		}

	}