<?php


	namespace App\Services\SMS;


	use App\Helpers\Debug\LoggerHelper;
	use Illuminate\Support\Arr;

	class SendDriver
	{
		/**
		 * @var SendConfig
		 */
		private $config;

		public function __construct(SendConfig $config)
		{
			$this->config = $config;
		}

		public function send(Sender $sender)
		{
			try{
				$this->sendMessage($sender);
			} catch (\Exception $e){
			    app(LoggerHelper::class)->error($e);
			}
		}

		public function tokenAuth()
		{
			$cacheKey = 'sms.auth.token';
			if (cache()->has($cacheKey)) {
				return cache()->get($cacheKey);
			}
			$ch = curl_init();

			$config = $this->config;
			$url = sprintf('%s/uaa/oauth/token?grant_type=password&username=%s&password=%s', $config->getApiUrl(), $config->getLogin(), $config->getPassword());
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			$headers = [];
			$headers[] = 'Authorization: Basic aW50ZXJuYWw6aW50ZXJuYWw=';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			$result = json_decode($result, true);
			if (curl_errno($ch) || isset($result['error'])) {
				logger()->error(curl_error($ch), $result);
				return false;
			}
			curl_close($ch);
			cache()->set($cacheKey, $result, now()->addHours(11)->diffInSeconds(now()));
			return $result;
		}

		public function sendMessage(Sender $sender)
		{
			if (!$this->getAccessToken()){
				return false;
			}
			$config = $this->config;
			$url = $config->getApiUrl() . '/communication-event/api/communicationManagement/v2/communicationMessage/send';
			$replace = [
				'%message%'  => $sender->getMessage(),
				'%MSISDN%'   => $sender->getPhoneNumber(),
				'%alphanum%' => $config->getAlphaNum(),
				'%distId%'   => $config->getDistributionId(),
			];
			$body = str_replace(array_keys($replace), array_values($replace),
				'
				{
				  "content": "%message%",
				  "type": "SMS",
				  "receiver": [
				    {
				      "id": 0,
				      "phoneNumber": %MSISDN%
				    }
				  ],
				  "sender": {
				    "id": "%alphanum%"
				  },
				  "characteristic": [
				    {
				      "name": "DISTRIBUTION.ID",
				      "value": %distId%
				    },
				    {
				      "name": "VALIDITY.PERIOD",
				      "value": "000000000100000R"
				    }
				  ]
				}
				'
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

			$headers = [];
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'Accept: */*';
			$headers[] = sprintf('Authorization: bearer %s', $this->getAccessToken());
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				logger()->error(curl_error($ch));
			}
			curl_close($ch);
			return $result;
		}

		private function getAccessToken(): string
		{
			return Arr::get($this->tokenAuth(), 'access_token', '');
		}

	}