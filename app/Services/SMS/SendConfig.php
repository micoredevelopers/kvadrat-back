<?php


	namespace App\Services\SMS;


	class SendConfig
	{
		public function __construct()
		{
		    $this->setLogin((string)config('sms.login'));
		    $this->setPassword((string)config('sms.password'));
		    $this->setAlphaNum((string)config('sms.alphaNum'));
		    $this->setDistributionId((int)config('sms.distributionId'));
		}

		private $login = '';

		private $password = '';

		private $apiUrl = 'https://a2p.vodafone.ua';

		private $alphaNum = '';

		private $distributionId = 0;

		/**
		 * @return string
		 */
		public function getLogin(): string
		{
			return $this->login;
		}

		/**
		 * @param string $login
		 */
		public function setLogin(string $login): void
		{
			$this->login = $login;
		}

		/**
		 * @return string
		 */
		public function getPassword(): string
		{
			return (string)$this->password;
		}

		/**
		 * @param string $password
		 */
		public function setPassword(?string $password): void
		{
			$this->password = $password;
		}

		/**
		 * @return string
		 */
		public function getApiUrl(): string
		{
			return $this->apiUrl;
		}

		/**
		 * @param string $apiUrl
		 */
		public function setApiUrl(string $apiUrl): void
		{
			$this->apiUrl = $apiUrl;
		}

		/**
		 * @return string
		 */
		public function getAlphaNum(): string
		{
			return $this->alphaNum;
		}

		/**
		 * @param string $alphaNum
		 */
		public function setAlphaNum(string $alphaNum): void
		{
			$this->alphaNum = $alphaNum;
		}

		/**
		 * @return int
		 */
		public function getDistributionId(): int
		{
			return $this->distributionId;
		}

		/**
		 * @param int $distributionId
		 */
		public function setDistributionId(int $distributionId): void
		{
			$this->distributionId = $distributionId;
		}

	}