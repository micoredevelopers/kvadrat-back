<?php


	namespace App\Services\SMS;


	use App\Models\Verification;
	use App\Services\Phone\PhoneVerificationService;

	class SMSSenderService
	{
		/**
		 * @var PhoneVerificationService
		 */
		private $phoneVerificationService;
		/**
		 * @var Sender
		 */
		private $sender;

		private $lastVerification;

		public function __construct(PhoneVerificationService $phoneVerificationService, Sender $sender)
		{
			$this->phoneVerificationService = $phoneVerificationService;
			$this->sender = $sender;
		}

		public function send(string $phone): self
		{
			$verification = $this->phoneVerificationService->refreshVerification($phone);
			if (!isLocalEnv()) {
				$this->sender->setMessage($verification->getCode())->setPhoneNumber($phone)->sendMessage();
			}
			$this->lastVerification = $verification;
			return $this;
		}

		public function getLastVerification(): ?Verification
		{
			return $this->lastVerification;
		}
	}