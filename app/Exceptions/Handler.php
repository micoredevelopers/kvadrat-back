<?php

	namespace App\Exceptions;

	use App\Helpers\Debug\LoggerHelper;
	use App\Mail\ExceptionOccurred;
	use App\ViewShares\NotFoundExceptionSharer;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
	use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
	use Throwable;

	class Handler extends ExceptionHandler
	{
		/**
		 * A list of the exception types that are not reported.
		 *
		 * @var array
		 */
		protected $dontReport = [
			//
		];

		/**
		 * A list of the inputs that are never flashed for validation exceptions.
		 *
		 * @var array
		 */
		protected $dontFlash = [
			'password',
			'password_confirmation',
		];

		/**
		 * @param Exception $
		 * @return mixed|void
		 * @throws Exception
		 */
		public function report(Throwable $throwable)
		{
			if ($throwable instanceof NotFoundHttpException || $throwable instanceof ModelNotFoundException) {
				try {
					app(NotFoundExceptionSharer::class)->share();
				} catch (\Exception $e) {
					app(LoggerHelper::class)->error($e);
				}
			}
			parent::report($throwable);

			if ($this->shouldReport($throwable)) {
				$this->sendEmail($throwable); // sends an email
			}
		}

		protected function sendEmail(Throwable $e)
		{
			try {
				$html = $this->renderExceptionWithSymfony($e, true);
				if (env('DEBUG_EMAIL') && !isLocalEnv()) {
					$excSha = sha1($html);
					$cacheKey = 'exception.' . $excSha;
					if (!\Cache::has($cacheKey)) {
						\Mail::queue(new ExceptionOccurred($html));
						\Cache::set($cacheKey, true);
					}
				}
			} catch (\Throwable $ex) {
			}
		}
	}
