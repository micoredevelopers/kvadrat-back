<?php

namespace App\Jobs\Order\Review;

use App\Models\User;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecountCustomerCountReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function handle(UserRepository $userRepository, ReviewRepository $reviewRepository)
    {
	    $count = $reviewRepository->toCustomerQuery($this->user)->count();
		$this->user->getSetter()->setCustomerReviewsCount($count);
		$userRepository->update([], $this->user);
	}
}
