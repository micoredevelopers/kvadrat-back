<?php

	namespace App\View\Components\Layout;

	use App\Models\User;
	use App\Platform\User\AuthenticatedUserResolver;
	use App\Platform\User\AuthenticatedUserType;
	use App\Platform\User\Type\UserTypeHelper;
	use App\Repositories\Order\OrderRepository;
	use Illuminate\Support\Facades\Cache;
	use Illuminate\View\Component;

	class HeaderUserTypeLinks extends Component
	{
		/**
		 * @var AuthenticatedUserType
		 */
		private $authenticatedUserType;
		/**
		 * @var AuthenticatedUserResolver
		 */
		private $userResolver;
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;

		/**
		 * Create a new component instance.
		 *
		 * @return void
		 */
		public function __construct(AuthenticatedUserType $authenticatedUserType, AuthenticatedUserResolver $userResolver, OrderRepository $orderRepository)
		{
			$this->authenticatedUserType = $authenticatedUserType;
			$this->userResolver = $userResolver;
			$this->orderRepository = $orderRepository;
		}

		/**
		 * Get the view / contents that represent the component.
		 *
		 * @return \Illuminate\Contracts\View\View|string
		 */
		public function render()
		{
			$user = $this->userResolver->getUser();
			$isPerformer = UserTypeHelper::isPerformer($this->authenticatedUserType->getUserType());
			$isCustomer = !$isPerformer;
			$countMyOrders = $this->getOrdersCounter($user, $isPerformer);
			$with = compact(array_keys(get_defined_vars()));
			return view('components.layout.header-user-type-links')->with($with);;
		}

		protected function getOrdersCounter(User $user, $isPerformer)
		{
			$key = sprintf('counters.orders.%d.%s', $user->getKey(), $this->authenticatedUserType->getUserType());
			return Cache::get($key, function () use ($user, $isPerformer, $key) {
				$count = $isPerformer
					? $this->orderRepository->where('performer_id', $user->getKey())->count()
					: $this->orderRepository->where('customer_id', $user->getKey())->count();
//				Cache::set($key, $count);
				return $count;
			});

		}
	}
