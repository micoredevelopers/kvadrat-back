<?php

namespace App\View\Components\Layout;

use App\Repositories\MenuRepository;
use App\View\Traits\CacheableComponent;
use Illuminate\View\Component;

class FooterMenus extends Component
{
	use CacheableComponent;

	protected $cacheKey = 'view-cache.layout.footerMenus';
	/**
	 * @var MenuRepository
	 */
	private $menuRepository;

	public function __construct(MenuRepository $menuRepository)
	{
		$this->menuRepository = $menuRepository;
	}

	public function render()
	{
		if ($this->hasCached()) {
			return $this->getCached();
		}
		$helpMenu = $this->menuRepository->with('lang')->findByGroup('footer_help');
		$sectionsMenu = $this->menuRepository->with('lang')->findByGroup('footer_sections');
		$with = compact(array_keys(get_defined_vars()));
		$view = view('components.layout.footer-menus')->with($with)->render();
//		$this->addToCached($view);
		return $view;
	}
}
