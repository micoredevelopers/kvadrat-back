<?php

namespace App\View\Components\Layout;

use App\Models\Menu;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class PagesMenu extends Component
{
	/**
	 * @var MenuRepository
	 */
	private $repository;

	private $cacheKey = 'menus.pages_menu';
	/**
	 * @var Request
	 */
	private $request;

	public function __construct(MenuRepository $repository, Request $request)
	{
		$this->repository = $repository;
		$this->request = $request;
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|string
	 */
	public function render()
	{
		/** @var  $menus Collection */
		$menus = Cache::get($this->cacheKey, function () {
			return tap($this->repository->findByGroup('pages_menu'), function ($menus) {
				Cache::set($this->cacheKey, $menus);
			});
		});
		$url = getUrlWithoutHost(getNonLocaledUrl($this->request->getPathInfo()));
		$menus->map(function (Menu $menu) use ($url){
			$menu->isActive = $url === $menu->getUrl();
			return $menu;
		});
		$with = compact(array_keys(get_defined_vars()));
		return view('components.layout.pages-menu')->with($with);
	}
}
