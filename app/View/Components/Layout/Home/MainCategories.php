<?php

namespace App\View\Components\Layout\Home;

use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\View\Traits\CacheableComponent;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;
use Psr\SimpleCache\InvalidArgumentException;

class MainCategories extends Component
{
	use CacheableComponent;

	protected $cacheKey = 'view-cache.main-categories';
	/**
	 * @var CategoryRepository
	 */
	protected $categoryRepository;

	public function __construct(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
		$this->cacheKey .= '.' . getCurrentLocale();
	}

	public function render()
	{
		if ($this->hasCached()) {
			return $this->getCached();
		}
		$categories = $this->categoryRepository->getTreePublic()->map(function (Category $category) {
			$url = 'category[]=' . implode('&category[]=', $category->getSubcategories()->pluck('id')->toArray());
			$category->setAttribute('selectedSubcategoriesUrl', $url);
			return $category;
		})
		;
		$with = compact(array_keys(get_defined_vars()));
		$view = view('components.layout.home.main-categories')->with($with)->render();
		$this->addToCached($view);
		return $view;
	}

}
