<?php declare(strict_types=1);

namespace App\View\Components\Layout\Home;

use App\Repositories\CategoryRepository;

class SubCategories extends MainCategories
{
	public function __construct(CategoryRepository $categoryRepository)
	{
		parent::__construct($categoryRepository);
		$this->cacheKey .= '.' . getCurrentLocale();
	}

	protected $cacheKey = 'view-cache.sub-categories';

	public function render()
	{
		if ($this->hasCached()) {
			return $this->getCached();
		}
		$categories = $this->categoryRepository->getTreePublic();
		$with = compact(array_keys(get_defined_vars()));
		$view = view('components.layout.home.sub-categories')->with($with)->render();
		$this->addToCached($view);

		return $view;
	}

}
