<?php

namespace App\View\Components;

use App\Helpers\Sidebar\PageUsedSettingsHelper;
use App\Helpers\Sidebar\PageUsedTranslateHelper;
use App\Models\Meta;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class AdminSidebar extends Component
{
	private $gate;

	public function __construct(Gate $gate)
	{
		$this->gate = $gate;
	}

	private static $alreadyRendered = false;

	/**
	 * @inheritDoc
	 */
	public function render(): string
	{
		if (static::$alreadyRendered || !isAdmin()) {
			return '';
		}
		static::$alreadyRendered = true;
		$admin = Auth::guard('admin')->user();
		$data = [];
		if ($meta = Meta::getMetaData()) {
			$data['meta'] = $meta;
		} else {
			$data['metaUrlCreate'] = getUrlWithoutHost(getNonLocaledUrl());
		}
		$rendered = $this->getRendered();
		$robotsClass = file_exists('robots.txt') ? 'btn-success' : 'btn-danger';
		$sitemapClass = file_exists('sitemap.xml') ? 'btn-success' : 'btn-danger';
		$productionLink = $this->getProductionLink();
		$usersSearch = $this->gate->forUser($admin,)->check('view_users');
		$settingsSearch = $this->gate->forUser($admin,)->check('view_settings');
		$usedSettings = $settingsSearch ? PageUsedSettingsHelper::getUsedSettingsList() : [];
		$usedTranslates = $settingsSearch ? PageUsedTranslateHelper::getUsedTranslatesList() : [];
		$canViewLogs = $this->gate->forUser($admin,)->check('view_logs');
		$data = array_merge($data, compact(
			'robotsClass'
			, 'sitemapClass'
			, 'productionLink'
			, 'usersSearch'
			, 'settingsSearch'
			, 'usedSettings'
			, 'usedTranslates'
			, 'canViewLogs'
			, 'rendered'
		));

		try {
			return view('public.dev.sidebar')
				->with($data)
				->render()
				;
		} catch (\Exception $e) {
			logger()->error($e);
			if (isLocalEnv()) {
				throw $e;
			}
		}
		return '';
	}

	private function getProductionLink(): string
	{
		if (!env('PRODUCTION_APP_URL')) {
			return '';
		}
		return Str::replaceFirst(env('APP_URL'), env('PRODUCTION_APP_URL'), request()->fullUrl());
	}

	private function getRendered()
	{
		return [
			$this->defaultViews(),
		];
	}

	private function defaultViews()
	{
		return '';
		try {
			$filesystem = app(Filesystem::class);
			$viewsPath = resource_path('views/public/pages/dev');
			$viewsArr = $filesystem->files($viewsPath);
			$views = $filenames = [];
			foreach ($viewsArr as $view) {
				if (!Str::contains($view->getFilename(), ['.blade.php'])) {
					continue;
				}
				$filenames[] = Str::replaceLast('.blade.php', '', $view->getFilename());
			}
			foreach ($filenames as $filename) {
				$views[] = sprintf('<a href="%s" class="badge badge-info ml-2">%s</a>', route('pages.default', $filename), $filename);
			}

			return implode('', $views);
		} catch (\Exception $e) {
			app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
			return '';
		}
	}

}
