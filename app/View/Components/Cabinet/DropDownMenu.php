<?php

namespace App\View\Components\Cabinet;

use App\Platform\Cabinet\Route\CabinetDestinationLinks;
use App\Platform\Cabinet\Route\LinkCabinetByUserType;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\Type\UserTypeHelper;
use Illuminate\View\Component;

class DropDownMenu extends Component
{
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;

	public function __construct(AuthenticatedUserType $authenticatedUserType)
	{
		$this->authenticatedUserType = $authenticatedUserType;
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|string
	 */
	public function render()
	{
		$userType = $this->authenticatedUserType->getUserType();
		$isCustomer = UserTypeHelper::isCustomer($userType);
		$switchText = $isCustomer ? 'Перейти в исполнителя' : 'Перейти в заказчика';
		$personalLink = LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PERSONAL);
		$passwordLink = LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PASSWORD);
		$switchLink = route('cabinet.switch.type');
		$with = compact(array_keys(get_defined_vars()));
		return view('components.cabinet.drop-down-menu')->with($with);
	}
}
