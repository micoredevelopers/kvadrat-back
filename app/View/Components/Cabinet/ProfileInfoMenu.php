<?php

namespace App\View\Components\Cabinet;

use App\Platform\Cabinet\Display\AuthenticatedUserTypeContainerResolver;
use Illuminate\View\Component;

class ProfileInfoMenu extends Component
{
	/**
	 * @var AuthenticatedUserTypeContainerResolver
	 */
	private $userTypeContainerResolver;

	/**
	 * Create a new component instance.
	 *
	 * @param AuthenticatedUserTypeContainerResolver $userTypeContainerResolver
	 */
    public function __construct(AuthenticatedUserTypeContainerResolver $userTypeContainerResolver)
    {
		$this->userTypeContainerResolver = $userTypeContainerResolver;
	}

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
    	$container = $this->userTypeContainerResolver->create();
    	$name = $container->getName();
    	$avatar = getPathToImage($container->getAvatar(), 'images/ProfileDefault.svg');
		$with = compact(array_keys(get_defined_vars()));
        return view('components.cabinet.profile-info-menu')->with($with);
    }
}
