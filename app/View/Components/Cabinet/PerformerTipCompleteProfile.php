<?php

namespace App\View\Components\Cabinet;

use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Cabinet\Display\Performer\Tips\PerformerCompleteProfileTipHelper;
use App\Platform\User\AuthenticatedUserResolver;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;
use Illuminate\View\View;

class PerformerTipCompleteProfile extends Component
{
	/**
	 * @var PerformerCompleteProfileTipHelper
	 */
	public $tipHelper;

	private $cacheKey = 'platform-cabinet.performers.tip-complete-profile';
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	/**
	 * Create a new component instance.
	 * @param AuthenticatedUserResolver $userResolver
	 */
	public function __construct(AuthenticatedUserResolver $userResolver)
	{
		$this->userResolver = $userResolver;
		$performer = app(DisplayPerformerContainer::class, ['user' => $userResolver->getUser()]);
		$this->tipHelper = app(PerformerCompleteProfileTipHelper::class, ['performer' => $performer]);
	}

	private function getCacheKey($userId): string
	{
		return $this->cacheKey . $userId;
	}

	private function needShow(): bool
	{
		$user = $this->userResolver->getUser();
		if (!$user) {
			return false;
		}
		$cacheKey = $this->getCacheKey($user->getKey());
		$fn = function () use ($cacheKey) {
			return tap($this->tipHelper->needShowTip(), function ($value) use ($cacheKey) {
				Cache::put($cacheKey, $value, now()->addHour());
			});
		};
		return (bool)Cache::get($cacheKey, $fn);
	}


	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return View|string
	 */
	public function render()
	{
		if (!$this->needShow() || !$this->tipHelper->getLinksItems()) {
			return '';
		}
		$links = $this->tipHelper->getLinksItems();
		$with = compact(array_keys(get_defined_vars()));
		return view('components.cabinet.performer-tip-complete-profile')->with($with);
	}
}
