<?php

namespace App\View\Components\Cabinet;

use App\Platform\Cabinet\Route\CabinetDestinationLinks;
use App\Platform\Cabinet\Route\LinkCabinetByUserType;
use App\Platform\Cache\CacheKeys;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\PortfolioRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class UserCabinetSidebar extends Component
{
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var PortfolioRepository
	 */
	private $portfolioRepository;

	public function __construct(
		Request $request,
		AuthenticatedUserType $authenticatedUserType,
		ReviewRepository $reviewRepository,
		PortfolioRepository $portfolioRepository
	)
	{
		$this->request = $request;
		$this->authenticatedUserType = $authenticatedUserType;
		$this->reviewRepository = $reviewRepository;
		$this->portfolioRepository = $portfolioRepository;
	}

	public function render()
	{
		$userType = $this->authenticatedUserType->getUserType();
		$links = $this->getLinks($userType);
		return view('components.cabinet.user-cabinet-sidebar')->with(compact('links'));
	}

	private function getUserType(): string
	{
		return (string)$this->authenticatedUserType->getUserType();
	}

	protected function getLinks(string $userType): array
	{
		/** @var  $user */
		$user = $this->getUser();
		$reviewsName = translateFormat('cabinet.comments', ['%comments%' => $this->getCountReviews()]);
		$portfolioName = !UserTypeHelper::isPerformer($userType) ? null : translateFormat('forms.portfolio-title', ['%number%' => $this->getCountPortfolio()]);
		$links = [
			['name' => getTranslate('cabinet.general.information', 'Общая информация'), 'url' => LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PERSONAL)],
			['name' => $portfolioName, 'role' => ['performer'], 'url' => LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PORTFOLIO)],
			['name' => getTranslate('registration.change.to.password', 'Изменить пароль'), 'url' => LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::PASSWORD)],
			['name' => $reviewsName, 'url' => LinkCabinetByUserType::getLinkByDestination($userType, CabinetDestinationLinks::REVIEWS)],
			['name' => getTranslate('header.public.profile', 'Публичный профиль'), 'url' => route(LinkCabinetByUserType::getRouteByDestination($userType, CabinetDestinationLinks::PUBLIC_PROFILE), $user->getKey()), 'target' => '_blank'],
		];
		$links = $this->filterMenuByAccountType($links, $userType);
		$pathInfo = $this->request->url();
		$links = array_map(function (array $item) use ($pathInfo) {
			try {
				$url = $item['url'] ?? route($item['route']);
			} catch (\Throwable $e) {
				$url = '';
			}
			$item['active'] = $pathInfo === $url;
			$item['url'] = $url;
			return $item;
		}, $links);
		return $links;
	}

	protected function filterMenuByAccountType(array $menus, ?string $account = null): array
	{
		if (!$account) {
			return $menus;
		}
		return array_filter($menus, function ($menu) use ($account) {
			if (!Arr::has($menu, 'role')) {
				return $menu;
			}
			return in_array($account, Arr::get($menu, 'role', []), true);
		});
	}

	private function getUser()
	{
		return Auth::user();
	}

	private function getCountReviews(): int
	{
		$cacheKey = CacheKeys::CABINET_COUNT_REVIEWS . $this->getUserType() . $this->getUser()->getKey();
		return Cache::get($cacheKey, function () use ($cacheKey) {
			$q = (UserTypeHelper::isCustomer($this->getUserType()))
				? $this->reviewRepository->toCustomerQuery($this->getUser())
				: $this->reviewRepository->toPerformerQuery($this->getUser());
			$count = $q->count();
//			Cache::set($cacheKey, $count, now()->addHour());
			return $count;
		});
	}

	private function getCountPortfolio(): int
	{
		$cacheKey = CacheKeys::CABINET_COUNT_PORTFOLIO . $this->getUser()->getKey();
		return Cache::get($cacheKey, function () use ($cacheKey) {
			$count = $this->portfolioRepository->addPublicCriteriaToQuery()->addUserCriteria($this->getUser())->count();
//			Cache::set($cacheKey, $count, now()->addHour());
			return $count;
		});
	}
}
