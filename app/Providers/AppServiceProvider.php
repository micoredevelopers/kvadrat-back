<?php

namespace App\Providers;

use App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Helpers\User\Model\UserHasSocialsContainer;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Repositories\Admin\AdminMenuRepository;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{

		Carbon::setLocale(config('app.locale'));
		Schema::defaultStringLength(191);
		$this->registerDev();
		$this->registerBind();
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{

		$this->bootBlade();
		$this->bootCollection();
		$this->bootRules();
		$this->bootDatabase();
		Paginator::useBootstrap();
	}

	private function registerDev()
	{
		if ($this->app->environment() !== 'production') {
			$this->app->register(IdeHelperServiceProvider::class);
		}
	}

	private function registerBind()
	{
		$this->app->bind(UserHasSocialsContract::class, UserHasSocialsContainer::class);
		$this->app->singleton(AdminMenuRepositoryContract::class, AdminMenuRepository::class);
	}

	private function bootBlade()
	{

	}

	private function bootCollection()
	{
		Collection::macro('whereActive', function ($active = 1) {
			return $this->filter(function ($item) use ($active) {
				return (int)\Arr::get($item, 'active') === $active;
			});
		});
		Collection::macro('columns', function ($columns = null) {
			$columns = is_array($columns) ? $columns : func_get_args();

			return $this->map(function ($item) use ($columns) {
				return $item->only($columns);
			});
		});
		Collection::macro('pluckDistinct', function (string $column) {
			return array_unique($this->pluck($column)->toArray());
		});
	}

	private function bootRules()
	{

	}

	private function bootDatabase()
	{
	}
}
