<?php

namespace App\Providers;

use App\DataContainers\City\CityRouteData;
use App\Enum\CitiesEnums;
use App\Helpers\CityHelper;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class CityRouteServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $city = $this->getCity();
        CityHelper::$cityRouteData = $city;
    }

    private function getCity(): CityRouteData
    {
        $segment = $this->getRequest()->segment(1);
        $segmentTwo = $this->getRequest()->segment(2);
        /** @var  $enums CitiesEnums[] */
        $enums = CitiesEnums::getEnums();
        $prefix = '';

        foreach ($enums as $enum) {
            if ($segment === $enum->getUrl() || $segmentTwo === $enum->getUrl()) {
                $prefix = sprintf('/%s', $enum->getUrl());
                return CityRouteData::create(
                    $prefix,
                    $enum
                );
            }
        }

        return CityRouteData::create(
            $prefix,
            $enums[0]
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    private function getRequest(): Request
    {
        return $this->app['request'];
    }
}
