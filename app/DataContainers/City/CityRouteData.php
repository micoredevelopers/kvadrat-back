<?php declare(strict_types=1);

namespace App\DataContainers\City;

use App\Enum\CitiesEnums;

class CityRouteData
{
    private $cityPrefix = '';

    /**
     * @var CitiesEnums
     */
    private $city;

    public static function create(string $prefix, CitiesEnums $citiesEnum): self
    {
        $self = new self();
        $self->cityPrefix = $prefix;
        $self->city = $citiesEnum;
        return $self;
    }

    /**
     * @return string
     */
    public function getCityPrefix(): string
    {
        return $this->cityPrefix;
    }

    /**
     * @return CitiesEnums
     */
    public function getCity(): CitiesEnums
    {
        return $this->city;
    }


}