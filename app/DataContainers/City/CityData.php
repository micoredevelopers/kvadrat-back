<?php

namespace App\DataContainers\City;

use App\Enum\CitiesEnums;

class CityData
{
    /**
     * @var
     */
    private $key;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;


    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    public static function createFromEnum(CitiesEnums $enum): self
    {
        $self = new self();
        $self->key = $enum->getKey();
        $self->slug = $enum->getUrl();

        return $self;
    }

}