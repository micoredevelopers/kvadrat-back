<?php declare(strict_types=1);

namespace App\DataContainers\Category;

class SearchCategoryRequestData
{
	private $isPublic = true;

	public static function createForAdmin(): self
	{
		$self = new self();
		$self->isPublic = false;

		return $self;
	}

	public static function createForPublic(): self
	{
		$self = new self();
		$self->isPublic = true;

		return $self;
	}

	/**
	 * @return bool
	 */
	public function isPublic(): bool
	{
		return $this->isPublic;
	}
}