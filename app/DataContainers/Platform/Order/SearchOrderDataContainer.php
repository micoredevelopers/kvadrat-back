<?php


namespace App\DataContainers\Platform\Order;


use App\Platform\Query\FilterCategories;
use App\Platform\Query\PaginationOnPage;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SearchOrderDataContainer
{
	/**
	 * @var PaginationOnPage
	 */
	private $paginationOnPage;

	public function __construct(PaginationOnPage $paginationOnPage)
	{
		$this->paginationOnPage = $paginationOnPage;
	}

	private $search = '';

	private $cityId;

	private $onPage;

	private $categoryIds = [];

	private $page;

	/**
	 * @return mixed
	 */
	public function getCityId()
	{
		return $this->cityId;
	}

	public function setCityId($cityId): self
	{
		$this->cityId = $cityId;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getOnPage(): int
	{
		return $this->onPage;
	}

	public function setOnPage(int $onPage): self
	{
		$this->onPage = $onPage;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCategoryIds(): array
	{
		return $this->categoryIds;
	}

	public function setCategoryIds($categoryIds): self
	{
		$this->categoryIds = Arr::sort((array)$categoryIds);
		return $this;
	}


	/**
	 * @return int
	 */
	public function getPage(): int
	{
		return $this->page;
	}

	public function setPage(int $page): self
	{
		$this->page = $page;
		return $this;
	}


	public function setSearch(string $search): self
	{
		$this->search = $search;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSearch(): string
	{
		return $this->search;
	}

	public function fillFromRequest(Request $request): self
	{
		$onPage = $this->paginationOnPage->itemSupports(($onPage = (int)$request->get('onpage')))
			? $onPage
			: $this->paginationOnPage->default();
		$this
			->setCategoryIds(FilterCategories::filter((array)$request->get('category')))
			->setCityId((int)$request->get('city'))
			->setPage(abs((int)$request->get('page') ?: 1))
			->setOnPage($onPage)
			->setSearch((string)$request->get('search'))
		;
		return $this;
	}


	public function toKey(): array
	{
		return [
			'categoryId' => $this->getCategoryIds(),
			'cityId'     => $this->getCityId(),
			'search'     => $this->getSearch(),
		];
	}


}