<?php

namespace App\DataContainers\Platform\Order\Create;

use App\Models\Category\Category;
use Illuminate\Support\Collection;

class CreateOrderCategoriesData
{
	/**
	 * @var array<Category>|Collection
	 */
	private $mainCategories;

	/**
	 * @var int|Category
	 */
	private $selectedCategory;

	/**
	 * @var Collection|array<Category>
	 */
	private $subCategories;

	public function __construct(Collection $mainCategories)
	{
		$this->mainCategories = $mainCategories;
	}

	public function isCategorySelected(Category $category)
	{
		if (null === $this->getSelectedCategory()) {
			return false;
		}

		if ($category->getKey() === $this->getSelectedCategory()->getKey()) {
			return true;
		}

		return $category->getKey() === (int)$this->getSelectedCategory()->parent()->getParentKey();
	}

	/**
	 * @return array<Category>|Collection
	 */
	public function getMainCategories(): Collection
	{
		return $this->mainCategories;
	}

	/**
	 * @return Category|null
	 */
	public function getSelectedCategory(): ?Category
	{
		return $this->selectedCategory;
	}

	public function hasSelectedCategory(): bool
	{
		return null !== $this->getSelectedCategory();
	}

	/**
	 * @param Category|null $selectedCategory
	 * @return CreateOrderCategoriesData
	 */
	public function setSelectedCategory(?Category $selectedCategory): self
	{
		$this->selectedCategory = $selectedCategory;

		return $this;
	}

	/**
	 * @return Category[]|Collection
	 */
	public function getSubCategories()
	{
		return $this->subCategories;
	}

	/**
	 * @param array<Category>|Collection $subCategories
	 * @return CreateOrderCategoriesData
	 */
	public function setSubCategories(Collection $subCategories): self
	{
		$this->subCategories = $subCategories;

		return $this;
	}

	public function hasSubCategories(): bool
	{
		return null !== $this->getSubCategories();
	}


}