<?php

namespace App\DataContainers\Layout;

class FooterData
{
    public $phone;

    public $inst;

    public $workTime;

    public $address;

    public $map;

    private $facebook;


    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getInst()
    {
        return $this->inst;
    }

    /**
     * @return mixed
     */
    public function getWorkTime()
    {
        return $this->workTime;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getMap()
    {
        return $this->map;
    }


    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }


    public static function createFromConfigArray(array $config): self
    {
        $self = new self();
        $self->phone = $config['phone'] ?? '';
        $self->address = $config['address'] ?? '';
        $self->inst = $config['inst'] ?? '';
        $self->workTime = $config['work_time'] ?? '';
        $self->map = $config['map'] ?? '';
        $self->facebook = $config['facebook'] ?? '';

        return $self;
    }



}