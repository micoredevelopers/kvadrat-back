<?php

	namespace App\Observers;

	use App\Models\Translate\Translate;

	class TranslateObserver
	{

		/**
		 * Handle the translate "updated" event.
		 *
		 * @param Translate $translate
		 * @return void
		 */
		public function updated(Translate $translate)
		{
		}

		/**
		 * Handle the translate "restored" event.
		 *
		 * @param Translate $translate
		 * @return void
		 */
		public function restored(Translate $translate)
		{
			//
		}


	}
