<?php


namespace App\Contracts\Models;


interface HasDeletedBy
{

	public function getKey();

	public function getForeignKey();

	public function deletable();

}