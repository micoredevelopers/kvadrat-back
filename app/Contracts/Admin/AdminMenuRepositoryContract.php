<?php


namespace App\Contracts\Admin;


use Illuminate\Support\Collection;

interface AdminMenuRepositoryContract
{
	public function getNestedMenu(): Collection;

	public function dropMenuCache(): bool;
}
