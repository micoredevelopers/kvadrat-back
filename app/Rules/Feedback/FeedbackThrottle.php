<?php

namespace App\Rules\Feedback;

use App\Repositories\FeedbackRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class FeedbackThrottle implements Rule
{
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var FeedbackRepository
	 */
	private $repository;


    public function __construct(Request $request, FeedbackRepository $repository)
    {
		$this->request = $request;
		$this->repository = $repository;
	}

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	$ip = $this->request->ip();
        if ($c = $this->repository->where('ip', $ip)->where('created_at', '>', now()->subMinute())->count()) {
			return false;
        }
		return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Отправка заявки доступна раз в минуту';
    }
}
