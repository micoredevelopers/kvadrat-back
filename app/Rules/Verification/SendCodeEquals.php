<?php

	namespace App\Rules\Verification;

	use App\Models\Verification;
	use App\Order\Helpers\DataKeeper;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\VerificationRepository;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use App\Services\Phone\PhoneVerificationService;
	use Illuminate\Contracts\Validation\Rule;

	class SendCodeEquals implements Rule
	{

		/**
		 * @var PhoneVerificationDataKeeper
		 */
		private $phoneVerificationDataKeeper;

		public function __construct(PhoneVerificationDataKeeper $phoneVerificationDataKeeper)
		{
			$this->phoneVerificationDataKeeper = $phoneVerificationDataKeeper;
		}

		/**
		 * Determine if the validation rule passes.
		 *
		 * @param string $attribute
		 * @param mixed  $value
		 * @return bool
		 */
		public function passes($attribute, $value)
		{
			return (int)$value === (int)$this->phoneVerificationDataKeeper->getCode();
		}

		/**
		 * Get the validation error message.
		 *
		 * @return string
		 */
		public function message()
		{
			return getTranslate('sms.code-non-equals');
		}
	}
