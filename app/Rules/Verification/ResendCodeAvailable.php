<?php

	namespace App\Rules\Verification;

	use App\Models\Verification;
	use App\Order\Helpers\DataKeeper;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\VerificationRepository;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use App\Services\Phone\PhoneVerificationService;
	use Illuminate\Contracts\Validation\Rule;

	class ResendCodeAvailable implements Rule
	{

		/**
		 * @var PhoneVerificationService
		 */
		private $phoneVerificationService;
		/**
		 * @var PhoneVerificationDataKeeper
		 */
		private $phoneVerificationDataKeeper;

		public function __construct(PhoneVerificationService $phoneVerificationService, PhoneVerificationDataKeeper $phoneVerificationDataKeeper)
		{
			$this->phoneVerificationService = $phoneVerificationService;
			$this->phoneVerificationDataKeeper = $phoneVerificationDataKeeper;
		}

		private function getVerification(): ?Verification
		{
			if (!$phone = $this->phoneVerificationDataKeeper->getPhone()) {
				return null;
			}
			return $this->phoneVerificationService->getVerification($phone);
		}

		/**
		 * Determine if the validation rule passes.
		 *
		 * @param string $attribute
		 * @param mixed  $value
		 * @return bool
		 */
		public function passes($attribute, $value)
		{
			$verification = $this->getVerification();
			if (!$verification) {
				return false;
			}
			return $verification->isAvailable();
		}

		/**
		 * Get the validation error message.
		 *
		 * @return string
		 */
		public function message()
		{
			return getTranslate('sms.code-resend-limit');
		}
	}
