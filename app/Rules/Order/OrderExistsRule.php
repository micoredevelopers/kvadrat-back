<?php

namespace App\Rules\Order;

use App\Helpers\Debug\LoggerHelper;
use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Validation\Rule;

class OrderExistsRule implements Rule
{
	/**
	 * @var OrderRepository
	 */
	private $repository;

	/**
	 * Create a new rule instance.
	 *
	 * @param OrderRepository $repository
	 */
	public function __construct(OrderRepository $repository)
	{
		//
		$this->repository = $repository;
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param string $attribute
	 * @param mixed $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		$value = (int)$value;
		if (!$value) {
			return false;
		}
		try {
			return $this->repository->addPublicCriteriaToQuery()->where('id', $value)->count();
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			return false;
		}
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return 'Нельзя добавить заявку к несуществующему заказу';
	}
}
