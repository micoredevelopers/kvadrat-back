<?php

namespace App\Events\User\Registration;

use App\Listeners\User\Registration\UserSelectTypeSelectedListener;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class UserSelectTypeSelectedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var \App\Models\User
	 */
	private $user;
	/**
	 * @var Request
	 */
	private $request;

	/**
	 * Create a new event instance.
	 *
	 * @param \App\Models\User $user
	 * @param Request $request
	 */
	public function __construct(User $user, Request $request)
	{
		$this->user = $user;
		$this->request = $request;
	}


	public static function getListeners(): array
	{
		return [
			UserSelectTypeSelectedListener::class,
		];
	}

	/**
	 * @return Request
	 */
	public function getRequest(): Request
	{
		return $this->request;
	}

	/**
	 * @return \App\Models\User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

}
