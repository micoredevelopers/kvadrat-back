<?php

namespace App\Events\User\Registration;

use App\Listeners\User\Registration\PhoneVerifyStepSuccessCompletedListener;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhoneVerifyStepSuccessCompletedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var \App\Models\User
	 */
	private $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function listeners(): array
	{
		return [
			PhoneVerifyStepSuccessCompletedListener::class,
		];
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

}
