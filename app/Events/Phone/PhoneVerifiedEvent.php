<?php

	namespace App\Events\Phone;

	use App\Listeners\Phone\PhoneVerifiedListener;
	use App\Models\Verification;
	use Illuminate\Broadcasting\InteractsWithSockets;
	use Illuminate\Foundation\Events\Dispatchable;
	use Illuminate\Queue\SerializesModels;

	class PhoneVerifiedEvent
	{
		use Dispatchable, InteractsWithSockets, SerializesModels;

		/**
		 * @var Verification
		 */
		private $verification;

		/**
		 * Create a new event instance.
		 *
		 * @return void
		 */
		public function __construct(Verification $verification)
		{
			$this->verification = $verification;
		}

		/**
		 * @return Verification
		 */
		public function getVerification(): Verification
		{
			return $this->verification;
		}

		public static function listeners()
		{
			return [
				PhoneVerifiedListener::class,
			];
		}
	}
