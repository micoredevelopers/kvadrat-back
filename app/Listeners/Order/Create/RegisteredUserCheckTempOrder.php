<?php

namespace App\Listeners\Order\Create;

use App\Platform\Order\Helpers\TempOrderHelper;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\Order\OrderRepository;
use App\Services\User\Registration\UserRegistrationStepsService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Throwable;

class RegisteredUserCheckTempOrder
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $stepsService;
	/**
	 * @var TempOrderHelper
	 */
	private $tempOrderHelper;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	/**
	 * Create the event listener.
	 *
	 * @param UserRegistrationStepsService $stepsService
	 * @param TempOrderHelper $tempOrderHelper
	 */
	public function __construct(
		UserRegistrationStepsService $stepsService,
		TempOrderHelper $tempOrderHelper,
		OrderRepository $orderRepository

	)
    {
		$this->stepsService = $stepsService;
		$this->tempOrderHelper = $tempOrderHelper;
		$this->orderRepository = $orderRepository;
	}

	/**
	 * Handle the event.
	 *
	 * @param Registered $event
	 * @return void
	 */
	public function handle(Registered $event): void
	{
		if (!$user = $event->user) {
			return;
		}
		if (!$orderId = $this->tempOrderHelper->getOrderId()) {
			return;
		}

		if (!$order = $this->orderRepository->find($orderId)) {
			return;
		}
		try {
			$redirectTo = route(PlatformRoutes::ORDER_SHOW, $order->getSlug());
			$this->stepsService->setRedirectTo($redirectTo);
		} catch (Throwable $e) {
			logger($e);
		}
	}
}
