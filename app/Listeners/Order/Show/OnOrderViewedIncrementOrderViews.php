<?php

namespace App\Listeners\Order\Show;

use App\Events\Order\Show\OrderViewedEvent;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderViewsRepository;

class OnOrderViewedIncrementOrderViews
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderViewsRepository
	 */
	private $orderViewsRepository;

	public function __construct(OrderRepository $orderRepository, OrderViewsRepository $orderViewsRepository)
    {
		$this->orderRepository = $orderRepository;
		$this->orderViewsRepository = $orderViewsRepository;
	}

    public function handle(OrderViewedEvent $event)
    {
		$order = $event->getOrder();
		$user = $event->getCurrentUser();
		if (is_null($user)) {
		    return;
		}
		if ($this->orderViewsRepository->isUserHasView($user, $order)) {
		    return;
		}
		$this->orderViewsRepository->createView($order, $user);
		$order->getSetter()->incrementViews();
		$this->orderRepository->update([], $order);
    }
}
