<?php

namespace App\Listeners\Order\Show\Bids;

use App\Events\Order\Show\Bids\BidEditedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Repositories\Order\OrderBidRepository;

class BidEditedListener
{
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;

	/**
	 * Create the event listener.
	 *
	 * @param OrderBidRepository $orderBidRepository
	 */
	public function __construct(OrderBidRepository $orderBidRepository)
	{
		$this->orderBidRepository = $orderBidRepository;
	}

	public function handle(BidEditedEvent $event)
	{
		$bid = $event->getOrderBid();
		$bid->getSetter()->decrementEdit()->wasModified();
		try{
			$this->orderBidRepository->update([], $bid);
		} catch (\Throwable $e){
		    app(LoggerHelper::class)->error($e);
		}
	}
}
