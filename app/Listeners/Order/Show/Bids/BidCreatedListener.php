<?php

namespace App\Listeners\Order\Show\Bids;

use App\Events\Order\Show\Bids\BidCreatedEvent;
use App\Repositories\Order\OrderBidRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BidCreatedListener
{
	/**
	 * @var OrderBidRepository
	 */
	private $repository;

	public function __construct(OrderBidRepository $repository)
	{
		$this->repository = $repository;
	}

	public function handle(BidCreatedEvent $event)
	{
		$bid = $event->getOrderBid();
		if (isSuperAdmin()) {
			$bid->getSetter()->setAvailableEdits(50);
			$this->repository->update([], $bid);
		}
	}
}
