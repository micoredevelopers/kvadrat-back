<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderCreateFilesUploadedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Platform\Order\Uploads\OrderCreateDetectImagePosterJob;
use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderCreatedDetectPosterImageFromUploadedFilesListener
{
	private $orderRepository;


    public function __construct(OrderRepository $orderRepository)
    {
		$this->orderRepository = $orderRepository;
	}


    public function handle(OrderCreateFilesUploadedEvent $event): void
	{
    	try{
    		$order = $event->getOrder();
			$image = app(OrderCreateDetectImagePosterJob::class, ['files'=> $event->getOrderUploads()])->run();
			if ($image) {
				$order->getSetter()->setImagePoster($image);
			    $this->orderRepository->update([], $order);
			}
    	} catch (\Throwable $e){
    	    app(LoggerHelper::class)->error($e);
    	}
    }
}
