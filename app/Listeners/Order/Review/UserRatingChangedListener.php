<?php

	namespace App\Listeners\Order\Review;

	use App\Contracts\Events\Order\EventHasReview;
	use App\Helpers\Debug\LoggerHelper;
	use App\Jobs\Order\Review\RecountCustomerCountReviews;
	use App\Jobs\Order\Review\RecountPerformerCountReviews;
	use App\Platform\Contract\UserTypeContract;
	use App\Platform\Model\UserFields;
	use App\Repositories\ReviewRepository;
	use App\Repositories\UserRepository;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Queue\InteractsWithQueue;
	use Illuminate\Support\Collection;

	class UserRatingChangedListener
	{
		/**
		 * @var ReviewRepository
		 */
		private $reviewRepository;
		/**
		 * @var UserRepository
		 */
		private $userRepository;

		public function __construct(ReviewRepository $reviewRepository, UserRepository $userRepository)
		{
			$this->reviewRepository = $reviewRepository;
			$this->userRepository = $userRepository;
		}

		public function handle(EventHasReview $event): void
		{
			try {
				$review = $event->getReview();
				$user = $this->userRepository->addPublicCriteriaToQuery()->find($review->getAttribute('to_id'));
				if (!$user) {
					return;
				}
				if (!$order = $review->getOrder()) {
					return;
				}
				$isAboutPerformer = $review->getReviewAbout() === UserTypeContract::TYPE_PERFORMER;
				/** @var  $reviews Collection */
				$rating = $isAboutPerformer
					? $this->reviewRepository->toPerformerQuery($user)->avg('rating')
					: $this->reviewRepository->toCustomerQuery($user)->avg('rating');
				$isAboutPerformer ? RecountPerformerCountReviews::dispatch($user) : RecountCustomerCountReviews::dispatch($user);
				$fieldRating = $isAboutPerformer ? 'rating' : UserFields::getCustomer('rating');
				$this->userRepository->update([$fieldRating => $rating], $user);
			} catch (\Throwable $e) {
				app(LoggerHelper::class)->error($e);
				throwIfDev($e);
			}
		}
	}
