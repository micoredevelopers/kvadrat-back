<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasOrder;
use App\Models\Order\Review;
use App\Repositories\ReviewRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckToNeedsPublishReviews
{

	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(ReviewRepository $reviewRepository)
	{
		//
		$this->reviewRepository = $reviewRepository;
	}

	public function handle(EventHasOrder $event)
	{
		$order = $event->getOrder();
		$reviews = $this->reviewRepository->whereModel($order)->get();
		if ($reviews->count() === 2) {
			/** @var  $review Review */
			foreach ($reviews as $review) {
				$review->getSetter()->publish();
				$this->reviewRepository->update([], $review);
			}
		}
	}
}
