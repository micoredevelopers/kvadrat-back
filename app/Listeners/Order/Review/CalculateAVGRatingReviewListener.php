<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasReview;
use App\Platform\Contract\UserTypeContract;

class CalculateAVGRatingReviewListener
{

    public function handle(EventHasReview $event)
    {
        $review = $event->getReview();
        $container = $review->getContainer();
        $isAboutPerformer = $review->getReviewAbout() === UserTypeContract::TYPE_PERFORMER;
        $subRating = $isAboutPerformer ? $container->getTotalSubRatingPerformer() : $container->getTotalSubRatingCustomer();
        $rating = $subRating / $container->getCountSubRatings();
        $review->getSetter()->setRating($rating);
    }
}
