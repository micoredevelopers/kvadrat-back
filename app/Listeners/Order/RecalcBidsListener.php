<?php

namespace App\Listeners\Order;

use App\Contracts\Events\Order\EventHasOrder;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RecalcBidsListener
{
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;

	public function __construct(OrderRepository $orderRepository, OrderBidRepository $orderBidRepository)
	{
		$this->orderRepository = $orderRepository;
		$this->orderBidRepository = $orderBidRepository;
	}

	public function handle(EventHasOrder $event)
	{
		$order = $event->getOrder();
		$bidsCount = $this->orderBidRepository->addPublicCriteriaToQuery()->whereModel($order)->count();
		$order->getSetter()->setBidsCount($bidsCount);
		$this->orderRepository->update([], $order);
	}
}

