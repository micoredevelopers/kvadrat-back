<?php

namespace App\Listeners\Order\OrderCreated;

use App\Events\Order\OrderBeforePublished;
use App\Helpers\Debug\LoggerHelper;
use App\Platform\Order\Jobs\OrderPublishJob;
use App\Repositories\Order\OrderRepository;

class PublishOrderListener
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderPublishJob
	 */
	private $publishJob;

	public function __construct(OrderRepository $orderRepository, OrderPublishJob $publishJob)
	{
		$this->orderRepository = $orderRepository;
		$this->publishJob = $publishJob;
	}

	public function handle(OrderBeforePublished $event)
	{
		try {
			$order = $event->getOrder();
			$this->publishJob->handle($order);
			$this->orderRepository->update([], $order);
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			throwIfDev($e);
		}
	}
}
