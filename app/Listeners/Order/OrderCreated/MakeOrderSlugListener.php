<?php

namespace App\Listeners\Order\OrderCreated;

use App\Events\Order\OrderCreatedEvent;
use App\Platform\Order\Jobs\OrderMakeSlug;
use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MakeOrderSlugListener
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderMakeSlug
	 */
	private $slugMaker;

	public function __construct(OrderRepository $orderRepository, OrderMakeSlug $slugMaker)
	{
		$this->orderRepository = $orderRepository;
		$this->slugMaker = $slugMaker;
	}

	public function handle(OrderCreatedEvent $event)
	{
		try {
			$order = $event->getOrder();
			$slug = $this->slugMaker->handle($order);
			$order->getSetter()->setSlug($slug);
			$this->orderRepository->update([], $order);
		} catch (\Throwable $e) {
			if (isLocalEnv()) {
				throwIfDev($e);
			}
		}
	}
}
