<?php

namespace App\Listeners\Phone;

use App\Events\Phone\VerificationGettedEvent;
use App\Models\Verification;
use App\Services\Phone\PhoneVerificationDataKeeper;
use Daaner\TurboSMS\Facades\TurboSMS;

class VerificationGettedListener
{
    /**
     * @var PhoneVerificationDataKeeper
     */
    private $dataKeeper;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PhoneVerificationDataKeeper $dataKeeper)
    {
        $this->dataKeeper = $dataKeeper;
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(VerificationGettedEvent $event)
    {
        /** @var  $verification Verification */
        $verification = $event->getVerification();
        $this->dataKeeper->setPhone($verification->getPhone());
        $this->dataKeeper->setCode($verification->getCode());
       $sms = TurboSMS::sendMessages("{$verification->getPhone()}","Your code: {$verification->getCode()}",'sms');
       debugInfo($sms);
    }
}
