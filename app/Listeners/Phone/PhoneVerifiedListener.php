<?php

	namespace App\Listeners\Phone;

	use App\Events\Phone\PhoneVerifiedEvent;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Queue\InteractsWithQueue;

	class PhoneVerifiedListener
	{
		/**
		 * @var PhoneVerificationDataKeeper
		 */
		private $dataKeeper;

		/**
		 * Create the event listener.
		 *
		 * @return void
		 */
		public function __construct(PhoneVerificationDataKeeper $dataKeeper)
		{
			$this->dataKeeper = $dataKeeper;
		}

		/**
		 * Handle the event.
		 *
		 * @param PhoneVerifiedEvent $event
		 * @return void
		 */
		public function handle(PhoneVerifiedEvent $event)
		{
			$event->getVerification()->delete();
			$this->dataKeeper->drop();
		}
	}
