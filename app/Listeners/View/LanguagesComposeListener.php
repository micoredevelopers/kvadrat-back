<?php declare(strict_types=1);

namespace App\Listeners\View;

use Illuminate\Support\Str;
use Illuminate\View\View;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use function getCurrentLocale;

class LanguagesComposeListener
{
	public function handle(View $event)
	{
		$languages = LaravelLocalization::getSupportedLocales();
		$fn = static function (&$arr, $langKey) {
			$active = $langKey === getCurrentLocale();
			$arr['active'] = $active;
			$arr['name'] = Str::upper($langKey);
			$arr['url'] = LaravelLocalization::getLocalizedURL($langKey);
			$arr['key'] = $langKey;
		};
		array_walk($languages, $fn);
		$event->with(compact('languages'));
	}
}
