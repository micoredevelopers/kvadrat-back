<?php declare(strict_types=1);

namespace App\Listeners\View\Compose;



use App\DataContainers\Layout\FooterData;
use App\Enum\CitiesEnums;
use CityHelper;
use DeprecationTests\Foo;

class MainViewListener
{
	private static $isLoaded = false;

    public function __construct()
    {
    }

    public function handle($event)
	{

		if (!$this->supports()) {
			return;
		}
		self::$isLoaded = true;

		$currentCity = CityHelper::getCity();
        $cities = CitiesEnums::getEnums();
		$footerData = FooterData::createFromConfigArray(config('city.' . CityHelper::getCity()->getKey(), []));

        $with = compact(array_keys(get_defined_vars()));
		\view()->share($with);
	}

	private function supports(): bool
	{
		if (app()->runningInConsole()) {
			return false;
		}
		if (self::$isLoaded) {
			return false;
		}

		return true;
	}
}
