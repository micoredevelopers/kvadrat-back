<?php

	namespace App\Listeners\Admin\User;

	use App\Models\User;
	use Illuminate\Auth\Events\Login;
	use Illuminate\Http\Request;

	class OnUserAuth
	{

		private $request;

		/**
		 * @var $user \App\Models\User
		 */
		private $user;

		public function __construct(Request $request)
		{
			$this->request = $request;
		}

		/**
		 * Handle the event.
		 * @param Login $event
		 */
		public function handle(Login $event)
		{
			$this->user = $event->user;
			$this->checkIp();
			$this->addDateAuth();
			$this->user->save();
		}

		private function addDateAuth()
		{
			$this->user->setAttribute('authenticated_at', now());
		}

		private function checkIp()
		{
			$currentIp = $this->request->getClientIp();
			$prevIp = $this->user->getAttribute('last_login_ip');
			// If login super admin do not left steps
			if ($this->isIpLoginDiff($prevIp, $currentIp)) {
				$message = sprintf('Your last ip is %s - current %s.<br> Previous user-agent: %s', $prevIp, $currentIp, $this->user->getAttribute('user_agent'));
				request()->session()->flash('warning', $message);
			}
			$userAgent = $this->request->userAgent();
			$this->user->setAttribute('user_agent', $userAgent);

			if (!session('superadmin-login')) {
				$this->user->setAttribute('last_login_ip', $currentIp);
			}
		}

		private function isIpLoginDiff($prevIp, $currentIp)
		{
			return ($prevIp && ($prevIp !== $currentIp));
		}
	}
