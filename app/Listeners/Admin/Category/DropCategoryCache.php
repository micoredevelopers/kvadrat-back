<?php

namespace App\Listeners\Admin\Category;

use App\Events\Admin\CategoriesChanged;
use App\Repositories\CategoryRepository;

class DropCategoryCache
{
    private $categoryRepository;

    /**
     * DropCategoryCache constructor.
     * @param CategoryRepository $categoryRepository
     *
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param CategoriesChanged $event
	 */
    public function handle(CategoriesChanged $event)
    {
        try {
            $this->categoryRepository->rebuildCategoriesTree();
        } catch (\Exception $exception) {

        }
    }
}
