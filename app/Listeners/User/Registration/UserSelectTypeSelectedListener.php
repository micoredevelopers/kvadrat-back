<?php

namespace App\Listeners\User\Registration;

use App\Events\User\Registration\UserSelectTypeSelectedEvent;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\AuthenticatedUserType;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\UserMetaRepository;
use App\Services\User\Registration\UserRegistrationStepsService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class UserSelectTypeSelectedListener
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;

	public function __construct(
		UserRegistrationStepsService $registrationStepsService,
		UserMetaRepository $userMetaRepository,
		AuthenticatedUserType $authenticatedUserType
	)
	{
		$this->registrationStepsService = $registrationStepsService;
		$this->userMetaRepository = $userMetaRepository;
		$this->authenticatedUserType = $authenticatedUserType;
	}

	public function handle(UserSelectTypeSelectedEvent $event): void
	{
		$this->registrationStepsService->finishRegistration();
		$user = $event->getUser();
		$request = $event->getRequest();
		$this->userMetaRepository->getMetaBuilder()->setKey(UserMetaActions::USER_REGISTERED_AS)->setValue($request->get('type'))->setUser($user)->build();

		$userType = UserTypeHelper::getType($request->get('type'));
		$this->authenticatedUserType->setUserType($userType);
	}
}
