<?php

namespace App\Listeners\User\Registration;

use App\Events\User\Registration\PhoneVerifyStepSuccessCompletedEvent;
use App\Repositories\UserRepository;
use App\Services\User\Registration\UserRegistrationStepsService;

class PhoneVerifyStepSuccessCompletedListener
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * Create the event listener.
	 *
	 * @param UserRepository $userRepository
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function handle(PhoneVerifyStepSuccessCompletedEvent $event): void
	{
		$user = $event->getUser();
		$this->userRepository->update(['phone_verified_at' => now()], $user);
		debugInfo($user->toArray());
		app(UserRegistrationStepsService::class)->phoneVerified();
	}
}
