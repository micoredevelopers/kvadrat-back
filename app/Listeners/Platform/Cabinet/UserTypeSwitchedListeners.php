<?php

namespace App\Listeners\Platform\Cabinet;

use App\Events\Platform\Cabinet\UserTypeSwitched;
use App\Platform\Contract\UserMetaActions;
use App\Repositories\UserMetaRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserTypeSwitchedListeners
{
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	/**
	 * Create the event listener.
	 *
	 * @param UserMetaRepository $userMetaRepository
	 */
    public function __construct(UserMetaRepository $userMetaRepository)
    {
		$this->userMetaRepository = $userMetaRepository;
	}

	/**
	 * Handle the event.
	 *
	 * @param UserTypeSwitched $event
	 * @return void
	 */
    public function handle(UserTypeSwitched $event)
    {
        $user = $event->getUser();
        $type = $event->getNewType();
		$meta = $this->userMetaRepository->getByUserAndKey($user, UserMetaActions::LAST_SELECTED_USER_TYPE);
		if (!$meta) {
			$this->userMetaRepository->getMetaBuilder(UserMetaActions::LAST_SELECTED_USER_TYPE)->setUser($user)->setValue($type)->build();
			return;
		}
		$meta->setValue($type);
		try{
			$this->userMetaRepository->update([], $meta);
		} catch (\Throwable $e){
		}
    }
}
