<?php

namespace App\Models;


/**
 * App\Models\MenuGroup
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MenuGroup extends Model
{
	protected $guarded = ['id'];

	public function menus($withLang = false)
	{
		$query = $this->hasMany(Menu::class);
		if ($withLang) {
			$query->with('lang');
		}
		return $query;
	}

	public function getGroupWithNestedMenu()
	{
		return $this->load(['menus' => function ($query) {
			$query->where(function ($query) {
				$query->whereNull('parent_id')->orWhere('parent_id', 0);
			});
		}, 'menus.lang', 'menus.menus.lang',
		]);
	}

	public static function getByRoleName($role): ?self
	{
		return self::where('role', $role)->first();
	}
}
