<?php

namespace App\Models;

use App\Observers\UserObserver;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Traits\EloquentExtend;
use App\Traits\EloquentScopes;
use App\Traits\Models\User\UserAccessorsTrait;
use App\Traits\Models\User\UserHelpersTrait;
use App\Traits\Models\User\UserMutatorTrait;
use App\Traits\Models\User\UserRelationTrait;
use App\Traits\Models\User\UserSettersTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $phone_verified_at
 * @property string|null $authenticated_at
 * @property string|null $last_seen_at
 * @property string $password
 * @property int $active
 * @property string|null $last_login_ip
 * @property string|null $user_agent
 * @property string|null $remember_token
 * @property string|null $gender
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $date_birth
 * @property string|null $description
 * @property string|null $company_workers
 * @property string|null $avatar
 * @property string|null $phone_second
 * @property string|null $about
 * @property string|null $customer_about
 * @property string|null $profession
 * @property int|null $city_id
 * @property bool $profile_enabled
 * @property float $rating
 * @property int $reviews_count
 * @property int $customer_profile_enabled
 * @property string|null $customer_avatar
 * @property float $customer_rating
 * @property int $customer_reviews_count
 * @property string|null $viber
 * @property string|null $telegram
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $site
 * @property int $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\City|null $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\UserMeta[] $metas
 * @property-read int|null $metas_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Portfolio[] $portfolios
 * @property-read int|null $portfolios_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Review[] $reviewsCustomer
 * @property-read int|null $reviews_customer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Review[] $reviewsPerformer
 * @property-read int|null $reviews_performer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\Category[] $skills
 * @property-read int|null $skills_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\SocialProvider[] $socialProviders
 * @property-read int|null $social_providers_count
 * @method static \Illuminate\Database\Eloquent\Builder|User active($active = 1)
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAuthenticatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCompanyWorkers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerProfileEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerReviewsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDateBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastSeenAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfileEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereReviewsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereViber($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
	use UserAccessorsTrait, UserMutatorTrait, UserHelpersTrait, UserRelationTrait, UserSettersTrait;

	use EloquentExtend, EloquentScopes;

	use Notifiable, HasFactory;

	private $type;

	private $customerContainer;
	private $performerContainer;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $guarded = [
		'id',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'profile_enabled'   => 'bool',
	];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
	}

	public function setUserType($type): User
	{
		$this->type = $type;
		return $this;
	}

	public function getUserType(): ?string
	{
		return $this->type;
	}

	public static function boot()
	{
		parent::boot();

		if (class_exists(UserObserver::class)) {
			static::observe(UserObserver::class);
		}
	}

	public function getCustomerContainer()
	{
		if (is_null($this->customerContainer)) {
			$this->customerContainer = app(DisplayCustomerContainer::class, ['user' => $this]);
		}
		return $this->customerContainer;
	}

	public function getPerformerContainer()
	{
		if (is_null($this->performerContainer)) {
			$this->performerContainer = app(DisplayPerformerContainer::class, ['user' => $this]);
		}
		return $this->performerContainer;
	}

}
