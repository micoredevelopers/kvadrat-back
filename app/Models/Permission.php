<?php

	namespace App\Models;


	use App\Observers\Admin\PermissionObserver;


	/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $group
 * @property string $guard_name
 * @property string $display_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\Admin[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends \Spatie\Permission\Models\Permission
	{
		public function __construct(array $attributes = [])
		{
			parent::__construct($attributes);
			$this->setAttribute('guard_name', 'admin');
		}

		public function getNameForRead()
		{
			return $this->getAttribute('display_name');
		}

		//
		public static function boot()
		{
			parent::boot();

			static::observe(PermissionObserver::class);
		}

		public static function getList()
		{
			$permissions = Permission::orderBy(\DB::raw('IF(`name` = "view_index", 1, 0)'))->orderBy('group')->get()->groupBy('group');
			return $permissions;
		}
	}
