<?php

namespace App\Models;

use App\Traits\Models\Localization\RedirectLangColumn;

class FranchisePackages extends Model
{
    use RedirectLangColumn;

    protected $hasOneLangArguments = [FranchisePackagesLang::class];
    protected $langColumns = ['name', 'population', 'invest', 'gn_invest', 'payback', 'profit', 'royalty'];

    protected $guarded = ['id'];

    public function getName(): string
    {
        return (string)$this->getAttribute('name');
    }

    public function getPopulation(): string
    {
        return (string)$this->getAttribute('population');
    }

    public function getInvest(): string
    {
        return (string)$this->getAttribute('invest');
    }

    public function getGnInvest(): string
    {
        return (string)$this->getAttribute('gn_invest');
    }

    public function getPayback(): string
    {
        return (string)$this->getAttribute('payback');
    }

    public function getProfit(): string
    {
        return (string)$this->getAttribute('profit');
    }

    public function getRoyalty(): string
    {
        return (string)$this->getAttribute('royalty');
    }
}
