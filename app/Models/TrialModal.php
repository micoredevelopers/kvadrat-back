<?php

namespace App\Models;

use App\Models\Model;

class TrialModal extends Model
{
   protected $guarded = ['id'];

    /**
     * @var mixed
     */
    private $name;

    /**
     * @var mixed
     */
    private $phone;

    /**
     * @var mixed
     */
    private $city;

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
