<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FranchisePackagesLang extends Model
{
    protected $primaryKey = ['package_id', 'language_id'];

    public function package(): BelongsTo
    {
        return $this->belongsTo(FranchisePackages::class);
    }



}
