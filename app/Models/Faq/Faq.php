<?php

namespace App\Models\Faq;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;

/**
 * App\Models\Faq\Faq
 *
 * @property int $id
 * @property int|null $sort
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $hasOneLangArguments = [FaqLang::class];
	protected $langColumns = ['question', 'answer'];

	protected $casts = [
		'active' => 'boolean',
	];

	protected $guarded = ['id'];

	public function getQuestion(): string
	{
		return (string)$this->getAttribute('question');
	}

	public function getAnswer(): string
	{
		return (string)$this->getAttribute('answer');
	}


}
