<?php

namespace App\Models\Faq;

use App\Models\ModelLang;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Faq\FaqLang
 *
 * @property int $faq_id
 * @property string|null $question
 * @property string|null $answer
 * @property int $language_id
 * @property-read \App\Models\Faq\Faq $faq
 * @property-read \App\Models\Language $language
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereFaqId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereLanguageId($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereQuestion($value)
 * @mixin \Eloquent
 */
class FaqLang extends ModelLang
{
	protected $primaryKey = ['faq_id', 'language_id'];

	public function faq(): BelongsTo
	{
		return $this->belongsTo(Faq::class);
	}

}
