<?php

namespace App\Models\User;

use App\Models\Category\Category;
use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\User\Skill
 *
 * @property int $user_id
 * @property int $category_id
 * @property-read Category $category
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Skill newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill query()
 * @method static \Illuminate\Database\Eloquent\Builder|Skill whereCategoryId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill whereUserId($value)
 * @mixin \Eloquent
 */
class Skill extends Model
{
	protected $guarded = ['id'];

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}
}
