<?php

namespace App\Models\User;

use App\Models\Category\Category;
use App\Models\Model;
use App\Traits\Models\HasImages;
use App\Traits\Models\MainOrLangDescriptionAttributeTrait;
use App\Traits\Models\NameAttributeTrait;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\User\Portfolio
 *
 * @property int $id
 * @property int|null $sort
 * @property int $active
 * @property mixed|string $description
 * @property string|null $excerpt
 * @property int $category_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio query()
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereUserId($value)
 * @mixin \Eloquent
 */
class Portfolio extends Model
{
	use HasImages;
	use NameAttributeTrait;
	use MainOrLangDescriptionAttributeTrait;

	protected $guarded = ['id'];

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function getCategory(): Category
	{
		return $this->category;
	}

}
