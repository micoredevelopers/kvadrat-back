<?php

	namespace App\Models;

	use App\Observers\VerificationObserver;
	use Illuminate\Support\Carbon;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	/**
 * App\Models\Verification
 *
 * @property int $id
 * @property string|null $available_at
 * @property string|null $ip
 * @property string|null $code
 * @property string|null $phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification query()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereAvailableAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereIp($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Verification extends Model
	{
		protected $guarded = ['id'];

		public $timestamps = ['available_at'];

		public function getPhone(): string
		{
			return (string)$this->getAttribute('phone');
		}

		public function getCode(): string
		{
			return (string)$this->getAttribute('code');
		}

		public function setCode($code): self
		{
			$this->setAttribute('code', $code);
			return $this;
		}

		public function getAvailableAt(): Carbon
		{
			return getDateCarbon($this->getAttribute('available_at'));
		}

		public function isAvailable()
		{
			return $this->getAvailableAt() <= now();
		}

		public function getDiffInSeconds()
		{
			return $this->getAvailableAt()->diffInSeconds(now());
		}

		protected static function boot()
		{
			parent::boot();
			if (class_exists(VerificationObserver::class)) {
				static::observe(VerificationObserver::class);
			}

		}
	}
