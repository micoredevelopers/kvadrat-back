<?php

namespace App\Models;

use App\Contracts\HasLocalized;
use App\Traits\Models\Localization\RedirectLangColumn;

class City extends Model implements HasLocalized
{
    use RedirectLangColumn;

    protected $hasOneLangArguments = [CityLang::class, 'city_id'];

    protected $guarded = ['id'];

    protected $langColumns = ['name', 'language_id'];

    public function getName()
    {
        return $this->name;
    }

}
