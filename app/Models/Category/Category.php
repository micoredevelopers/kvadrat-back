<?php

namespace App\Models\Category;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class Category extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $langColumns = ['name', 'language_id', 'description'];

	protected $hasOneLangArguments = [CategoryLang::class];

	protected $guarded = ['id'];

	public function categories()
	{
		return $this->hasMany(self::class, 'parent_id');
	}

	public function allCategories()
	{
		return $this->categories()->with(['lang', 'allCategories']);
	}

	public function parent(): BelongsTo
	{
		return $this->belongsTo(self::class, 'parent_id');
	}

	public function getCategoryName()
	{
		return $this->getNameDisplay();
	}

	public function getNameDisplay(): ?string
	{
		return (string)$this->getAttribute('name');
	}

	public function getDescription()
	{
		return $this->getAttribute('description');
	}

	public function getSlug()
	{
		return $this->getKey();
	}

	/**
	 * @return Collection | static[]
	 */
	public function getSubcategories(): Collection
	{
		return $this->categories;
	}

	/**
	 * @param Collection | Category[] $collection
	 * @return self
	 */
	public function setSubcategories(Collection $collection): self
	{
		$this->setRelation('categories', $collection);
		return $this;
	}

	public function isMainCategory()
	{
		return !((int)$this->parent_id);
	}

	public function scopeParentMenu($query, $parentId = 0)
	{
		return $query->where(function ($query) use ($parentId) {
			return $query->where('parent_id', $parentId)->orWhereNull('parent_id');
		});
	}

	public static function createReference($id): self
	{
		return parent::createReference($id);
	}
}
