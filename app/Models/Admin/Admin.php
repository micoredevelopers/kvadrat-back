<?php

	namespace App\Models\Admin;

	use App\Models\Admin\Traits\AdminAccessorsTrait;
	use App\Models\Admin\Traits\AdminRelationsTrait;
	use App\Notifications\Admin\Auth\ResetPassword;
	use App\Notifications\Admin\Auth\VerifyEmail;
	use App\Traits\EloquentExtend;
	use App\Traits\EloquentScopes;
	use App\Traits\Models\User\SuperAdminTrait;
	use Database\Factories\AdminFactory;
	use Illuminate\Database\Eloquent\Factories\HasFactory;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Illuminate\Notifications\Notifiable;
	use Spatie\Permission\Traits\HasRoles;

	/**
 * App\Models\Admin\Admin
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $login
 * @property string|null $authenticated_at
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $locale
 * @property string|null $last_login_ip
 * @property string|null $user_agent
 * @property int $active
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Staff\DeletedBy|null $deletable
 * @property-read \App\Models\Staff\ModifiedBy|null $modifiable
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Admin active($active = 1)
 * @method static \Database\Factories\AdminFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAuthenticatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUserAgent($value)
 * @mixin \Eloquent
 */
class Admin extends Authenticatable
	{
		use AdminAccessorsTrait, AdminRelationsTrait;
		use HasFactory, Notifiable;
		use EloquentExtend, EloquentScopes;
		use SuperAdminTrait, HasRoles;

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'password', 'remember_token',
		];

		protected $guarded = [
			'id',
		];

		/**
		 * The attributes that should be cast to native types.
		 *
		 * @var array
		 */
		protected $casts = [
			'email_verified_at' => 'datetime',
		];

		/**
		 * Send the password reset notification.
		 *
		 * @param string $token
		 * @return void
		 */
		public function sendPasswordResetNotification($token)
		{
			$this->notify(new ResetPassword($token));
		}

		/**
		 * Send the email verification notification.
		 *
		 * @return void
		 */
		public function sendEmailVerificationNotification()
		{
			$this->notify(new VerifyEmail);
		}
		protected static function newFactory()
		{
			return new AdminFactory();
		}
	}