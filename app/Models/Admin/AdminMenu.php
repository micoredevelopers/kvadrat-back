<?php

namespace App\Models\Admin;


use App\Models\Model;
use App\Scopes\SortOrderScope;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\NameAttributeTrait;
use Illuminate\Support\Str;

/**
 * App\Models\Admin\AdminMenu
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $active
 * @property mixed $name
 * @property string|null $url
 * @property string|null $gate_rule
 * @property string|null $route
 * @property string|null $image
 * @property string|null $icon_font
 * @property string|null $content_provider
 * @property string|null $option
 * @property string|null $description
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\AdminMenu[] $childrens
 * @property-read int|null $childrens_count
 * @property-read \App\Models\Admin\AdminMenu|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereContentProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereGateRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereIconFont($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereUrl($value)
 * @mixin \Eloquent
 */
class AdminMenu extends Model
{
	use NameAttributeTrait;
	use ImageAttributeTrait;

	protected $guarded = [
		'id',
	];

	public static function boot(): void
	{
		parent::boot();

		self::addGlobalScope(new SortOrderScope());
	}

	public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
	{
		return $this->belongsTo(__CLASS__, 'parent_id', 'id');
	}

	public function childrens(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(__CLASS__, 'parent_id', 'id');
	}

	public function scopeParentMenu($query, $parentId = 0)
	{
		return $query->where(function ($query) use ($parentId) {
			return $query->where('parent_id', $parentId)->orWhereNull('parent_id');
		});
	}

	protected function getUrl()
	{
		return $this->url;
	}


	public function isAnchor(): bool
	{
		return Str::startsWith($this->getUrl(), '#');
	}

	public function getFullUrl(): ?string
	{
		if ($this->isAnchor()) {
		    return $this->getUrl();
		}
		return langUrl($this->getUrlWithPrefix());
	}

	public function getUrlWithPrefix()
	{
		if (Str::startsWith($this->url, '#')) {
			return $this->url;
		}
		return sprintf('/%s/%s', \Config::get('app.admin-url', 'admin'), trim($this->url, '/'));
	}
}
