<?php

	namespace App\Models;


	/**
 * App\Models\MetaLang
 *
 * @property int $meta_id
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $text_top
 * @property string|null $text_bottom
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Meta $meta
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereKeywords($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereLanguageId($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTextBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTextTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTitle($value)
 * @mixin \Eloquent
 */
class MetaLang extends ModelLang
	{
		protected $table = 'meta_lang';

		protected $primaryKey = ['meta_id', 'language_id'];

		protected $guarded = ['id'];

		public function meta()
		{
			return $this->belongsTo(Meta::class);
		}
	}
