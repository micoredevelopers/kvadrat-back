<?php declare(strict_types=1);

namespace App\Models;

use App\Contracts\HasLocalized;
use App\Models\Translate\Translate;
use App\Traits\EloquentExtend;
use App\Traits\EloquentScopes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @mixin \Eloquent
 */
class Model extends EloquentModel
{

	use EloquentScopes;
	use EloquentExtend;

	/**
	 * @var array
	 */
	protected $hasOneLangArguments = [];

	/**
	 * @param string $table
	 * @return Model|null
	 */
	public static function getModelByTable(string $table): ?Model
	{
		$className = self::getModelClassNameByTable($table);
		$model = null;
		if ($className) {
			try {
				$model = app()->make($className);
			} catch (\Exception $e) {

			}
		}
		return $model;
	}

	/**
	 * @param null $language_id
	 * @return Builder
	 */
	public function lang($language_id = null)
	{
		if (!$language_id) {
			$language_id = getCurrentLangId();
		}
		return $this->hasOne(...$this->hasOneLangArguments)->WhereLanguage($language_id);
	}

	/**
	 * @param string $table
	 * @return string|null
	 */
	public static function getModelClassNameByTable(string $table): ?string
	{
		$model = null;
		switch ($table) {
			case 'translates':
				$model = Translate::class;
				break;
		}
		return $model;
	}

	public function toArray()
	{
		if (!classImplementsInterface($this, HasLocalized::class) || !$this->lang) {
			return parent::toArray();
		}
		return array_merge(parent::toArray(), $this->lang->toArray());
	}

	public function canDelete()
	{
		return true;
	}

}



