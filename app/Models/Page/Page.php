<?php

	namespace App\Models\Page;


	use App\Contracts\HasImagesContract;
	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Page\Traits\PageHelpers;
	use App\Traits\Models\HasImages;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Support\Arr;

	/**
 * App\Models\Page
 *
 * @property int                                                               $id
 * @property int|null                                                          $parent_id
 * @property int|null                                                          $sort
 * @property mixed                                                             $url
 * @property bool                                                              $manual
 * @property int                                                               $active
 * @property string|null                                                       $image
 * @property string|null                                                       $page_type
 * @property string|null                                                       $options
 * @property \Illuminate\Support\Carbon|null                                   $created_at
 * @property \Illuminate\Support\Carbon|null                                   $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @mixin \Eloquent
 * @property string|null $sub_image
 * @property-read int|null $images_count
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereImage($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereManual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page wherePageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSubImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUrl($value)
 */
	class Page extends Model implements HasImagesContract, HasLocalized
	{
		use RedirectLangColumn;
		use PageHelpers;
		use ImageAttributeTrait;
		use HasImages;

		protected $hasOneLangArguments = [PageLang::class];
		protected $langColumns = ['title', 'description', 'excerpt', 'sub_title', 'sub_description', 'name'];

		protected $casts = [
			'manual'  => 'boolean',
			'options' => 'array',
		];
		protected $guarded = [
			'id',
		];

		protected static $pageTypes = [
			'main'         => 'Main',
		];

		public function parent($withLang = false)
		{
			$query = $this->belongsTo(__CLASS__, 'parent_id', 'id');
			if ($withLang) {
				$query->with('lang');
			}
			return $query;
		}

		public static function getPageTypes()
		{
			return self::$pageTypes;
		}

		public function pageTypeExists($pageType)
		{
			return Arr::exists(self::$pageTypes, $pageType);
		}

		public function setUrlAttribute($url): self
		{
			if (!$this->getAttribute('manual')) {
				$url = \Str::slug($url);
			} else {
				$url = '/' . ltrim($url, '/');
			}
			$column = 'url';
			$this->attributes[ $column ] = $url;
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getUrlAttribute()
		{
			$column = 'url';
			return \Arr::get($this->attributes, $column);
		}

		public function getRouteUrl()
		{
			try{
				if ($this->isManualLink()) {
					return url($this->getUrlAttribute());
				}
				return route('pages.show', $this->getUrlAttribute());
			} catch (\Exception $e){
			    app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
			    return '';
			}
		}

		public function isManualLink()
		{
			return $this->getAttribute('manual');
		}

		public function setPageTypeAttribute($type)
		{
			if (!$this->pageTypeExists($type)) {
				$type = null;
			}
			$this->attributes['page_type'] = $type;
		}

	}
