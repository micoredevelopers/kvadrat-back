<?php

	namespace App\Models\Page;


	use App\Models\ModelLang;

	/**
 * App\Models\Page\PageLang
 *
 * @property int                       $page_id
 * @property int                       $language_id
 * @property string|null               $title
 * @property string|null               $description
 * @property string|null               $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page     $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @property string|null $excerpt
 * @property string|null $sub_title
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubTitle($value)
 */
	class PageLang extends ModelLang
	{

		protected $table = 'pages_lang';

		protected $guarded = ['id'];

		protected $primaryKey = ['page_id', 'language_id'];

		public function page()
		{
			return $this->belongsTo(Page::class);
		}

	}
