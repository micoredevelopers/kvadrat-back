<?php

namespace App\Models;

class CityLang extends ModelLang
{

    protected $primaryKey = ['city_id', 'language_id'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
