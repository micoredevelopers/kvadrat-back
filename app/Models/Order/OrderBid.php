<?php

namespace App\Models\Order;

use App\Models\Model;
use App\Models\User;
use App\Platform\Order\Containers\Show\OrderBidContainer;
use App\Platform\Order\Model\OrderBidSetter;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Order\OrderBid
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property int $active
 * @property string|null $text
 * @property int $bid
 * @property int $days
 * @property int $available_edits
 * @property \Illuminate\Support\Carbon|null $modified_at
 * @property \Illuminate\Support\Carbon|null $canceled_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereAvailableEdits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereBid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereCanceledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereModifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereUserId($value)
 * @mixin \Eloquent
 */
class OrderBid extends Model
{
	private $container;

	protected $guarded = ['id'];

	protected $casts = [
		'days'        => 'integer',
		'modified_at' => 'datetime',
		'canceled_at' => 'datetime',
	];

	public function order(): BelongsTo
	{
		return $this->belongsTo(Order::class);
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function getUserId()
	{
		return $this->getAttribute('user_id');
	}

	public function getContainer()
	{
		if (is_null($this->container)) {
			$this->container = app(OrderBidContainer::class, ['bid' => $this]);
		}
		return $this->container;
	}

	public function getSetter()
	{
		return app(OrderBidSetter::class, ['orderBid' => $this]);
	}
}
