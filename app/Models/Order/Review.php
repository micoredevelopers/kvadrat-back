<?php

	namespace App\Models\Order;

	use App\Models\Model;
	use App\Platform\Order\Model\ReviewSetter;
	use App\Platform\Reviews\ReviewDisplayContainer;
	use App\Traits\Models\HasImages;
	use App\Models\User;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	/**
 * App\Models\Order\Review
 *
 * @property int $id
 * @property float $rating
 * @property int $adequate_rating Адекватный
 * @property int $legibility_rating Чётко ставит задачу
 * @property int $suggestions_reacts
 * @property int $punctuality_rating
 * @property int $recommend_rating
 * @property int $from_id
 * @property int $to_id
 * @property int $is_published
 * @property string|null $published_at
 * @property string|null $comment
 * @property int $active
 * @property int $order_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $about Commented about performer or about customer
 * @property int $coast_rating Стоимость
 * @property int $quality_rating Качество
 * @property int $professionalism_rating Профессионализм
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\Order\Order $order
 * @property-read User $reviewFrom
 * @property-read User $reviewTo
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereAdequateRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCoastRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereIsPublished($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereLegibilityRating($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereProfessionalismRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePunctualityRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereQualityRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRecommendRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereSuggestionsReacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Review extends Model
	{
		use HasImages;

		protected $guarded = ['id'];
		protected $hidden = [
			'active',
		];

		public function order(): BelongsTo
		{
			return $this->belongsTo(Order::class);
		}

		public function reviewFrom(): BelongsTo
		{
			return $this->belongsTo(User::class, 'from_id');
		}

		public function reviewTo(): BelongsTo
		{
			return $this->belongsTo(User::class, 'to_id');
		}

		public function getReviewFrom(): User
		{
			return $this->reviewFrom;
		}

		public function getReviewTo(): User
		{
			return $this->reviewTo;
		}

		public function getOrder(): Order
		{
			return $this->order;
		}

		public function getRating(): float
		{
			return (float)$this->rating;
		}

		public function getComment()
		{
			return $this->comment;
		}

		public function getSetter()
		{
			return app(ReviewSetter::class, ['entity' => $this]);
		}

		public function getContainer()
		{
			return app(ReviewDisplayContainer::class, ['review' => $this]);
		}

		public function getOrderKey()
		{
			return $this->getAttribute('order_id');
		}

		public function getReviewAbout()
		{
			return $this->getAttribute('about');
		}

	}
