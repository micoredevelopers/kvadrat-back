<?php

namespace App\Models\Order;

use App\Models\Model;
use App\Platform\Order\Services\Files\FilesPreviewer;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Order\OrderUpload
 *
 * @property int $id
 * @property int $order_id
 * @property string|null $file
 * @property string|null $original_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderUpload extends Model
{
	protected $guarded = ['id'];

	public function order(): BelongsTo
	{
		return $this->belongsTo(Order::class);
	}

	public function setPath($path): OrderUpload
	{
		$this->setAttribute('file', $path);
		return $this;
	}

	public function getPath()
	{
		return $this->getAttribute('file');
	}

	public function setOriginalName($path): OrderUpload
	{
		$this->setAttribute('original_name', $path);
		return $this;
	}

	public function getOriginalName()
	{
		return $this->getAttribute('original_name');
	}


	public function getFilesPreviewer():FilesPreviewer
	{
		return  app(FilesPreviewer::class, ['orderUpload' => $this]);
	}
}
