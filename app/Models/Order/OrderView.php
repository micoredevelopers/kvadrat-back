<?php

namespace App\Models\Order;

use App\Models\Category\Category;
use App\Models\City;
use App\Models\Model;
use App\Models\User;
use App\Platform\Order\Containers\ShowMainDataContainer;
use App\Platform\Order\Model\OrderSetter;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * App\Models\Order\OrderView
 *
 * @property int $order_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereCreatedAt($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereUserId($value)
 * @mixin \Eloquent
 */
class OrderView extends Model
{

	protected $guarded = [];

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function order(): BelongsTo
	{
		return $this->belongsTo(Order::class);
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function getOrder(): Order
	{
		return $this->order;
	}

}
