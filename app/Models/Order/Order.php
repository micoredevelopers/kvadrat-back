<?php

namespace App\Models\Order;

use App\Models\Category\Category;
use App\Models\City;
use App\Models\Model;
use App\Models\User;
use App\Platform\Order\Containers\ShowMainDataContainer;
use App\Platform\Order\Model\OrderSetter;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $performer_id
 * @property int|null $customer_id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $description
 * @property int $budget
 * @property string|null $address
 * @property bool $is_urgent
 * @property int $views
 * @property int $city_id
 * @property int $active
 * @property int $is_blank
 * @property string|null $slug
 * @property-read int|null $bids_count
 * @property bool $is_closed
 * @property \Illuminate\Support\Carbon|null $closes_at
 * @property string|null $available_at feature, if will be need to publish at the specific time
 * @property \Illuminate\Support\Carbon|null $published_at for display date publish, because publishing can diff with date create
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $show_phone
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrderBid[] $bids
 * @property-read Category|null $category
 * @property-read City $city
 * @property-read User|null $customer
 * @property-read User|null $performer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrderUpload[] $uploads
 * @property-read int|null $uploads_count
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAvailableAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBidsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereClosesAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsBlank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsUrgent($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePerformerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShowPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereViews($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
	private $setter;

	protected $container;

	protected $guarded = ['id'];

	protected $casts = [
		'closes_at'    => 'datetime',
		'datetime'     => 'datetime',
		'published_at' => 'datetime',
		'is_urgent'    => 'boolean',
		'is_closed'    => 'boolean',
		'bids_count'   => 'int',
		'views'        => 'int',
	];

	public function getTitle()
	{
		return $this->title;
	}

	public function uploads(): HasMany
	{
		return $this->hasMany(OrderUpload::class);
	}

	/**
	 * @return Collection | OrderUpload[]
	 */
	public function getUploads():Collection
	{
		return  $this->uploads;
	}

	public function getSlug()
	{
		return $this->getAttribute('slug');
	}

	public function getSetter(): OrderSetter
	{
		if (null === $this->setter) {
			$this->setter = app(OrderSetter::class, ['order' => $this]);
		}
		return $this->setter;
	}

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function getCategory(): ?Category
	{
		return $this->category;
	}

	public function customer()
	{
		return $this->belongsTo(User::class, 'customer_id');
	}

	public function getCustomer(): User
	{
		return $this->customer;
	}

	public function performer()
	{
		return $this->belongsTo(User::class, 'performer_id');
	}

	public function getPerformer(): ?User
	{
		return $this->performer;
	}

	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}

	public function getCity(): City
	{
		return $this->city;
	}

	public function bids(): HasMany
	{
		return $this->hasMany(OrderBid::class);
	}

	public function getBids(): Collection
	{
		return $this->bids;
	}

	public function getContainer(): ShowMainDataContainer
	{
		if (!isset($this->container)) {
			$this->container = app(ShowMainDataContainer::class, ['order' => $this]);
		}
		return $this->container;
	}

}
