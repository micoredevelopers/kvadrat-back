<?php

namespace App\Models\Translate\Traits;

use Illuminate\Support\Arr;

trait TranslateAsStorageTrait
{
	/** @see \App\Models\Translate\Translate */

	private static $translates;

	public static function getTranslate($key, $asObject = false)
	{
		return $asObject
			? static::getTranslates(getCurrentLangId(), $key)
			: Arr::get(static::getTranslates(getCurrentLangId(), $key), 'value');
	}

	public static function getTranslates($langId, $key = false)
	{
		static::loadTranslatesOfLanguageIfNotLoaded($langId);

		if (!$key) {
			return Arr::get(static::$translates, $langId);
		}
		if (!static::isExistsTranslateByLangAndKey($langId, $key)) {
			return null;
		}

		return static::getTranslateByTranslateByLangAndKey($langId, $key);

	}

	private static function loadTranslatesOfLanguageIfNotLoaded($languageKey): void
	{
		if (!Arr::has(static::$translates, $languageKey)) {
			$translates = static::with('lang')->get()->keyBy('key');
			Arr::set(static::$translates, $languageKey, $translates);
		}
	}

	private static function isExistsTranslateByLangAndKey($langId, $key): bool
	{
		if (!\Arr::has(self::$translates, $langId)) {
			return false;
		}

		return \Arr::has(\Arr::get(self::$translates, $langId), $key);
	}

	private static function getTranslateByTranslateByLangAndKey($langId, $key): ?self
	{
		return (!self::isExistsTranslateByLangAndKey($langId, $key)) ? null : \Arr::get(\Arr::get(self::$translates, $langId), $key);
	}

}