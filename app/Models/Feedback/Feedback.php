<?php

namespace App\Models\Feedback;

use App\Models\Model;

/**
 * App\Models\Feedback\Feedback
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $type
 * @property string|null $fio
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $message
 * @property string|null $ip
 * @property string|null $referer
 * @property array|null $data
 * @property array|null $files
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereFiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereIp($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Feedback extends Model
{
	protected $guarded = ['id'];

	protected $casts = [
		'data'  => 'array',
		'files' => 'array',
	];

	public function setFiles(array $files): Feedback
	{
		$this->setAttribute('files', $files);
		return $this;
	}

	public function getPhoneDisplay(): string
	{
		return extractDigits($this->getAttribute('phone'));
	}

	public function isReport()
	{
		return $this->type === FeedbackContract::TYPE_REPORT;
	}

	public function getFiles()
	{
		return (array)$this->files;
	}
}
