<?php

namespace App\Models;



use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\ModelLang
 *
 * @property-read \App\Models\Language $language
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang query()
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @mixin \Eloquent
 */
class ModelLang extends Model
{
	use BelongsToLanguage;

	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = ['id'];

	public $incrementing = false;

	public $timestamps = false;

}



