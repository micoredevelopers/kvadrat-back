<?php

namespace App\Models;

use App\Models\Model;

class FranchModal extends Model
{
   protected $guarded = ['id'];
    /**
     * @var mixed
     */
    private $name;
    /**
     * @var mixed
     */
    private $phone;

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
