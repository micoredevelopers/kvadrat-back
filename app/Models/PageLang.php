<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;
use App\Traits\Models\TitleAttributeTrait;

/**
 * App\Models\PageLang
 *
 * @property int $page_id
 * @property int $language_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @property string|null $excerpt
 * @property string|null $sub_title
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubTitle($value)
 */
class PageLang extends Model
{
    use TitleAttributeTrait;
    use BelongsToLanguage;
	use EloquentMultipleForeignKeyUpdate;

	public $incrementing = false;

	public $timestamps = false;

    protected $table = 'pages_lang';

	protected $guarded = ['id'];

	protected $primaryKey = ['page_id', 'language_id'];

    public function page()
    {
        return $this->belongsTo(Page::class);
	}

    public function getNameForHeader($words = 1)
    {
        return $this->getTitleAttribute();
    }

}
