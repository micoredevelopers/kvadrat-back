<?php

	namespace App\Console\Commands\Dev\Test;

	use App\Helpers\Debug\LoggerHelper;
	use App\Models\Order\Order;
	use App\Models\Order\Review;
	use App\Repositories\Order\OrderRepository;
	use Faker\Generator;
	use Illuminate\Console\Command;

	class ReviewsPerfomanceSeederTest extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'test:seed:rev';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Seed test reviews to orders';
		/**
		 * @var Generator
		 */
		private $factory;
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var Order[]
		 */
		private $orders;

		public function __construct(Generator $factory, OrderRepository $orderRepository)
		{
			parent::__construct();
			$this->factory = $factory;
			$this->orderRepository = $orderRepository;
		}

		private function load()
		{
			$this->orders = $this->orderRepository->whereNotNull('performer_id')->get()->keyBy('id');
		}

		public function handle()
		{
			$this->load();
			$range = range(1, 100000);
			$batch = [];
			$per = 5000;
			$bar = $this->output->createProgressBar(count($range) / $per);
			foreach ($range as $item) {
				$order = $this->randomOrder();
				$batch[] = [
					$order->getForeignKey() => $order->getKey(),
					'from_id'               => $order->getAttribute('customer_id'),
					'to_id'                 => $order->getAttribute('performer_id'),
					'comment'               => 'asd',
					'rating'                => random_int(1, 5),
					'is_published'          => true,
				];
				if ($item % $per === 0) {
					$bar->advance();
					try{
						Review::insert($batch);
					} catch (\Throwable $e){
					    app(LoggerHelper::class)->error($e);
					}
					$batch = [];
				}
			}
			$bar->finish();

			return 1;
		}

		private function randomOrder(): Order
		{
			static $count;
			if (null === $count) {
				$count = $this->orders->count();
			}
			return $this->orders->get(random_int(1, $count));
		}
	}
