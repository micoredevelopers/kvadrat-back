<?php

	namespace App\Console\Commands\Dev\Test;

	use App\Repositories\CategoryRepository;
	use App\Repositories\PerformerRepository;
	use App\Repositories\PerformerSkillsRepository;
	use Illuminate\Console\Command;
	use Illuminate\Support\Collection;

	class PerformerSkillsPerfomanceSeederTest extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'test:seed:skills';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Command description';
		/**
		 * @var PerformerSkillsRepository
		 */
		private $skillsRepository;
		/**
		 * @var PerformerRepository
		 */
		private $performerRepository;
		/**
		 * @var CategoryRepository
		 */
		private $categoryRepository;
		/**
		 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
		 */
		private $categories;
		/**
		 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
		 */
		private $performers;

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct(
			PerformerSkillsRepository $skillsRepository,
			PerformerRepository $performerRepository,
			CategoryRepository $categoryRepository
		)
		{
			parent::__construct();
			$this->skillsRepository = $skillsRepository;
			$this->performerRepository = $performerRepository;
			$this->categoryRepository = $categoryRepository;
		}

		private function load()
		{
			$this->categories = $this->categoryRepository->all('id')->keyBy('id');
			$this->performers = $this->performerRepository->all('id')->keyBy('id');
		}

		public function handle(): int
		{
			$this->load();
			$inserts = [];
			$batch = 1000;
			$bar = $this->output->createProgressBar($this->performers->count() / $batch);
			foreach ($this->performers as $item => $performer) {
				foreach ($this->getCategories() as $category) {
					$inserts[] = [
						$performer->getForeignKey() => $performer->getKey(),
						$category->getForeignKey()  => $category->getKey(),
					];
				}
				if ($item % $batch === 0) {
					$bar->advance();
					$this->skillsRepository->insert($inserts);
					$inserts = [];
				}
			}
			$bar->finish();

			return 1;
		}

		public function getCategories(): Collection
		{
			static $count;
			if (null === $count) {
				$count = $this->categories->count();
			}
			return $this->categories->random(floor(random_int(1, $count)));
		}
	}
