<?php

namespace App\Console\Commands\Dev;

use Illuminate\Console\Command;

class ReplaceHtmlViewsToBlade extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'views:to-blade';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';


	public function handle()
	{
		try {
			$replaced = replaceFileExtensionInPath(resource_path('views/public/pages/dev'), '.html', '.blade.php');
			foreach ($replaced as $item) {
				$this->info($item);
			}
		} catch (\Throwable $e) {
			$this->warn($e->getMessage());
		}
		return 1;
	}
}
