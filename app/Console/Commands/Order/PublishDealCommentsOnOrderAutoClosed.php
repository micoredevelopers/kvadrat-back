<?php

namespace App\Console\Commands\Order;

use App\Models\Order\Review;
use App\Repositories\ReviewRepository;
use Illuminate\Console\Command;

class PublishDealCommentsOnOrderAutoClosed extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'deals:reviews-publish';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'When order close automatic by date, we need to publish reviews to deal for selected performer or to customer';
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(ReviewRepository $reviewRepository)
	{
		parent::__construct();
		$this->reviewRepository = $reviewRepository;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$reviews = $this->reviewRepository->findUnpublishedReviewsToClosedDeals();
		$reviews->map(function(Review $review){
			$review->getSetter()->publish();
			$this->reviewRepository->update([], $review);
		});
		return 1;
	}
}
