<?php

namespace App\Console\Commands\Order;

use App\Events\Order\BeforeOrderClosedEvent;
use App\Events\Order\OrderClosedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class CloseDealsByDate extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'deals:close-by-date';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
	}


	public function handle()
	{
		/** @var  $orders Collection | Order[] */
		$orders = $this->orderRepository->where('is_closed', false)->where('closes_at', '<=', now())->limit(50)->get();
		try {
			foreach ($orders as $order) {
				event(app(BeforeOrderClosedEvent::class, compact('order')));
				$order->getSetter()->close();
				if ($this->orderRepository->update([], $order)) {
					event(app(OrderClosedEvent::class, compact('order')));
				}
			}
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			throw $e;
		}
		return 1;
	}
}
