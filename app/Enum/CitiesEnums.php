<?php declare(strict_types=1);

namespace App\Enum;

final class CitiesEnums extends AbstractIntEnum
{
    public const ODESSA = 1;
    public const KHARKIV = 2;
    public const KYIV = 3;
    public const DNEPR = 4;

    public static $enums = [
        'cities.odessa' => self::ODESSA,
        'cities.kharkiv' => self::KHARKIV,
        'cities.kyiv' => self::KYIV,
        'cities.dnepr' => self::DNEPR,
    ];

    private $urls = [
        self::ODESSA => 'odessa',
        self::KHARKIV => 'kharkiv',
        self::KYIV => 'kyiv',
        self::DNEPR => 'dnepr',
    ];

    public function getUrl(): string
    {
        return $this->urls[$this->getKey()];
    }

    public function getTitle(): string
    {
        return __(parent::getTitle());
    }

}