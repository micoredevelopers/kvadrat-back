<?php

namespace App\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SortCriteria.
 *
 * @package namespace App\Criteria;
 */
class FaqSortCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param   Builder             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
	    $model = $model->orderBy('type')->orderBy('sort');
	    return $model;
    }
}
