<?php


namespace App\Platform\User;


use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthenticatedUserResolver
{
	public function getUser(): ?User
	{
		return Auth::guard('web')->user();
	}
}