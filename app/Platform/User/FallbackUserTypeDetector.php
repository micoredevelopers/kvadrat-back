<?php


namespace App\Platform\User;


use App\Models\User;
use App\Platform\User\Type\PlatformUserTypes;
use App\Repositories\UserMetaRepository;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;

class FallbackUserTypeDetector
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	private $lastTypeKey;
	/**
	 * @var PlatformUserTypes
	 */
	private $userTypes;

	public function __construct(User $user, UserMetaRepository $userMetaRepository, PlatformUserTypes $userTypes)
	{
		$this->user = $user;
		$this->userMetaRepository = $userMetaRepository;
		$this->lastTypeKey = UserMetaActions::LAST_SELECTED_USER_TYPE;
		$this->userTypes = $userTypes;
	}

	/**
	 * @return User
	 */
	private function getUser(): User
	{
		return $this->user;
	}

	public function getUserType(): ?string
	{
		$meta = $this->userMetaRepository->getByUserAndKey($this->getUser(), $this->lastTypeKey);
		if (!$meta){
			$meta = $this->userMetaRepository->getByUserAndKey($this->getUser(), UserMetaActions::USER_REGISTERED_AS);
		}
		/** @var  $type */
		$type = $this->userTypes->typeExists($meta->value ?? null) ? $meta->value : UserTypeContract::TYPE_CUSTOMER;
		return $type;
	}


}