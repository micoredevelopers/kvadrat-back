<?php


namespace App\Platform\User\Type;


use App\Platform\Contract\UserTypeContract;

class PlatformUserTypes
{

	public function getTypes(): array
	{
		return [
			UserTypeContract::TYPE_CUSTOMER  => UserTypeContract::TYPE_CUSTOMER,
			UserTypeContract::TYPE_PERFORMER => UserTypeContract::TYPE_PERFORMER,
		];
	}

	public function typeExists(?string $type): bool
	{
		if (!$type) {
			return false;
		}
		return $this->getTypes()[$type] ?? false;
	}

}