<?php


namespace App\Platform\User\Type;


use App\Platform\Contract\UserTypeContract;

class UserTypeHelper
{
	public static function isPerformer($type): bool
	{
		return UserTypeContract::TYPE_PERFORMER === $type || UserTypeContract::TYPE_PERFORMER_COMPANY === $type;
	}

	public static function isCustomer($type): bool
	{
		return UserTypeContract::TYPE_CUSTOMER === $type;
	}

	/**
	 * @param $type
	 * @return string
	 * @throws UserTypeNotFound
	 */
	public static function getType($type): string
	{
		if (self::isCustomer($type)) {
			return UserTypeContract::TYPE_CUSTOMER;
		}

		if (self::isPerformer($type)) {
			return UserTypeContract::TYPE_PERFORMER;
		}
		throw new UserTypeNotFound(sprintf('User type %s not exists', $type));
	}

	public static function revert($type): string
	{
		return self::isPerformer($type) ? UserTypeContract::TYPE_CUSTOMER : UserTypeContract::TYPE_PERFORMER;
	}
}