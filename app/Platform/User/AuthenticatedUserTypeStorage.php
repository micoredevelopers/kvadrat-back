<?php


namespace App\Platform\User;


class AuthenticatedUserTypeStorage
{
	private $key = 'platform.user.authenticated.type';

	public function getStoredUserType(): ?string
	{
		return session($this->makeKey());
	}

	public function storeUserType(string $type): void
	{
		$this->store($this->makeKey(), $type);
	}

	private function store($key, $value)
	{
		session()->put($key, $value);
	}

	private function makeKey(): string
	{
		return $this->key;
	}

}