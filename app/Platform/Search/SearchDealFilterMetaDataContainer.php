<?php


namespace App\Platform\Search;


use App\Models\User\UserMeta;

class SearchDealFilterMetaDataContainer
{
	protected $isSerialized = false;
	protected $isUnSerialized = false;
	/**
	 * @var UserMeta
	 */
	private $userMeta;

	public function __construct(UserMeta $userMeta)
	{
		$this->userMeta = $userMeta;
	}

	/**
	 * @param UserMeta $userMeta
	 * @return static
	 */
	public static function make(UserMeta $userMeta)
	{
		return app(__CLASS__, compact('userMeta'));
	}

	public function setOnPage($limit)
	{
		$this->setData('limit', $limit);
		return $this;
	}

	public function getOnPage()
	{
		return $this->getData('limit');
	}

	public function setCategories(array $categories): self
	{
		$this->setData('categories', $categories);
		return $this;
	}

	public function getCategories()
	{
		return $this->getData('categories');
	}

	public function setCity($cityId): self
	{
		$this->setData('city', $cityId);
		return $this;
	}

	public function getCity()
	{
		return $this->getData('city');
	}

	protected function setData($key, $value)
	{
		$this->unserialize();
		$metaValue = (array)$this->getUserMeta()->getValue();
		$metaValue[$key] = $value;
		$this->getUserMeta()->setValue($metaValue);
		return $this;
	}

	protected function getData($key)
	{
		$this->unserialize();
		return $this->getUserMeta()->getValue()[$key] ?? null;
	}

	public function serializeForSave(): self
	{
		if (!$this->isSerialized) {
			$this->userMeta->setValue(json_encode($this->userMeta->getValue(), JSON_THROW_ON_ERROR));
			$this->isUnSerialized = !($this->isSerialized = true);
		}
		return $this;
	}

	public function unserialize(): self
	{
		if (!$this->isUnSerialized) {
			$this->userMeta->setValue(json_decode($this->userMeta->getValue(), true, 512, JSON_THROW_ON_ERROR));
			$this->isSerialized = !($this->isUnSerialized = true);
		}
		return $this;
	}

	/**
	 * @return UserMeta
	 */
	public function getUserMeta(): UserMeta
	{
		return $this->userMeta;
	}

}