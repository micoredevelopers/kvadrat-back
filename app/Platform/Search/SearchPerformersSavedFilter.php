<?php


namespace App\Platform\Search;


use App\Helpers\Debug\LoggerHelper;
use App\Models\User;
use App\Platform\Contract\UserMetaActions;
use App\Repositories\UserMetaRepository;
use Illuminate\Http\Request;

class SearchPerformersSavedFilter extends SearchDealsSavedFilter
{

	/**
	 * @param User $user
	 * @return self
	 */
	public static function make(User $user)
	{
		return app(__CLASS__, compact('user'));
	}

	protected function getKeyMeta():string
	{
		return UserMetaActions::USER_FILTERS_PERFORMERS;
	}
}