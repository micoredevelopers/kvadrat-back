<?php


namespace App\Platform\Search;


use App\Helpers\Debug\LoggerHelper;
use App\Models\User;
use App\Platform\Contract\UserMetaActions;
use App\Repositories\UserMetaRepository;
use Illuminate\Http\Request;

class SearchDealsSavedFilter
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	public function __construct(User $user, UserMetaRepository $userMetaRepository)
	{
		$this->user = $user;
		$this->userMetaRepository = $userMetaRepository;
	}

	/**
	 * @param User $user
	 * @return self
	 */
	public static function make(User $user)
	{
		return app(__CLASS__, compact('user'));
	}

	public function syncWithRequest(Request $request)
	{
		if ($this->supportsRenewFilters($request)) {
			$container = SearchDealFilterMetaDataContainer::make($this->getMeta());
			$container->setCity($request->get('city', null));
			if ($request->get('onpage')) {
				$container->setOnPage($request->get('onpage'));
			}
			$container->setCategories($request->get('category', []));
			$container->serializeForSave();
			try{
				$this->userMetaRepository->update([], $container->getUserMeta());
			} catch (\Throwable $e){
				throwIfDev($e);
			    app(LoggerHelper::class)->error($e);
			}
			return $request;
		}
		return $this->fillRequest($request);
	}

	public function fillRequest(Request $request)
	{
		$container = SearchDealFilterMetaDataContainer::make($this->getMeta());
		if ($container->getCity()) {
			$request->merge(['city' => $container->getCity()]);
		}
		if ($container->getOnPage()) {
			$request->merge(['onpage' => $container->getOnPage()]);
		}
		if ($container->getCategories()) {
			$request->merge(['category' => $container->getCategories()]);
		}
		return $request;
	}

	protected function supportsRenewFilters(Request $request): bool
	{
		return $request->has('search');
	}

	protected function getMeta()
	{
		$meta = $this->userMetaRepository->getByUserAndKey($this->user, $this->getKeyMeta());
		if ($meta) {
			return $meta;
		}
		return $this->userMetaRepository->getMetaBuilder()->setKey($this->getKeyMeta())->setValue(json_encode([]))
			->setUser($this->user)->build()
			;
	}

	protected function getKeyMeta():string
	{
		return UserMetaActions::USER_FILTERS_DEALS;
	}
}