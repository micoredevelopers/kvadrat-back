<?php


namespace App\Platform\Cabinet\Service;


use App\Platform\Contract\UserMetaActions;
use App\Repositories\UserMetaRepository;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;

/**
 * Class PerformerTypeService
 * @package App\Models\User\Cabinet\Service
 * Get performer type, is physical face, or company
 */
class PerformerTypeService implements CabinetUserTypeServiceContract
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var UserMetaRepository
	 */
	private $userMetaRepository;

	public function __construct(User $user, UserMetaRepository $userMetaRepository)
	{
		$this->user = $user;
		$this->userMetaRepository = $userMetaRepository;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	private function getPerformerType(): ?string
	{
		if ($this->user->relationLoaded('metas')) {
			$type = $this->user->metas->where('key', UserMetaActions::USER_REGISTERED_AS)->first();
		} else {
			$type = $this->userMetaRepository->getByUserAndKey($this->getUser(), UserMetaActions::USER_REGISTERED_AS);
		}
		if (!$type) {
			return null;
		}
		return (string)$type->getAttribute('value');
	}

	public function isCompany(): bool
	{
		return $this->getPerformerType() === UserTypeContract::TYPE_PERFORMER_COMPANY;
	}

	public function isPhysicFace(): bool
	{
		return !$this->isCompany();
	}

	public function getType(): string
	{
		return (string)$this->getPerformerType();
	}
}