<?php


namespace App\Platform\Cabinet\Service;


use App\Models\User;

interface CabinetUserTypeServiceContract
{
	public function getUser(): User;

	public function getType():string;
}