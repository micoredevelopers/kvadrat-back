<?php


namespace App\Platform\Cabinet\Display\Performer;


use App\Helpers\Date\YearName\YearNameHelper;
use App\Helpers\User\Company\CompanyEmployersAmountHelper;
use App\Models\User;
use App\Platform\Cabinet\Display\DisabledProfileText;
use App\Platform\Cabinet\Display\DisplayUserTypeContainerContract;
use App\Platform\Cabinet\Display\Traits\GeneralMethodsUserContainer;
use App\Platform\Cabinet\Service\PerformerTypeService;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Reviews\RatingPercentageTrait;
use App\Platform\Route\PlatformRoutes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class DisplayPerformerContainer implements DisplayUserTypeContainerContract
{
	use GeneralMethodsUserContainer;

	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var PerformerTypeService
	 */
	private $performerTypeService;
	/**
	 * @var YearNameHelper
	 */
	private $yearNameHelper;
	/**
	 * @var DisabledProfileText
	 */
	private $disabledProfileText;

	public function __construct(User $user, YearNameHelper $yearNameHelper, DisabledProfileText $disabledProfileText)
	{
		$this->user = $user;
		$this->performerTypeService = app(PerformerTypeService::class, ['user' => $user]);
		$this->yearNameHelper = $yearNameHelper;
		$this->disabledProfileText = $disabledProfileText;
	}

	public function getAvatar(): string
	{
		return (string)getPathToImage($this->getUser()->getAvatar() ?? '', asset('images/ProfileDefault.svg'));
	}


	public function isProfileEnabled(): bool
	{
		return (bool)$this->getUser()->getAttribute('profile_enabled');
	}

	public function isCompany(): bool
	{
		return $this->getPerformerTypeService()->isCompany();
	}

	public function getProfession(): string
	{
		return (string)$this->getUser()->getAttribute('profession');
	}

	public function getCompanyEmployeeAmount(): string
	{
		return (string)CompanyEmployersAmountHelper::getByKey($this->getUser()->getAttribute('company_workers'));
	}

	public function getAbout(): string
	{
		return (string)$this->getUser()->getAttribute('about');
	}

	public function getShortAbout()
	{
		return nl2br(Str::limit($this->getAbout(), 150));
	}


	public function skillChecked($categoryId): bool
	{
		return (bool)$this->getUser()->getSkills()->where('id', $categoryId)->first();
	}

	/**
	 * @return PerformerTypeService
	 */
	public function getPerformerTypeService(): PerformerTypeService
	{
		return $this->performerTypeService;
	}

	public function getRating(): float
	{
		return (float)$this->getUser()->getAttribute('rating');
	}

	public function getUserType(): string
	{
		return UserTypeContract::TYPE_PERFORMER;
	}

	public function getReviewsCount(): int
	{
		return (int)$this->getUser()->getAttribute('reviews_count');
	}


	public function getPublicProfileLink()
	{
		return route(PlatformRoutes::PERFORMERS_SHOW, $this->getSlug());
	}

}