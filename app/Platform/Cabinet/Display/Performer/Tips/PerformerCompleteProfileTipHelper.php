<?php


namespace App\Platform\Cabinet\Display\Performer\Tips;


use App\Platform\Cabinet\Route\CabinetDestinationLinks;
use App\Platform\Cabinet\Route\LinkCabinetByUserType;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\PerformerSkillsRepository;
use App\Repositories\PortfolioRepository;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use Illuminate\Support\Facades\Route;

class PerformerCompleteProfileTipHelper
{
	/**
	 * @var PortfolioRepository
	 */
	private $portfolioRepository;
	/**
	 * @var DisplayPerformerContainer
	 */
	private $performer;
	/**
	 * @var PerformerSkillsRepository
	 */
	private $performerSkillsRepository;

	public function __construct(
		DisplayPerformerContainer $performer,
		PortfolioRepository $portfolioRepository,
		PerformerSkillsRepository $performerSkillsRepository
	)
	{
		$this->portfolioRepository = $portfolioRepository;
		$this->performer = $performer;
		$this->performerSkillsRepository = $performerSkillsRepository;
	}

	public function needShowTip(): bool
	{
		return !($this->isGeneralDataCompleted() && $this->isPortfolioCompleted());
	}

	public function isGeneralDataCompleted(): bool
	{
		return $this->isPersonalDataCompleted() && $this->isSkillsCompleted();
	}

	public function isPersonalDataCompleted(): bool
	{
		$personal = [
			$this->performer->getName(),
			$this->performer->getSurname()
		];
		$company = [$this->performer->getName(),];
		$fields = [
			$this->performer->getCity(),
			$this->performer->getDateBirth(),
		];
		$fields = array_merge($fields, $this->performer->isCompany() ? $company : $personal);
		$count = count($fields);
		$fields = array_filter($fields, 'strlen');
		return $count === count($fields);
	}

	public function isSkillsCompleted(): bool
	{
		return (bool)$this->performerSkillsRepository
			->addPublicCriteriaToQuery()
			->addUserCriteria($this->performer->getUser())
			->count()
			;
	}

	public function isPortfolioCompleted(): bool
	{
		static $res;
		if (null !== $res) {
			return $res;
		}
		$user = $this->performer->getUser();
		$res = (bool)$this->portfolioRepository->addPublicCriteriaToQuery()->addUserCriteria($user)->count();
		return $res;
	}

	public function getLinksItems(): array
	{
		$links = [];
		if (!$this->isPortfolioCompleted()) {
			$links[] = ['name' => 'портфолио', 'url' => LinkCabinetByUserType::getLinkByDestination(UserTypeContract::TYPE_PERFORMER, CabinetDestinationLinks::PORTFOLIO),];
		}
		if (!$this->isPersonalDataCompleted()) {
			$links[] = ['name' => 'общую информацию', 'url' => LinkCabinetByUserType::getLinkByDestination(UserTypeContract::TYPE_PERFORMER, CabinetDestinationLinks::PERSONAL),];
		}
		if (!$this->isSkillsCompleted()) {
			$route = LinkCabinetByUserType::getRouteByDestination(UserTypeContract::TYPE_PERFORMER, CabinetDestinationLinks::PERSONAL);
			$links[] = ['name' => 'навыки', 'url' => Route::is($route) ? '#skills' : route($route) . '#skills',];
		}
		return $links;
	}
}