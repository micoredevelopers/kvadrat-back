<?php


namespace App\Platform\Cabinet\Display\Customer;


use App\Helpers\Date\YearName\YearNameHelper;
use App\Models\User;
use App\Platform\Cabinet\Display\DisabledProfileText;
use App\Platform\Cabinet\Display\DisplayUserTypeContainerContract;
use App\Platform\Cabinet\Display\Traits\GeneralMethodsUserContainer;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Reviews\RatingPercentageTrait;
use App\Platform\Route\PlatformRoutes;
use DateTimeInterface;
use Illuminate\Support\Carbon;

class DisplayCustomerContainer implements DisplayUserTypeContainerContract
{
	use GeneralMethodsUserContainer;

	/**
	 * @var \App\Models\User
	 */
	private $user;
	/**
	 * @var DisabledProfileText
	 */
	private $disabledProfileText;
	/**
	 * @var YearNameHelper
	 */
	private $yearNameHelper;

	public function __construct(User $user, YearNameHelper $yearNameHelper, DisabledProfileText $disabledProfileText)
	{
		$this->setUser($user);
		$this->yearNameHelper = $yearNameHelper;
		$this->disabledProfileText = $disabledProfileText;
	}

	public function getAvatar(): string
	{
		return (string)getPathToImage($this->getUser()->getAvatar('customer_avatar'), asset('images/ProfileDefault.svg'));
	}

	public function isProfileEnabled(): bool
	{
		return (bool)$this->getUser()->getAttribute('customer_profile_enabled');
	}

	public function getAbout(): string
	{
		return (string)$this->getUser()->getAttribute('customer_about');
	}

	public function setUser(User $user): self
	{
		$this->user = $user;
		return $this;
	}

	public function getRating(): float
	{
		return (float)$this->getUser()->getAttribute('customer_rating');
	}

	public function getUserType(): string
	{
		return UserTypeContract::TYPE_CUSTOMER;
	}

	public function getReviewsCount(): int
	{
		return (int)$this->getUser()->getAttribute('customer_reviews_count');
	}

	public function getPublicProfileLink()
	{
		return route(PlatformRoutes::CUSTOMERS_SHOW, $this->getSlug());
	}
}