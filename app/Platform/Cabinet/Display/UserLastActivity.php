<?php


namespace App\Platform\Cabinet\Display;


use App\Helpers\Date\DateNameHelper;
use App\Models\User;
use App\Platform\Analytics\UserLastSeenAt;
use Illuminate\Support\Carbon;

class UserLastActivity
{
	/**
	 * @var UserLastSeenAt
	 */
	private $lastSeenAt;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user, UserLastSeenAt $lastSeenAt)
	{
		$this->lastSeenAt = $lastSeenAt;
		$this->user = $user;
	}

	public function isOnline(): bool
	{
		$lastSeen = $this->lastSeenAt->getLastSeen($this->user->getKey());
		if (!$lastSeen) {
			return false;
		}
		return (5 > $lastSeen->diffInMinutes(now()));
	}

	public function getActivityText(): string
	{
		$lastSeen = $this->lastSeenAt->getLastSeen($this->user->getKey());
		if (!$lastSeen) {
			$lastSeen = $this->user->getDateLastSeen();
		}
		if (!$lastSeen) {
		    return '';
		}

		return $this->getLastWasAt($lastSeen);
	}

	protected function getLastWasAt(Carbon $lastSeen): string
	{
		return DateNameHelper::make($lastSeen)->setWithAgo(true)->default();
	}

	public function getOnlineText()
	{
		return 'В сети';
	}


}