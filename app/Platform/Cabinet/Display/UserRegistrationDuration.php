<?php


namespace App\Platform\Cabinet\Display;


use App\Helpers\Date\DaysName\DaysNameHelper;
use App\Helpers\Date\MonthName\MonthNameHelper;
use App\Helpers\Date\YearName\YearNameHelper;
use App\Models\User;

class UserRegistrationDuration
{

	/**
	 * @var DaysNameHelper
	 */
	private $daysNameHelper;
	/**
	 * @var MonthNameHelper
	 */
	private $monthNameHelper;
	/**
	 * @var YearNameHelper
	 */
	private $yearNameHelper;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(
		User $user,
		DaysNameHelper $daysNameHelper,
		MonthNameHelper $monthNameHelper,
		YearNameHelper $yearNameHelper
	)
	{
		$this->user = $user;
		$this->daysNameHelper = $daysNameHelper;
		$this->monthNameHelper = $monthNameHelper;
		$this->yearNameHelper = $yearNameHelper;
	}


	public function getText(): string
	{
		$registeredAt = $this->user->getCreatedAt();
		if (!$registeredAt) {
			return '';
		}
		$registeredAt = $registeredAt->subDay();
		$now = now();
		$diffYears = $registeredAt->diffInYears($now);
		$diffMonths = $registeredAt->diffInMonths($now);
		$diffDays = $registeredAt->diffInDays($now);
		if ($diffYears) {
			return $diffYears . ' ' . $this->yearNameHelper->getName($diffYears);
		}
		if ($diffMonths) {
			return $diffMonths . ' ' . $this->monthNameHelper->getName($diffMonths);
		}
		return $diffDays . ' ' . $this->daysNameHelper->getName($diffDays);
	}


}