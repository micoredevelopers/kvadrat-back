<?php


namespace App\Platform\Cabinet\Display;


use App\Models\User;
use App\Platform\Cabinet\Service\PerformerTypeService;
use DateTimeInterface;
use Illuminate\Support\Carbon;

interface DisplayUserTypeContainerContract
{
	public function getSlug();

	public function getPublicProfileLink();

	public function getAvatar(): string;

	public function getFullName(): string;

	public function getName(): string;

	public function getSurname(): string;

	public function getCity(): int;

	public function getGender(): string;

	public function getEmail(): string;

	public function getPhone(): string;

	public function getPhoneSecond(): string;

	public function getDateBirth(): ?DateTimeInterface;

	public function isProfileEnabled(): bool;

	public function getAbout(): string;

	public function getRating(): float;

	public function getLastSeenAt(): ?Carbon;

	public function getUser(): User;

	public function getUserType(): string;

	public function getRatingPercents($rating): float;

	public function getReviewsCount():int;
}