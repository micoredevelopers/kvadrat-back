<?php


namespace App\Platform\Cabinet\Display;


use Illuminate\Support\Carbon;

class DisabledProfileText
{
	public function getPerformerDisabledAtText(Carbon $carbon): string
	{
		$dateFormat = now()->diffInYears($carbon) > 0 ? '%e %B %Y, в %R' : '%e %B, в %R';
		return $this->getProfileIsActive(false) . ' ' . $carbon->formatLocalized($dateFormat);
	}

	public function getCustomerDisabledAtText(Carbon $carbon): string
	{
		return $this->getProfileIsActive(false) . ' ' . $carbon->formatLocalized('%e %B, в %R');
	}

	public function getToggleTextBtn(bool $profileEnabled)
	{
		return $profileEnabled
			? getTranslate('cabinet.profile.disable', 'Отключить профиль')
			: getTranslate('cabinet.profile.enable', 'Включить профиль');
	}

	public function getToggleTextConfirm(bool $profileEnabled, ?Carbon $carbon = null)
	{
		return $profileEnabled
			? getTranslate('cabinet.profile.disable.confirm', 'Отключить профиль?')
			: getTranslate('cabinet.profile.enable.confirm', 'Включить профиль?');
	}

	public function getToggleTextHeadTitle(bool $profileEnabled)
	{
		return $profileEnabled
			? getTranslate('cabinet.profile.disable.title', '')
			: getTranslate('cabinet.profile.enable.title', '');
	}

	public function getProfileIsActive(bool $profileEnabled)
	{
		return $profileEnabled
			? getTranslate('cabinet.profile.is-enabled', 'Профиль активен')
			: getTranslate('cabinet.profile.is-disabled', 'Профиль отключен');
	}
}