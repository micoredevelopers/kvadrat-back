<?php


namespace App\Platform\Cabinet\Display;


use App\Platform\User\AuthenticatedUserResolver;
use App\Platform\User\AuthenticatedUserType;

class AuthenticatedUserTypeContainerResolver
{
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var AuthenticatedUserType
	 */
	private $authenticatedUserType;

	public function __construct(AuthenticatedUserResolver $userResolver, AuthenticatedUserType $authenticatedUserType) {

		$this->userResolver = $userResolver;
		$this->authenticatedUserType = $authenticatedUserType;
	}

	public function create(): DisplayUserTypeContainerContract
	{
		return DisplayUserTypeContainerStaticFactory::create($this->authenticatedUserType->getUserType(), $this->userResolver->getUser());
	}
}