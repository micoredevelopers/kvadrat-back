<?php


namespace App\Platform\Cabinet\Display;


use App\Platform\Cabinet\Display\DisabledProfileText;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Contract\UserMetaActions;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\UserMetaRepository;
use App\Models\User;
use Illuminate\Support\Carbon;

class UserMetaContainer
{
	/**
	 * @var DisplayUserTypeContainerContract
	 */
	private $userContainer;
	/**
	 * @var UserMetaRepository
	 */
	private $metaRepository;
	/**
	 * @var DisabledProfileText
	 */
	private $disabledProfileText;

	public function __construct(DisplayUserTypeContainerContract $userContainer, UserMetaRepository $metaRepository, DisabledProfileText $disabledProfileText)
	{
		$this->userContainer = $userContainer;
		$this->metaRepository = $metaRepository;
		$this->disabledProfileText = $disabledProfileText;
	}

	protected function getUserContainer(): DisplayUserTypeContainerContract
	{
		return $this->userContainer;
	}

	protected function getUser(): User
	{
		return $this->userContainer->getUser();
	}

	protected function getUserMeta(string $key = null)
	{
		return $key ? $this->metaRepository->getByUser($this->getUser())->get($key) : $this->metaRepository->getByUser($this->getUser());
	}

	public function getDisabledAtDate(): ?Carbon
	{
		$key = (UserTypeHelper::isPerformer($this->userContainer->getUserType()))
			? UserMetaActions::PERFORMER_DISABLED_AT
			: UserMetaActions::CUSTOMER_DISABLED_AT;
		/** @var  $date User\UserMeta*/
		$date = $this->getUserMeta($key);
		return $date ? getDateCarbon($date->getValue()) : null;
	}

	public function getProfileDisabledAtText(): string
	{
		$date = $this->getDisabledAtDate();
		if (!$date) {
			return '';
		}
		return $this->disabledProfileText->getPerformerDisabledAtText($date);
	}

	public function getToggleTextConfirm()
	{
		return $this->disabledProfileText->getToggleTextConfirm($this->userContainer->isProfileEnabled());
	}

	public function getToggleTextHeadTitle()
	{
		return $this->disabledProfileText->getToggleTextHeadTitle($this->userContainer->isProfileEnabled());
	}

	public function getToggleTextBtn()
	{
		return $this->disabledProfileText->getToggleTextBtn($this->userContainer->isProfileEnabled());
	}
}