<?php


namespace App\Platform\Cabinet\Display;


use App\Models\User;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Contract\UserTypeContract;

class DisplayUserTypeContainerStaticFactory
{
	public static function create(string $type, User $user): DisplayUserTypeContainerContract
	{
		switch ($type) {
			case UserTypeContract::TYPE_PERFORMER:
			case UserTypeContract::TYPE_PERFORMER_COMPANY:
				return app(DisplayPerformerContainer::class, ['user' => $user]);
			default:
				return app(DisplayCustomerContainer::class, ['user' => $user]);
		}
	}
}