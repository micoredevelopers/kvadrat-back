<?php


	namespace App\Platform\Cabinet\Display\Traits;


	use App\Models\User;
	use App\Platform\Cabinet\Display\DisabledProfileText;
	use App\Platform\Reviews\RatingPercentageTrait;
	use DateTimeInterface;
	use Illuminate\Support\Carbon;

	trait GeneralMethodsUserContainer
	{
		use RatingPercentageTrait;

		public function getAge()
		{
			return getDateCarbon($this->getUser()->getAttribute('date_birth'))->diffInYears(now());
		}

		public function getAgeText(): string
		{
			return $this->getAge() . ' ' . $this->yearNameHelper->getName($this->getAge());
		}

		public function getToggleActiveProfileText(): string
		{
			return $this->disabledProfileText->getToggleText($this->isProfileEnabled());
		}

		public function getDisableProfileText(): DisabledProfileText
		{
			return $this->disabledProfileText;
		}

		public function getFullName(): string
		{
			return $this->getName() . ' ' . $this->getSurname();
		}

		public function getName(): string
		{
			return $this->getUser()->getName();
		}

		public function getSurname(): string
		{
			return (string)$this->getUser()->getAttribute('surname');
		}

		public function getCity(): int
		{
			return (int)$this->getUser()->getAttribute('city_id');
		}

		public function getGender(): string
		{
			return (string)$this->getUser()->getAttribute('gender');
		}

		public function getPhone(): string
		{
			return (string)$this->getUser()->getAttribute('phone');
		}

		public function getEmail(): string
		{
			return (string)$this->getUser()->getAttribute('email');
		}

		public function getPhoneSecond(): string
		{
			return (string)$this->getUser()->getAttribute('phone_second');
		}

		public function getDateBirth(): ?DateTimeInterface
		{
			return isDateValid($this->getUser()->getAttribute('date_birth')) ?  getDateCarbon($this->getUser()->getAttribute('date_birth')) : null;
		}

		/**
		 * @return User
		 */
		public function getUser(): User
		{
			return $this->user;
		}

		public function getLastSeenAt(): ?Carbon
		{
			return $this->getUser()->getDateLastSeen();
		}

		public function getSlug()
		{
			return $this->getUser()->getKey();
		}
	}