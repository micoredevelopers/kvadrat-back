<?php


namespace App\Platform\Cabinet\Route;


interface CabinetDestinationLinks
{
	public const PERSONAL = 'PERSONAL';
	public const PASSWORD = 'PASSWORD';
	public const PORTFOLIO = 'PORTFOLIO';
	public const REVIEWS = 'REVIEWS';
	public const PUBLIC_PROFILE = 'PUBLIC_PROFILE';
}