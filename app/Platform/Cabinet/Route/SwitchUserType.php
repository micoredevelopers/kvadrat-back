<?php


namespace App\Platform\Cabinet\Route;


use Illuminate\Support\Facades\Route;

class SwitchUserType
{

	/**
	 * @param $currentUserType
	 * @param $newUserType
	 * @return string|null
	 * if return null - no need redirect, just back
	 */
	public function getSwitchLink($currentUserType, $newUserType): ?string
	{
		//роуты которым важно какой сейчас тип пользователя
		$in = [
			LinkCabinetByUserType::getRouteByDestination($currentUserType, CabinetDestinationLinks::PERSONAL),
			LinkCabinetByUserType::getRouteByDestination($currentUserType, CabinetDestinationLinks::REVIEWS),
		];
		$prevRoute = app('router')->getRoutes()->match(app('request')->create(url()->previous()));
		if (!$prevRoute) {
			return null;
		}
		// если сейчас находимся на роутах где важен тип пользователя
		if (in_array($prevRoute->getName(), $in, true)) {
			return LinkCabinetByUserType::getMirroredLinkByCurrentRoute($currentUserType, $newUserType, $prevRoute->getName());
		}
		return null;
	}
}