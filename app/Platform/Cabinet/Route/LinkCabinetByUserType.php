<?php


namespace App\Platform\Cabinet\Route;


use App\Platform\Contract\UserTypeContract;
use App\Platform\Route\PlatformRoutes;

class LinkCabinetByUserType
{
	public static function getRouteByDestination(string $userType, string $destination): ?string
	{
		$matches = self::getList();
		return $matches[$userType][$destination] ?? null;
	}

	public static function getLinkByDestination(string $userType, string $destination): ?string
	{
		try {
			return route(self::getRouteByDestination($userType, $destination));
		} catch (\Throwable $e) {
			return null;
		}
	}

	public static function getList(): array
	{
		return [
			UserTypeContract::TYPE_PERFORMER => [
				CabinetDestinationLinks::PERSONAL       => PlatformRoutes::CABINET_PERFORMER_PERSONAL,
				CabinetDestinationLinks::PORTFOLIO      => PlatformRoutes::CABINET_PERFORMER_PORTFOLIO,
				CabinetDestinationLinks::PASSWORD       => PlatformRoutes::CABINET_PERFORMER_PASSWORD,
				CabinetDestinationLinks::REVIEWS        => PlatformRoutes::CABINET_PERFORMER_REVIEWS,
				CabinetDestinationLinks::PUBLIC_PROFILE => PlatformRoutes::PERFORMERS_SHOW,
			],
			UserTypeContract::TYPE_CUSTOMER  => [
				CabinetDestinationLinks::PERSONAL       => PlatformRoutes::CABINET_CUSTOMER_PERSONAL,
				CabinetDestinationLinks::PASSWORD       => PlatformRoutes::CABINET_CUSTOMER_PASSWORD,
				CabinetDestinationLinks::REVIEWS        => PlatformRoutes::CABINET_CUSTOMER_REVIEWS,
				CabinetDestinationLinks::PUBLIC_PROFILE => PlatformRoutes::CUSTOMERS_SHOW,
			],
		];
	}

	public static function getListByType(string $type): array
	{
		return self::getList()[$type] ?? [];
	}

	public static function getMirroredLinkByCurrentRoute(string $currentType, string $needType, string $currentRoute): ?string
	{
		$destination = array_flip(self::getListByType($currentType))[$currentRoute];
		return self::getLinkByDestination($needType, $destination);
	}
}