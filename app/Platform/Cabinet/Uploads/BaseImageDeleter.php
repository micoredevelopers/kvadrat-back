<?php


namespace App\Platform\Cabinet\Uploads;


abstract class BaseImageDeleter
{
    public static function delete($path)
    {
        if (!self::supports($path)) {
            return;
        }
        if (storageFileExists($path)) {
            storageDelete($path);
        }
        if (storageFileExists(imgPathOriginal($path))) {
            storageDelete(imgPathOriginal($path));
        }
    }

    public static function supports($path): bool
    {
        return (storageFileExists($path) || storageFileExists(imgPathOriginal($path)));
    }

}