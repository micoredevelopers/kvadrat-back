<?php


namespace App\Platform\Cabinet\Uploads;

use App\Models\Image;

class PerformerPortfolioImagesDeleter extends BaseImageDeleter
{
    /**
     * @param iterable | Image[] $imgs
     */
public static function deleteImgCollection(iterable $imgs){
    foreach ($imgs as $img){
        self::delete($img->getImage());
    }
}


}