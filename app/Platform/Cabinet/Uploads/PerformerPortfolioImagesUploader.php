<?php


namespace App\Platform\Cabinet\Uploads;


use App\Helpers\Media\ImageSaver;
use App\Models\User;
use Illuminate\Http\UploadedFile;

class PerformerPortfolioImagesUploader
{
	/**
	 * @var UploadedFile[]
	 */
	private $uploadedFiles;
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;

	private $folderName = 'users/portfolio';
	/**
	 * PerformerPortfolioImagesUploader constructor.
	 * @param UploadedFile[] $uploadedFiles
	 * @param ImageSaver $imageSaver
	 */
	public function __construct(array $uploadedFiles, ImageSaver $imageSaver)
	{
		$this->uploadedFiles = $uploadedFiles;
		$this->imageSaver = $imageSaver;
	}

	public function upload(): array
	{
		$this->imageSaver->setFolderName($this->folderName)
			->setWithThumbnail(false)
			->setResizeTo(1200)
		;
		$paths = [];
		foreach ($this->uploadedFiles as $uploadedFile) {
			$paths[] = $this->imageSaver->saveFromUploadedFile($uploadedFile);
		}
		return $paths;
	}

	/**
	 * @param string $folderName
	 */
	public function setFolderName(string $folderName): void
	{
		$this->folderName = $folderName;
	}

}