<?php


namespace App\Platform\Cabinet\Uploads\Avatar;


use App\Helpers\Media\ImageSaver;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;
use Illuminate\Http\UploadedFile;

class AvatarUploader
{
	/**
	 * @var UploadedFile
	 */
	private $file;
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(UploadedFile $file, ImageSaver $imageSaver, User $user)
	{
		$this->file = $file;
		$this->imageSaver = $imageSaver;
		$this->user = $user;
	}

	public function uploadDefault(): string
	{
		return $this->uploadPerformer();
	}

	public function uploadPerformer(): string
	{
		$this->defaults()->imageSaver->setFolderName($this->user->getTable() . DIRECTORY_SEPARATOR . UserTypeContract::TYPE_PERFORMER);
		return $this->imageSaver->saveFromUploadedFile($this->file);
	}

	public function uploadCustomer(): string
	{
		$this->defaults()->imageSaver->setFolderName($this->user->getTable() . DIRECTORY_SEPARATOR . UserTypeContract::TYPE_CUSTOMER);
		return $this->imageSaver->saveFromUploadedFile($this->file);
	}

	protected function defaults(): self
	{
		$this->imageSaver
			->setThumbnailSizes(290, 290)
			->setResizeTo(1200)
		;
		return $this;
	}
}