<?php


namespace App\Platform\Reviews;


trait RatingPercentageTrait
{

	public function getRatingPercents($rating): float
	{
		return RatingHelper::getPercentageRating((float)$rating);
	}
}