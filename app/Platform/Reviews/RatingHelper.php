<?php


namespace App\Platform\Reviews;


class RatingHelper
{
	public const MAX_RATE = 5;

	public static function getPercentageRating(float $rating)
	{
		return (100 / self::MAX_RATE) * $rating;
	}

}