<?php


namespace App\Platform\Reviews;


use App\Models\Order\Review;
use App\Platform\Cabinet\Display\DisplayUserTypeContainerContract;
use App\Platform\Contract\UserTypeContract;

class ReviewDisplayContainer
{
    use RatingPercentageTrait;

    private $commentOwnerContainer;

    /**
     * @var Review
     */
    private $review;

    private $user;

    public function __construct(Review $review, ?DisplayUserTypeContainerContract $user = null)
    {
        $this->review = $review;
        $this->user = $user;
    }

    /**
     * @return Review
     */
    public function getEntity(): Review
    {
        return $this->review;
    }

    public function getOrder()
    {
        return $this->getEntity()->getOrder();
    }

    public function getUserContainer(): DisplayUserTypeContainerContract
    {
        if (is_null($this->user)) {
            $this->user = $this->getCommentOwner();
        }
        return $this->user;
    }

    public function getAbout()
    {
        return $this->getAttribute('about') ?: UserTypeContract::TYPE_PERFORMER;
    }

    public function isAboutCustomer(): bool
    {
        return $this->getAbout() === UserTypeContract::TYPE_CUSTOMER;
    }

    public function isAboutPerformer(): bool
    {
        return $this->getAbout() === UserTypeContract::TYPE_PERFORMER;
    }

    public function getCommentOwner(): DisplayUserTypeContainerContract
    {
        if (is_null($this->commentOwnerContainer)) {
            $user = $this->getEntity()->getReviewFrom();
            $this->commentOwnerContainer = $this->isAboutCustomer() ? $user->getPerformerContainer() : $user->getCustomerContainer();
        }
        return $this->commentOwnerContainer;
    }

    public function getRating(): float
    {
        return $this->review->getRating();
    }


    public function isRatingPositive(): bool
    {
        return $this->getRating() > 2;
    }

    public function getOrderTitle()
    {
        return $this->getOrder()->getTitle();
    }

    public function getComment()
    {
        return $this->getEntity()->getComment();
    }

    public function getNameCommentOwner(): string
    {
        return $this->getCommentOwner()->getFullName();
    }

    public function getAvatarCommentOwner(): string
    {
        return $this->getCommentOwner()->getAvatar();
    }

    public function getOrderPrice()
    {
        return $this->getOrder()->price;
    }

    public function getReviewImages()
    {
        return $this->getEntity()->getImages();
    }

    public function getReviewDateLocalized()
    {
        return $this->getEntity()->getCreatedAt() ? $this->getEntity()->getCreatedAt()->formatLocalized('%e %B %H:%M') : '';
    }

    public function isPublished()
    {
        return (bool)(int)$this->getEntity()->getAttribute('is_published');
    }

    public function getSubRatingAdeqate(): int
    {
        return (int)$this->getAttribute('adequate_rating');
    }

    public function getSubRatingLegibility(): int
    {
        return (int)$this->getAttribute('legibility_rating');
    }

    public function getSubRatingSuggestionsReacts(): int
    {
        return (int)$this->getAttribute('suggestions_reacts');
    }

    public function getSubRatingPunctualityRating(): int
    {
        return (int)$this->getAttribute('punctuality_rating');
    }

    public function getSubRatingRecommend(): int
    {
        return (int)$this->getAttribute('recommend_rating');
    }
    public function getSubRatingCoastRating(): int
    {
        return (int)$this->getAttribute('coast_rating');
    }
    public function getSubRatingQualityRating(): int
    {
        return (int)$this->getAttribute('quality_rating');
    }
    public function getSubRatingProfessionalismRating(): int
    {
        return (int)$this->getAttribute('professionalism_rating');
    }

    public function getTotalSubRatingCustomer(): int
    {
        return $this->getSubRatingAdeqate() +
            $this->getSubRatingLegibility() +
            $this->getSubRatingSuggestionsReacts() +
            $this->getSubRatingPunctualityRating() +
            $this->getSubRatingRecommend();
    }
    public function getTotalSubRatingPerformer(): int
    {
        return $this->getSubRatingProfessionalismRating() +
            $this->getSubRatingPunctualityRating() +
            $this->getSubRatingCoastRating() +
            $this->getSubRatingQualityRating() +
            $this->getSubRatingRecommend();
    }

    public function getCountSubRatings(): int
    {
        return 5;
    }

    private function getAttribute($attr)
    {
        return $this->getEntity()->getAttribute($attr);
    }

}