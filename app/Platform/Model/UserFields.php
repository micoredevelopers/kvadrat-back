<?php


	namespace App\Platform\Model;


	use App\Platform\Contract\UserTypeContract;

	class UserFields
	{
		public static function get($type, string $field): string
		{
			$types = [
				UserTypeContract::TYPE_CUSTOMER => [
					'profile_enabled' => 'customer_profile_enabled',
					'about'           => 'customer_about',
					'avatar'          => 'customer_avatar',
					'rating'          => 'customer_rating',
					'reviews_count'   => 'customer_reviews_count',
				],
			];
			return $types[ $type ][ $field ] ?? $field;
		}

		public static function getCustomer($field): string
		{
			return self::get(UserTypeContract::TYPE_CUSTOMER, $field);
		}
	}