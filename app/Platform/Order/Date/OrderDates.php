<?php


namespace App\Platform\Order\Date;

use App\Helpers\Date\DateNameHelper;
use Illuminate\Support\Carbon;

class OrderDates
{
	public function getTextLeftToClose(Carbon $closeDate): string
	{
		if (now()->greaterThan($closeDate)) {
			return '';
		}
		return DateNameHelper::make($closeDate)->doubleValue();
	}

	public function getTextClosedAt(Carbon $closeDate): string
	{
		return $closeDate->formatLocalized('%d %B %Y');
	}

	public function getPublishedDateText(Carbon $pubDate)
	{
		return DateNameHelper::make($pubDate)->setWithAgo(true)->default();
	}




}