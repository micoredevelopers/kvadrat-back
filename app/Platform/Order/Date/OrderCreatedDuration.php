<?php


namespace App\Platform\Order\Date;


use App\Helpers\Date\DateNameHelper;
use Illuminate\Support\Carbon;

class OrderCreatedDuration
{
	public function getText(Carbon $lastSeen): string
	{
		return DateNameHelper::make($lastSeen)->setWithAgo(true)->default();
	}
}