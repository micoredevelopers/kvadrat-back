<?php

namespace App\Platform\Order\Helpers;

use App\Traits\DataKeepers\StoreToSessionTrait;

class TempOrderHelper
{
	use StoreToSessionTrait;

	private $prefix = 'order.temp';

	private $orderId;

	private $key = 'orderId';

	public function __construct()
	{
		$this->orderId = $this->getData($this->key);
	}

	public function getOrderId()
	{
		return $this->orderId;
	}

	public function hasUnfinishedOrder(): bool
	{
		return (bool)$this->orderId;
	}

	public function setUnfinishedOrder($orderId): self
	{
		$this->storeData($this->key, $orderId);
		$this->orderId = $orderId;
		return $this;
	}
}