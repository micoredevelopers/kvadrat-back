<?php


namespace App\Platform\Order\Services;


use App\Models\Order\Order;

class IsUserCanCloseDeal
{
	public function check(Order $order)
	{
		return !$order->getContainer()->isClosed();
	}
}