<?php declare(strict_types=1);

namespace App\Platform\Order\Services;

use App\DataContainers\Category\SearchCategoryRequestData;
use App\DataContainers\Platform\Order\Create\CreateOrderCategoriesData;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CreateOrderCategoriesService
{

	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(Request $request, CategoryRepository $categoryRepository)
	{
		$this->request = $request;
		$this->categoryRepository = $categoryRepository;
	}

	public function generateCreateOrderCategoriesData(): CreateOrderCategoriesData
	{
		$mainCategories = $this->categoryRepository->getMainCategories(SearchCategoryRequestData::createForPublic());
		$data = new CreateOrderCategoriesData($mainCategories);

		if ($categoryId = (int)$this->request->get('category')) {
			/** @var  $category Category */
			try {
				$category = $this->categoryRepository->find($categoryId);
				$data->setSelectedCategory($category);
				$categoryId = $category->isMainCategory() ? $category->getKey() : $category->parent()->getParentKey();
				$subcategories = $this->categoryRepository->getSubCategories(
					Category::createReference($categoryId),
					SearchCategoryRequestData::createForPublic()
				);
				$data->setSubCategories($subcategories);
			} catch (ModelNotFoundException $e) {
			}
		}

		return $data;
	}
}