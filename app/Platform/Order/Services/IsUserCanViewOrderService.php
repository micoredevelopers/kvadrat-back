<?php


namespace App\Platform\Order\Services;


use App\Models\Order\Order;
use Illuminate\Contracts\Auth\Authenticatable;

class IsUserCanViewOrderService
{
	public function check(?Authenticatable $currentUser, Order $order)
	{
		return true;
//		return !is_null($currentUser);
	}
}