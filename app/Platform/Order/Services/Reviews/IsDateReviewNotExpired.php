<?php


namespace App\Platform\Order\Services\Reviews;


use Illuminate\Support\Carbon;

class IsDateReviewNotExpired
{
	public function check(Carbon $dateClose)
	{
		if ($dateClose > now()) {
		    return true;
		}
		return $dateClose->diffInDays(now()) < (int)config('deals.reviews.days_to_publish');
	}
}