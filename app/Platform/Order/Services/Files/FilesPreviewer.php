<?php


namespace App\Platform\Order\Services\Files;


use App\Helpers\File\File;
use App\Models\Order\OrderUpload;
use Illuminate\Support\Str;

class FilesPreviewer
{
	/**
	 * @var OrderUpload
	 */
	private $orderUpload;

	public function __construct(OrderUpload $orderUpload)
	{
		$this->orderUpload = $orderUpload;
	}

	public function getPreview(): ?string
	{
		$path = $this->orderUpload->getPath();
		if (!storageFileExists($path)) {
			return null;
		}
		$absPath = storageDisk()->path($this->orderUpload->getPath());
		if ($this->isImage($absPath)) {
			return getPathToImage($path);
		}

		return '';
	}

	private function isImage($absPathToFile): bool
	{
		try {
			return Str::startsWith(mime_content_type($absPathToFile), 'image');
		} catch (\Throwable $e) {
			return false;
		}
	}


	public function getExtension(): string
	{
		return File::extractExtension($this->orderUpload->getPath());
	}
}