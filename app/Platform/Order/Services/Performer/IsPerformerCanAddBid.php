<?php


namespace App\Platform\Order\Services\Performer;


use App\Models\Order\Order;
use App\Models\User;
use App\Repositories\Order\OrderBidRepository;

class IsPerformerCanAddBid
{
	/**
	 * @var OrderBidRepository
	 */
	private $repository;

	public function __construct(OrderBidRepository $repository)
	{
		$this->repository = $repository;
	}

	public function check(User $performer, Order $order): bool
	{
		if ($order->getContainer()->isClosed()) {
			return false;
		}
		if ($order->getContainer()->hasPerformer()) {
			return false;
		}
		return 0 === $this->repository->getCountByOrderAndUser($order, $performer);
	}
}