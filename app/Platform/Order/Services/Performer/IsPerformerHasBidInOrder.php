<?php


namespace App\Platform\Order\Services\Performer;


use App\Models\Order\Order;
use App\Models\User;
use App\Repositories\Order\OrderBidRepository;
use Illuminate\Contracts\Auth\Authenticatable;

class IsPerformerHasBidInOrder
{
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;

	public function __construct(OrderBidRepository $bidRepository)
	{
		$this->bidRepository = $bidRepository;
	}

	public function check(User $performer, Order $order): bool
	{
		return $this->hasBid($performer, $order);
	}

	private function hasBid(User $performer, Order $order)
	{
		return (bool)$this->bidRepository->getCountByOrderAndUser($order, $performer);
	}
}