<?php


namespace App\Platform\Order\Services\Performer;


use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use Illuminate\Contracts\Auth\Authenticatable;

class IsPerformerCanEditBid
{
	public function check(Authenticatable $performer, OrderBid $bid, Order $order): bool
	{
		if (!$bid->getContainer()->isEnabled()) {
		    return false;
		}
		if (!$bid->getContainer()->getLimitEdit()) {
		    return false;
		}
		if ($order->getContainer()->isClosed() || $order->getContainer()->hasPerformer()) {
		    return false;
		}
		return (int)$performer->getAuthIdentifier() === (int)$bid->getUserId();
	}
}