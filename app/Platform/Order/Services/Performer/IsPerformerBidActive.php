<?php


namespace App\Platform\Order\Services\Performer;


use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Models\User;
use App\Repositories\Order\OrderBidRepository;
use Illuminate\Contracts\Auth\Authenticatable;

class IsPerformerBidActive
{
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;

	public function __construct(OrderBidRepository $bidRepository)
	{
		$this->bidRepository = $bidRepository;
	}

	public function check(User $performer, Order $order): bool
	{
		if (!$bid  = $this->getBid($performer, $order)) {
		    return false;
		}
		return $bid->getContainer()->isEnabled();
	}

	private function getBid(User $performer, Order $order):?OrderBid
	{
		return $this->bidRepository->getByOrderAndUser($order, $performer);
	}
}