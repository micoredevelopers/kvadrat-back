<?php


	namespace App\Platform\Order\Services\Performer;


	use App\Models\Order\Order;
	use App\Models\User;

	class IsPerformerCanViewCustomerContacts
	{
		public function check(?User $performer, Order $order): bool
		{
			if ($order->getContainer()->isNeedShowPhone()) {
				return true;
			}
			if (is_null($performer)) {
				return false;
			}

			if (app(IsUserSelectedAsPerformerInOrder::class)->check($performer, $order)) {
				return true;
			}

			return !$order->getContainer()->hasPerformer() && !$order->getContainer()->isClosed();
		}

	}