<?php


namespace App\Platform\Order\Services\Performer;


use App\Models\Order\Order;
use App\Models\User;

class IsUserSelectedAsPerformerInOrder
{

	public function check(User $user, Order $order)
	{
		if (!$order->getContainer()->hasPerformer()) {
		    return false;
		}
		return (int)$order->getContainer()->getPerformerKey() === (int)$user->getKey();
	}
}