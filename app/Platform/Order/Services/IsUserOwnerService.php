<?php


namespace App\Platform\Order\Services;


use Illuminate\Contracts\Auth\Authenticatable;

class IsUserOwnerService
{
	public function check(?Authenticatable $customer, ?Authenticatable $checkableUser)
	{
		if (is_null($checkableUser) || is_null($customer)) {
		    return false;
		}
		return (int)$customer->getAuthIdentifier() === (int)$checkableUser->getAuthIdentifier();
	}
}