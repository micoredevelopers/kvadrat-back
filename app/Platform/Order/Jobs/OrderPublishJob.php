<?php


namespace App\Platform\Order\Jobs;


use App\Models\Order\Order;
use Illuminate\Support\Str;

class OrderPublishJob
{
	public function handle(Order $order)
	{
		$order->getSetter()->setNotBlank()->makeAvailable()->setPublishDate(now());
		return $order;
	}
}