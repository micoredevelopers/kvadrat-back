<?php


namespace App\Platform\Order\Jobs;


use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;
use Illuminate\Support\Str;

class OrderMakeSlug
{
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	private $maxLength = 160;

	private $words = 5;

	public function handle(Order $order)
	{
		$title = Str::limit($order->getContainer()->getTitle(), $this->maxLength);
		try {
			$words = Str::words($title, $this->words, '');
		} catch (\Throwable $e) {
			$words = $title;
		}
		$slug = Str::slug($words);
		if ($this->slugExists($slug)) {
			$slug = implode('-', [$order->getKey(), $slug]);
		}

		if ($this->words && Str::length($slug) > $this->maxLength) {
			return $this->decrement()->handle($order);
		}
		return $slug;
	}

	private function slugExists(string $slug): bool
	{
		return (bool)$this->orderRepository->where('slug', $slug)->count();
	}

	private function decrement(): self
	{
		$this->words = --$this->words;
		return $this;
	}
}