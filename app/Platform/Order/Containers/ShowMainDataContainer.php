<?php


namespace App\Platform\Order\Containers;


use App\DataContainers\Category\SearchCategoryRequestData;
use App\Helpers\Date\DaysName\DaysNameHelper;
use App\Helpers\Date\HoursName\HoursNameHelper;
use App\Helpers\Date\MinutesName\MinutesNameHelper;
use App\Models\Category\Category;
use App\Models\City;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Date\OrderDates;
use App\Platform\Order\Date\OrderCreatedDuration;
use App\Platform\Order\Services\Reviews\IsDateReviewNotExpired;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class ShowMainDataContainer
{
    private $canViewOrder = false;

    /**
     * @var Order
     */
    private $order;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(Order $order, CategoryRepository $categoryRepository)
    {
        $this->order = $order;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getCategory(): ?Category
    {
        return $this->getOrder()->getCategory();
    }

    /**
     * @return array | Category[]
     */
    public function getChainCategories(): array
    {
        $chain = [];
        if (!$category = $this->getCategory()) {
            return $chain;
        }
        $chain[] = $category;
        tap($this->categoryRepository->getParent($category, SearchCategoryRequestData::createForPublic()), function ($category) use (&$chain) {
            if ($category) {
                $chain[] = $category;
            }
        });
        return array_reverse($chain);
    }

    public function getCity(): City
    {
        return $this->getOrder()->getCity();
    }

    public function getFullAddress()
    {
        $cityName = $this->getCity()->getName();
        $address = $this->getAddress();
        if (!$address) {
            return $cityName;
        }
        if (Str::contains(Str::lower($address), Str::lower($cityName))) {
            return $address;
        }
        return implode(', ', [$cityName, $address]);
    }

    public function getAddress(): string
    {
        return (string)$this->getOrder()->getAttribute('address');
    }

    public function getTitle()
    {
        return $this->getOrder()->getTitle();
    }

    public function getBudget(): int
    {
        return (int)$this->getOrder()->getAttribute('budget');
    }

    public function getBudgetWithCurrency(): string
    {
        return $this->getBudget() ? implode(' ', [$this->getBudget(), getCurrencyIcon()]) : '';
    }

    public function getDescription(): string
    {
        return (string)$this->getOrder()->getAttribute('description');
    }

    public function getShortDescription(): string
    {
        return Str::limit(strip_tags($this->getDescription()), 200);
    }

    public function getDateTo(): ?Carbon
    {
        $date = $this->getOrder()->getAttribute('datetime');
        return isDateValid($date) ? getDateCarbon($date) : null;
    }

    public function getDateCreate(): ?Carbon
    {
        return $this->getOrder()->getCreatedAt();
    }

    public function getDateText(): string
    {
        return $this->getDateCreate() ? app(OrderCreatedDuration::class)->getText($this->getDateCreate()) : '';
    }

    public function isUrgent(): bool
    {
        return (bool)$this->getOrder()->getAttribute('is_urgent');
    }

    public function getCustomer(): User
    {
        return $this->getOrder()->getCustomer();
    }

    public function getCustomerKey()
    {
        return $this->getOrder()->getAttribute('customer_id');
    }

    public function getPerformer(): ?User
    {
        return $this->getOrder()->getPerformer();
    }

    public function getPerformerKey()
    {
        return $this->getOrder()->getAttribute('performer_id');
    }

    public function getBidsCount(): int
    {
        return $this->getOrder()->getAttribute('bids_count');
    }

    public function getBidsCountText()
    {
        return trans_choice('deal.publish.published_bids', $this->getBidsCount(), ['bids' => $this->getBidsCount()]);
    }

    public function getViewsCount(): int
    {
        return $this->getOrder()->getAttribute('views');
    }

    public function getViewsCountText(): string
    {
        return trans_choice('deal.publish.published_views', $this->getViewsCount(), ['views' => $this->getViewsCount()]);
    }

    public function isCanViewOrder(): bool
    {
        return $this->canViewOrder;
    }

    public function setCanViewOrder(bool $canViewOrder): self
    {
        $this->canViewOrder = $canViewOrder;
        return $this;
    }

    public function getSlug(): string
    {
        return (string)$this->getOrder()->getAttribute('slug');
    }

    public function getImage()
    {
        return $this->getOrder()->getAttribute('image');
    }

    public function isClosed(): bool
    {
        if ((bool)$this->getOrder()->getAttribute('is_closed')) {
            return true;
        }
        return !app(IsDateReviewNotExpired::class)->check($this->getOrder()->getAttribute('closes_at'));
    }

    public function isOpened(): bool
    {
        return !$this->isClosed();
    }

    public function inProgress()
    {
       return $this->hasPerformer();

    }

    public function hasPerformer(): bool
    {
        return !is_null($this->getOrder()->getAttribute('performer_id'));
    }

    public function getDateClose(): ?Carbon
    {
        return $this->getOrder()->getAttribute('closes_at');
    }

    public function getClosedDateText(): string
    {
        if (!$this->getDateClose()) {
            return '';
        }

        return app(OrderDates::class)->getTextClosedAt($this->getDateClose());
    }

    public function getClosedDateLeftText(): string
    {
        if (!$this->getDateClose()) {
            return '';
        }
        return app(OrderDates::class)->getTextLeftToClose($this->getDateClose());
    }

    public function getDatePublished(): ?Carbon
    {
        return $this->getOrder()->getAttribute('published_at') ?: $this->getOrder()->getCreatedAt();
    }

    public function getPublishedDateText(): string
    {
        if (!$pubDate = $this->getDatePublished()) {
            return '';
        }
        if ($pubDate->diffInMinutes(now()) === 0) {
            return __('deal.publish.now');
        }
        return app(OrderDates::class)->getPublishedDateText($pubDate);
    }

    protected function getAttribute($attr)
    {
        return $this->getOrder()->getAttribute($attr);
    }

    public function isNeedShowPhone()
    {
        return (bool)$this->getAttribute('show_phone');
    }
}