<?php


namespace App\Platform\Order\Containers;


use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Platform\Reviews\ReviewDisplayContainer;
use App\Repositories\ReviewRepository;

class ReviewsDataContainer
{

	private $performerReview = false;

	private $customerReview = false;
	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(Order $order, ReviewRepository $reviewRepository)
	{
		$this->order = $order;
		$this->reviewRepository = $reviewRepository;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function hasBotsReviews(): bool
	{
		return $this->hasCustomerReview() && $this->hasPerformerReview();
	}

	public function hasCustomerReview(?User $customer = null): bool
	{
		$customer = $this->getCustomer($customer);
		if (!$customer) {
			return false;
		}
		return !is_null($this->getCustomerReview($customer));
	}

	public function hasPerformerReview(?User $performer = null): bool
	{
		$performer = $this->getPerformer($performer);
		if (!$performer) {
			return false;
		}
		return !is_null($this->getPerformerReview($performer));
	}

	public function getCustomerReview(User $customer): ?Review
	{
		if (false === $this->customerReview) {
			$this->customerReview = $this->reviewRepository->customerReviewByOrder($customer, $this->getOrder());
		}
		return $this->customerReview;
	}

	public function getPerformerReview(User $performer): ?Review
	{
		if (false === $this->performerReview) {
			$this->performerReview = $this->reviewRepository->performerReviewByOrder($performer, $this->getOrder());
		}
		return $this->performerReview;
	}

	public function getCustomerReviewContainer(User $customer): ReviewDisplayContainer
	{
		return app(ReviewDisplayContainer::class, [
			'review' => $this->getCustomerReview($customer),
			'user' => app(DisplayCustomerContainer::class, ['user' => $customer])
		]);
	}

	public function getPerformerReviewContainer(User $performer): ReviewDisplayContainer
	{
		return app(ReviewDisplayContainer::class, [
			'review' => $this->getPerformerReview($performer),
			'user' => app(DisplayPerformerContainer::class, ['user' => $performer])
		]);
	}

	private function getPerformer(?User $performer = null): ?User
	{
		return $performer ?? $this->getOrder()->getContainer()->getPerformer();
	}

	private function getCustomer(?User $customer = null): User
	{
		return $customer ?? $this->getOrder()->getContainer()->getCustomer();
	}

	public function isNeedsShowBothReviews(): bool
	{
		if (!$this->getOrder()->getContainer()->hasPerformer()) {
		    return false;
		}
		if ($this->getOrder()->getContainer()->isOpened()) {
		    return false;
		}
		return true;
	}
}