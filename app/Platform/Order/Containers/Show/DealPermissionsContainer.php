<?php


namespace App\Platform\Order\Containers\Show;


use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Models\User;
use App\Platform\Order\Services\IsUserCanCloseDeal;
use App\Platform\Order\Services\IsUserOwnerService;
use App\Platform\Order\Services\Performer\IsPerformerBidActive;
use App\Platform\Order\Services\Performer\IsPerformerCanAddBid;
use App\Platform\Order\Services\Performer\IsPerformerCanEditBid;
use App\Platform\Order\Services\Performer\IsPerformerCanViewCustomerContacts;
use App\Platform\Order\Services\Performer\IsPerformerHasBidInOrder;
use App\Platform\Order\Services\Performer\IsUserSelectedAsPerformerInOrder;
use App\Platform\Order\Services\Reviews\IsDateReviewNotExpired;

class DealPermissionsContainer
{
	/**
	 * @var User | null
	 */
	private $performer;
	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var User|null
	 */
	private $customer;

	public function __construct(?User $performer, Order $order, ?User $customer)
	{
		$this->performer = $performer;
		$this->order = $order;
		$this->customer = $customer;
	}

	public function getPerformer(): ?User
	{
		return $this->performer;
	}

	/**
	 * @return User|null
	 */
	public function getCustomer(): ?User
	{
		return $this->customer;
	}

	public function canAddBid(): bool
	{
		if (!$this->getPerformer()) {
			return false;
		}
		if ($this->isUserOwnerDeal()) {
			return false;
		}
		return app(IsPerformerCanAddBid::class)->check($this->getPerformer(), $this->order);
	}

	public function canEditBid(OrderBid $bid)
	{
		if (!$this->getPerformer()) {
			return false;
		}
		return app(IsPerformerCanEditBid::class)->check($this->getPerformer(), $bid, $this->order);
	}

	public function isCanCancelBid(): bool
	{
		if (!$this->getPerformer()) {
			return false;
		}
		if ($this->order->getContainer()->isClosed()) {
			return false;
		}
		$isNotCancelled = app(IsPerformerBidActive::class)->check($this->getPerformer(), $this->order);
		return $this->isHasBid() && $isNotCancelled;
	}

	public function isHasBid(): bool
	{
		return app(IsPerformerHasBidInOrder::class)->check($this->getPerformer(), $this->order);
	}

	public function canViewCustomerContacts(): bool
	{
//		if (is_null($this->getPerformer())) {
//			return false;
//		}
		return app(IsPerformerCanViewCustomerContacts::class)->check($this->getPerformer(), $this->order);
	}

	public function canCloseDeal(): bool
	{
		return $this->isUserOwnerDeal() && app(IsUserCanCloseDeal::class)->check($this->order);
	}

	public function isUserOwnerDeal(?User $checkableUser = null): bool
	{
		return app(IsUserOwnerService::class)->check($this->getCustomer(), $checkableUser ?? $this->getPerformer());
	}

	public function isUserSelectedAsPerformer(): bool
	{
		if (is_null($this->getPerformer())) {
			return false;
		}
		return app(IsUserSelectedAsPerformerInOrder::class)->check($this->getPerformer(), $this->order);
	}

	public function canPerformerAddReview(): bool
	{
		if ($this->order->getContainer()->isOpened()) {
			return false;
		}

		if (!$this->isUserSelectedAsPerformer()) {
			return false;
		}

		return app(IsDateReviewNotExpired::class)->check($this->order->getContainer()->getDateClose());
	}

	public function canCustomerAddReview(): bool
	{
		if ($this->order->getContainer()->isOpened() || !$this->order->getContainer()->hasPerformer()) {
			return false;
		}

		if (!$this->isUserOwnerDeal()) {
			return false;
		}

		return app(IsDateReviewNotExpired::class)->check($this->order->getContainer()->getDateClose());
	}

	public function canSelectPerformer(): bool
	{
		if (!$this->isUserOwnerDeal()) {
			return false;
		}
		if ($this->order->getContainer()->isClosed()) {
			return false;
		}
		return !$this->order->getContainer()->hasPerformer();
	}

}