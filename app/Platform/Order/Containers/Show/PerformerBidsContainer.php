<?php


namespace App\Platform\Order\Containers\Show;


use App\Models\Order\OrderBid;

class PerformerBidsContainer
{

	private $bid;

	public function __construct(?OrderBidContainer $container = null)
	{
		$this->bid = $container;
	}

	public function hasBid(): bool
	{
		return !is_null($this->bid);
	}

	public function getBid(): ?OrderBidContainer
	{
		return $this->bid;
	}


}