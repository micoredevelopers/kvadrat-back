<?php


namespace App\Platform\Order\Containers\Show;


use App\Helpers\Date\DaysName\DaysNameHelper;
use App\Models\Order\OrderBid;
use App\Platform\Cabinet\Display\DisplayUserTypeContainerContract;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use Illuminate\Support\Carbon;

class OrderBidContainer
{

	private $choosedAsPerformer = false;
	/**
	 * @var OrderBid
	 */
	private $bid;
	/**
	 * @var DaysNameHelper
	 */
	private $daysNameHelper;

	public function __construct(OrderBid $bid, DaysNameHelper $daysNameHelper)
	{
		$this->bid = $bid;
		$this->daysNameHelper = $daysNameHelper;
	}

	public function getEntity(): OrderBid
	{
		return $this->bid;
	}

	public function getBid(): int
	{
		return (int)$this->getAttribute('bid');
	}

	public function getBidText(): string
	{
		return (int)$this->getAttribute('bid') . ' ' . getCurrencyIcon();
	}

	public function setBid(OrderBid $bid): self
	{
		$this->bid = $bid;
		return $this;
	}

	public function getDateCreate(): string
	{
		return getDateFormatted($this->getEntity()->getCreatedAt());
	}

	public function getDays(): string
	{
		return $this->getEntity()->getAttribute('days');
	}

	public function getDaysText(): string
	{
		$days = $this->getEntity()->getAttribute('days');
		return $days . ' ' . $this->daysNameHelper->getName($days);
	}

	public function getText(): string
	{
		return $this->getEntity()->getAttribute('text');
	}

	public function getTextDisplay(): string
	{
		return nl2br($this->getText());
	}

	public function isEnabled(): bool
	{
		return (bool)((int)$this->getAttribute('active'));
	}

	public function getLimitEdit(): int
	{
		return (int)$this->getAttribute('available_edits');
	}

	public function getDateEdit(): ?Carbon
	{
		return $this->getAttribute('modified_at');
	}

	public function getDateEditText()
	{
		if (!$this->getDateEdit()) {
			return '';
		}

		return getDateFormatted($this->getDateEdit());
	}

	protected function getAttribute($attr)
	{
		return $this->getEntity()->getAttribute($attr);
	}

	public function getDateCancel(): ?Carbon
	{
		return $this->getEntity()->getAttribute('canceled_at');
	}

	public function getUser(): DisplayUserTypeContainerContract
	{
		return $this->getEntity()->getUser()->getPerformerContainer();
	}

	public function getPerformerKey(): int
	{
		return (int)$this->getAttribute('user_id');
	}

	public function isChoosedAsPerformer(): bool
	{
		return $this->choosedAsPerformer;
	}

	/**
	 * @param bool $choosedAsPerformer
	 * @return OrderBidContainer
	 */
	public function setChoosedAsPerformer(bool $choosedAsPerformer): OrderBidContainer
	{
		$this->choosedAsPerformer = $choosedAsPerformer;
		return $this;
	}

	public function getPerformer(): \App\Models\User
	{
		return $this->getEntity()->getUser();
	}
}