<?php


namespace App\Platform\Order\Model;


use App\Models\Order\OrderBid;

class OrderBidSetter
{
	/**
	 * @var OrderBid
	 */
	private $orderBid;

	public function __construct(OrderBid $orderBid)
	{
		$this->orderBid = $orderBid;
	}

	/**
	 * @return OrderBid
	 */
	public function getOrderBid(): OrderBid
	{
		return $this->orderBid;
	}

	public function decrementEdit(): self
	{
		$edits = (int)$this->getOrderBid()->getAttribute('available_edits');
		if ($edits > 0) {
			--$edits;
			$this->setAttribute('available_edits', $edits);
		}
		return $this;
	}

	public function setAvailableEdits(int $int): OrderBidSetter
	{
		return $this->setAttribute('available_edits', $int);
	}

	public function disable()
	{
		return $this->setAttribute('active', 0)->setAttribute('canceled_at', now());
	}

	public function wasModified()
	{
		return $this->setAttribute('modified_at', now());
	}

	private function setAttribute($key, $value): self
	{
		$this->getOrderBid()->setAttribute($key, $value);
		return $this;
	}

}