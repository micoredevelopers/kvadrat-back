<?php


namespace App\Platform\Order\Model;


use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use Illuminate\Support\Carbon;

class ReviewSetter
{
	/**
	 * @var Review
	 */
	private $entity;

	public function __construct(Review $entity)
	{
		$this->entity = $entity;
	}


	public function setFromId($id): ReviewSetter
	{
		$this->setAttribute('from_id', $id);
		return $this;
	}

	public function setFrom(User $user): ReviewSetter
	{
		return $this->setFromId($user->getKey());
	}

	public function setToId($id): ReviewSetter
	{
		$this->setAttribute('to_id', $id);
		return $this;
	}

	public function setTo(User $user): ReviewSetter
	{
		return $this->setToId($user->getKey());
	}

	public function setRating($rating): ReviewSetter
	{
		return $this->setAttribute('rating', $rating);
	}

	public function setAbout($about): ReviewSetter
	{
		return $this->setAttribute('about', $about);
	}

	public function setOrderId($id)
	{
		return $this->setAttribute('order_id', $id);
	}

	public function setOrder(Order $order)
	{
		return $this->setOrderId($order->getKey());
	}

	protected function setAttribute($attr, $value): self
	{
		$this->entity->setAttribute($attr, $value);
		return $this;
	}

	public function setPublished(): ReviewSetter
	{
		return $this->setAttribute('is_published', 1);
	}

	public function setPublishedAt(?Carbon $now = null): ReviewSetter
	{
		$now = $now ?? now();
		return $this->setAttribute('published_at', $now);
	}

	public function publish(): ReviewSetter
	{
		return $this->setPublished()->setPublishedAt();
	}
}