<?php


	namespace App\Platform\Order\Model;


	use App\Models\User;

	class UserSetter
	{
		/**
		 * @var User
		 */
		private $entity;

		public function __construct(User $user)
		{
			$this->entity = $user;
		}

		/**
		 * @return User
		 */
		public function getEntity(): User
		{
			return $this->entity;
		}

		private function setAttribute($key, $value): self
		{
			$this->getEntity()->setAttribute($key, $value);
			return $this;
		}

		public function setPerformerReviewsCount($count)
		{
			return $this->setAttribute('reviews_count', $count);
		}

		public function setCustomerReviewsCount($count)
		{
			return $this->setAttribute('customer_reviews_count', $count);
		}

	}