<?php


namespace App\Platform\Order\Model;


use App\Models\Order\Order;
use Illuminate\Support\Carbon;

class OrderSetter
{
	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	public function setCustomerId($customerId): OrderSetter
	{
		return $this->setAttribute('customer_id', $customerId);
	}

	public function setPerformerId($customerId): OrderSetter
	{
		return $this->setAttribute('performer_id', $customerId);
	}

	public function setNotBlank(): OrderSetter
	{
		return $this->setAttribute('is_blank', false);
	}

	public function makeAvailable(): OrderSetter
	{
		return $this->setAttribute('available_at', now());
	}

	public function setCreatedAt($createdAt): OrderSetter
	{
		return $this->setAttribute($this->getOrder()->getCreatedAtColumn(), $createdAt);
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function setSlug($slug): OrderSetter
	{
		return $this->setAttribute('slug', $slug);
	}

	public function setImagePoster(?string $path): OrderSetter
	{
		return $this->setAttribute('image', $path);
	}

	public function setPublishDate(Carbon $date): OrderSetter
	{
		return $this->setAttribute('published_at', $date);
	}

	public function setBidsCount(int $count): OrderSetter
	{
		return $this->setAttribute('bids_count', $count);
	}

	private function setAttribute($attr, $value): OrderSetter
	{
		$this->getOrder()->setAttribute($attr, $value);
		return $this;
	}

	private function getAttribute($attr)
	{
		$this->getOrder()->getAttribute($attr);
		return $this;
	}

	public function setClosesAt($date = null): OrderSetter
	{
		$date = $date ?? now();
		return $this->setAttribute('closes_at', $date);
	}

	public function setClosed(): OrderSetter
	{
		return $this->setAttribute('is_closed', true);
	}

	public function close(): OrderSetter
	{
		return $this->setClosed()->setClosesAt();
	}

	public function incrementViews()
	{
		$count = $this->getOrder()->getContainer()->getViewsCount() + 1;
		return  $this->setAttribute('views', $count);
	}

}