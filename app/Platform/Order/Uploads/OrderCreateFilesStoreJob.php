<?php


namespace App\Platform\Order\Uploads;


use App\Models\Order\Order;
use App\Models\Order\OrderUpload;
use App\Repositories\Order\OrderUploadsRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class OrderCreateFilesStoreJob
{
	/**
	 * @var UploadedFile[]
	 */
	private $files;

	private $orderUploads;
	/**
	 * @var OrderUploadsRepository
	 */
	private $uploadsRepository;
	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var OrderCreateUploader
	 */
	private $uploader;

	public function __construct(
		array $files,
		Order $order,
		OrderUploadsRepository $uploadsRepository,
		OrderCreateUploader $uploader
	)
	{
		$this->files = $files;
		$this->orderUploads = collect([]);
		$this->uploadsRepository = $uploadsRepository;
		$this->order = $order;
		$this->uploader = $uploader;
	}

	public function run(): OrderCreateFilesStoreJob
	{
		$this->uploader->setPath($this->uploader->getPath() . DIRECTORY_SEPARATOR . $this->order->getKey());
		foreach ($this->getFiles() as $file) {
			/** @var  $model OrderUpload */
			$model = $this->uploadsRepository->makeModel();
			$path = $this->uploader->upload($file);
			$model->setPath($path)->setOriginalName($file->getClientOriginalName());
			$model = $this->uploadsRepository->createRelated($model->toArray(), $this->order);
			$this->orderUploads->push($model);
		}
		return $this;
	}

	/**
	 * @return array | UploadedFile[]
	 */
	private function getFiles(): array
	{
		return $this->files;
	}

	/**
	 * @return Collection
	 */
	public function getOrderUploads(): Collection
	{
		return $this->orderUploads;
	}

}