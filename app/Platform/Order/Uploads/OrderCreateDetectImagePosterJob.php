<?php


namespace App\Platform\Order\Uploads;


use App\Helpers\Debug\LoggerHelper;
use App\Models\Order\Order;
use App\Models\Order\OrderUpload;
use App\Repositories\Order\OrderUploadsRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class OrderCreateDetectImagePosterJob
{
	/**
	 * @var OrderUpload[] | Collection
	 */
	private $files;

	public function __construct(
		 $files
	)
	{
		$this->files = $files;
	}

	public function run(): ?string
	{
		$image = null;
		foreach ($this->getFiles() as $orderUpload) {
			if (!storageFileExists($orderUpload->getPath())) {
				continue;
			}
			try{
				$mime = mime_content_type(storageDisk()->path(trim($orderUpload->getPath(), DIRECTORY_SEPARATOR)));
				if ($mime && Str::startsWith($mime, 'image')) {
					$image = $orderUpload->getPath();
					break;
				}
			} catch (\Throwable $e){
				app(LoggerHelper::class)->error($e);
			}
		}
		return $image;
	}


	private function getFiles()
	{
		return $this->files;
	}


}