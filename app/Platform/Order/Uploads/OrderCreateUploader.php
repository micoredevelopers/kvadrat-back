<?php


namespace App\Platform\Order\Uploads;


use App\Uploaders\BaseFileUploader;

class OrderCreateUploader extends BaseFileUploader
{
	protected $path = 'deals/uploads';
}