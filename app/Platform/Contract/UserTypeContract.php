<?php

namespace App\Platform\Contract;

interface UserTypeContract
{
	public const TYPE_PERFORMER = 'performer';
	public const TYPE_PERFORMER_COMPANY = 'company';
	public const TYPE_CUSTOMER = 'customer';

}