<?php


namespace App\Platform\Query;


class PaginationOnPage
{

	public const DEFAULT = 12;

	public function default(): int
	{
		return $this->getItems()[0] ?? self::DEFAULT;
	}

	public function getItems(): array
	{
		return [
			12,
			24,
			36
		];
	}

	public function itemSupports($item): bool
	{
		return (bool)(array_flip($this->getItems())[$item] ?? false);
	}
}