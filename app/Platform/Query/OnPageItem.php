<?php


namespace App\Platform\Query;


class OnPageItem
{
	private $value;
	private $isActive;

	public function __construct(int $value, bool $isActive = false)
	{
		$this->value = $value;
		$this->isActive = $isActive;
	}

	public function isActive(): bool
	{
		return $this->isActive;
	}

	public function getValue()
	{
		return $this->value;
	}
}