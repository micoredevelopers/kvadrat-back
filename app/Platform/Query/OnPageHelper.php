<?php


namespace App\Platform\Query;


use Illuminate\Support\Collection;

class OnPageHelper
{
	/**
	 * @var array
	 */
	private $items;
	/**
	 * @var null
	 */
	private $activeItem;

	public function __construct(array $items, ?int $activeItem = null)
	{
		$this->items = $items;
		$this->activeItem = $activeItem;
	}

	/**
	 * @return Collection | OnPageItem[]
	 */
	public function getItems(): Collection
	{
		return collect($this->items)->transform(function ($item) {
			$item = (int)$item;
			$isActive = $this->activeItem && $item === $this->activeItem;
			return app(OnPageItem::class, ['value' => $item, 'isActive' => $isActive,]);
		});
	}
}