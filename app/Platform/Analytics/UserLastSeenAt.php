<?php


namespace App\Platform\Analytics;


use Illuminate\Support\Carbon;
use DateTimeInterface;
use Illuminate\Support\Facades\Cache;

class UserLastSeenAt
{
	private $key = 'platform.users.last-seen';

	public function __construct()
	{
	}

	private function getData($key)
	{
		return Cache::get($key);
	}

	private function setData($key, $value): self
	{
		Cache::set($key, $value, now()->addHour());
		return $this;
	}

	protected function getKey(string $key): string
	{
		return implode('.', [$this->key, $key]);
	}

	public function getLastSeen($userIdAsKey): ?Carbon
	{
		return $this->transform($this->getData($this->getKey($userIdAsKey)));
	}

	public function setLastSeen($userIdAsKey, ?Carbon $date = null): self
	{
		if (!$date) {
			$date = now();
		}
		return $this->setData($this->getKey($userIdAsKey), $date->toDateTimeString());
	}

	private function transform($date)
	{
		return isDateValid($date) ? getDateCarbon($date) : null;
	}

}