<?php


namespace App\Platform\Route;


interface PlatformRoutes
{
	public const CABINET_PERFORMER_PERSONAL = 'cabinet.personal';
	public const CABINET_PERFORMER_PORTFOLIO = 'cabinet.portfolio';
	public const CABINET_PERFORMER_PASSWORD = 'cabinet.password';
	public const CABINET_PERFORMER_REVIEWS = 'cabinet.reviews';
	public const CABINET_PERFORMER_DELETE_AVATAR = 'cabinet.avatar.delete';

	public const CABINET_CUSTOMER_PERSONAL = 'cabinet.customer.personal';
	public const CABINET_CUSTOMER_PASSWORD = 'cabinet.password';
	public const CABINET_CUSTOMER_REVIEWS = 'cabinet.customer.reviews';
	public const CABINET_CUSTOMER_DELETE_AVATAR = 'cabinet.customer.avatar.delete';

	public const CABINET_ORDERS = 'cabinet.my-orders';

	public const PERFORMERS_LIST = 'platform.performers.index';
	public const PERFORMERS_SHOW = 'platform.performers.show';

	public const CUSTOMERS_SHOW = 'platform.customers.show';

	public const ORDER_LIST = 'platform.order.index';
	public const ORDER_CREATE = 'platform.order.create';
	public const ORDER_SHOW = 'platform.order.show';
	public const ORDER_CLOSE = 'platform.order.close';
	public const ORDER_CHOOSE_PERFORMER = 'platform.order.choose';

	public const ORDER_REVIEWS_PERFORMER_ADD = 'platform.order.review.performer';
	public const ORDER_REVIEWS_CUSTOMER_ADD = 'platform.order.review.customer';

	public const ORDER_BID_ADD = 'platform.order.bid.add';
	public const ORDER_BID_EDIT = 'platform.order.bid.edit';
	public const ORDER_BID_CANCEL = 'platform.order.bid.cancel';


}