<?php


namespace App\Platform\Cache;


interface CacheKeys
{
	public const CABINET_COUNT_REVIEWS = 'CABINET_COUNT_REVIEWS.';
	public const CABINET_COUNT_PORTFOLIO = 'CABINET_COUNT_PORTFOLIO.';
}