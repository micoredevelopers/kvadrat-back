<?php

return[
    \App\Enum\CitiesEnums::ODESSA => [
        'phone' => '+38 (063) 889 29 54',
        'inst' => 'https://www.instagram.com/kvadrat.academy.odessa/',
        'work_time' => ' 10:00-18:00',
        'address' => 'contacts.odessa.address',
        'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2748.501016726051!2d30.738364215591073!3d46.45858317912528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c633d72ee42d9d%3A0x4b05a342c7ec432b!2z0YPQuy4g0KHRgNC10LTQvdC10YTQvtC90YLQsNC90YHQutCw0Y8sIDE50JAsINCe0LTQtdGB0YHQsCwg0J7QtNC10YHRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNjUwMDA!5e0!3m2!1sru!2sua!4v1633102337753!5m2!1sru!2sua',
        'facebook' => 'https://www.facebook.com/kvadrat.academy',
    ],
    \App\Enum\CitiesEnums::KHARKIV => [
        'phone' => '+380 93 393 2813',
        'inst' => 'https://instagram.com/kvadrat.academy.kh',
        'work_time' => ' 10:00-18:00',
        'address' => 'contacts.kharkiv.address',
        'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2564.9469536758534!2d36.22974931571491!3d49.99360397941496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a0f003880a0f%3A0x72072c3abf395115!2z0YPQuy4g0KHRg9C80YHQutCw0Y8sIDEsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2MTAwMA!5e0!3m2!1sru!2sua!4v1637853803876!5m2!1sru!2sua',
        'facebook' => 'https://www.facebook.com/kvadrat.academy.kh',
    ],
    \App\Enum\CitiesEnums::KYIV => [
        'phone' => '+380 93 421 2763',
        'inst' => 'https://instagram.com/kvadrat.academy.kyiv',
        'work_time' => ' 10:00-18:00',
        'address' => 'contacts.kyiv.address',
        'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.0181889891714!2d30.444127815731804!3d50.459385979476615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cc322dcfeddd%3A0x2aae8dc7760c4c8a!2z0YPQuy4g0JDQu9C10LrRgdCw0L3QtNGA0LAg0JTQvtCy0LbQtdC90LrQviwgMywg0JrQuNC10LIsIDAyMDAw!5e0!3m2!1sru!2sua!4v1637758639983!5m2!1sru!2sua',
        'facebook' => 'https://www.facebook.com/kvadrat.academy',
    ],
    \App\Enum\CitiesEnums::DNEPR => [
        'phone' => '+380 93 454 5975',
        'inst' => 'https://instagram.com/kvadrat.academy.dneprua',
        'work_time' => ' 10:00-18:00',
        'address' => 'contacts.dnepr.address',
        'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2645.1736458654445!2d35.0223045156609!3d48.47238277925161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbe385fe93c0a1%3A0x2d6670c2f5cc750f!2z0L_RgC3Rgi4g0JTQvNC40YLRgNC40Y8g0K_QstC-0YDQvdC40YbQutC-0LPQviwgMTExLCDQlNC90LjQv9GA0L4sINCU0L3QtdC_0YDQvtC_0LXRgtGA0L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA0OTAwMA!5e0!3m2!1sru!2sua!4v1637758463219!5m2!1sru!2sua',
        'facebook' => 'https://www.facebook.com/kvadrat.academy',
    ],
];