<?php
	return [
		'login'          => env('SMS_LOGIN'),
		'password'       => env('SMS_PASSWORD'),
		'alphaNum'       => env('SMS_ALPHA_NUM', ''),
		'distributionId' => env('SMS_DISTRIBUTION_ID'),
	];
