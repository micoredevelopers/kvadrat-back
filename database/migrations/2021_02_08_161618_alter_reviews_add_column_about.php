<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReviewsAddColumnAbout extends Migration
{

	public function up()
	{
		Schema::table('reviews', function (Blueprint $table) {
			$table->char('about')->nullable()->index()->comment('Commented about performer or about customer');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function (Blueprint $table) {
			$table->dropColumn('about');
		});
	}
}
