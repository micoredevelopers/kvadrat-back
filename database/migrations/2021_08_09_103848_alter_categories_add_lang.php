<?php

use App\Models\Category\CategoryLang;
use App\Models\Language;
use App\Repositories\Admin\LanguageRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterCategoriesAddLang extends Migration
{

    /**
     * @var MigrationBuilder|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $builder;


    private $table = 'categories';

    private $foreignKey = 'category_id';

    private $tableLang = 'category_langs';

    /**
     * @var LanguageRepository
     */
    private $languageRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct()
    {
        $this->builder = app(MigrationBuilder::class);
        $this->languageRepository = app(LanguageRepository::class);
        $this->categoryRepository = app(CategoryRepository::class);
    }


    public function up()
    {
        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);
            $this->builder
                ->createLanguageKey()
                ->createName()
                ->createDescription();

            $this->builder->addForeign($this->foreignKey, $this->table);
        });

        /** @var  $category \App\Models\Category\Category */
        foreach ($this->categoryRepository->all() as $category) {
            foreach ($this->getLanguages() as $language) {
                $langCat = new CategoryLang();
                $langCat->category()->associate($category);
                $langCat->associateWithLanguage($language);
                $langCat->setAttribute('name', $category->name);
                $langCat->save();
            }
        }
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('excerpt');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);
            $this->builder
                ->createName()
                ->createDescription()
                ->createExcerpt()
            ;
        });

        /** @var  $category \App\Models\Category\Category */
        foreach ($this->categoryRepository->all() as $category) {
            $category->setAttribute('name', $category->lang()->first()->getAttribute('name'));
        }

        Schema::dropIfExists($this->tableLang);
    }

    /**
     * @return array<Language>
     */
    private function getLanguages(): \Illuminate\Support\Collection
    {
        static $languages;
        if (null === $languages) {
            $languages = $this->languageRepository->all();
        }
        return $languages;
    }

}
