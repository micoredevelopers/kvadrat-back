<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderUploads extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'order_uploads';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
			$this->builder->belongsToOrder()
				->createNullableChar('file')
				->createNullableString('original_name', 1000)
            ;
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
