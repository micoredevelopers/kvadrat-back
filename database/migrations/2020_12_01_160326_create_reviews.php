<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviews extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'reviews';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

			$table->id();
			$table->float('rating');
			$table->unsignedTinyInteger('adequate_rating')->default(1)->comment('Адекватный');
			$table->unsignedTinyInteger('legibility_rating')->default(1)->comment('Чётко ставит задачу');
			$table->unsignedTinyInteger('sociability_rating')->default(1);
			$table->unsignedTinyInteger('conflict_rating')->default(1);
			$table->unsignedTinyInteger('recommend_rating')->default(1);
            $table->unsignedBigInteger('from_id')->index();
            $table->unsignedBigInteger('to_id')->index();

            $table->boolean('is_published')->default(false)->index();
			$table->dateTime('published_at')->nullable();
            $this->builder
				->createNullableString('comment', 5000)
                ->createActiveIndexed()
				->belongsToOrder()
            ;
            $table->timestamps();
        });



    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
