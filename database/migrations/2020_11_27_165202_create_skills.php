<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkills extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'skills';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $this->builder
				->belongsToUser()
				->belongsToCategory()
            ;
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
