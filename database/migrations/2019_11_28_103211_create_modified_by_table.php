<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModifiedByTable extends Migration
{
	use \App\Traits\Migrations\MigrationCreateFieldTypes;
	use \App\Traits\Migrations\ForeignKeys\UsersForeignKey;

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void
	{
		Schema::create('modified_by', function (Blueprint $table) {
			$this->setTable($table);
			$table->bigIncrements('id');
			$table->nullableMorphs('modifiable');
			$this->addForeignUsers();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void
	{
		Schema::dropIfExists('modified_by');
	}
}
