<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetaTable extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'user_metas';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $this->builder
				->belongsToUser()
				->createNullableChar('key')
				->createNullableString('value')
            ;
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
