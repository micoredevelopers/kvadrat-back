<?php

	use App\Builders\Migration\MigrationBuilder;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateVerifications extends Migration
	{

		/**
		 * @var MigrationBuilder
		 */
		private $builder;

		private $table = 'verifications';

		public function __construct()
		{
			$this->builder = app(MigrationBuilder::class);
		}

		public function up()
		{
			Schema::create($this->table, function (Blueprint $table) {
				$this->builder->setTable($table);

				$table->id();
				$table->timestamp('available_at')->nullable();
				$this->builder
					->createNullableChar('ip')
					->createNullableChar('code')
					->createNullableChar('phone')
				;
				$table->timestamps();
			});


		}

		public function down()
		{
			Schema::dropIfExists($this->table);
		}
	}
