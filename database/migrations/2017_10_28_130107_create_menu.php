<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenu extends Migration
{
    use MigrationCreateFieldTypes;

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	protected $table = 'menus';
	protected $foreign = 'menu_id';
	protected $tableLang = 'menu_lang';


	public function up()
	{
		Schema::create('menu_groups', function (Blueprint $table) {
            $this->setTable($table);
			$table->bigIncrements('id');
			$table->string('name', 250)->nullable();
			$table->string('role', 250)->nullable();
			$table->timestamps();
		});

		Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
			$table->increments('id');
			$table->unsignedBigInteger('menu_group_id')->index();
			$table->integer('parent_id')->nullable();
			$table->tinyInteger('active')->default(1);
			$this->createType();
			$table->string('url', 160)->nullable();
			$table->string('icon', 255)->nullable()->comment('Шрифтовые изображения');
			$table->string('image', 255)->nullable()->comment('обычная картинка, впринципе любого типа');
			$table->smallInteger('sort')->default(0);
			$table->text('options')->nullable();
			$table->timestamps();

			$table->foreign('menu_group_id')
				->references('id')->on('menu_groups')
				->onUpdate('cascade')->onDelete('cascade');
		});

		Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
			$table->integer($this->foreign)->unsigned();

			$table->string('name', 255)->nullable();
			$table->smallInteger('language_id');
			//
			$table->index($this->foreign);
			$table->index('language_id');

			$table->foreign($this->foreign)
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
		Schema::dropIfExists('menu_groups');
	}
}
