<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'orders';


	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$this->builder
				->belongsToCategory('category_id', true)
				->belongsToUser('performer_id', true)
				->belongsToUser('customer_id', true)
				->createTitle(1000)
				->createImage()
				->createNullableString('description', 3000)
				->unsignedBigInt('budget')
				->createNullableString('address', 1000)
				->createBoolean('is_urgent', false)
				->unsignedBigInt('views')
				->unsignedBigInt('city_id')
				->createActive()
				->createBoolean('is_blank', true)
				->createUrl(true, 'slug')
				->smallInt('bids_count')
				->createBoolean('is_closed', false)
				->getSelf($table->dateTime('closes_at')->nullable())
				->getSelf($table->dateTime('available_at')->nullable()->comment('feature, if will be need to publish at the specific time'))
				->getSelf($table->dateTime('published_at')->nullable()->comment('for display date publish, because publishing can diff with date create'))
			;
			$table->index(['city_id', 'category_id']);
			$table->timestamps();
		});
	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
