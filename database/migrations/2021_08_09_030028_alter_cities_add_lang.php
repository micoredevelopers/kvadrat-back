<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterCitiesAddLang extends Migration
{

	/**
	 * @var MigrationBuilder|\Illuminate\Contracts\Foundation\Application|mixed
	 */
	private $builder;

    private $table = 'cities';

    private $foreignKey = 'city_id';

    private $tableLang = 'city_langs';

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);

            $this->builder
                ->createName()
                ->createLanguageKey()
            ;
            $this->builder->addForeign($this->foreignKey, $this->table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            $this->builder->setTable($table);
            $this->builder->createName();
        });
        Schema::dropIfExists($this->tableLang);
    }
}
