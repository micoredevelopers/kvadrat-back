<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFranchisePackagesTable extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'franchise_packages';

   private $foreignKey = 'franchise_packages_id';

   private $tableLang = 'franchise_packages_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $this->builder->createSort();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);

            $table->mediumText('population')->nullable();
            $table->mediumText('invest')->nullable();
            $table->mediumText('gn_invest')->nullable();
            $table->mediumText('payback')->nullable();
            $table->mediumText('profit')->nullable();
            $table->mediumText('royalty')->nullable();

            $this->builder
                ->createName()
                ->createLanguageKey()
            ;
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
