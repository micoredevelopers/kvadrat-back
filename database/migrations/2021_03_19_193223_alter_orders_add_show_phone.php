<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterOrdersAddShowPhone extends Migration
{

	/**
	 * @var MigrationBuilder|\Illuminate\Contracts\Foundation\Application|mixed
	 */
	private $builder;

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $this->builder->setTable($table);
            $this->builder->createBoolean('show_phone', false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('show_phone');
        });
    }
}
