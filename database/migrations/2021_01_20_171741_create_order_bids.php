<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderBids extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'order_bids';

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$this->builder
				->belongsToUser()
				->belongsToOrder()
				->createActive()
				->createNullableString('text', 1000)
				->unsignedInt('bid')
				->unsignedSmallInt('days')
			;
			$table->tinyInteger('available_edits')->default(3)->unsigned();
			$table->dateTime('modified_at')->nullable();
			$table->dateTime('canceled_at')->nullable();
			$table->timestamps();
		});
	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
