<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReviewsRenameAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->renameColumn('sociability_rating', 'suggestions_reacts')->comment('Нормально реагирует на предложения');
            $table->renameColumn('conflict_rating', 'punctuality_rating')->comment('Пунктуальность');
            $table->unsignedTinyInteger('coast_rating')->default(1)->comment('Стоимость');
            $table->unsignedTinyInteger('quality_rating')->default(1)->comment('Качество');
            $table->unsignedTinyInteger('professionalism_rating')->default(1)->comment('Профессионализм');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->dropColumn('coast_rating');
            $table->dropColumn('quality_rating');
            $table->dropColumn('professionalism_rating');
            $table->renameColumn('suggestions_reacts', 'sociability_rating');
            $table->renameColumn('punctuality_rating', 'conflict_rating');
        });
    }
}
