<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqs extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'faqs';

   private $foreignKey = 'faq_id';

   private $tableLang = 'faq_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $this->builder
                ->createSort()
                ->createActive()
            ;
            $table->timestamps();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);

			$table->string('question', 1000)->nullable();
			$table->text('answer')->nullable();
            $this->builder
                ->createLanguageKey()
            ;
            $this->builder->addForeign($this->foreignKey, $this->table);
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
