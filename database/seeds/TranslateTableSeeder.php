<?php

namespace Database\Seeders;

use App\Models\FranchisePackages;
use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TranslateTableSeeder extends Seeder
{
    private $translates;
    /**
     * @var TranslateRepository
     */
    private $repository;

    public function __construct(TranslateRepository $repository)
    {
        Model::reguard();
        $this->translates = $repository->all()->keyBy('key');
        $this->repository = $repository;
    }

    public function run()
    {
        $groups = [
            'global' => [
                ['key' => '404', 'value' => 'Страница не найдена'],
                ['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адресс', 'type' => 'textarea'],
                'feedback' => 'Отзывы',
                'trial' => 'Записаться на пробное занятие',
                'work-hours' => 'График работы:',
                'reviews' => 'Отзывы',
            ],
            'trial.benefits' => [
                $this->getBuilder('title')->setValue('Для кого?')->build(),
                $this->getBuilder('item.1')->setValue('Для тех, кто хочет освоить основы актерского мастерства.')->build(),
                $this->getBuilder('item.2')->setValue('Для тех, кто хочет открыть новые грани своей индивидуальности.')->build(),
                $this->getBuilder('item.3')->setValue('Для тех, кто хочет сменить свою профессию на более творческую.')->build(),
                $this->getBuilder('item.4')->setValue('Для тех, кто хочет весело провести 2 часа своего времени.')->build(),
            ],
            'levelUp.benefits' => [
                $this->getBuilder('title')->setValue('Для кого?')->build(),
                $this->getBuilder('item.1')->setValue('Для тех, кто хочет освоить основы актерского мастерства.')->build(),
                $this->getBuilder('item.2')->setValue('Для тех, кто хочет открыть новые грани своей индивидуальности.')->build(),
                $this->getBuilder('item.3')->setValue('Для тех, кто хочет сменить свою профессию на более творческую.')->build(),
                $this->getBuilder('item.4')->setValue('Для тех, кто хочет весело провести 2 часа своего времени.')->build(),
            ],
            'community.benefits' => [
                $this->getBuilder('title')->setValue('Для кого?')->build(),
                $this->getBuilder('item.1')->setValue('Для тех, кто хочет освоить основы актерского мастерства.')->build(),
                $this->getBuilder('item.2')->setValue('Для тех, кто хочет открыть новые грани своей индивидуальности.')->build(),
                $this->getBuilder('item.3')->setValue('Для тех, кто хочет сменить свою профессию на более творческую.')->build(),
                $this->getBuilder('item.4')->setValue('Для тех, кто хочет весело провести 2 часа своего времени.')->build(),
            ],
            'intro.benefits' => [
                $this->getBuilder('title')->setValue('Для кого?')->build(),
                $this->getBuilder('item.1')->setValue('Для тех, кто хочет освоить основы актерского мастерства.')->build(),
                $this->getBuilder('item.2')->setValue('Для тех, кто хочет открыть новые грани своей индивидуальности.')->build(),
                $this->getBuilder('item.3')->setValue('Для тех, кто хочет сменить свою профессию на более творческую.')->build(),
                $this->getBuilder('item.4')->setValue('Для тех, кто хочет весело провести 2 часа своего времени.')->build(),
            ],
            'placeholders' => [
                $this->getBuilder('name')->setValue('Имя')->build(),
                $this->getBuilder('phone')->setValue('Телефон')->build(),

            ],
            'modal' => [
                $this->getBuilder('title1')->setValue('Записаться на пробный урок')->build(),
                $this->getBuilder('franchise.title1')->setValue('Записаться на пробный урок')->build(),
                $this->getBuilder('franchise.subtitle1')->setValue('Записаться на пробный урок')->build(),
                $this->getBuilder('subtitle1')->setValue('Оставьте заявку и наш менеджер свяжется с вами в ближайшее время')->build(),
                $this->getBuilder('title2')->setValue('Заявка отправлена!')->build(),
                $this->getBuilder('subtitle2')->setValue('Заявка отправлена!')->build(),

            ],
            'user-benefits.trial' => [
                $this->getBuilder('title')->setValue('Что даст этот курс?')->build(),
                $this->getBuilder('item.title1')->setValue('Коммуникация')->build(),
                $this->getBuilder('item1')->setValue('Общение с единомышленниками, прокачка взаимодействий с другими людьми.')->build(),
                $this->getBuilder('item.title2')->setValue('Настроение')->build(),
                $this->getBuilder('item2')->setValue('Все происходит в дружественной атмосфере, только с положительными эмоциями')->build(),
                $this->getBuilder('item.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('item3')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
            ],
            'user-benefits.intro' => [
                $this->getBuilder('title')->setValue('Что даст этот курс?')->build(),
                $this->getBuilder('item.title1')->setValue('Коммуникация')->build(),
                $this->getBuilder('item1')->setValue('Общение с единомышленниками, прокачка взаимодействий с другими людьми.')->build(),
                $this->getBuilder('item.title2')->setValue('Настроение')->build(),
                $this->getBuilder('item2')->setValue('Все происходит в дружественной атмосфере, только с положительными эмоциями')->build(),
                $this->getBuilder('item.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('item3')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
            ],
            'user-benefits.levelUp' => [
                $this->getBuilder('title')->setValue('Что даст этот курс?')->build(),
                $this->getBuilder('item.title1')->setValue('Коммуникация')->build(),
                $this->getBuilder('item1')->setValue('Общение с единомышленниками, прокачка взаимодействий с другими людьми.')->build(),
                $this->getBuilder('item.title2')->setValue('Настроение')->build(),
                $this->getBuilder('item2')->setValue('Все происходит в дружественной атмосфере, только с положительными эмоциями')->build(),
                $this->getBuilder('item.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('item3')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
            ],
            'user-benefits.community' => [
                $this->getBuilder('title')->setValue('Что даст этот курс?')->build(),
                $this->getBuilder('item.title1')->setValue('Коммуникация')->build(),
                $this->getBuilder('item1')->setValue('Общение с единомышленниками, прокачка взаимодействий с другими людьми.')->build(),
                $this->getBuilder('item.title2')->setValue('Настроение')->build(),
                $this->getBuilder('item2')->setValue('Все происходит в дружественной атмосфере, только с положительными эмоциями')->build(),
                $this->getBuilder('item.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('item3')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
            ],
            'program' => [
                $this->getBuilder('title.trial')->setValue('Программа занятия')->build(),
                $this->getBuilder('trial.item1')->setValue('Освобождение от физических “зажимов”')->build(),
                $this->getBuilder('trial.item1.text')->setValue('Наши преподаватели проведут тренинги на баланс и координацию, расскажут и покажут секреты контроля твоего тела. Научат как раскрепоститься, избавится от физических зажимов, дать своему телу немного свободы.')->build(),
                $this->getBuilder('trial.item2')->setValue('Проработка базовых принципов сценической речи')->build(),
                $this->getBuilder('trial.item2.text')->setValue('Покажем простой и легкий трюк, который улучшит качество речи, сделает ее четкой с поставленной дикцией. Поделимся основами словесной импровизации. Ты поймешь, как говорить уверенно в жизни и на сцене.')->build(),
                $this->getBuilder('trial.item3')->setValue('Упражнения на скорость и многозадачность')->build(),
                $this->getBuilder('trial.item3.text')->setValue('Потренируем фокус и умение быстро “включаться” в любой процесс, независимо от внешних обстоятельств.')->build(),
                $this->getBuilder('trial.item4')->setValue('Наработка навыков актерского мастерства')->build(),
                $this->getBuilder('trial.item4.text')->setValue('Прокачаем актерские техники, играя. Обучим базовым навыкам импровизации. Покажем как прокачать память. Забудь о боязни публичных выступлений с академией КВАДРАТ.')->build(),
                $this->getBuilder('trial.item5')->setValue('Работа над контролем эмоций')->build(),
                $this->getBuilder('trial.item5.text')->setValue('Перепады настроения, неумение проявлять свои эмоции, контролировать их - давай разберемся с этим. Наши преподаватели покажут как обуздать весь спектр эмоций от ярости до бесконечного счастья и научиться правильно использовать их в жизни, что поможет обрести уверенность также и на сцене.')->build(),

                $this->getBuilder('title.introductory')->setValue('Программа занятия')->build(),
                $this->getBuilder('introductory.item1')->setValue('Введение в курс актерского мастерства')->build(),
                $this->getBuilder('introductory.item1.text')->setValue('Начинаем курс с того, что закладываем основу основ. Вы узнаете что такое актерское мастерство, что в нем особенного, почему так много людей мечтают перевоплотится и сыграть кого-то другого на сцене.')->build(),
                $this->getBuilder('introductory.item2')->setValue('Мышечная свобода и снятие зажимов')->build(),
                $this->getBuilder('introductory.item2.text')->setValue('Наши преподаватели проведут тренинги на баланс и координацию, расскажут и покажут секреты контроля твоего тела. Научат как раскрепоститься, избавится от физических зажимов, дать своему телу немного свободы.')->build(),
                $this->getBuilder('introductory.item3')->setValue('Развитие сценического внимания, памяти')->build(),
                $this->getBuilder('introductory.item3.text')->setValue('Сосредоточенность и внимание на сцене - неотъемлемые части как репетиций, так и выступлений на сцене. Мы учим концентрировать внимание для полного перевоплощения. Тебя ждут упражнение на развитие и усиление памяти.')->build(),
                $this->getBuilder('introductory.item4')->setValue('Работа с партнером')->build(),
                $this->getBuilder('introductory.item4.text')->setValue('Быть актером самому и уметь взаимодействовать с партнерами по сцене - этому мы учим в нашей академии.')->build(),
                $this->getBuilder('introductory.item5')->setValue('Тренинг эмоций')->build(),
                $this->getBuilder('introductory.item5.text')->setValue('Управление своими эмоции - это бесценный навык, который нужен как на сцене, так и в обычной жизни, на работе. Правдоподобность переживаний необходима, чтобы зритель действительно поверил в изображаемую эмоцию.')->build(),
                $this->getBuilder('introductory.item6')->setValue('Актерские наблюдения')->build(),
                $this->getBuilder('introductory.item6.text')->setValue('Умение наблюдать за всем и всеми вокруг тебя - важный навык актера. Чем больше ты наблюдаешь - тем проще перевоплощаться и ставить себя на место того или иного персонажа, образа.')->build(),
                $this->getBuilder('introductory.item7')->setValue('Сценическая речь')->build(),
                $this->getBuilder('introductory.item7.text')->setValue('Наши преподаватели помогут вам поставить четкую дикцию, улучшить качество речи как для жизни и работы, так и для игры на сцене.')->build(),
                $this->getBuilder('introductory.item8')->setValue('Сценическое движение')->build(),
                $this->getBuilder('introductory.item8.text')->setValue('Умение владеть своим телом, двигаться отточено, использовать правильные движения с соответствующей эмоцией показывают внешнюю технику игры актера. Научитесь контролировать свои жесты, чтобы и в обычной жизни вы могли управлять своим телом в совершенстве.')->build(),
                $this->getBuilder('introductory.item9')->setValue('Импровизация i')->build(),
                $this->getBuilder('introductory.item9.text')->setValue('Наши преподаватели не только прорабатывают базовые навыки речи и движений, но и умение концентрироваться, быстро реагировать на меняющиеся обстоятельства и условия. Как быстро сориентироваться и отыграть тот или иной эпизод или эмоцию - все это на наших занятиях.')->build(),
                $this->getBuilder('introductory.item10')->setValue('Актерская фантазия')->build(),
                $this->getBuilder('introductory.item10.text')->setValue('Дети постоянно о чем-то фантазируют - как они летают, попадают на космический корабль, воображают, что кровать - это машина, а пол - это лава. У взрослых появляется много забот и со временем умение фантазировать притупляется. Курс актерского мастерства помогает нам снова научится фантазировать, воображать и играть на сцене разные роли, переносится во времени и пространстве.')->build(),
                $this->getBuilder('introductory.item11')->setValue('Восприятие, воздействие и создание атмосферы')->build(),
                $this->getBuilder('introductory.item11.text')->setValue('Атмосферу в театре создают декорации, аплодисменты зрителей, одежда, макияж, мизансцены, музыка, но главный в воздействии и создании правильного настроения - однозначно актер. Мы вместе с вами научимся как создавать уникальную атмосферу на каждом спектакле.')->build(),
                $this->getBuilder('introductory.item12')->setValue('Выпускной и вручение сертификатов')->build(),
                $this->getBuilder('introductory.item12.text')->setValue('После окончания тебе предоставляется возможность продолжить образование в Академии КВАДРАТ с курсом Level Up.')->build(),
                $this->getBuilder('introductory.item13')->setValue('+')->build(),
                $this->getBuilder('introductory.item13.text')->setValue('+')->build(),

                $this->getBuilder('title.level-up')->setValue('Программа занятия')->build(),
                $this->getBuilder('level-up.item1')->setValue('Вхождение в различные образы')->build(),
                $this->getBuilder('level-up.item1.text')->setValue('Воображение и возможность представить себя в определенной роли и сыграть эту роль - это умение, которое нужно постоянно развивать. Профессия актера предполагает постоянный рост и работу над всеми навыками для того, чтобы легко входить в различные образы.')->build(),
                $this->getBuilder('level-up.item2')->setValue('Раскрытие своего глубинного потенциала')->build(),
                $this->getBuilder('level-up.item2.text')->setValue('скрыто.')->build(),
                $this->getBuilder('level-up.item3')->setValue('Импровизация i')->build(),
                $this->getBuilder('level-up.item3.text')->setValue('Навыки импровизации нужны не только актеру, но и обычному человеку. Учимся быстро реагировать на меняющиеся условия и обстоятельства, концентрировать внимание на цели разговора или действий. Навыки импровизации повышают уровень ваших творческих задатков.')->build(),
                $this->getBuilder('level-up.item4')->setValue('Глубинное раскрепощение')->build(),
                $this->getBuilder('level-up.item4.text')->setValue('Мышечные зажимы блокируют возможность в полной мере проявлять эмоции и чувства. Раскрепощение позволяет высвободить психическую энергию, которую актер направляет в свою игру. Именно достижение единства слов, эмоций и движений максимально раскрывает возможности человека в жизни и на сцене.')->build(),
                $this->getBuilder('level-up.item5')->setValue('Развитие воображения')->build(),
                $this->getBuilder('level-up.item5.text')->setValue('Часто даже в обычной жизни мы сталкиваемся с нетривиальными ситуациями, когда нужно срочно включить воображение и придумать решение. Такие решения часто нужно находить и на сцене. Чем богаче воображение актера, тем проще ему играть роль и быстро реагировать на любые изменения, включая и импровизацию.')->build(),
                $this->getBuilder('level-up.item6')->setValue('Формирование новых моделей поведения')->build(),
                $this->getBuilder('level-up.item6.text')->setValue('Начиная с детского возраста в нас закладываются определенные модели поведения, которые остаются с нами на всю жизнь и могут меняться с возрастом или дополнятся новыми моделями поведения. Наша задача - пополнить ваш запас формированием новых моделей поведения.')->build(),
                $this->getBuilder('level-up.item7')->setValue('Развитие творческой интуиции')->build(),
                $this->getBuilder('level-up.item7.text')->setValue('Мало освоить ремесло быть актером. Без высокого уровня творческого мышления, интуиции - любой человек не сможет подняться выше определенной планки, как на сцене, так и в другой работе.')->build(),
                $this->getBuilder('level-up.item8')->setValue('Умение управлять своим состоянием')->build(),
                $this->getBuilder('level-up.item8.text')->setValue('Умение управлять своим эмоциональным состоянием тесно связано с умением управлять своим телом. Эта связь работает в две стороны, поэтому актеру так важно понимать какая поза присуща определенному психоэмоциональному состоянию: радости, грусти, влюбленности, удивления. Этому мы обучаем на нашем курсе.')->build(),
                $this->getBuilder('level-up.item9')->setValue('Продуктивность и внутренняя энергия')->build(),
                $this->getBuilder('level-up.item9.text')->setValue('Откройте в себе неиссякаемый источник энергии и творчества. Мы помогаем вам повысить жизненные силы, успевать больше за меньшее количество времени, и при этом не забывать заботится о себе.')->build(),
                $this->getBuilder('level-up.item10')->setValue('Создание индивидуального образа')->build(),
                $this->getBuilder('level-up.item10.text')->setValue('Индивидуальность актера - это именно то, что позволяет зрителю без сомнений угадывать в любом образе именно тебя. На наших занятиях будем рассказывать и показывать как создать индивидуальный образ.')->build(),
                $this->getBuilder('level-up.item11')->setValue('Взаимодействие со зрителем')->build(),
                $this->getBuilder('level-up.item11.text')->setValue('Зритель - это неотъемлемая часть театрального действа. Талант актера также оценивается и его умением завлечь, заинтересовать зрителя, взаимодействовать с ним.')->build(),
                $this->getBuilder('level-up.item12')->setValue('+')->build(),
                $this->getBuilder('level-up.item12.text')->setValue('+')->build(),

                $this->getBuilder('title.community')->setValue('Программа занятия')->build(),
                $this->getBuilder('community.item1')->setValue('Закрытые занятия 1 раз в неделю только для выпускников')->build(),
                $this->getBuilder('community.item1.text')->setValue('Каждую неделю мы встречаемся, чтобы провести время с пользой и удовольствием в кругу единомышленников, таких же актеров как и мы. Ведь с Академией КВАДРАТ нет предела совершенству.')->build(),
                $this->getBuilder('community.item2')->setValue('Участие в мастер-классах и неформальных встречах Академии')->build(),
                $this->getBuilder('community.item2.text')->setValue('Для участников клуба мы организовываем различные мастер-классы, которые позволяют продолжать совершенствоваться в актерском мастерстве. Мы также организовываем неформальные вечера настолок, мафии, тусовки.')->build(),
                $this->getBuilder('community.item3')->setValue('Постановка выступлений')->build(),
                $this->getBuilder('community.item3.text')->setValue('Совместные постановки - это реальная практика для актеров, которая происходит под присмотром и с подсказками преподавателей Академии КВАДРАТ - действующих актеров театра и кино с многолетним опытом.')->build(),
                $this->getBuilder('community.item4')->setValue('Съемка коротких метров')->build(),
                $this->getBuilder('community.item4.text')->setValue('Приобщение к кинематографу - еще один бонус для членов нашего клуба. Мы постоянно снимаем короткий метр с членами нашего клуба, нашими бывшими учениками.')->build(),
                $this->getBuilder('community.item5')->setValue('Совместные выезды')->build(),
                $this->getBuilder('community.item5.text')->setValue('Клуб КВАДРАТ Community предлагает не только проводить занятия в помещении, но и совершать совместные выезды.')->build(),
                $this->getBuilder('community.item6')->setValue('Единый абонемент с помесячной оплатой')->build(),
                $this->getBuilder('community.item6.text')->setValue('Для членов клуба действует система помесячных абонементов, которые включают в себя 4 занятия в КВАДРАТ Community каждый месяц.')->build(),
            ],
            'main' => [
                $this->getBuilder('main.sub-title')->setValue('Академия актёрского мастерства в одессе')->build(),
                $this->getBuilder('main.title')->setValue('Открой в себе талант!')->build(),
                $this->getBuilder('main.text')->setValue('Пересматриваешь любимый фильм снова и снова и представляешь себя в главной роли? Думаешь, что не смог бы так же? Хватит гадать - давай откроем в
								тебе талант вместе! Преподаватели академии КВАДРАТ подарят тебе главное - опыт, который можно пережить только самому , и который останется с
								тобой на всю жизнь.')->build(),

                $this->getBuilder('about.video-title')->setValue('Видео о нас')->build(),
                $this->getBuilder('about.title')->setValue('Об академии')->build(),
                $this->getBuilder('about.text1')->setValue('Академия КВАДРАТ смело берется за проработку четырех сфер твоей индивидуальности.')->build(),
                $this->getBuilder('about.text2')->setValue('Мышление. Раскрепощаем и помогаем тебе быть собой в жизни и на сцене. Эмоции. Открываем скрытые таланты, вызываем на эмоцию, помогаем раскрасить
								твою жизнь новыми оттенками и красками.')->build(),
                $this->getBuilder('about.text3')->setValue('Тело. Актерское мастерство - это не только о психоэмоциональном развитии, а еще и о соответствующей физической подготовке. Мы готовим тебя быть
								выносливым при любых обстоятельствах в жизни или на сцене.')->build(),
                $this->getBuilder('about.text4')->setValue('Коммуникация. С нами ты обретешь решительность, упорство, уверенность в себе. Научись мыслить шире, видеть больше, чувствовать сильнее. Найди в
								себе легкость, научись парить даже на собственной кухне.')->build(),
                $this->getBuilder('about.text5')->setValue('Коммуникация. С нами ты обретешь решительность, упорство, уверенность в себе. Научись мыслить шире, видеть больше, чувствовать сильнее. Найди в
                                                себе легкость, научись парить даже на собственной кухне.')->build(),

                $this->getBuilder('benefits.main-title')->setValue('Преимущества')->build(),
                $this->getBuilder('benefits.title1')->setValue('Подход')->build(),
                $this->getBuilder('benefits.text1')->setValue('Мы не обещаем тебе, что ты 100% будешь актером. Наша команда влюбляет тебя в себя в каждом этюде, импровизации, взаимодействии. Быть актером или
                        просто весело проводить время с нами - решать тебе самому.')->build(),
                $this->getBuilder('benefits.title2')->setValue('Качество сервиса')->build(),
                $this->getBuilder('benefits.text2')->setValue('Команда школы КВАДРАТ постоянно стремиться расти, развиваться и повышать уровень сервиса. Каждое наше занятие - это праздник, на который наши
                        клиенты хотят возвращаться снова и снова.')->build(),
                $this->getBuilder('benefits.title3')->setValue('Профессионализм')->build(),
                $this->getBuilder('benefits.text3')->setValue('Все наши преподаватели - действующие профессиональные актеры с опытом работы на театральной сцене и в кино. Они помогут раскрыть и прокачать
                        актерские навыки, и научится их применять в работе и жизни.')->build(),

                $this->getBuilder('franchise.title')->setValue('Франшиза')->build(),
                $this->getBuilder('franchise.text')->setValue('Театральная школа КВАДРАТ - это крупнейшая сеть актерских школ в Украине, и это не предел. В наших планах расширяться и открывать наши филиалы в
                    новых городах. Команда КВАДРАТ - это профессиональные дипломированные педагоги, которые находят подход к любому ученику. Для таких же как мы,
                    влюбленных в актерское дело, мы предлагаем успешно проверенную бизнес-модель с поддержкой для партнеров на всех этапах запуска. Успей
                    присоединится к команде творческих и креативных ребят став нашим партнером.')->build(),

                $this->getBuilder('gallery.title')->setValue('Галерея')->build(),

                $this->getBuilder('service.title')->setValue('Галерея')->build(),
                $this->getBuilder('service.text')->setValue('Академия актерского мастерства КВАДРАТ предоставляет всем желающим возможность немного приблизится к пониманию того, кто же такой актер.
                            Бесплатное занятие по актерскому мастерству для взрослых позволит приоткрыть кулисы и погрузится в дружелюбную атмосферу нашей школы. Забудь о
                            зажимах и стереотипах, отбрось все страхи и брось себе вызов - начни менять жизнь. Мы предлагаем тебе позитивную энергетику, тренинги на
                            баланс и координацию, наработку навыков актерского мастерства и многое другое.')->build(),

                $this->getBuilder('teacher.title')->setValue('Преподаватели')->build(),
                $this->getBuilder('teacher.name1')->setValue('Богдан ЛОБОДА')->build(),
                $this->getBuilder('teacher.profile1')->setValue('Актерское мастерство и сценическая речь')->build(),
                $this->getBuilder('teacher.description1')->setValue('Актер театра и кино. Ведущий и организатор мероприятий. Финалист проекта “Україна має талант”. Музыкант фронтмен группы B.A.R.D.')->build(),
                $this->getBuilder('teacher.name2')->setValue('Валентин ЕЛЕНЕВСКИЙ')->build(),
                $this->getBuilder('teacher.profile2')->setValue('Актёрское мастерство')->build(),
                $this->getBuilder('teacher.description2')->setValue('Актер театра и кино. Ведущий мероприятий. Опыт преподавания более 5 лет. Медицинское образование. Увлекается психологией, нейробиологией,
                        музыкой.')->build(),
                $this->getBuilder('teacher.name3')->setValue('Евгений МОРГУН')->build(),
                $this->getBuilder('teacher.profile3')->setValue('Актёрское мастерство')->build(),
                $this->getBuilder('teacher.description3')->setValue('Дипломированный актер театра и кино. Театральный педагог. Участник международных театральных фестивалей в Польше, Сербии и других странах.
                        Лауреат стипендии им. Леся Курбаса.')->build(),
                $this->getBuilder('teacher.name4')->setValue('Екатерина РЕЗНИК')->build(),
                $this->getBuilder('teacher.profile4')->setValue('Актёрское мастерство')->build(),
                $this->getBuilder('teacher.description4')->setValue('Дипломированная актриса театра и кино. Преподаватель актерского мастерства. 10 лет на сцене театра. Актриса театра “Кімната Т”.')->build(),
                $this->getBuilder('teacher.name5')->setValue('Ярослав СОЛОМКО')->build(),
                $this->getBuilder('teacher.profile5')->setValue('Актерское мастерство и сценическая пластика')->build(),
                $this->getBuilder('teacher.description5')->setValue('Дипломированный актер театра и кино. Участник всеукраинских и международных театральных фестивалей в Польше, Словакии, Германии и других
                        странах. Актер театра “P.S.” Ведущий событий различного формата и масштаба.')->build(),
                $this->getBuilder('teacher.name6')->setValue('Владислав АРТЕМОВ')->build(),
                $this->getBuilder('teacher.profile6')->setValue('Актерское мастерство и сценическая речь')->build(),
                $this->getBuilder('teacher.description6')->setValue('Магистр факультета Кино и телевидения. Актер театра и кино (“Новенька”, “Цвет страсти”). Провел более 100 групповых занятий. Увлекается спортом,
                        юмором и поэзией.')->build(),
                $this->getBuilder('teacher.name7')->setValue('Дмитрий ГАЛИМОВ-ЦВЕТНОЙ')->build(),
                $this->getBuilder('teacher.profile7')->setValue('Актерское мастерство и сценическая речь')->build(),
                $this->getBuilder('teacher.description7')->setValue('Магистр факультета Театрального искусства. Актер театра и кино. Более 10 лет стажа. Увлекается паркуром, кино, юмором и написанием сценариев.')->build(),

            ],
            'about-course' => [
                $this->getBuilder('trial.title')->setValue('Пробный урок')->build(),
                $this->getBuilder('level-up.title')->setValue('КВАДРАТ LEVEL UP')->build(),
                $this->getBuilder('introduction.title')->setValue('КВАДРАТ вводный')->build(),
                $this->getBuilder('community.title')->setValue('КВАДРАТ Community')->build(),
                $this->getBuilder('level-up')->setValue('Хватит жить страхами, предубеждениями и стереотипами. Приходи на пробное занятие в Академию актерского мастерства КВАДРАТ - здесь ты можешь быть
                    собой, быть разным. Помнишь как в детстве? Вспомни как здорово и легко было быть грустным, смешным, кривляться, баловаться и даже злиться по
                    особому. Теперь ты даже взрослый сможешь погрузится в атмосферу волшебства.')->build(),
                $this->getBuilder('introduction')->setValue('Хватит жить страхами, предубеждениями и стереотипами. Приходи на пробное занятие в Академию актерского мастерства КВАДРАТ - здесь ты можешь быть
                    собой, быть разным. Помнишь как в детстве? Вспомни как здорово и легко было быть грустным, смешным, кривляться, баловаться и даже злиться по
                    особому. Теперь ты даже взрослый сможешь погрузится в атмосферу волшебства.')->build(),
                $this->getBuilder('community')->setValue('Хватит жить страхами, предубеждениями и стереотипами. Приходи на пробное занятие в Академию актерского мастерства КВАДРАТ - здесь ты можешь быть
                    собой, быть разным. Помнишь как в детстве? Вспомни как здорово и легко было быть грустным, смешным, кривляться, баловаться и даже злиться по
                    особому. Теперь ты даже взрослый сможешь погрузится в атмосферу волшебства.')->build(),
                $this->getBuilder('trial')->setValue('Хватит жить страхами, предубеждениями и стереотипами. Приходи на пробное занятие в Академию актерского мастерства КВАДРАТ - здесь ты можешь быть
                    собой, быть разным. Помнишь как в детстве? Вспомни как здорово и легко было быть грустным, смешным, кривляться, баловаться и даже злиться по
                    особому. Теперь ты даже взрослый сможешь погрузится в атмосферу волшебства.')->build(),
            ],
            'founder' => [
                $this->getBuilder('founder.title')->setValue('Слово основателя')->build(),
                $this->getBuilder('founder.text1')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений. Равным
                образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных задач. А ещё
                активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими инстанциями.')->build(),
                $this->getBuilder('founder.text2')->setValue('Равным образом, убеждённость некоторых оппонентов обеспечивает актуальность как самодостаточных, так и внешне зависимых концептуальных решений! А
                ещё независимые государства указаны как претенденты на роль ключевых факторов.')->build(),
            ],

            'franchise' => [
                $this->getBuilder('achiev.main-title')->setValue('Достижение бренда')->build(),
                $this->getBuilder('achiev.title1')->setValue('Более 2000')->build(),
                $this->getBuilder('achiev.text1')->setValue('Человек посетили Академию Квадрат только с начала этого года')->build(),
                $this->getBuilder('achiev.title2')->setValue('Более 100')->build(),
                $this->getBuilder('achiev.text2')->setValue('Наших выпускников выступили на сцене театра только с начала этого года')->build(),
                $this->getBuilder('achiev.title3')->setValue('75%')->build(),
                $this->getBuilder('achiev.text3')->setValue('Средний NPS наших клиентов выше (мы постоянно собираем обратную связь, NPS - уровень удовлетворенности клиентов, средний бал 8.7 из 10 по
                    обратной связи наших клиентов)')->build(),
                $this->getBuilder('achiev.title4')->setValue('Более 20')->build(),
                $this->getBuilder('achiev.text4')->setValue('Человек в команде Управляющей компании (Менеджеры и педагоги помогут Вам с открытием и развитием в Вашем городе)')->build(),
                $this->getBuilder('achiev.title5')->setValue('Более 40 лет')->build(),
                $this->getBuilder('achiev.text5')->setValue('Суммарный опыт преподования наших педагогов')->build(),

                $this->getBuilder('audience.main-title')->setValue('О нашей ЦА')->build(),
                $this->getBuilder('audience.title1')->setValue('от 15 до 60 лет')->build(),
                $this->getBuilder('audience.text1')->setValue('Так как в первую очередь мы продаем эмоции и тусовку, то Ваша потенциальная ЦА это все жители Вашего региона в возрасте от 15 до 60 лет')->build(),
                $this->getBuilder('audience.title2')->setValue('760 000 человек')->build(),
                $this->getBuilder('audience.text2')->setValue('В Одессе к примеру наша ЦА это 760 000 человек')->build(),
                $this->getBuilder('audience.title3')->setValue('15% средняя конверсия')->build(),
                $this->getBuilder('audience.text3')->setValue('15% средняя конверсия из холодного лида в клиента.')->build(),

                $this->getBuilder('franchise-trial.text')->setValue('Записаться на пробное занятие')->build(),
                $this->getBuilder('franchise-trial.btn')->setValue('Записаться на пробное занятие')->build(),

                $this->getBuilder('benefits.main-title')->setValue('Преимущества франшизы')->build(),
                $this->getBuilder('benefits.title1')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits.text1')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
                $this->getBuilder('benefits.title2')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits.text2')->setValue(' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
                $this->getBuilder('benefits.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits.text3')->setValue(' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),

                $this->getBuilder('invest.title')->setValue('Инвестиции в франшизу')->build(),
                $this->getBuilder('invest.subtitle')->setValue('Стоимость пашульного взноса:')->build(),
                $this->getBuilder('invest.text')->setValue('Готовый пошаговый план запуска и развития бизнеса (полные пошаговые инструкции открытия, начиная от поиска и найма педагогов, настройки
                    рекламных компаний и получения первых лидов, методологии и сценарии проведения занятий, скрипты и система продаж, CRM система и автоматизация)')->build(),
                $this->getBuilder('invest.subtitle')->setValue(FranchisePackages::class)->build(),
                $this->getBuilder('invest.subtitle')->setValue('Стоимость пашульного взноса:')->build(),
                $this->getBuilder('invest.item1.title')->setValue('Показатели')->build(),
                $this->getBuilder('invest.item2.title')->setValue('Население города')->build(),
                $this->getBuilder('invest.item2.value1')->setValue('до 400 тыс. чел.')->build(),
                $this->getBuilder('invest.item2.value2')->setValue('от 400 тыс. до 900 тыс. чел.')->build(),
                $this->getBuilder('invest.item2.value3')->setValue('от 900 тыс')->build(),
                $this->getBuilder('invest.item3.title')->setValue('Стоимость франшизы')->build(),
                $this->getBuilder('invest.item4.title')->setValue('Общая сумма')->build(),
                $this->getBuilder('invest.item5.title')->setValue('Срок окупаемости')->build(),
                $this->getBuilder('invest.item5.value1')->setValue('от 3-4 месяца')->build(),
                $this->getBuilder('invest.item5.value2')->setValue('от 3-4 месяца')->build(),
                $this->getBuilder('invest.item5.value3')->setValue('от 5-6 месяца')->build(),
                $this->getBuilder('invest.item6.title')->setValue('Планируемая чистая прибыль партнера')->build(),
                $this->getBuilder('invest.item7.title')->setValue('Роялти (от выручки)')->build(),

                $this->getBuilder('pack.title')->setValue('Франшиза')->build(),
                $this->getBuilder('pack.subtitle')->setValue('Франч пакет состоит из таких разделов :')->build(),
                $this->getBuilder('pack.plan')->setValue('Готовый пошаговый план запуска и развития бизнеса (полные пошаговые инструкции открытия, начиная от поиска и найма педагогов, настройки рекламных
                компаний и получения первых лидов, методологии и сценарии проведения занятий, скрипты и система продаж, CRM система и автоматизация)')->build(),
                $this->getBuilder('pack.item1')->setValue('Поиск и найм педагогов')->build(),
                $this->getBuilder('pack.item2')->setValue('Поиск помещения')->build(),
                $this->getBuilder('pack.item3')->setValue('Оформление социальных сетей')->build(),
                $this->getBuilder('pack.item4')->setValue('Запуск рекламы')->build(),
                $this->getBuilder('pack.item5')->setValue('Приглашение на пробное')->build(),
                $this->getBuilder('pack.item6')->setValue('Проведение пробного занятия')->build(),
                $this->getBuilder('pack.item7')->setValue('Найм администраторов \ менеджеров')->build(),
                $this->getBuilder('pack.item8')->setValue('Управленческая отчетность и ежедневные метрики')->build(),
                $this->getBuilder('pack.item9')->setValue('Курс "Квадрат Вводный"')->build(),
                $this->getBuilder('pack.item10')->setValue('Выпускной')->build(),
                $this->getBuilder('pack.item11')->setValue('Лого и раздаточные материалы')->build(),
                $this->getBuilder('pack.item12')->setValue('Обучение по продажам')->build(),
                $this->getBuilder('pack.item13')->setValue('Юридические договора')->build(),
                $this->getBuilder('pack.item14')->setValue('CRM система')->build(),
                $this->getBuilder('pack.item15')->setValue('Отзывы клиентов')->build(),
                $this->getBuilder('pack.item16')->setValue('Бренд и товарный знак')->build(),
                $this->getBuilder('pack.checkbox1')->setValue('Настроим рекламные компании в интернете и получим первых 100 Лидов')->build(),
                $this->getBuilder('pack.checkbox2')->setValue('Настроим рекламные компании в интернете и получим первых 100 Лидов')->build(),
                $this->getBuilder('pack.checkbox3')->setValue('Настроим рекламные компании в интернете и получим первых 100 Лидов')->build(),

                $this->getBuilder('main.title')->setValue('Франшиза')->build(),
                $this->getBuilder('main.text')->setValue('Хотите стать актером? Успешным и востребованным? Сниматься в большом кино и играть на сцене театра? Или просто хотите раскрыть себя и узнать на
                что вы способны? Съемки, постановки этюдов, режиссура – все это неотъемлемая практическая часть обучения под руководством знаменитых актеров и
                педагогов.')->build(),
                
                $this->getBuilder('review.title')->setValue('Что говорят наши франчайзи-партнеры ?')->build(),

                $this->getBuilder('services.title1')->setValue('Пробное занятие')->build(),
                $this->getBuilder('services.text1')->setValue(' Академия актерского мастерства КВАДРАТ предоставляет всем желающим возможность немного приблизится к пониманию того, кто же такой актер.
                            Бесплатное занятие по актерскому мастерству для взрослых позволит приоткрыть кулисы и погрузится в дружелюбную атмосферу нашей школы. Забудь
                            о зажимах и стереотипах, отбрось все страхи и брось себе вызов - начни менять жизнь. Мы предлагаем тебе позитивную энергетику, тренинги на
                            баланс и координацию, наработку навыков актерского мастерства и многое другое.')->build(),
                $this->getBuilder('services.title2')->setValue('КВАДРАТ вводный')->build(),
                $this->getBuilder('services.text2')->setValue('Что дает вводный курс нашей академии? Ты получишь актерские навыки для жизни, сцены, отношений, работы и бизнеса, которые пригодятся во все
                            времена, любые кризисы и эпохи. На 3 месяца ты погружаешься в атмосферу веселья и одновременно огромной работы над собой. Мы поможем тебе
                            уверенно выйти на сцену, обрести спокойствие и баланс внутри себя.')->build(),
                $this->getBuilder('services.title3')->setValue('КВАДРАТ LEVEL UP')->build(),
                $this->getBuilder('services.text3')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений.
                            Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных
                            задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими
                            инстанциями.')->build(),
                $this->getBuilder('services.title4')->setValue('КВАДРАТ Community')->build(),
                $this->getBuilder('services.text4')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений.
                            Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных
                            задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими
                            инстанциями.')->build(),

                $this->getBuilder('start.main-title')->setValue('Этапы запуска франшизы')->build(),
                $this->getBuilder('start.step1')->setValue('ШАГ 1')->build(),
                $this->getBuilder('start.title1')->setValue('Знакомство')->build(),
                $this->getBuilder('start.text1')->setValue('Вы оставляете заявку, заполняете анкету потенциального партнера и мы знакомимся с Вами по телефону или в ZOOM. Обсуждаем ожидания от
                    сотрудничества')->build(),
                $this->getBuilder('start.step2')->setValue('ШАГ 2')->build(),
                $this->getBuilder('start.title2')->setValue('Знакомство с условиями')->build(),
                $this->getBuilder('start.text2')->setValue(' Вы изучаете договор, финансовую модель, узнаете о том, как происходит взаимодействие франчайзи с управляющей компанией')->build(),
                $this->getBuilder('start.step3')->setValue('ШАГ 3')->build(),
                $this->getBuilder('start.title3')->setValue('Договор и пакет франшизы')->build(),
                $this->getBuilder('start.text3')->setValue('Подписываем документы. Вы оплачиваете паушальный взнос. Передаем вам пакет франшизы и назначаем дату обучения')->build(),
                $this->getBuilder('start.step4')->setValue('ШАГ 4')->build(),
                $this->getBuilder('start.title4')->setValue('Обучение в Одессе')->build(),
                $this->getBuilder('start.text4')->setValue('Основатели и команда «Квадрата» расскажут все о франшизе, научат вести бизнес, управлять и приводить клиентов и главное покажут все на примере
                    нашей работающей академии.')->build(),
                $this->getBuilder('start.step5')->setValue('ШАГ 5')->build(),
                $this->getBuilder('start.title5')->setValue('Ваша собственная школа')->build(),
                $this->getBuilder('start.text5')->setValue('Запуск и поддержка.. Руководитель выезжает к вам. И помогает открыть вашу собственную академию "Квадрат"')->build(),
                $this->getBuilder('start.start-step')->setValue('Время действовать!')->build(),
                $this->getBuilder('start.start-step.text')->setValue('Свяжись прямо сейчас!')->build(),

                $this->getBuilder('story.title')->setValue('История компании')->build(),
                $this->getBuilder('story.text')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений.
                            Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных
                            задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими
                            инстанциями.')->build(),

                $this->getBuilder('benefits-user.main-title')->setValue('Преимущества компании')->build(),
                $this->getBuilder('benefits-user.title1')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits-user.text1')->setValue('Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
                $this->getBuilder('benefits-user.title2')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits-user.text2')->setValue(' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),
                $this->getBuilder('benefits-user.title3')->setValue('Название преимущества')->build(),
                $this->getBuilder('benefits-user.text3')->setValue(' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')->build(),

                $this->getBuilder('slovo.title')->setValue('Слово основателя')->build(),
                $this->getBuilder('slovo.item1')->setValue('У меня есть мечта Я хочу видеть больше улыбок, на лицах людей которых я встречаю каждый день Видеть больше открытых людей, которые не бояться коммуницировать с окружающими Людей которые могут танцевать без алкоголя и не бояться проявлять свои эмоции Людей мечтателей, которые делают то что им нравится и не зависят от оценки окружающих Людей которые не бояться реализовать свою мечту в любом возрасте ')->build(),
                $this->getBuilder('slovo.item2')->setValue('Людей для которых любое публичное выступление на сцене это удовольствие в перемешку с приятным волнением, в не зависимости от того, сколько людей в зале перед ними Людей которые не прячут свои таланты в долгий ящик, а раскрывают их Людей которые кайфуют от каждого момента своей жизни и любят себя и весь Мир вокруг них ')->build(),
                $this->getBuilder('slovo.item3')->setValue('И я верю в то, что Квадрат объединяет таких людей по всей стране И скоро будет объединять по всему Миру Меня всегда зажигали проекты, в которых видны наглядные позитивные изменения людей, и я знаю что каждый человек который пришёл к нам даже на 2 часа бесплатного пробного занятия становиться лучше, чем был до нас В этом и есть наша миссия - делать жизни людей лучше')->build(),


            ],
            'buttons' => [
                $this->getBuilder('sign-up.button')->setValue('Записаться на урок')->build(),
                $this->getBuilder('show-more.button')->setValue('Узнать подробнее')->build(),
                $this->getBuilder('send.button')->setValue('Отправить')->build(),
                $this->getBuilder('show-details.button')->setValue('Узнать детали')->build(),
                $this->getBuilder('send.ok')->setValue('Окей')->build(),

            ],
            'header' => [
                $this->getBuilder('course-title')->setValue('Курсы')->build(),
                $this->getBuilder('trial')->setValue('Пробное занятие')->build(),
                $this->getBuilder('intro')->setValue('КВАДРАТ вводный')->build(),
                $this->getBuilder('upper')->setValue('КВАДРАТ LEVEL UP')->build(),
                $this->getBuilder('community')->setValue('КВАДРАТ Community')->build(),
                $this->getBuilder('franchise')->setValue('Франшиза')->build(),
                $this->getBuilder('city.odessa')->setValue('Одесса')->build(),
                $this->getBuilder('city.kyiv')->setValue('Киев')->build(),
                $this->getBuilder('city.kharkov')->setValue('Харьков')->build(),
                $this->getBuilder('city.dnepr')->setValue('Днепр')->build(),
                $this->getBuilder('city.gorod')->setValue('Город')->build(),
            ],
            'error' => [
                $this->getBuilder('input.required')->setValue('Заполните это поле')->build(),
            ],
            'footer' => [
                $this->getBuilder('title')->setValue('Контакты')->build(),
                $this->getBuilder('work-time')->setValue('Время работы:')->build(),
                $this->getBuilder('address')->setValue('Адрес:')->build(),
                $this->getBuilder('сonfidentiality')->setValue('Политика Конфиденциальности')->build(),
                $this->getBuilder('oferta')->setValue('Публичная оферта:')->build(),
            ],






        ];
        foreach ($groups as $groupName => $group) {
            foreach ($group as $key => $item) {
                if (!is_array($item)) {
                    $item = ['key' => $key, 'value' => $item];
                }
                $item['group'] = $groupName;
                $item['key'] = $this->addPrefixGroupToKey($item['key'], $groupName);
                if ($this->translateExists($item['key'])) {
                    continue;
                }
                $translate = $this->repository->create($item);
                $this->addTranslate($translate->getAttribute('key'), $translate);
            }
        }
    }

    private function getTranslateByKey(string $key): ?Translate
    {
        return \Arr::get($this->translates, $key);
    }

    private function translateExists(string $key): bool
    {
        return (bool)$this->getTranslateByKey($key);
    }

    private function addPrefixGroupToKey(string $key, string $prefix)
    {
        return implode('.', [$prefix, $key]);
    }

    private function getBuilder(?string $key = null)
    {
        $builder = new class implements \ArrayAccess {
            private $key = '';

            private $value = '';

            private $type = 'text';

            private $displayName = '';

            private $subGroup = '';

            /** @var array */
            private $variables = [];

            public function setKey(string $key): self
            {
                $this->key = $key;
                return $this;
            }

            public function setTypeText(): self
            {
                $this->type = Translate::TYPE_TEXT;
                return $this;
            }

            public function setTypeTextarea(): self
            {
                $this->type = Translate::TYPE_TEXTAREA;
                return $this;
            }

            public function setTypeEditor(): self
            {
                $this->type = Translate::TYPE_EDITOR;
                return $this;
            }

            public function setValue(?string $value = null): self
            {
                $this->value = $value;
                return $this;
            }

            public function setDisplayName(string $displayName): self
            {
                $this->displayName = $displayName;
                return $this;
            }

            /**
             * @param array $variables
             */
            public function setVariables(array $variables): self
            {
                $this->variables = $variables;
                return $this;
            }

            public function build()
            {
                return [
                    'key' => $this->key,
                    'value' => $this->value,
                    'type' => $this->type,
                    'display_name' => $this->displayName,
//					'variables'    => $this->variables,
                    'sub_group' => $this->subGroup,
                ];
            }

            public function offsetGet($offset)
            {
                return $this->offsetExists($offset) ? $this->{$offset} : null;
            }

            public function offsetExists($offset)
            {
                return property_exists($this, $offset);
            }

            public function offsetSet($offset, $value)
            {
                $method = Str::camel('set' . ucfirst($offset));
                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }

            public function offsetUnset($offset)
            {
                if ($this->offsetExists($offset)) {
                    $this->{$offset} = null;
                }
            }

            /**
             * @param string $subGroup
             */
            public function setSubGroup(string $subGroup): self
            {
                $this->subGroup = $subGroup;
                return $this;
            }

        };
        if ($key) {
            $builder->setKey($key);
        }
        return $builder;
    }

    private function addTranslate(string $key, Translate $translate)
    {
        $this->translates->offsetSet($key, $translate);
    }
}
