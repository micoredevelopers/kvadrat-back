<?php

namespace Database\Seeders;

use App\Models\MenuGroup;
use Illuminate\Database\Seeder;

class MenuGroupSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$groups = [
			['name' => 'Основное меню', 'role' => 'main_menu'],
			['name' => 'Меню страниц', 'role' => 'pages_menu'],
			['name' => 'Футер разделы', 'role' => 'footer_sections'],
			['name' => 'Футер помощь', 'role' => 'footer_help'],
		];
		foreach ($groups as $group) {
			if ($this->exists($group['role'])) {
				continue;
			}
			MenuGroup::create($group);
		}
	}

	private function exists($role): bool
	{
		return (bool)MenuGroup::where('role', $role)->count();
	}
}
