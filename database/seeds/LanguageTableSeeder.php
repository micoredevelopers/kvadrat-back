<?php
	namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$languages = [
			[
				'name'    => 'Русский',
				'key'     => 'ru',
				'active'  => 1,
				'default' => 0,
			],
//			[
//				'name'    => 'English',
//				'key'     => 'en',
//				'active'  => 1,
//				'default' => 1,
//			],
			[
				'name'    => 'Українська',
				'key'     => 'ua',
				'active'  => 1,
				'default' => 1,
			],
		];
		foreach ($languages as $language) {
			\App\Models\Language::create($language);
		}

	}
}
