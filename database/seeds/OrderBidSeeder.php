<?php
namespace Database\Seeders;

use App\Models\User;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;

class OrderBidSeeder extends Seeder
{
	private $users;

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(\Faker\Generator $factory,
								OrderRepository $orderRepository,
								OrderBidRepository $bidRepository,
								UserRepository $userRepository
	)
	{
		$this->factory = $factory;
		$this->orderRepository = $orderRepository;
		$this->bidRepository = $bidRepository;
		$this->userRepository = $userRepository;
		$this->users = $userRepository->all()->keyBy('id');
	}


	public function run()
	{
		$batches = [];
		/** @var  $order \App\Models\Order\Order */
		$now = now();
		foreach ($this->orderRepository->all() as $order) {
			foreach (range(1, random_int(3, 6)) as $i) {
				$user = $this->randomUser();
				$batches[] = [
					$order->getForeignKey() => $order->getKey(),
					$user->getForeignKey()  => $user->getKey(),
					'text'                  => $this->factory->text,
					'bid'                   => $this->factory->numberBetween(50, 35550),
					'days'                  => $this->factory->numberBetween(3, 30),
					'created_at'           => $now,
					'updated_at'           => $now,
				];
			}
		}
		foreach (array_chunk($batches, 5000) as $num => $chunk) {
			$this->bidRepository->insert($chunk);
		}
	}

	private function randomUser(): User
	{
		static $count;
		if (null === $count) {
			$count = $this->users->count();
		}
		return $this->users->get(random_int(1, $count));
	}
}
