<?php

	namespace Database\Seeders;

	use App\Events\Order\OrderBeforePublished;
	use App\Events\Order\OrderCreatedEvent;
	use App\Events\Order\OrderPublished;
	use App\Models\Category\Category;
	use App\Models\City;
	use App\Models\Order\Order;
	use App\Models\User;
	use App\Repositories\CategoryRepository;
	use App\Repositories\CityRepository;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\UserRepository;
	use Illuminate\Database\Seeder;

	class OrdersSeeder extends Seeder
	{

		private $limit = 100000;
		/**
		 * @var \Faker\Generator
		 */
		private $factory;
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
		 */
		private $users;
		/**
		 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
		 */
		private $categories;
		/**
		 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
		 */
		private $cities;

		public function __construct(
			\Faker\Generator $factory,
			OrderRepository $orderRepository,
			UserRepository $userRepository,
			CategoryRepository $categoryRepository,
			CityRepository $cityRepository
		)
		{
			$this->factory = $factory;
			$this->orderRepository = $orderRepository;
			$this->users = User::limit($this->limit)->get()->keyBy('id');
			$this->categories = $categoryRepository->all()->keyBy('id');
			$this->cities = $cityRepository->all()->keyBy('id');
		}

		public function run()
		{
			$batch = [];
			$now = now();
			$closesAt = now()->addDays(14);
			foreach (range(1, env('DEV_SEED') ? 10000 : 100) as $i) {
				$performerId = $this->randomUser()->getKey();
				$customerId = $this->randomUser()->getKey();
				$categoryId = $this->randomCategory()->getKey();
				$cityId = $this->randomCity()->getKey();
				$batch[] = [
					'performer_id' => $performerId,
					'customer_id'  => $customerId,
					'category_id'  => $categoryId,
					'description'  => '....',
					'title'        => $this->factory->jobTitle,
					'budget'       => $i,
					'address'      => $this->factory->address,
					'city_id'      => $cityId,
					'created_at'   => $now,
					'updated_at'   => $now,
					'closes_at'    => $closesAt,
				];
			}
			foreach (array_chunk($batch, 5000) as $num => $chunk) {
				dump($num);
				$this->orderRepository->insert($chunk);
			}
			$this->orderRepository->all()->map(function (Order $order) {
				event(app(OrderCreatedEvent::class, compact('order')));
				event(app(OrderBeforePublished::class, compact('order')));
				event(app(OrderPublished::class, compact('order')));
			})
			;

		}

		private function randomUser(): User
		{
			static $count;
			if (null === $count) {
				$count = $this->users->count();
			}
			return $this->users->get(random_int(1, $count));
		}

		private function randomCategory(): Category
		{
			static $count;
			if (null === $count) {
				$count = $this->categories->count();
			}
			return $this->categories->get(random_int(1, $count));
		}

		private function randomCity(): City
		{
			static $count;
			if (null === $count) {
				$count = $this->cities->count();
			}
			return $this->cities->get(random_int(1, $count));
		}
	}
