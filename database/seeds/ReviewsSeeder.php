<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Order\Order;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\Order\OrderRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Database\Seeder;

class ReviewsSeeder extends Seeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var \App\Repositories\Order\OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var ReviewRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, OrderRepository $orderRepository, ReviewRepository $repository)
	{
		$this->factory = $factory;
		$this->orderRepository = $orderRepository;
		$this->repository = $repository;
	}

	public function run()
	{
		/** @var  $orders Order[] */
		$reviews = [];
		$orders = $this->orderRepository->all();
		foreach ($orders as $order) {
			$reviews[] = [
				$order->getForeignKey() => $order->getKey(),
				'from_id'               => $order->performer_id,
				'to_id'                 => $order->customer_id,
				'comment'               => $this->factory->text(500),
				'rating'                => random_int(1, 5),
				'is_published'          => true,
				'about'                 => UserTypeContract::TYPE_CUSTOMER,
			];
		}

		foreach ($orders as $order) {
			$reviews[] = [
				$order->getForeignKey() => $order->getKey(),
				'from_id'               => $order->customer_id,
				'to_id'                 => $order->performer_id,
				'comment'               => $this->factory->text(500),
				'rating'                => (float)$this->factory->randomFloat(2, 1, 5),
				'is_published'          => true,
				'about'                 => UserTypeContract::TYPE_PERFORMER,
			];
		}
		foreach (array_chunk($reviews, env('DEV_SEED') ? 10000 : 30) as $chunk) {
			$this->repository->insert($chunk);
		}

		$this->addImages($this->repository->all());
	}

	/**
	 * @param array | \App\Models\Order\Review[] $reviews
	 */
	private function addImages($reviews): void
	{
		$imagesBatch = [];
		foreach ($reviews as $review) {
			foreach (range(1, 2) as $i) {
				$imagesBatch[] = [
					'imageable_type' => get_class($review),
					'imageable_id'   => $review->getKey(),
					'image'          => $this->image($this->factory->imageUrl()),
				];
			}
		}
		foreach (array_chunk($imagesBatch, 10000) as $chunk) {
			Image::insert($chunk);
		}
	}

	private function image($path)
	{
		return \Illuminate\Support\Str::replaceFirst('lorempixel.com', 'picsum.photos', $path);
	}
}
