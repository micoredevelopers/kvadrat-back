<?php

namespace Database\Seeders;

use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{

    /**
     * @var \Faker\Generator
     */
    private $factory;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(\Faker\Generator $factory, CategoryRepository $categoryRepository)
    {
        $this->factory = $factory;
        Category::reguard();
        $this->categoryRepository = $categoryRepository;
    }

    public function run()
    {
        foreach ($this->getCategories() as $categoryArr) {
            $category = $this->categoryRepository->create($categoryArr);

            if ($category && $categoryArr['categories'] ?? false) {

                foreach ($categoryArr['categories'] as $item) {
                    $item = [
                        'name' => $item,
                        'parent_id' => $category->getKey()
                    ];
                    $this->categoryRepository->create($item);
                }
            }
        }
    }


    private function getCategories()
    {
        return [];
    }
}
