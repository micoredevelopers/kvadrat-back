<?php
	namespace Database\Seeders;
	use App\Models\Setting;
	use App\Repositories\Admin\SettingsRepository;
	use Illuminate\Database\Seeder;

	class SettingsTableSeeder extends Seeder
	{
		private $settings;
		/**
		 * @var SettingsRepository
		 */
		private $repository;

		public function __construct(SettingsRepository $repository)
		{
			$this->settings = $repository->withTrashed()->get()->keyBy('key');
			$this->repository = $repository;
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$groups = [
				'global'    => [
					$this->getBuilder('sitename')->setValue('Poleznus')->setDisplayName('Название сайта')->build(),
					$this->getBuilder('schedule.weekdays')->setValue('Пн — Пт: c 8:00 до 20:00')->setDisplayName('График работы будни')->build(),
					$this->getBuilder('schedule.weekend')->setValue('Сб — Вс: c 9:00 до 18:00')->setDisplayName('График работы выходные')->build(),
					$this->getBuilder('phone-one')->setValue('+38 (063) 291 29 61')->setDisplayName('Номер телефона')->build(),
					$this->getBuilder('currency')->setValue('грн.')->setDisplayName('Название валюты')->build(),
				],
				'email'     => [
					$this->getBuilder('contact-email')->setValue(isLocalEnv() ? 'exceptions.manticore@gmail.com' : 'poleznus@gmail.com')->setDisplayName('e-mail Администратора (для уведомлений на почту)')->build(),
					$this->getBuilder('name-from')->setValue('Poleznus')->setDisplayName('Имя отправителя в письме')->build(),
					$this->getBuilder('email-from')->setValue('info@poleznus.com.ua')->setDisplayName('E-mail отправителя в письмах')->build(),
					$this->getBuilder('public-email')->setValue('info@poleznus.com')->setDisplayName('Публичный E-mail для контактов с пользователями')->build(),
				],
				'google'    => [
					$this->getBuilder('gtag-head')->setDisplayName('Google tag Head')->setTypeTextarea()->setValue('')->build(),
					$this->getBuilder('gtag-body')->setDisplayName('Google tag Body')->setTypeTextarea()->setValue('')->build(),
				],
				'downloads' => [
					$this->getBuilder('test')->setDisplayName('Тест')->setTypeFile()->setValue(null)->build(),
				],
				'socials' => [
					$this->getBuilder('facebook')->setDisplayName('Facebook')->setValue('https://www.facebook.com/')->build(),
					$this->getBuilder('instagram')->setDisplayName('Instagram')->setValue('instagram.com/')->build(),
				],
			];
			$groups = array_reverse($groups);
			foreach ($groups as $groupName => $settings) {
				foreach ($settings as $setting) {
					$setting['group'] = $groupName;
					$setting['key'] = implode('.', [$groupName, $setting['key']]);
					if ($this->settingExists($setting['key'])) {
						continue;
					}
					$this->repository->create($setting);
				}
			}
		}

		private function getSettingByKey(string $key): ?Setting
		{
			return \Arr::get($this->settings, $key);
		}

		private function settingExists(string $key): bool
		{
			return (bool)$this->getSettingByKey($key);
		}

		private function getBigText(string $id)
		{
			$arr = [
				'seo.head-global-codes' => '',
			];

			return \Arr::get($arr, $id);
		}

		private function getBuilder(?string $key)
		{
			$builder = new class implements \ArrayAccess {
				private $key = '';

				private $value = '';

				private $type = 'text';

				private $displayName = '';

				public function setKey(string $_): self
				{
					$this->key = $_;
					return $this;
				}

				public function setType(string $_): self
				{
					$this->type = $_;
					return $this;
				}

				public function setTypeTextarea(): self
				{
					$this->type = 'rich_text_box';
					return $this;
				}

				public function setTypeEditor(): self
				{
					$this->type = 'ckeditor';
					return $this;
				}

				public function setTypeFile(): self
				{
					$this->type = 'file';
					return $this;
				}

				public function setTypeCheckbox(): self
				{
					$this->setType('checkbox');
					return $this;
				}

				public function setValue(?string $_ = null): self
				{
					$this->value = $_;
					return $this;
				}

				public function setDisplayName(string $_): self
				{
					$this->displayName = $_;
					return $this;
				}

				public function build()
				{
					return [
						'key'          => $this->key,
						'value'        => $this->value,
						'type'         => $this->type,
						'display_name' => $this->displayName,
					];
				}

				public function offsetExists($offset)
				{
					return property_exists($this, $offset);
				}

				public function offsetGet($offset)
				{
					return $this->offsetExists($offset) ? $this->{$offset} : null;
				}

				public function offsetSet($offset, $value)
				{
					$method = Str::camel('set' . ucfirst($offset));
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}

				public function offsetUnset($offset)
				{
					if ($this->offsetExists($offset)) {
						$this->{$offset} = null;
					}
				}

			};
			if ($key) {
				$builder->setKey($key);
			}
			return $builder;
		}

	}
