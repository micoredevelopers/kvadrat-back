<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AbstractSeeder extends Seeder
{
	protected function image($path = null)
	{
		if (null === $path) {
			$path = $this->factory->imageUrl();
		}
		$path = Str::replaceFirst('lorempixel.com', 'picsum.photos', $path);
		return $path . '?' . Str::random(6);
	}

	protected function reguard(): void
	{
		Model::reguard();
	}
}
