<?php

namespace Database\Seeders;

use App\Events\Admin\MenusChanged;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\MenuRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MenuTableSeeder extends Seeder
{
	/**
	 * @var MenuRepository
	 */
	private $repository;

	public function __construct(MenuRepository $repository)
	{
		Menu::reguard();
		$this->repository = $repository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menuGroups = [
			'main_menu'  => [
			],
		];
		$menuGroups = $this->arrayReverseRecursive($menuGroups);
		$default = ['active' => 1];
		foreach ($menuGroups as $role => $menus) {
			if (!$menus) {
				continue;
			}
			$group = MenuGroup::getByRoleName($role);
			if (!$group) {
				continue;
			}

			foreach ($menus as $item) {
				$item = $this->arrayMerge($default, $item);
				$item['url'] = $item['url'] ?? getUrlWithoutHost(getUrlByPage($item['type'] ?? ''));
				if ($this->repository->where($group->getForeignKey(), $group->getKey())->where('url', $item['url'])->count()) {
					continue;
				}
				$menu = $this->saveMenu($item, $group);
				if (Arr::has($item, 'childrens')) {
					foreach (Arr::get($item, 'childrens') as $itemChild) {
						$itemChild = $this->arrayMerge($default, $itemChild);
						$itemChild['parent_id'] = $menu->id;
						$this->saveMenu($itemChild, $group);
					}
				}
			}
		}

		event(new MenusChanged());
	}

	private function saveMenu(array $item, MenuGroup $menuGroup): Menu
	{
		return $this->repository->createRelated($item, $menuGroup);
	}

	// for remove inspection warning - using merge in loop
	private function arrayMerge()
	{
		return array_merge(...func_get_args());
	}

	private function arrayReverseRecursive($arr)
	{
		foreach ($arr as $key => $val) {
			if (is_array($val)) {
				$arr[$key] = $this->arrayReverseRecursive($val);
			}
		}
		return array_reverse($arr);
	}

}
