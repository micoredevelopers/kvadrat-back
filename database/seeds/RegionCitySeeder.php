<?php

namespace Database\Seeders;

use App\Models\City;
use App\Repositories\CityRepository;
use Illuminate\Database\Seeder;

class RegionCitySeeder extends Seeder
{

    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        City::reguard();
        $this->cityRepository = $cityRepository;
    }

    public function run()
    {
        foreach ($this->getFormattedCities() as $city) {
            $this->cityRepository->create($city);
        }
    }

    protected function getFormattedCities(): array
    {
        $now = now();
        $cb = static function ($city) use ($now) {
            return ['name' => $city, 'created_at' => $now, 'updated_at' => $now];
        };
        return array_map($cb, $this->getCities());
    }

    protected function getCities(): array
    {
        return [
            'Александрия',
            'Белая Церковь',
            'Белгород-Днестровский',
            'Бердичев',
            'Бердянск',
            'Березань',
            'Борисполь',
            'Боярка',
            'Бровары',
            'Буча',
            'Васильков',
            'Винница',
            'Вишнёвое',
            'Вышгород',
            'Днепр (Днепропетровск)',
            'Дрогобыч',
            'Житомир',
            'Запорожье',
            'Ивано-Франковск',
            'Измаил',
            'Ирпень',
            'Каменец-Подольский',
            'Каменское (Днепродзержинск)',
            'Киев',
            'Ковель',
            'Конотоп',
            'Краматорск',
            'Кременчуг',
            'Кривой Рог',
            'Кропивницкий (Кировоград)',
            'Лисичанск',
            'Лозовая',
            'Луцк',
            'Львов',
            'Мариуполь',
            'Мелитополь',
            'Мукачево',
            'Нежин',
            'Николаев',
            'Никополь',
            'Новомосковск',
            'Обухов',
            'Одесса',
            'Павлоград',
            'Первомайск',
            'Переяслав-Хмельницкий',
            'Полтава',
            'Прилуки',
            'Ржищев',
            'Ровно',
            'Северодонецк',
            'Славутич',
            'Славянск',
            'Смела',
            'Сумы',
            'Тернополь',
            'Ужгород',
            'Украинка',
            'Умань',
            'Фастов',
            'Харьков',
            'Херсон',
            'Хмельницкий',
            'Червоноград',
            'Черкассы',
            'Чернигов',
            'Черновцы',
            'Черноморск (Ильичевск)',
            'Шостка',
            'Энергодар',
            'Яготин',
        ];
    }
}
