<?php

namespace Database\Seeders;

use App\Helpers\Debug\LoggerHelper;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Services\Phone\PhoneUkraineFormatter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
	/**
	 * @var UserRepository
	 */
	private $repository;
	/**
	 * @var \Faker\Generator
	 */
	private $factory;

	public function __construct(UserRepository $repository, \Faker\Generator $factory)
	{
		$this->repository = $repository;
		$this->factory = $factory;
	}

	public function run()
	{
		$this->required();
		$users = [];
		$range = range(1, env('DEV_SEED') ? 500000 : 10);
		foreach ($range as $item) {
			$users[] = $this->default();
		}
		foreach (array_chunk($users, 5000) as $num =>$chunk) {
			try{
				$this->repository->insert($chunk);
				du(sprintf('Users chunk #%s', $num));
			} catch (\Throwable $e){
				du(Str::limit($e->getMessage(), 100));
				du(sprintf('FAIL Users chunk #%s', $num));
			}
		}

	}

	private function required()
	{
		try{
			$this->repository->updateOrCreate(array_merge($this->default(), [
				'email' => 'performer@qq.com',
				'phone' => '+380999999997',
			]), []);
			$this->repository->updateOrCreate(array_merge($this->default(), [
				'email' => 'customer@qq.com',
				'phone' => '+380999999998',
			]), []);
		} catch (\Throwable $e){
		    app(LoggerHelper::class)->error($e);
		}
	}

	private function default()
	{
		return [
			'name'     => $this->factory->name,
			'email'    => $this->factory->email,
			'phone'    => $this->factory->phoneNumber,
			'password' => $this->password(),
			'rating'   => 3.22,
			'customer_rating'   => 4.22,
			'about'    => 'adsaadsa',
			'avatar'   => env('DEV_SEED') ? $this->image() : '',
		];
	}

	private function password()
	{
		static $pass;
		if (null === $pass) {
			$pass = bcrypt('qweqweqwe');
		}
		return $pass;
	}

	private function image()
	{
		static $path;
		if (null === $path){
			$path = Str::replaceFirst('lorempixel.com', 'picsum.photos', $this->factory->imageUrl());
		}
		return $path . '?' . Str::random(6);
	}

}
