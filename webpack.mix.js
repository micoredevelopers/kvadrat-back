const mix = require('laravel-mix');

mix.options({
    processCssUrls: false
});

//Admin side
mix.scripts([
    'public/js/core/popper.min.js',
    'public/js/core/bootstrap-material-design.min.js',
    'public/js/plugins/bootstrap-notify.js',
    'public/js/plugins/bootstrap-selectpicker.js',
    'public/js/lib/jquery.fancybox.min.js',
    'public/js/lib/bootstrap-filestyle.min.js',
    'public/js/lib/Sortable.min.js',
], 'public/js/admin/libraries.js')
    .version();

mix.sass('resources/sass/material-dashboard/material-dashboard.scss', 'public/css/admin')
    .sass('resources/sass/admin/admin-main.scss', 'public/css/admin')
    .sass('resources/sass/prolite/styles.scss', 'public/css/admin')
    .sourceMaps();
//End Admin side

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/sass/app/style.scss', 'public/static/css')
    .js('resources/js/app.js', 'public/static/js')
    .sourceMaps()
    .version();



