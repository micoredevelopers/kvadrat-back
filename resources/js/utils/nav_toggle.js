// import {maxWidth} from '../helpers/constants';
window.addEventListener('load', function () {
  const mobileMenu = document.querySelector('.mobile_menu_wrapper');
  const ham = document.querySelector('.nav_toggle_wrap');
  const menuTop = document.querySelector('.bar_top');
  const menuMiddle = document.querySelector('.bar_mid');
  const menuBottom = document.querySelector('.bar_bot');
  ham.addEventListener('click', () => {
    menuTop.classList.toggle('bar_top_click');
    menuMiddle.classList.toggle('bar_mid_click');
    menuBottom.classList.toggle('bar_bot_click');
    mobileMenu.classList.toggle('active');
    document.body.classList.toggle('no-scroll');
  })
  window.addEventListener('resize', (e) => {
    if(e.target.innerWidth < 678){
      menuTop.classList.remove('bar_top_click');
      menuMiddle.classList.remove('bar_mid_click');
      menuBottom.classList.remove('bar_bot_click');
      mobileMenu.classList.remove('active');
      document.body.classList.remove('no-scroll');
    }
  })
})