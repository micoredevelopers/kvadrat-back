<form action="{{ route('menu-group.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf

    @include('admin.menu-group.partials.form')

    @include('admin.partials.submit_create_buttons')
</form>
