<?php /** @var $edit \App\Models\Menu */ ?>
<form action="{{ route('menu-group.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    @include('admin.menu-group.partials.form')

    @include('admin.partials.submit_update_buttons')
</form>
