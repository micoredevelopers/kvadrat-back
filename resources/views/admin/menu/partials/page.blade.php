@isset($pages)
    <?php /** @var $page \App\Models\MenuGroup */ ?>
	<label for="page_id">Page</label>
	<select name="page_id" id="page_id" class="form-control selectpicker">
		<option value="">None</option>
		@foreach($pages as $page)
			@php
				$selected = (isset($edit)) ? $page->id == $edit->page_id : false;
			@endphp
			<option value="{{ $page->id }}"
				{!! selectedIfTrue($selected) !!}>{{ $page->title }}
			</option>
		@endforeach
	</select>
@endisset
