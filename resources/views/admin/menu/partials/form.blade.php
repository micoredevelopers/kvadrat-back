@include('admin.partials.crud.elements.name')

@include('admin.partials.crud.elements.url')

{{--@include('admin.partials.crud.default', ['name' => 'icon', 'title' => __('modules.menu.icon_font')])--}}

@include('admin.partials.crud.elements.active')
@if (isSuperAdmin())
    @include('admin.partials.crud.default', ['name' => 'type'])
@endif

{{--@include('admin.partials.crud.elements.image-upload-group')--}}

<div class="row">
    <div class="col-md-4">
        @include('admin.menu.partials.parent')
    </div>
    <div class="col-md-4">
        @include('admin.menu.partials.group')
    </div>
    <div class="col-md-4">
        {{--		@include('admin.menu.partials.page')--}}
    </div>
</div>

<input type="hidden" name="menu_group_id" value="{{ $group->id }}">
<input type="hidden" name="group" value="{{ $group->id }}">
