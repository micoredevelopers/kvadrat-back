<?php /** @var $edit \App\Models\Service\Service */ ?>

<form action="{{ route($routeKey.'.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    @include('admin.partials.submit_update_buttons')

    @include('admin.cities.partials.form')

    @include('admin.partials.submit_update_buttons')
</form>


