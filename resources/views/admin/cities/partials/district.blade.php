<?php /** @var $districts \Illuminate\Support\Collection | \App\Models\District []*/ ?>
<?php /** @var $edit \App\Models\City */ ?>


@isset($districts)
    @if(isset($edit) && $edit->getDistrict() ?? null)
        <label> <a href="{{ urlEntityEdit($edit->district) }}">{{ __('modules.districts.title_singular') }}</a></label>
    @else
        <label>{{ __('modules.districts.title_singular') }}</label>
    @endif
    <select name="district_id" class="form-control selectpicker" data-live-search="true" autocomplete="off">
        <option>{{ __('modules.districts.choose') }}</option>
        @foreach($districts as $district)
            @php
                $isSelected = ($edit->district_id ?? null) == $district->getKey();
                $name =  $district->getAttribute('name') . sprintf(' (%s)', $district->getRegion()->getAttribute('name'))
            @endphp
            <option {!! selectedIfTrue($isSelected) !!} value="{{ $district->id }}">{{ $name }}</option>
        @endforeach
    </select>
@endisset
