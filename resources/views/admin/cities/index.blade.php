<?php /** @var $item \App\Models\City */ ?>
<?php /** @var $permissionKey string */ ?>
@php
    $canEdit = $canEdit ?? Gate::allows('edit_'. $permissionKey);
    $canDelete = $canDelete ?? Gate::allows('delete_'. $permissionKey);
@endphp
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="th-description">@lang('form.title')</th>
            <th class="text-right">
                @can('create_' . $permissionKey)
                    <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
                @endcan
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>
                    <input type="text" data-url-update="{{ route($routeKey . '.update', $item->getKey()) }}"
                           value="{{ $item->getName() }}" name="name"
                           placeholder="Название" class="form-control">
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}