@php
    $props = sprintf(' min="0" step="%s"', $step ?? '0.01');
    $type = 'number';
@endphp
@include('admin.partials.crud.default')