<?php /** @var $permissionKey string */ ?>
@php
    $canEditCategory = Gate::allows('edit_'. $permissionKey );
    $canDeleteCategory = Gate::allows('delete_'. $permissionKey );
@endphp
<div class="row">
    <div class="col-6 text-right">
        <a href="{{ route($routeKey . '.create') }}" class="btn btn-primary">@lang('form.create')</a>
    </div>
</div>

<?php /** @var $category \App\Models\Category\Category */ ?>
<div class="row">
    <div class="dd menu col-md-12">
        @include('admin.category.partials.menu-loop', ['categories' => $list,])
    </div>
</div>


{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        const nestableUrl = '{{ route($routeKey. '.nesting') }}';--}}
{{--        $('.dd').nestable({--}}
{{--            callback: function (l, e) {--}}
{{--                $serialized = $('.dd').nestable('serialize');--}}
{{--                updateNesting($serialized);--}}
{{--            }--}}
{{--        });--}}

{{--        function updateNesting(data) {--}}
{{--            const $data = {--}}
{{--                'category': data,--}}
{{--            };--}}
{{--            $.post(nestableUrl, $data,--}}
{{--                function (data) {--}}
{{--                    if (data.message) {--}}
{{--                        let $status = (data.status) ? 'success' : 'error';--}}
{{--                        message(data.message, $status);--}}
{{--                    }--}}
{{--                })--}}
{{--        }--}}
{{--    });--}}
{{--</script>--}}