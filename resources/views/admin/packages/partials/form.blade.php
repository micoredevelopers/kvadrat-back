<?php /** @var $edit \App\Models\FranchisePackages */ ?>
@include('admin.partials.crud.elements.name')

{{--@include('admin.partials.crud.elements.url')--}}

<div class="row">
    <div class="col-md-6">
        @include('admin.partials.crud.elements.active')
    </div>
</div>

@include('admin.partials.crud.elements.image-upload-group')

{{--@include('admin.partials.crud.textarea', ['name'=>'except', 'title'=>'Краткое описание категории'])--}}