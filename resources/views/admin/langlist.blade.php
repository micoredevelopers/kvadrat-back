
@if (count($languages) > 1)<div class="localisation-block">
    <div class="dropup">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="true">
            <img src="{{asset('/images/staff/flags/' .  getCurrentLocale()  . '.png')}}" alt="">
            <span class="d-inline-block ml-1">{{ LaravelLocalization::getCurrentLocaleNative() }}</span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            @foreach($languages as $language)
                @if($language->key !== getCurrentLocale())
                    <li>
                        <a rel="alternate" hreflang="{{ $language->key }}" class="dropdown-item"
                           href="{{ langUrl(null, $language->key) }}">
                            <img src="{{asset('/images/staff/flags/' . $language->key . '.png')}}" alt="" class="langIcon">
                            <span class="d-inline-block ml-1">{{ $language->name}}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
@endif