<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">
<head>
	<meta charset="utf-8"/>
	<title>{!! \Artesaos\SEOTools\Facades\SEOMeta::getTitle() !!}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"
		  name="viewport"/>
	<link rel="stylesheet" href="{{ mix('css/admin/admin-main.css') }}"/>
	<link rel="stylesheet" href="{{ mix('css/admin/material-dashboard.css') }}"/>
	<link rel="stylesheet" href="{{ mix('css/admin/styles.css') }}"/>
	<script type="text/javascript" src="{{ asset( 'js/core/jquery.min.js') }}"></script>

	{!! $styles !!}
</head>
<body>

@include('admin.partials.preloader')

<div class="wrapper blurred-app" id="app">

	<div class="sidebar {{ getNightMode() }}" data-color="dark" data-background-color="white" data-image="">
		<div class="sidebar-background" style="background-image: url({{ asset('images/staff/sidebar-1.jpg') }});"></div>
		<div class="logo text-center">
			<a href="{{ route('home')}}" class="simple-text logo-normal mx-3 px-2">
				<img src="{{ asset('img/logo@2x.png') }}" alt="" class="img-fluid" width="60">
			</a>
			<small class="badge badge-dark no-radius">{{ Auth::guard('admin')->user()->login }}</small>
		</div>
		<div class="sidebar-wrapper position-s">
			@include('admin.menu')
		</div>
	</div>
	<div class="main-panel {{ getNightMode() }}">
		@include('admin.header')

		@if (\Arr::has($sections, 'contentHasWrapper'))
			{!! $content ?? '' !!}
		@else
			@include('admin.layouts.includes.sections-wrapper-card')
		@endif
	</div>
</div>

@include('admin.langlist')

@include('admin.layouts.partials.styles')

@include('admin.layouts.partials.scripts')

@include('admin.partials.flash-message')
</body>
</html>















