<?php /** @var $edit \App\Models\User */ ?>
<main>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            @if($edit->last_login_ip ?? false)
                                <div class="badge">Last IP:</div>
                                <b>{{ $edit->last_login_ip }}</b>
                            @endif
                        </div>
                        <div class="col-md-2">
                            @if($edit->authenticated_at ?? false)
                                <div class="badge">Last login date</div>
                                <b>{{ $edit->authenticated_at }}</b>
                            @endif
                        </div>
                        <div class="col-md-6">
                            @if($edit->user_agent ?? false)
                                <div class="badge">User agent:</div>
                                <b>{{ $edit->user_agent }}</b>
                            @endif
                        </div>
                        <div class="col-2">
                            @if (isSuperAdmin())
                                <form action="{{ route('signsuperadmin') }}" method="post"
                                      data-form-confirm="true">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{ $edit->id }}">
                                    <button class="btn  btn-danger">
                                        <i class="fa fa-user-secret" aria-hidden="true"></i>Login as user
                                    </button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::model($edit, ['method' => 'PUT', 'route' => [$routeKey . '.update',  $edit->id ] ]) !!}
            @include('admin.user._form')
            @include('admin.partials.submit_update_buttons')
            {!! Form::close() !!}
        </div>
    </div>
</main>