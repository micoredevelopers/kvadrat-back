<?php /** @var $item \App\Models\Feedback\Feedback */ ?>
<?php /** @var $permissionKey string */ ?>
@php
    $canEdit = false;
@endphp

@includeIf('admin.feedback.head')

<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="th-description">ID</th>
            <th>{{ __('validation.attributes.name') }}</th>
            <th>{{ __('validation.attributes.email') }}</th>
            <th>Описание</th>
            <th>Файлы</th>
            <th>Дата создания</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->getAttribute('name') }}</td>
                <td>{{ $item->getAttribute('email') }}</td>
                <td>{{ $item->getAttribute('message') }}</td>
                <td>
                    @foreach($item->getFiles() as $filePath)
                        @if (storageFileExists($filePath))
                            <a href="{{ getStorageFilePath($filePath) }}" title="{{ getLastFromExploded($filePath) }}" class="badge badge-success"
                               target="_blank">Файл {{ $loop->iteration }} [.{{ \App\Helpers\File\File::extractExtension($filePath) }}]</a>
                        @endif
                    @endforeach
                </td>
                <td>{{ getDateFormatted($item->getCreatedAt()) }}</td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}