<?php /** @var $item \App\Models\Feedback\Feedback */ ?>
<?php /** @var $permissionKey string */ ?>
@php
    $canEdit = false;
@endphp

<form action="" method="get">
    <div class="form-group">
        <div class="row">
            <div class="col-10">
                <input type="text" class="form-control" name="search" value="{{ request('search') }}" autocomplete="off">
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>
