<div class="form-group">
    <div class="form-icon form-icon-pass form-icon-pass-show to-right clickable">
        <img src="{{ asset('images/Show.svg') }}" alt="Show"/>
    </div>
    <div class="form-icon form-icon-pass form-icon-pass-hide to-right clickable hidden">
        <img src="{{ asset('images/Hide.svg') }}" alt="Hide"/>
    </div>
    <input class="form-input full-width with-icon-right" type="password" value="{{ $value ?? old($name ?? '',getTestField('password')) }}"
           name="{{ $name ?? 'password' }}" minlength="8" placeholder="{{ $placeholder ?? 'Пароль' }}" required/>
</div>