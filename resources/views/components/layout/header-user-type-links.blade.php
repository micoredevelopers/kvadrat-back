<div class="header__links-wrap">
    @if($isPerformer)
        @includeIf('public.layout.includes.partials.header.link-orders-list')
    @endif
    @if($countMyOrders)
        @includeIf('public.layout.includes.partials.header.link-my-orders')
    @endif
    @includeIf('public.layout.includes.partials.header.link-performers')
    @includeIf('public.layout.includes.partials.header.link-how-works')
</div>
@if ($isCustomer)
    @includeIf('public.layout.includes.partials.header.link-create')
@endif