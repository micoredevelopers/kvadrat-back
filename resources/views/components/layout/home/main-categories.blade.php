@if ($categories->isNotEmpty())
    @foreach($categories as $category)
        <div class="col-12 col-lg-6" data-aos="transform-up-20" data-aos-delay="{{ $loop->index * 200 }}">
            <div class="service-card">
                <div class="service-card__info">
                    <p class="service-card__name">{{ $category->getNameDisplay() }}</p>
                    <p class="service-card__description">{{ $category->getDescription() }}</p>
                </div>
                <a class="service-card__link"
                   href="{{ route(\App\Platform\Route\PlatformRoutes::ORDER_LIST,$category->selectedSubcategoriesUrl ) }}">
                    <span>{{ getTranslate('order.more-details', 'Подробнее') }}</span>
                    <img src="{{ asset('images/LinkArrow.svg') }}" alt="Link"/>
                </a>
                <div class="service-card__service-img"><img
                            src="{{ asset(sprintf('images/Service_%s.svg', $loop->iteration)) }}" alt="Service"/></div>
            </div>
        </div>
    @endforeach
@endif