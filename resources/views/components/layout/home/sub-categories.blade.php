@if ($categories->isNotEmpty())
    @foreach($categories as $mainCategory)
        <div class="row">
            <div class="col-12">
                <h2 class="works-title" data-aos="transform-up-20">{{ $mainCategory->getNameDisplay() }}</h2></div>
            <div class="col-12 col-works-list" data-aos="transform-up-20" data-aos-delay="{{ $loop->index * 200 }}">
                @if ($mainCategory->getSubcategories()->isNotEmpty())
                    <div class="works-list">
                        @foreach($mainCategory->getSubcategories() as $category)
                            <div class="works-list__item">
                                <a class="works-list__link is-link"
                                   href="{{ route(\App\Platform\Route\PlatformRoutes::ORDER_CREATE) }}?category={{ $category->getKey() }}">{{ $category->getNameDisplay() }}</a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    @endforeach
@endif