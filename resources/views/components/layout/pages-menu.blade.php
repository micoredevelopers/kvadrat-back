<?php /** @var $menus \App\Models\Menu[] | \Illuminate\Support\Collection */ ?>
@if(isset($menus) && $menus->isNotEmpty())
    <ul class="performer-links">
        @foreach($menus as $menu)
            <li class="performer-links__item {{ $menu->isActive ? 'active' : '' }}" data-aos="transform-up-20"
                data-aos-delay="{{ $loop->index * 100 }}">
                <a class="performer-links__link" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a>
            </li>
        @endforeach
    </ul>
@endif