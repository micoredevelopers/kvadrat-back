<?php /** @var $sectionsMenu \App\Models\Menu[] | \Illuminate\Support\Collection */ ?>
<?php /** @var $helpMenu \App\Models\Menu[] | \Illuminate\Support\Collection */ ?>

<div class="col-12 col-sm-6 col-md-5 col-lg-3">
    <h5 class="footer__title d-none d-lg-block" data-aos="transform-up-20" data-aos-delay="400">
        {{ getTranslate('footer.sections', 'Разделы') }}
    </h5>
    <h5 class="footer__title d-block d-lg-none" data-aos="transform-up-20" data-aos-delay="400">
        {{ getTranslate('footer.sections-help', 'Разделы и помощь') }}
    </h5>
    <ul class="footer__links-list" data-aos="transform-up-20" data-aos-delay="600">
        @if ($sectionsMenu->isNotEmpty())
            @foreach($sectionsMenu as $menu)
                <li class="footer__links-item"><a class="footer__link is-link" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a></li>
            @endforeach
        @endif
    </ul>
    <ul class="footer__links-list d-flex d-lg-none" data-aos="transform-up-20" data-aos-delay="600">
        @if ($helpMenu->isNotEmpty())
            @foreach($helpMenu as $menu)
                <li class="footer__links-item"><a class="footer__link is-link" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a></li>
            @endforeach
        @endif
    </ul>
</div>
<div class="col-lg-4 d-none d-lg-block">
    <h5 class="footer__title" data-aos="transform-up-20" data-aos-delay="400">
        {{ getTranslate('footer.help', 'Помощь') }}
    </h5>
    <ul class="footer__links-list" data-aos="transform-up-20" data-aos-delay="600">
        @if ($helpMenu->isNotEmpty())
            @foreach($helpMenu as $menu)
                <li class="footer__links-item"><a class="footer__link is-link" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a></li>
            @endforeach
        @endif
    </ul>
</div>