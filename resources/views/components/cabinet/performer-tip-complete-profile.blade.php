<div class="performer-recommend performer-recommend_info" data-aos="transform-up-20" data-aos-delay="400">
    <h2 class="performer-recommend__title">{{ getTranslate('cabinet.recomend', 'Рекомендуем!')}}</h2>
    <p class="performer-recommend__desc">{{ getTranslate('cabinet.fill', 'Заполнить')}}
        @foreach($links as $link)
            @if(!$loop->last && !$loop->first), @endif
            @if($loop->last && !$loop->first)и @endif
            <a href="{{ $link['url'] ?? '' }}">{{ $link['name'] ?? '' }}</a>
        @endforeach
        .{{ getTranslate('cabinet.fill.info', 'Это нужно, чтобы заказчики были уверены в ваших навыках и выбрали вас исполнителем')}}
    </p>
</div>