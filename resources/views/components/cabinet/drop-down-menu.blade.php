<ul class="header__account-list">
  <li class="header__account-list-item">
    <div href="{{ $personalLink ?? '' }}" class="header__account-link">
      <img src="{{ asset('images/Lang.svg') }}" alt="Link">
      <div class="lang-wrap">
        <span class="lang-text lang-text_active">UA</span>
        @foreach($languages ?? [] as $localeCode => $language)
          @if ($language['active'])
            <span class="lang-text lang-text_active">{{ $language['name'] }}</span>
          @else
            <a class="lang-text {{ $language['active'] ? 'active' : ''}}" href="{{ $language['url'] }}"
               hreflang="{{ $language['name'] }}"
               rel="alternate">{{ $language['name'] }}</a>
          @endif
        @endforeach
        <a class="lang-text">RU</a>
        <span class="lang-text lang-text_divider">/</span>
      </div>
    </div>
  </li>
  <li class="header__account-list-item">
    <a href="{{ $personalLink ?? '' }}" class="header__account-link">
      <img src="{{ asset('images/Profile.svg') }}" alt="Link">
      <span>{{ getTranslate('cabinet.my.profile', 'Профиль')}}</span>
    </a>
  </li>
  <li class="header__account-list-item">
    <a href="{{ $passwordLink ?? '' }}" class="header__account-link">
      <img src="{{ asset('images/Lock.svg') }}" alt="Link">
      <span>{{ getTranslate('registration.change.to.password', 'Изменить пароль') }}</span>
    </a>
  </li>
  <li class="header__account-list-item">
    <form action="{{ $switchLink }}" method="post">
      <button class="header__account-link {{ $userType }}">
        @csrf
        <img class="{{ $userType }}" src="{{ asset('images/Switch.svg') }}" alt="Link">
        <span>{{ $switchText }}</span>
      </button>
    </form>
  </li>
  <li class="header__account-list-item">
    <form action="{{ route('logout') }}" method="post">
      @csrf
      <button type="submit" class="header__account-link header__account-link_disabled">
        <img src="{{ asset('images/LogoutGray.svg') }}" alt="Link">
        <span>{{ getTranslate('registration.profile.logout', 'Выйти из профиля') }}</span>
      </button>
    </form>
  </li>
</ul>
