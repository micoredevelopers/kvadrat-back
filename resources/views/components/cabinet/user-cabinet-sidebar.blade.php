<div>
    <ul class="performer-links">
        @foreach($links as $link)
            @php
                $liClass = ($link['active'] ?? false) ? 'active' : '';
            @endphp
            <li class="performer-links__item {{ $liClass }}" data-aos="transform-up-20" data-aos-delay="{{ $loop->index * 100 }}">
                <a class="performer-links__link"
                   @if($link['target'] ?? false) target="{{ $link['target'] ?: '_blank' }}" @endif
                   href="{{ $link['url'] ?? '' }}"
                >{{  $link['name'] ?? ''}}</a>
            </li>
        @endforeach
    </ul>
</div>