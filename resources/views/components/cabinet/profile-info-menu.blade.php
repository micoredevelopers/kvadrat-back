<div class="header__account-profile">
    <img src="{{ $avatar ?? '' }}" alt="Profile">
</div>
<p>{{ $name ?? '' }}</p>