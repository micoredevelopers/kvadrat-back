<div class="modal_box modal_2">
    <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close" />
    <h1 class="modal_title">{{getTranslate('modal.title2', 'Заявка отправлена!')}}</h1>
    <h2 class="modal_subtitle">{{getTranslate('modal.subtitle2', 'Заявка отправлена!')}}</h2>
    <button class="modal_btn close-modal">{{getTranslate('buttons.send.ok', 'Окей')}}</button>
</div>