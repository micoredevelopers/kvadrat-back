<?php /** @var $paginator \Illuminate\Pagination\LengthAwarePaginator*/ ?>
@if ($paginator->hasPages())
    <div class="page-pagination__links">


        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <span class="page-pagination__arrow page-pagination__arrow_prev"></span>
        @else
            <a class="page-pagination__arrow page-pagination__arrow_prev"
               href="{{ (2 === $paginator->currentPage()) ? $paginator->path() : $paginator->previousPageUrl()  }}">
                <img src="{{ asset('images/PaginationArrow.svg') }}" rel="prev" aria-label="@lang('pagination.previous')"
                     alt="Prev"/></a>
        @endif

        <div class="page-pagination__links-list">
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <p class="page-pagination__link" aria-disabled="true">{{ $element }}</p>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span class="page-pagination__link active">{{ $page }}</span>
                        @else
                            <a class="page-pagination__link" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </div>

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="page-pagination__arrow page-pagination__arrow_next" href="{{ $paginator->nextPageUrl() }}" aria-label="@lang('pagination.next')">
                <img src="{{ asset('images/PaginationArrow.svg') }}" alt="Next"/>
            </a>
        @endif
    </div>
@endif
