<section class="teachers aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000">
    <div class="container">
        <h1 class="teachers_title">{{getTranslate('main.teacher.title', 'Преподаватели')}}</h1>
        <div class="teachers_wrapper">
            <div class="swiper-wrapper">
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/Bogdan.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name1', 'Богдан ЛОБОДА')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile1', 'Актерское мастерство и сценическая речь')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description1', 'Актер театра и кино. Ведущий и организатор мероприятий. Финалист проекта “Україна має талант”. Музыкант фронтмен группы B.A.R.D.')}}</p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/elenevsky.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name2', 'Валентин ЕЛЕНЕВСКИЙ')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile2', 'Актёрское мастерство')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description2', 'Актер театра и кино. Ведущий мероприятий. Опыт преподавания более 5 лет. Медицинское образование. Увлекается психологией, нейробиологией, музыкой.')}}
                    </p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/morgun.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name3', 'Евгений МОРГУН')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile3', 'Актёрское мастерство')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description3', 'Дипломированный актер театра и кино. Театральный педагог. Участник международных театральных фестивалей в Польше, Сербии и других странах. Лауреат стипендии им. Леся Курбаса.')}}
                    </p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/resnik.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name4', 'Екатерина РЕЗНИК')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile4', 'Актёрское мастерство')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description4', 'Дипломированная актриса театра и кино. Преподаватель актерского мастерства. 10 лет на сцене театра. Актриса театра “Кімната Т”.')}}
                    </p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/solomko.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name5', 'Ярослав СОЛОМКО')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile5', 'Актерское мастерство и сценическая пластика')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description5', 'Дипломированный актер театра и кино. Участник всеукраинских и международных театральных фестивалей в Польше, Словакии, Германии и других                        странах. Актер театра “P.S.” Ведущий событий различного формата и масштаба.')}}
                    </p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/artemov.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name6', 'Владислав АРТЕМОВ')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile6', 'Актерское мастерство и сценическая речь')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description6', 'Магистр факультета Кино и телевидения. Актер театра и кино (“Новенька”, “Цвет страсти”). Провел более 100 групповых занятий. Увлекается спортом, юмором и поэзией.')}}
                    </p>
                </div>
                <div class="teacher_item swiper-slide">
                    <div class="tieacher_item_img"><img src="{{ asset('/img/teachers/zvetnoy.jpg') }}" alt="teacher" /></div>
                    <h1 class="teacher_item_name">{{getTranslate('main.teacher.name7', 'Дмитрий ГАЛИМОВ-ЦВЕТНОЙ')}}</h1>
                    <h2 class="teacher_item_profile">{{getTranslate('main.teacher.profile7', 'Актерское мастерство и сценическая речь')}}</h2>
                    <p class="teacher_item_description">
                        {{getTranslate('main.teacher.description7', 'Магистр факультета Театрального искусства. Актер театра и кино. Более 10 лет стажа. Увлекается паркуром, кино, юмором и написанием сценариев.')}}
                    </p>
                </div>
            </div>
            <div class="swiper-pagination teachers"></div>
            <div class="swiper-buttons_wrap">
                <div class="swiper-buttons">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
</section>