.<section class="services">
    <div class="container">
        <div class="services_wrap">
            <div class="services_item" data-aos="fade-left" data-aos-duration="700" data-aos-delay="200">
                <div class="services_item_col">
                    <div class="services_item_img pink"><img src="{{ asset('/img/services/6575-min.jpg') }}" alt="services" /></div>
                </div>
                <div class="services_item_col">
                    <div class="services_item_description">
                        <h1 class="services_item_description_title" data-aos="fade-left" data-aos-duration="1000">{{getTranslate('franchise.services.title1', 'Пробное занятие')}}</h1>
                        <p class="services_item_description_text" data-aos="fade-left" data-aos-duration="1000">
                            {{getTranslate('franchise.services.text1', ' Академия актерского мастерства КВАДРАТ предоставляет всем желающим возможность немного приблизится к пониманию того, кто же такой актер. Бесплатное занятие по актерскому мастерству для взрослых позволит приоткрыть кулисы и погрузится в дружелюбную атмосферу нашей школы. Забудь                            о зажимах и стереотипах, отбрось все страхи и брось себе вызов - начни менять жизнь. Мы предлагаем тебе позитивную энергетику, тренинги на                            баланс и координацию, наработку навыков актерского мастерства и многое другое.')}}
                        </p>
                        <a class="services_item_description_btn btn_type_1" href="{{route('trial')}}" data-aos="fade-left" data-aos-duration="1000">{{getTranslate('buttons.show-more.button', 'Узнать подробнее')}}</a>
                    </div>
                </div>
            </div>
            <div class="services_item" data-aos="fade-right" data-aos-duration="700" data-aos-delay="200">
                <div class="services_item_col order_2">
                    <div class="services_item_img purple"><img src="{{ asset('/img/services/6991-min.jpg') }}" alt="services" /></div>
                </div>
                <div class="services_item_col order_1">
                    <div class="services_item_description no-pl">
                        <h1 class="services_item_description_title" data-aos="fade-right" data-aos-duration="1000">
                            {{getTranslate('franchise.services.title2', 'КВАДРАТ вводный')}}</h1>
                        <p class="services_item_description_text" data-aos="fade-right" data-aos-duration="1000">
                            {{getTranslate('franchise.services.text2', 'Что дает вводный курс нашей академии? Ты получишь актерские навыки для жизни, сцены, отношений, работы и бизнеса, которые пригодятся во все                            времена, любые кризисы и эпохи. На 3 месяца ты погружаешься в атмосферу веселья и одновременно огромной работы над собой. Мы поможем тебе                            уверенно выйти на сцену, обрести спокойствие и баланс внутри себя.')}}
                        </p>
                        <a class="services_item_description_btn btn_type_1" href="{{route('intro')}}" data-aos="fade-right" data-aos-duration="1000">
                            {{getTranslate('buttons.show-more.button', 'Узнать подробнее')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="services_item" data-aos="fade-left" data-aos-duration="700" data-aos-delay="200">
                <div class="services_item_col">
                    <div class="services_item_img yellow"><img src="{{ asset('/img/services/7006 (1)-min.jpg') }}" alt="services" /></div>
                </div>
                <div class="services_item_col">
                    <div class="services_item_description">
                        <h1 class="services_item_description_title" data-aos="fade-left" data-aos-duration="1000">{{getTranslate('franchise.services.title3', 'КВАДРАТ LEVEL UP')}}</h1>
                        <p class="services_item_description_text" data-aos="fade-left" data-aos-duration="1000">
                            {{getTranslate('franchise.services.text3', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений.                            Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных                            задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими инстанциями.')}}
                        </p>
                        <a class="services_item_description_btn btn_type_1" href="{{route('levelUp')}}" data-aos="fade-left" data-aos-duration="1000">
                            {{getTranslate('buttons.show-more.button', 'Узнать подробнее')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="services_item" data-aos="fade-right" data-aos-duration="700" data-aos-delay="200">
                <div class="services_item_col order_2">
                    <div class="services_item_img blue"><img src="{{ asset('/img/services/7012 (1)-min.jpg') }}" alt="services" /></div>
                </div>
                <div class="services_item_col order_1">
                    <div class="services_item_description no-pl">
                        <h1 class="services_item_description_title" data-aos="fade-right" data-aos-duration="1000">{{getTranslate('franchise.services.title4', 'КВАДРАТ Community')}}</h1>
                        <p class="services_item_description_text" data-aos="fade-right" data-aos-duration="1000">
                            {{getTranslate('franchise.services.text4', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений.                            Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных                            задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими инстанциями.')}}</p>
                        <a class="services_item_description_btn btn_type_1" href="{{route('community')}}" data-aos="fade-right" data-aos-duration="1000">
                            {{getTranslate('buttons.show-more.button', 'Узнать подробнее')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>