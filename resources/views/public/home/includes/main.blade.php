<section class="main-block">
    <div class="main-block-bg" data-aos="clip-bottom"></div>
    <div class="container">
        <div class="main-block_wrap">
            <div class="main-block_left_col">
                <h2 class="main-block_subtitle"
                    data-aos="clip-top">{{getTranslate('main.main.sub-title', 'Академия актёрского мастерства в одессе')}}</h2>
                <h1 class="main-block_title animate_title">{{getTranslate('main.main.title', 'Открой в себе талант!')}}</h1>
                <p class="main-block_text" data-aos="new-anim" data-aos-anchor="body" data-aos-delay="0">
                    {{getTranslate('main.main.text', 'Пересматриваешь любимый фильм снова и снова и представляешь себя в главной роли? Думаешь, что не смог бы так же? Хватит гадать - давай откроем в тебе талант вместе! Преподаватели академии КВАДРАТ подарят тебе главное - опыт, который можно пережить только самому , и который останется с тобой на всю жизнь.')}}
                </p>
                @include('public.layout.includes.buttons.open-modal-yellow')
            </div>
            <div class="main-block_right_col">
                <div class="img-wrap" data-aos="fade-in">
                    <div class="main-block_img_wrap">
                        <div class="main-block_img_wrap_col_left">
                            <img class="img_1" src="{{ asset('img/main-1.png') }}" data-aos="clip-bottom"
                                 data-aos-delay="200"/>
                            <img class="img_2" src="{{ asset('img/main-2.png') }}" alt="main-img" data-aos="clip-right"
                                 data-aos-delay="400"/>
                        </div>
                        <div class="main-block_img_wrap_col_right">
                            <img class="img_3" src="{{ asset('img/main-3.png') }}" alt="main-img" data-aos="clip-bottom"
                                 data-aos-delay="800"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>