<section class="benefits">
    <div class="container">
        <h1 class="benefits_title animate_title">{{getTranslate('main.benefits.main-title', 'Преимущества')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('img/icons/Качество сервиса.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('main.benefits.title1', 'Подход')}}</h1>
                    <p class="benefits_item_text">
                        {{ getTranslate('main.benefits.text1','Мы не обещаем тебе, что ты 100% будешь актером.')}}
                    </p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="150">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('img/icons/Подход.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('main.benefits.title2', 'Качество сервиса')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('main.benefits.text2', 'Команда школы КВАДРАТ постоянно стремиться расти, развиваться и повышать уровень сервиса. Каждое наше занятие - это праздник, на который наши клиенты хотят возвращаться снова и снова.')}}
                    </p>
                </div>
            </div>

            <div class="benefits_item" data-aos="fade-up" data-aos-delay="300">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('img/icons/Профессионализм.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('main.benefits.title3', 'Профессионализм')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('main.benefits.text3', 'Все наши преподаватели - действующие профессиональные актеры с опытом работы на театральной сцене и в кино. Они помогут раскрыть и прокачать актерские навыки, и научится их применять в работе и жизни.')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>