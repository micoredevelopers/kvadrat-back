<section class="galery" data-aos="fade-right" data-aos-duration="1000">
    <div class="container">
        <h1 class="galery_title">{{getTranslate('main.gallery.title', 'Галерея')}}</h1>
        <div class="galery_wrapper swiper-container">
            <div class="swiper-wrapper">
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_4924.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5015 (2).jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5052.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5068.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5363.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5446.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5457.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_5496.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_20210710_202803.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_20210911_211137_943.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/IMG_20210914_201240.jpg') }}" alt="review" /></div>
                <div class="galery_item swiper-slide"><img src="{{ asset('/img/galery/photo_2021-09-16 19.45.26.jpeg') }}" alt="review" /></div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-buttons_wrap">
                <div class="swiper-buttons">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
</section>