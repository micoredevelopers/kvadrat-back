
<section class="about_company">
    <div class="container">
        <div class="about_company_wrap">
            <div class="about_company_left_col" data-aos="clip-right"><img src="{{ asset('/img/about-company.png') }}" alt="about-company" /></div>
            <div class="about_company_right_col">
                <h1 class="about_company_title animate_title">{{getTranslate('main.about.title', 'Об академии')}}</h1>
                <p class="about_company_text" data-aos="fade-up-10">{{getTranslate('main.about.text1', 'Академия КВАДРАТ смело берется за проработку четырех сфер твоей индивидуальности.')}}</p>
                <p class="about_company_text" data-aos="fade-up-10">
                    {{getTranslate('main.about.text2', 'Мышление. Раскрепощаем и помогаем тебе быть собой в жизни и на сцене. Эмоции. Открываем скрытые таланты, вызываем на эмоцию, помогаем раскрасить твою жизнь новыми оттенками и красками.')}}
                </p>
                <p class="about_company_text" data-aos="fade-up-10">
                    {{getTranslate('main.about.text3', 'Эмоции. Открываем скрытые таланты, вызываем на эмоцию, помогаем раскрасить твою жизнь новыми оттенками и красками.')}}
                </p>
                <p class="about_company_text" data-aos="fade-up-10">
                    {{getTranslate('main.about.text4', 'Тело. Актерское мастерство - это не только о психоэмоциональном развитии, а еще и о соответствующей физической подготовке. Мы готовим тебя быть	выносливым при любых обстоятельствах в жизни или на сцене.')}}
                </p>
                <p class="about_company_text" data-aos="fade-up-10">
                    {{getTranslate('main.about.text5', 'Коммуникация. С нами ты обретешь решительность, упорство, уверенность в себе. Научись мыслить шире, видеть больше, чувствовать сильнее. Найди в	себе легкость, научись парить даже на собственной кухне.')}}
                </p>
            </div>
        </div>
    </div>
</section>