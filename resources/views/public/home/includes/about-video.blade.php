<section class="about_us">
    <div class="paralax-bg about_us-bg"></div>
    <div class="container">
        <h1 class="about_us_title animate_title">{{getTranslate('main.about.video-title', 'Видео о нас')}}</h1>
        <div class="about_us_wrap active" data-aos="clip-right">
            <img src="{{ asset('/img/about-us.png') }}" alt="about_us" />
            <img class="about_us_play_btn" src="{{ asset('/img/youtube.svg') }}" alt="play" />
            <iframe
                    width="448"
                    height="252"
                    src="https://www.youtube.com/embed/EO8l0AdK7_I"
                    title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
            ></iframe>
        </div>
    </div>
</section>