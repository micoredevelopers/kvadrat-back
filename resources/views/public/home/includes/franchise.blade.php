<section class="franchise">
    <div class="container">
        <div class="franchise_wrap">
            <div class="franchise_img" data-aos="fade-right"><img src="{{ asset('/img/franchise.png') }}" alt="review" /></div>
            <div class="franchise_description">
                <h1 class="franchise_description_title" data-aos="fade-left">{{getTranslate('main.franchise.title', 'Франшиза')}}</h1>
                <p class="franchise_description_text" data-aos="fade-left">
                    {{getTranslate('main.franchise.text', 'Театральная школа КВАДРАТ - это крупнейшая сеть актерских школ в Украине, и это не предел. В наших планах расширяться и открывать наши филиалы в                    новых городах. Команда КВАДРАТ - это профессиональные дипломированные педагоги, которые находят подход к любому ученику. Для таких же как мы,                    влюбленных в актерское дело, мы предлагаем успешно проверенную бизнес-модель с поддержкой для партнеров на всех этапах запуска. Успей присоединится к команде творческих и креативных ребят став нашим партнером.')}}
                </p>
                <a class="franchise_description_btn btn_type_1" href="/franchise-page" data-aos="fade-left">{{getTranslate('buttons.show-more.button', 'Узнать подробнее')}}</a>
            </div>
        </div>
    </div>
</section>