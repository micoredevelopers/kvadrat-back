@extends('public.layout.app')
@section('content')
    <main id="main-page">
        @include('public.home.includes.main')
        @include('public.home.includes.about-video')
        @include('public.home.includes.about')
        @include('public.home.includes.benefits')
        @include('public.layout.includes.trial')
        @include('public.home.includes.services')
        @include('public.home.includes.teachers')
        @include('public.layout.includes.reviews')
        @include('public.home.includes.franchise')
        @include('public.home.includes.galery')
    </main>

@endsection