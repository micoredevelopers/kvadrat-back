<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">
<head>
    @include('public.layout.includes.head')
</head>
<body>

@include('public.layout.includes.header')

<div class="wrapper">
    @hasSection('content')
        @yield('content')
    @else
        {!! $content ?? '' !!}
    @endif

</div>

@include('public.layout.includes.modal')
@include('public.layout.includes.footer')

@include('public.layout.includes.assets.scripts')

</body>
</html>