<div class="modal_bg">
    <div class="modal_box modal_4">
        <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close"/>
        <h1 class="modal_title">{{getTranslate('modal.franchise.title1', 'Имя')}}</h1>
        <h2 class="modal_subtitle">{{getTranslate('modal.franchise.subtitle1', 'Номер')}}</h2>
        <form class="modal_form" id="feedbackFormFranchise" method="post" action="{{route('franch.modal')  }}" enctype="multipart/form-data">
            @csrf
            <div class="input_box">
                <label> <input name="name" placeholder="{{getTranslate('placeholders.name', 'Имя')}}"/> </label>
                <div class="input_errpor_message">{{getTranslate('input.required', 'Заполните это поле')}}</div>
            </div>
            <div class="input_box">
                <label> <input name="phone" maxlength="19" data-phone-input placeholder="{{getTranslate('placeholders.phone', 'Телефон')}}"/> </label>
                <div class="input_errpor_message">{{getTranslate('input.required', 'Заполните это поле')}}</div>
            </div>
            <div class="form_error_message"></div>
            <button class="modal_btn">{{getTranslate('buttons.send.button', 'Отправить')}}</button>
        </form>
    </div>
    <div class="modal_box modal_3">
        <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close"/>
        <div class="galery_modal_img"></div>
    </div>
    <div class="modal_box modal_2">
        <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close"/>
        <h1 class="modal_title">{{getTranslate('modal.title2', 'Заявка отправлена!')}}</h1>
        <h2 class="modal_subtitle">
            {{getTranslate('modal.subtitle2', 'Заявка отправлена!')}}
        </h2>
        <button class="modal_btn close-modal">{{getTranslate('buttons.send.ok', 'Окей')}}</button>
    </div>
    <div class="modal_box modal_1">
        <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close"/>
        <h1 class="modal_title">{{getTranslate('modal.title1', 'Имя')}}</h1>
        <h2 class="modal_subtitle">{{getTranslate('modal.subtitle1', 'Номер')}}</h2>
        <form class="modal_form" id="feedbackForm" method="post" action="{{ route('trial.modal') }}" enctype="multipart/form-data">
            @csrf
            <div class="input_box">
                <label> <input name="name" placeholder="{{getTranslate('placeholders.name', 'Имя')}}"/> </label>
                <div class="input_errpor_message">{{getTranslate('input.required', 'Заполните это поле')}}</div>
            </div>
            <div class="input_box">
                <label> <input name="phone" maxlength="19" data-phone-input placeholder="{{getTranslate('placeholders.phone', 'Телефон')}}"/> </label>
                <div class="input_errpor_message">{{getTranslate('input.required', 'Заполните это поле')}}</div>
            </div>
            <div class="select" id="select-1">

                <button type="button"
                        data-select-city
                        class="select__toggle"
                        name="city"
                        value="city"
                        data-select="toggle"
                        data-index="-1">{{getTranslate('header.city.gorod', 'Город')}}</button>
                <div class="input_errpor_message">{{getTranslate('input.required', 'Заполните это поле')}}</div>
                <div class="select__dropdown">
                    <ul class="select__options">
                        <li class="select__option" data-select="option" data-value="Одесса" data-index="0">Одесса</li>
                        <li class="select__option" data-select="option" data-value="Киев" data-index="1">Киев</li>
                        <li class="select__option" data-select="option" data-value="Харьков" data-index="2">Харьков</li>
                        <li class="select__option" data-select="option" data-value="Харьков" data-index="3">Днепр</li>
                    </ul>
                </div>
            </div>
            <div class="form_error_message"></div>
            <button class="modal_btn">{{getTranslate('buttons.send.button', 'Отправить')}}</button>
        </form>
    </div>
</div>