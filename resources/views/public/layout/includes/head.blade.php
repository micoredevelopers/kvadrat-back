<meta charset="UTF-8" />
<title>Квадрат</title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="ie=edge, chrome=1" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" />
<script defer="defer">
    // import {maxWidth} from '../helpers/constants';
    window.addEventListener("load", function () {
        const mobileMenu = document.querySelector(".mobile_menu_wrapper");
        const ham = document.querySelector(".nav_toggle_wrap");
        const menuTop = document.querySelector(".bar_top");
        const menuMiddle = document.querySelector(".bar_mid");
        const menuBottom = document.querySelector(".bar_bot");
        ham.addEventListener("click", () => {
            menuTop.classList.toggle("bar_top_click");
            menuMiddle.classList.toggle("bar_mid_click");
            menuBottom.classList.toggle("bar_bot_click");
            mobileMenu.classList.toggle("active");
            document.body.classList.toggle("no-scroll");
        });
        window.addEventListener("resize", (e) => {
            if (e.target.innerWidth < 678) {
                menuTop.classList.remove("bar_top_click");
                menuMiddle.classList.remove("bar_mid_click");
                menuBottom.classList.remove("bar_bot_click");
                mobileMenu.classList.remove("active");
                document.body.classList.remove("no-scroll");
            }
        });
    });
</script>
<link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" />
<link href="{{ asset('static/css/style.css') }}" rel="stylesheet" />
<script src="{{ asset('static/js/app.js') }}"></script>