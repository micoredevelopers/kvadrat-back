
<div class="modal_bg">
        <div class="modal_box modal_3">
            <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close" />
            <div class="galery_modal_img"></div>
        </div>
        <div class="modal_box modal_2">
            <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close" />
            <h1 class="modal_title">{{getTranslate('modal.title2', 'Заявка отправлена!')}}</h1>
            <h2 class="modal_subtitle">{{getTranslate('modal.subtitle2', 'Заявка отправлена!')}}</h2>
            <button class="modal_btn close-modal">{{getTranslate('buttons.send.ok', 'Окей')}}</button>
        </div>
    <div class="modal_box modal_4">
        <img class="modal_close_btn close-modal" src="{{ asset('img/close-modal.svg') }}" alt="close" />
        <h1 class="modal_title">{{getTranslate('modal.title1', 'Имя')}}</h1>
        <h2 class="modal_subtitle">{{getTranslate('modal.subtitle1', 'Номер')}}</h2>
        <form id="feedbackForm" method="post" action="/franch-modal" enctype="multipart/form-data" >
            @csrf
            <div class="input_box">
                <label>
                    <input name="name" placeholder="{{getTranslate('placeholders.name', 'Имя')}}" />
                </label>
            </div>
            <div class="input_box">
                <label>
                    <input name="phone" placeholder="{{getTranslate('placeholders.phone', 'Телефон')}}" />
                </label>
            </div>
            <button class="modal_btn">{{getTranslate('buttons.send.button', 'Отправить')}}</button>
        </form>
    </div>
</div>