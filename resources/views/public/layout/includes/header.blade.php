<?php /** @var $footerData app/DataContainers/Layout */ ?>
<?php  /** @var $currentCity \App\Enum\CitiesEnums */ ?>
<?php  /** @var $cities \App\Enum\CitiesEnums[] */ ?>
<header class="header">
    <div class="container">
        <div class="header_wrapper">
            <div class="nav_toggle_wrap">
                <div class="nav_toggle">
                    <div class="toggle_menu bar_top"></div>
                    <div class="toggle_menu bar_mid"></div>
                    <div class="toggle_menu bar_bot"></div>
                </div>
            </div>
            <a class="header_logo" href="{{route('home')}}" data-aos="clip-square">
                <img class="arrow" src="{{ asset('/img/logo.svg') }}"/>
            </a>
            <div class="header_nav" data-aos="fade-up-new" data-aos-delay="200">
                <div class="header_nav_item">
                    <a href="#">
                        {{getTranslate('header.course-title', 'Курсы')}}
                        <div class="header_nav_item_sublist_wrap">
                            <ul class="header_nav_item_sublist">
                                <li class="active"><a
                                            href="{{route('trial')}}">{{getTranslate('header.trial', 'Пробное занятие')}}</a>
                                </li>
                                <li><a href="{{route('intro')}}">{{getTranslate('header.intro', 'КВАДРАТ вводный')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('levelUp')}}">{{getTranslate('header.upper', 'КВАДРАТ LEVEL UP')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('community')}}">{{getTranslate('header.community', 'КВАДРАТ Community')}}</a>
                                </li>
                            </ul>
                        </div>
                    </a>
                    <img class="arrow" src="{{ asset('/img/arrow-wertical.svg') }}"/>
                </div>
                <div class="header_nav_item"><a
                            href="{{route('franch')}}">{{getTranslate('header.franchise', 'Франшиза')}}</a></div>
                <div class="header_nav_item">
                    <a href="#">
                        {{ trans($currentCity->getTitle()) }}
                        <div class="header_nav_item_sublist_wrap">
                            @include('public.layout.includes.cities')
                        </div>
                    </a>
                    <img class="location" src="{{ asset('/img/location.svg') }}"/>
                    <img class="arrow" src="{{ asset('/img/arrow-wertical.svg') }}"/>
                </div>
                <div class="header_nav_lang_mobile">
                    @include('public.layout.includes.langlist')
                </div>
                <a href="tel:{{$footerData->getPhone()}}" class="header_nav_item phone">
                    <img src="{{ asset('/img/phone.svg') }}"/>
                    <span>{{$footerData->getPhone()}}</span>
                </a>
                <div class="lang-wrap d-none d-lg-block ml-5">
                    @include('public.layout.includes.langlist')
                </div>
            </div>
        </div>
        <div class="mobile_menu_wrapper">
            <div class="mobile_menu_nav">
                <a class="mobile_menu_nav_item"
                   href="{{ langUrl('/trial-lesson') }}">{{getTranslate('header.trial', 'Пробное занятие')}}</a>
                <a class="mobile_menu_nav_item"
                   href="{{ langUrl('/square-introductory-course') }}">{{getTranslate('header.intro', 'КВАДРАТ вводный')}}</a>
                <a class="mobile_menu_nav_item"
                   href="{{ langUrl('/level-up-course') }}">{{getTranslate('header.upper', 'КВАДРАТ LEVEL UP')}}</a>
                <a class="mobile_menu_nav_item"
                   href="{{ langUrl('/community-course') }}">{{getTranslate('header.community', 'КВАДРАТ Community')}}</a>
                <a class="mobile_menu_nav_item"
                   href="{{ langUrl('/franchise-page') }}">{{getTranslate('header.franchise', 'Франшиза')}}</a>
            </div>
            <div class="mobile_menu_location">
                <img class="location" src="{{ asset('/img/location.svg') }}"/>
                {{ trans($currentCity->getTitle()) }}
                <img class="arrow" src="{{ asset('/img/arrow-wertical.svg') }}"/>
                <div class="header_nav_item_sublist_wrap">
                    @include('public.layout.includes.cities')
                </div>
            </div>
        </div>
    </div>
</header>