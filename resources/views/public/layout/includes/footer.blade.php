<?php /** @var $footerData \App\DataContainers\Layout\FooterData */ ?>
<footer class="footer">
    <div class="container no-pading-mobile">
        <div class="footer_wrap">
            <div class="footer_col">
                <h1 class="footer_title">{{getTranslate('footer.title', 'Контакты')}}</h1>
                <div class="footer_contacts">
                    <a class="footer_phone_link footer_item"
                       href="callto:{{$footerData->getPhone()}}">{{$footerData->getPhone()}}</a>
                    <div class="footer_time footer_item">{{getTranslate('footer.work-time', 'Время работы:') . $footerData->getWorkTime()}}</div>
                    <div class="footer_address footer_item">{{getTranslate('footer.address', 'Адрес: ')}} {{trans($footerData->getAddress())}}</div>
                    <div class="footer_socials">
                        <a class="footer_social_link footer_item" href="{{$footerData->getFacebook()}}">
                            Facebook
                        </a>
                        <a class="footer_social_link footer_item" href="{{$footerData->getInst()}}">
                            Instagram
                        </a>
                    </div>
                </div>
                <button class="trieal-lesson-btn btn_type_2 open-modal open-modal">{{getTranslate('buttons.sign-up.button', 'Записаться на урок')}}</button>
                <div class="footer_privacy_policy">
                    <a href="/privacy-policy/Политика_конфиденциальности_сайт.pdf">{{getTranslate('footer.сonfidentiality', 'Политика Конфиденциальности')}}</a>
                </div>
                <div class="footer_privacy_policy">
                    <a href="/privacy-policy/ofert.pdf">{{getTranslate('footer.oferta', 'ПУБЛІЧНА ОФЕРТА')}}</a>
                </div>
                <div class="footer_privacy_policy">square.com.ua © 2021 by
                    <a href="https://micorestudio.com/ru">MantiCore Development</a>
                </div>
            </div>
            <div class="footer_col map">
                <div class="footer_map">
                    <img src="{{ asset('/img/map.png') }}" alt="map"/>
                    <iframe src="{{$footerData->getMap()}}"
                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</footer>