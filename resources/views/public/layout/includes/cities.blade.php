<ul class="header_nav_item_sublist">
    @foreach($cities as $city)
        <li class="{{ $city->getKey() === $currentCity->getKey()  ? 'active' : '' }}">
            <a class="link" href="{{ langCityUrl(route('home'), $city->getUrl()) }}">{{trans($city->getTitle())}}</a>
        </li>
    @endforeach
</ul>