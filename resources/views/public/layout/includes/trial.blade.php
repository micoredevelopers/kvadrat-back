<section class="trial-lesson" data-aos="slide-left" data-aos-duration="1000">
    <div class="container">
        <div class="tiral-lesson-wrap">
                <p class="trial-lesson-title">{{getTranslate('global.trial', 'Записаться на пробное занятие')}}</p>
            @include('public.layout.includes.buttons.open-modal')
        </div>
    </div>
</section>