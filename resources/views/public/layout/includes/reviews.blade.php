<section class="" data-aos="fade-right" data-aos-duration="1000">
    <div class="container">
        <h1 class="review_title">{{getTranslate('global.reviews', 'Отзывы')}}</h1>
        <div class="reviews_wrapper swiper-container">
            <div class="swiper-wrapper">
                <div class="reviews_item swiper-slide active">
                    <img class="reviews_item_img" src="{{ asset('/img/review.png') }}" alt="review" />
                    <a class="reviews_item_play-link" href="#"><img src="{{ asset('/img/youtube.svg') }}" alt="play" /></a>
                    <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/dU3E5bg_VdY"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                    ></iframe>
                </div>
                <div class="reviews_item swiper-slide active">
                    <img class="reviews_item_img" src="{{ asset('/img/review.png') }}" alt="review" />
                    <a class="reviews_item_play-link" href="#"><img src="{{ asset('/img/youtube.svg') }}" alt="play" /></a>
                    <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/6A7NCxtoxzQ"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                    ></iframe>
                </div>
                <div class="reviews_item swiper-slide active">
                    <img class="reviews_item_img" src="{{ asset('/img/review.png') }}" alt="review" />
                    <a class="reviews_item_play-link" href="#"><img src="{{ asset('/img/youtube.svg') }}" alt="play" /></a>
                    <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/pDkDj-69Zzs"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                    ></iframe>
                </div>
                <div class="reviews_item swiper-slide active">
                    <img class="reviews_item_img" src="{{ asset('/img/review.png') }}" alt="review" />
                    <a class="reviews_item_play-link" href="#"><img src="{{ asset('/img/youtube.svg') }}" alt="play" /></a>
                    <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/9tzoCgcx7gY"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                    ></iframe>
                </div>
                <div class="reviews_item swiper-slide active">
                    <img class="reviews_item_img" src="{{ asset('/img/review.png') }}" alt="review" />
                    <a class="reviews_item_play-link" href="#"><img src="{{ asset('/img/youtube.svg') }}" alt="play" /></a>
                    <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/Frwy8rA4PAQ"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                    ></iframe>
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-buttons_wrap">
                <div class="swiper-buttons">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
</section>