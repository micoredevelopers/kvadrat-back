@foreach($languages ?? [] as $localeCode => $language)
    @if ($language['active'])
        <span class="header_nav_lang_item active">{{ $language['name'] }}</span>
    @else

        <a class="lang-text {{ $language['active'] ? 'active' : ''}}" href="{{ $language['url'] }}"
           hreflang="{{ $language['name'] }}"
           rel="alternate">{{ $language['name'] }}</a>
    @endif
    @if(!$loop->last)/@endif
@endforeach
<span class="lang-text lang-text_divider"></span>