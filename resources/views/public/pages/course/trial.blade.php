@extends('public.layout.app')

@section('content')
    <div class="wrapper">
        <main id="main-page">
            @include('public.pages.course.includes.trial.about')
            @include('public.pages.course.includes.trial.benefits')
        </main>
    @include('public.pages.course.includes.programs.trial-program')
    @include('public.layout.includes.trial')
    @include('public.pages.course.includes.trial.user-benefits')
    @include('public.layout.includes.reviews')
@endsection