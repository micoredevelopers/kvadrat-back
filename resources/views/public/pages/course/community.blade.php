@extends('public.layout.app')

@section('content')
<div class="wrapper">
    <main id="main-page">
        @include('public.pages.course.includes.community.about')
        @include('public.pages.course.includes.community.benefits')
    </main>
@include('public.pages.course.includes.programs.community-program')
@include('public.layout.includes.trial')
@include('public.pages.course.includes.community.user-benefits')
@include('public.layout.includes.reviews')
@endsection