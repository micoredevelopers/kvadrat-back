@extends('public.layout.app')

@section('content')
<div class="wrapper">
    <main id="main-page">
        @include('public.pages.course.includes.introduction.about')
        @include('public.pages.course.includes.introduction.benefits')
    </main>
@include('public.pages.course.includes.programs.introduction-program')
@include('public.layout.includes.trial')
@include('public.pages.course.includes.introduction.user-benefits')
@include('public.layout.includes.reviews')
@endsection
