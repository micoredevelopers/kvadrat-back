<section class="benefits for_whom">
    <div class="container">
        <h1 class="benefits_title" data-aos="fade-left" data-aos-duration="0">{{getTranslate('intro.benefits.title', 'Для кого?')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">{{getTranslate('intro.benefits.item.1', 'Для тех, кто хочет освоить основы актерского мастерства.')}}</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="200">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">{{getTranslate('intro.benefits.item.2', 'Для тех, кто хочет открыть новые грани своей индивидуальности.')}}</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="400">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">{{getTranslate('intro.benefits.item.3', 'Для тех, кто хочет сменить свою профессию на более творческую.')}}</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="600">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">{{getTranslate('intro.benefits.item.4', 'Для тех, кто хочет весело провести 2 часа своего времени.')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>