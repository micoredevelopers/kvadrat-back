<section class="benefits for_whom">
    <div class="container">
        <h1 class="benefits_title" data-aos="fade-left" data-aos-duration="0">{{getTranslate('benefits.title', 'Для кого?')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">Для выпускников второй ступени обучения в Академии КВАДРАТ</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="200">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">Для действующих и начинающих актеров</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="400">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">Для тех, кто в поиск клуба по интересам</p>
                </div>
            </div>
            <div class="benefits_item for_whom" data-aos="fade-up" data-aos-delay="600">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/for-whom.svg') }}" alt="for whom" />
                    <p class="benefits_item_text">Для активных, позитивных и наполненных энергией</p>
                </div>
            </div>
        </div>
    </div>
</section>