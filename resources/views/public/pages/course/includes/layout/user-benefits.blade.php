<section class="benefits for_whom">
    <div class="container">
        <h1 class="benefits_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('user-benefits.title', 'Что даст этот курс?')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/benefit-icon.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('user-benefits.item.title1', 'Коммуникация')}}</h1>
                    <p class="benefits_item_text">{{getTranslate('user-benefits.item1', 'Общение с единомышленниками, прокачка взаимодействий с другими людьми.')}}</p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="200">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/benefit-icon.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('user-benefits.item.title2', 'Настроение')}}</h1>
                    <p class="benefits_item_text">{{getTranslate('user-benefits.item2', 'Все происходит в дружественной атмосфере, только с положительными эмоциями')}}</p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="400">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/benefit-icon.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('user-benefits.item.title3', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('user-benefits.item3', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>