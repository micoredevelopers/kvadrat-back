<section class="course_program">
    <div class="container">
        <div class="course_program_wrap">
            <h1 class="course_program_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('program.title', 'Программа занятия')}}</h1>
            <div class="course_program_accordeon">
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="0">
                    <h1 class="course_program_item_title">{{getTranslate('program.item.title1', 'Освобождение от физических “зажимов”')}}</h1>
                    <p class="course_program_item_text">
                        {{getTranslate('program.item1', 'Наши преподаватели проведут тренинги на баланс и координацию, расскажут и покажут секреты
                        контроля твоего тела.')}}
                    </p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.item.title2', 'Проработка базовых принципов сценической речи')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.item2', 'Покажем простой и легкий трюк, который улучшит качество речи. Поделимся основами словесной импровизации.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.item.title3', 'Упражнения на скорость и многозадачность')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.item3', 'Потренируем фокус и умение быстро “включаться” в любой процесс, независимо от внешних обстоятельств.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="600">
                    <h1 class="course_program_item_title">{{getTranslate('program.item.title4', 'Наработка навыков актерского мастерства')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.item4', 'Прокачаем актерские техники, играя. Обучим базовым навыкам импровизации.')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>