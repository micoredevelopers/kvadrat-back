<section class="about_course">
    <div class="container">
        <div class="about_course_wrap">
            <div class="about_course_col">
                <div class="img-wrap" data-aos="clip-right" data-aos-duration="0"><img src="{{ asset('/img/trial-main.jpg') }}" alt="course name" /></div>
            </div>
            <div class="about_course_col">
                <h2 class="about_course_info" data-aos="fade-left" data-aos-delay="0"></h2>
                <h1 class="about_course_title" data-aos="fade-left" data-aos-delay="200">{{getTranslate('about-course.trial.title', 'Пробное занятие')}}</h1>
                <p class="about_course_text" data-aos="fade-left" data-aos-delay="400">{{getTranslate('about-course.trial', 'Хватит жить страхами, предубеждениями и стереотипами. Приходи на пробное занятие в Академию актерского мастерства КВАДРАТ - здесь ты можешь быть                    собой, быть разным. Помнишь как в детстве? Вспомни как здорово и легко было быть грустным, смешным, кривляться, баловаться и даже злиться по особому. Теперь ты даже взрослый сможешь погрузится в атмосферу волшебства.')}}</p>
                @include('public.layout.includes.buttons.open-modal-yellow')
            </div>
        </div>
    </div>
</section>