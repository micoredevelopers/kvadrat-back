<section class="course_program">
    <div class="container">
        <div class="course_program_wrap">
            <h1 class="course_program_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('program.title.trial', 'Программа занятия')}}</h1>
            <div class="course_program_accordeon">
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.trial.item1', 'Освобождение от физических “зажимов”')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.trial.item1.text', 'Наши преподаватели проведут тренинги на баланс и координацию, расскажут и покажут секреты контроля твоего тела. Научат как раскрепоститься, избавится от физических зажимов, дать своему телу немного свободы.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.trial.item2', 'Проработка базовых принципов сценической речи')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.trial.item2.text', 'Покажем простой и легкий трюк, который улучшит качество речи, сделает ее четкой с поставленной дикцией. Поделимся основами словесной импровизации. Ты поймешь, как говорить уверенно в жизни и на сцене.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.trial.item3', 'Упражнения на скорость и многозадачность')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.trial.item3.text', 'Потренируем фокус и умение быстро “включаться” в любой процесс, независимо от внешних обстоятельств.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.trial.item4', 'Наработка навыков актерского мастерства')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.trial.item4.text', 'Прокачаем актерские техники, играя. Обучим базовым навыкам импровизации. Покажем как прокачать память. Забудь о боязни публичных выступлений с академией КВАДРАТ.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.trial.item5', 'Работа над контролем эмоций')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.trial.item5.text', 'Перепады настроения, неумение проявлять свои эмоции, контролировать их - давай разберемся с этим. Наши преподаватели покажут как обуздать весь спектр эмоций от ярости до бесконечного счастья и научиться правильно использовать их в жизни, что поможет обрести уверенность также и на сцене.')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>