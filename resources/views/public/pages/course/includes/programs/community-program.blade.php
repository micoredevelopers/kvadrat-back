<section class="course_program">
    <div class="container">
        <div class="course_program_wrap">
            <h1 class="course_program_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('program.title.community', 'Программа занятия')}}</h1>
            <div class="course_program_accordeon">
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item1', 'Закрытые занятия 1 раз в неделю только для выпускников')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item1.text', 'Каждую неделю мы встречаемся, чтобы провести время с пользой и удовольствием в кругу единомышленников, таких же актеров как и мы. Ведь с Академией КВАДРАТ нет предела совершенству.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item2', 'Участие в мастер-классах и неформальных встречах Академии')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item2.text', 'Для участников клуба мы организовываем различные мастер-классы, которые позволяют продолжать совершенствоваться в актерском мастерстве. Мы также организовываем неформальные вечера настолок, мафии, тусовки.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item3', 'Постановка выступлений')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item3.text', 'Совместные постановки - это реальная практика для актеров, которая происходит под присмотром и с подсказками преподавателей Академии КВАДРАТ - действующих актеров театра и кино с многолетним опытом.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item4', 'Съемка коротких метров')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item4.text', 'Приобщение к кинематографу - еще один бонус для членов нашего клуба. Мы постоянно снимаем короткий метр с членами нашего клуба, нашими бывшими учениками.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item5', 'Совместные выезды')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item5.text', 'Клуб КВАДРАТ Community предлагает не только проводить занятия в помещении, но и совершать совместные выезды.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.community.item6', 'Единый абонемент с помесячной оплатой')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.community.item6.text', 'Для членов клуба действует система помесячных абонементов, которые включают в себя 4 занятия в КВАДРАТ Community каждый месяц.')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>