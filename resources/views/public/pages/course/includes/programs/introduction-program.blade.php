<section class="course_program">
    <div class="container">
        <div class="course_program_wrap">
            <h1 class="course_program_title"
                    data-aos="fade-left"
                    data-aos-delay="0">{{getTranslate('program.title.introductory', 'Программа занятия')}}</h1>
            <div class="course_program_accordeon">
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item1', 'Введение в курс актерского мастерства')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item1.text', 'Начинаем курс с того, что закладываем основу основ. Вы узнаете что такое актерское мастерство, что в нем особенного, почему так много людей мечтают перевоплотится и сыграть кого-то другого на сцене.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item2', 'Мышечная свобода и снятие зажимов')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item2.text', 'Наши преподаватели проведут тренинги на баланс и координацию, расскажут и покажут секреты контроля твоего тела. Научат как раскрепоститься, избавится от физических зажимов, дать своему телу немного свободы.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item3', 'Развитие сценического внимания, памяти')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item3.text', 'Сосредоточенность и внимание на сцене - неотъемлемые части как репетиций, так и выступлений на сцене. Мы учим концентрировать внимание для полного перевоплощения. Тебя ждут упражнение на развитие и усиление памяти.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item4', 'Работа с партнером')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item4.text', 'Быть актером самому и уметь взаимодействовать с партнерами по сцене - этому мы учим в нашей академии.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item5', 'Тренинг эмоций')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item5.text', 'Управление своими эмоции - это бесценный навык, который нужен как на сцене, так и в обычной жизни, на работе. Правдоподобность переживаний необходима, чтобы зритель действительно поверил в изображаемую эмоцию.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item6', 'Актерские наблюдения')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item6.text', 'Умение наблюдать за всем и всеми вокруг тебя - важный навык актера. Чем больше ты наблюдаешь - тем проще перевоплощаться и ставить себя на место того или иного персонажа, образа.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item7', 'Сценическая речь')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item7.text', 'Наши преподаватели помогут вам поставить четкую дикцию, улучшить качество речи как для жизни и работы, так и для игры на сцене.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item8', 'Сценическое движение')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item8.text', 'Умение владеть своим телом, двигаться отточено, использовать правильные движения с соответствующей эмоцией показывают внешнюю технику игры актера. Научитесь контролировать свои жесты, чтобы и в обычной жизни вы могли управлять своим телом в совершенстве.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item9', 'Импровизация i')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item9.text', 'Наши преподаватели не только прорабатывают базовые навыки речи и движений, но и умение концентрироваться, быстро реагировать на меняющиеся обстоятельства и условия. Как быстро сориентироваться и отыграть тот или иной эпизод или эмоцию - все это на наших занятиях.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item10', 'Актерская фантазия')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item10.text', 'Дети постоянно о чем-то фантазируют - как они летают, попадают на космический корабль, воображают, что кровать - это машина, а пол - это лава. У взрослых появляется много забот и со временем умение фантазировать притупляется. Курс актерского мастерства помогает нам снова научится фантазировать, воображать и играть на сцене разные роли, переносится во времени и пространстве.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item11', 'Восприятие, воздействие и создание атмосферы')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item11.text', 'Атмосферу в театре создают декорации, аплодисменты зрителей, одежда, макияж, мизансцены, музыка, но главный в воздействии и создании правильного настроения - однозначно актер. Мы вместе с вами научимся как создавать уникальную атмосферу на каждом спектакле.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item12', 'Выпускной и вручение сертификатов')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item12.text', 'После окончания тебе предоставляется возможность продолжить образование в Академии КВАДРАТ с курсом Level Up.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.introductory.item13', '+')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.introductory.item13.text', '+')}}</p>
                </div>
            </div>
        </div>
</section>