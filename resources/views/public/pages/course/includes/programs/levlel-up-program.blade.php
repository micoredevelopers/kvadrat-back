<section class="course_program">
    <div class="container">
        <div class="course_program_wrap">
            <h1 class="course_program_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('program.title.level-up', 'Программа занятия')}}</h1>
            <div class="course_program_accordeon">
                <div class="corse_program_item" data-aos="fade-left" data-aos-delay="200">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item1', 'Вхождение в различные образы')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item1.text', 'Воображение и возможность представить себя в определенной роли и сыграть эту роль - это умение, которое нужно постоянно развивать. Профессия актера предполагает постоянный рост и работу над всеми навыками для того, чтобы легко входить в различные образы.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item2', 'Раскрытие своего глубинного потенциала')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item2.text', 'скрыто.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item3', 'Импровизация i')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item3.text', 'Навыки импровизации нужны не только актеру, но и обычному человеку. Учимся быстро реагировать на меняющиеся условия и обстоятельства, концентрировать внимание на цели разговора или действий. Навыки импровизации повышают уровень ваших творческих задатков.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item4', 'Глубинное раскрепощение')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item4.text', 'Мышечные зажимы блокируют возможность в полной мере проявлять эмоции и чувства. Раскрепощение позволяет высвободить психическую энергию, которую актер направляет в свою игру. Именно достижение единства слов, эмоций и движений максимально раскрывает возможности человека в жизни и на сцене.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item5', 'Развитие воображения')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item5.text', 'Часто даже в обычной жизни мы сталкиваемся с нетривиальными ситуациями, когда нужно срочно включить воображение и придумать решение. Такие решения часто нужно находить и на сцене. Чем богаче воображение актера, тем проще ему играть роль и быстро реагировать на любые изменения, включая и импровизацию.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item6', 'Формирование новых моделей поведения')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item6.text', 'Начиная с детского возраста в нас закладываются определенные модели поведения, которые остаются с нами на всю жизнь и могут меняться с возрастом или дополнятся новыми моделями поведения. Наша задача - пополнить ваш запас формированием новых моделей поведения.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item7', 'Развитие творческой интуиции')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item7.text', 'Мало освоить ремесло быть актером. Без высокого уровня творческого мышления, интуиции - любой человек не сможет подняться выше определенной планки, как на сцене, так и в другой работе.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item8', 'Умение управлять своим состоянием')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item8.text', 'Умение управлять своим эмоциональным состоянием тесно связано с умением управлять своим телом. Эта связь работает в две стороны, поэтому актеру так важно понимать какая поза присуща определенному психоэмоциональному состоянию: радости, грусти, влюбленности, удивления. Этому мы обучаем на нашем курсе.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item9', 'Продуктивность и внутренняя энергия')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item9.text', 'Откройте в себе неиссякаемый источник энергии и творчества. Мы помогаем вам повысить жизненные силы, успевать больше за меньшее количество времени, и при этом не забывать заботится о себе.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item10', 'Создание индивидуального образа')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item10.text', 'Индивидуальность актера - это именно то, что позволяет зрителю без сомнений угадывать в любом образе именно тебя. На наших занятиях будем рассказывать и показывать как создать индивидуальный образ.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item11', 'Взаимодействие со зрителем')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item11.text', 'Зритель - это неотъемлемая часть театрального действа. Талант актера также оценивается и его умением завлечь, заинтересовать зрителя, взаимодействовать с ним.')}}</p>
                </div>
                <div class="corse_program_item" data-aos="fade-right" data-aos-delay="400">
                    <h1 class="course_program_item_title">{{getTranslate('program.level-up.item12', '+')}}</h1>
                    <p class="course_program_item_text">{{getTranslate('program.level-up.item12.text', '+')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>