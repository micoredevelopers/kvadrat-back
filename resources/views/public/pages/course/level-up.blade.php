@extends('public.layout.app')

@section('content')
<div class="wrapper">
    <main id="main-page">
        @include('public.pages.course.includes.level-up.about')
        @include('public.pages.course.includes.level-up.benefits')
    </main>
@include('public.pages.course.includes.programs.levlel-up-program')
@include('public.layout.includes.trial')
@include('public.pages.course.includes.level-up.user-benefits')
@include('public.layout.includes.reviews')
@endsection