<section class="reviews franchise_reviews" data-aos="fade-right" data-aos-duration="1000">
    <div class="container">
        <h1 class="review_title">{{getTranslate('franchise.review.title', 'Что говорят наши франчайзи-партнеры ?')}}</h1>
        @include('public.layout.includes.franchise-reviews')
    </div>
</section>
