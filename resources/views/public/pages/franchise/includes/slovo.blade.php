<section class="about_company" data-aos="fade-right"></section>
<div class="container">
    <div class="about_company_wrap">
        <div class="about_company_left_col"><img src="/18eb113a2d9e058b91ba.png" data-aos="clip-right" alt="about-company" /></div>
        <div class="about_company_right_col">
            <h1 class="about_company_title" data-aos="fade-up-left">{{getTranslate('franchise.slovo.title', 'Название преимущества')}}</h1>
            <p class="about_company_text" data-aos="fade-up-left">
                Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений. Равным
                образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных задач. А ещё
                активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими инстанциями.
            </p>
            <p class="about_company_text" data-aos="fade-up-left">
                Равным образом, убеждённость некоторых оппонентов обеспечивает актуальность как самодостаточных, так и внешне зависимых концептуальных решений! А
                ещё независимые государства указаны как претенденты на роль ключевых факторов.
            </p>
        </div>
    </div>
</div>