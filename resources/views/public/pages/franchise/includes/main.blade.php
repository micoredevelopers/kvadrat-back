<section class="franchise_main_block">
    <div class="container">
        <div class="franchise_main_wrap">
            <h1 class="franchise_main_title animate_title">{{getTranslate('franchise.main.title', 'Франшиза')}}</h1>
            <p class="franchise_main_text" data-aos="slide-up">
                {{getTranslate('franchise.main.text', 'Хотите стать актером? Успешным и востребованным? Сниматься в большом кино и играть на сцене театра? Или просто хотите раскрыть себя и узнать на что вы способны? Съемки, постановки этюдов, режиссура – все это неотъемлемая практическая часть обучения под руководством знаменитых актеров и педагогов.')}}
            </p>
            @include('public.layout.includes.buttons.franchise-open-modal')
        </div>
    </div>
</section>