<div class="container">
    <div class="title_block_wrap">
        <div class="title_block_col">
            <h1 class="title_block_title" data-aos="fade-up-right" data-aos-delay="0">{{getTranslate('franchise.pack.title', 'Франшиза')}}</h1>
            <h2 class="title_block_subtitle" data-aos="fade-up-right" data-aos-delay="0">{{getTranslate('franchise.pack.subtitle', 'Франч пакет состоит из таких разделов :')}}</h2>
        </div>
        <div class="title_block_col">
            <p class="title_block_text" data-aos="fade-up-left" data-aos-delay="0">
                {{getTranslate('franchise.pack.plan', 'Готовый пошаговый план запуска и развития бизнеса (полные пошаговые инструкции открытия, начиная от поиска и найма педагогов, настройки рекламных компаний и получения первых лидов, методологии и сценарии проведения занятий, скрипты и система продаж, CRM система и автоматизация)')}}
            </p>
        </div>
    </div>
</div>
<section class="franch-packet">
    <div class="container">
        <div class="franch-packet_wrap">
            <ul class="franch-packet_list">
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>1</span>
                        {{getTranslate('franchise.pack.item1', 'Поиск и найм педагогов')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>2</span>
                        {{getTranslate('franchise.pack.item2', 'Поиск помещения')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>3</span>
                        {{getTranslate('franchise.pack.item3', 'Оформление социальных сетей')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>4</span>
                        {{getTranslate('franchise.pack.item4', 'Запуск рекламы')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>5</span>
                        {{getTranslate('franchise.pack.item5', 'Приглашение на пробное')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>6</span>
                        {{getTranslate('franchise.pack.item6', 'Проведение пробного занятия')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>7</span>
                        {{getTranslate('franchise.pack.item7', 'Найм администраторов \ менеджеров')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>8</span>
                        {{getTranslate('franchise.pack.item8', 'Управленческая отчетность и ежедневные метрики')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>9</span>
                        {{getTranslate('franchise.pack.item9', 'Курс "Квадрат Вводный"')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>10</span>
                        {{getTranslate('franchise.pack.item10', 'Выпускной')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        {{getTranslate('franchise.pack.item11', 'Лого и раздаточные материалы')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        {{getTranslate('franchise.pack.item12', 'Обучение по продажам')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        {{getTranslate('franchise.pack.item13', 'Юридические договора')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        CRM система
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-right" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        {{getTranslate('franchise.pack.item15', 'Отзывы клиентов')}}
                    </div>
                </li>
                <li class="franch-packet_item" data-aos="fade-left" data-aos-delay="200">
                    <div class="franch-packet_item_wrap">
                        <span>*</span>
                        {{getTranslate('franchise.pack.item16', 'Бренд и товарный знак')}}
                    </div>
                </li>
            </ul>
            <div class="franch-packet_checkbox">
                <div class="franch-packet_checkbox_item" data-aos="fade-up" data-aos-delay="0">
                    <div class="franch-packet_checkbox_img"><img src="/42711148792674016a36.svg" alt="services" /></div>
                    <p class="franch-packet_checkbox_text">{{getTranslate('franchise.pack.checkbox1', 'Настроим рекламные компании в интернете и получим первых 100 Лидов')}}</p>
                </div>
                <div class="franch-packet_checkbox_item" data-aos="fade-up" data-aos-delay="200">
                    <div class="franch-packet_checkbox_img"><img src="/42711148792674016a36.svg" alt="services" /></div>
                    <p class="franch-packet_checkbox_text">{{getTranslate('franchise.pack.checkbox2', 'Настроим рекламные компании в интернете и получим первых 100 Лидов')}}</p>
                </div>
                <div class="franch-packet_checkbox_item" data-aos="fade-up" data-aos-delay="400">
                    <div class="franch-packet_checkbox_img"><img src="/42711148792674016a36.svg" alt="services" /></div>
                    <p class="franch-packet_checkbox_text">{{getTranslate('franchise.pack.checkbox3', 'Настроим рекламные компании в интернете и получим первых 100 Лидов')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>