<section class="about_company founder" data-aos="fade-right">
    <div class="container">
        <div class="about_company_wrap ">
            <div class="about_company_left_col"><img src="/img/founder.png" data-aos="clip-right" alt="about-company" /></div>
            <div class="about_company_right_col">
                <h1 class="about_company_title" data-aos="fade-up-left">{{getTranslate('franchise.slovo.title', 'Слово основателя')}}</h1>
                <p class="about_company_text" data-aos="fade-up-left">{{getTranslate('franchise.slovo.item1', 'У меня есть мечта Я хочу видеть больше улыбок, на лицах людей которых я встречаю каждый день Видеть больше открытых людей, которые не бояться коммуницировать с окружающими Людей которые могут танцевать без алкоголя и не бояться проявлять свои эмоции Людей мечтателей, которые делают то что им нравится и не зависят от оценки окружающих Людей которые не бояться реализовать свою мечту в любом возрасте ')}}</p>
                <p class="about_company_text" data-aos="fade-up-left">{{getTranslate('franchise.slovo.item2', 'Людей для которых любое публичное выступление на сцене это удовольствие в перемешку с приятным волнением, в не зависимости от того, сколько людей в зале перед ними Людей которые не прячут свои таланты в долгий ящик, а раскрывают их Людей которые кайфуют от каждого момента своей жизни и любят себя и весь Мир вокруг них ')}}</p>
                <p class="about_company_text" data-aos="fade-up-left">{{getTranslate('franchise.slovo.item3', 'И я верю в то, что Квадрат объединяет таких людей по всей стране И скоро будет объединять по всему Миру Меня всегда зажигали проекты, в которых видны наглядные позитивные изменения людей, и я знаю что каждый человек который пришёл к нам даже на 2 часа бесплатного пробного занятия становиться лучше, чем был до нас В этом и есть наша миссия - делать жизни людей лучше')}}</p>
                <div class="about_company_footer">
                    <div class="name">Кирилл Роганов</div>
                    <a href="https://instagram.com/roganovkv?utm_medium=copy_link" target="_blank" class="link">@roganovkv</a>
                </div>
            </div>
        </div>
    </div>
</section>
