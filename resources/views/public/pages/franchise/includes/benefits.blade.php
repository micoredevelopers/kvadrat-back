<section class="benefits for_whom benefits_franchise">
    <div class="container">
        <h1 class="benefits_title" data-aos="fade-left" data-aos-delay="0">{{getTranslate('franchise.benefits.main-title', 'Преимущества франшизы')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src={{ asset("/img/franchise-page/Готовый-план-запуска.svg") }} alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits.title1', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits.text1', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="200">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src={{ asset("/img/franchise-page/Крупная-сеть.svg") }} alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits.title2', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits.text2', ' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="400">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src={{ asset("/img/franchise-page/Уникальные-методики.svg") }} alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits.title3', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits.text3', ' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>