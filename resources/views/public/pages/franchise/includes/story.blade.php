<section class="services story">
    <div class="container">
        <div class="services_wrap">
            <div class="services_item">
                <div class="services_item_col order_2">
                    <div class="services_item_img blue" data-aos="clip-left"><img src="{{ asset('/img/history.png') }}" alt="history" /></div>
                </div>
                <div class="services_item_col order_1">
                    <div class="services_item_description no-pl">
                        <h1 class="services_item_description_title animate_title">{{getTranslate('franchise.story.title', 'История компании')}}</h1>
                        <p class="services_item_description_text" data-aos="fade-up-right">
                            {{getTranslate('franchise.story.text', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных рассуждений. Равным образом, высококачественный прототип будущего проекта требует от нас анализа позиций, занимаемых участниками в отношении поставленных задач. А ещё активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть объективно рассмотрены соответствующими инстанциями.')}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>