<section class="achievements CA">
    <div class="container">
        <div class="achievements_title" data-aos="fade-left">{{getTranslate('franchise.audience.main-title', 'О нашей ЦА')}}</div>
        <div class="achievements_wrap">
            <div class="achievement_item long" data-aos="fade-up" data-aos-delay="0">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.audience.title1', 'от 15 до 60 лет')}}</span></h1>
                <p class="achievement_text">
                    {{getTranslate('franchise.audience.text1', 'Так как в первую очередь мы продаем эмоции и тусовку, то Ваша потенциальная ЦА это все жители Вашего региона в возрасте от 15 до 60 лет')}}
                </p>
            </div>
            <div class="achievement_item long" data-aos="fade-up" data-aos-delay="200">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.audience.title2', '760 000 человек')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.audience.text2', 'В Одессе к примеру наша ЦА это 760 000 человек')}}</p>
            </div>
            <div class="achievement_item long" data-aos="fade-up" data-aos-delay="400">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.audience.title3', '15% средняя конверсия')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.audience.text3', '15% средняя конверсия из холодного лида в клиента.')}}</p>
            </div>
        </div>
    </div>
</section>