<section class="benefits for_whom">
    <div class="container">
        <h1 class="benefits_title" data-aos="fade-right" data-aos-delay="0">{{getTranslate('franchise.benefits-user.main-title', 'Преимущества компании')}}</h1>
        <div class="benefits_wrap">
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="0">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/icons/Качество сервиса.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits-user.title1', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits-user.text1', 'Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="200">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/icons/Подход.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits-user.title2', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits-user.text2', ' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
            <div class="benefits_item" data-aos="fade-up" data-aos-delay="400">
                <div class="img-wrap">
                    <img class="benefits_item_icon" src="{{ asset('/img/icons/Профессионализм.svg') }}" alt="benefit" />
                    <h1 class="benefits_item_title">{{getTranslate('franchise.benefits-user.title3', 'Название преимущества')}}</h1>
                    <p class="benefits_item_text">
                        {{getTranslate('franchise.benefits-user.text3', ' Равным образом, социально-экономическое развитие, в своём классическом представлении, допускает внедрение глубокомысленных')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>