<section class="achievements franch">
    <div class="container">
        <div class="achievements_title" data-aos="fade-right">{{getTranslate('franchise.achiev.main-title', 'Достижение бренда')}}</div>
        <div class="achievements_wrap">
            <div class="achievement_item" data-aos="fade-right" data-aos-delay="0">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.achiev.title1', 'Более 2000')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.achiev.text1', 'Человек посетили Академию Квадрат только с начала этого года')}}</p>
            </div>
            <div class="achievement_item" data-aos="fade-right" data-aos-delay="200">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.achiev.title2', 'Более 100')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.achiev.text2', 'Наших выпускников выступили на сцене театра только с начала этого года')}}</p>
            </div>
            <div class="achievement_item" data-aos="fade-right" data-aos-delay="400">
                <h1 class="achievement_title"><span>75%</span></h1>
                <p class="achievement_text">
                    {{getTranslate('franchise.achiev.text3', 'Средний NPS наших клиентов выше (мы постоянно собираем обратную связь, NPS - уровень удовлетворенности клиентов, средний бал 8.7 из 10 по                    обратной связи наших клиентов)')}}
                </p>
            </div>
            <div class="achievement_item" data-aos="fade-right" data-aos-delay="600">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.achiev.title4', 'Более 20')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.achiev.text4', 'Человек в команде Управляющей компании (Менеджеры и педагоги помогут Вам с открытием и развитием в Вашем городе)')}}</p>
            </div>
            <div class="achievement_item" data-aos="fade-right" data-aos-delay="700">
                <h1 class="achievement_title"><span>{{getTranslate('franchise.achiev.title5', 'Более 40 лет')}}</span></h1>
                <p class="achievement_text">{{getTranslate('franchise.achiev.text5', 'Суммарный опыт преподования наших педагогов')}}</p>
            </div>
        </div>
    </div>
</section>