<section class="franchise-start">
    <div class="container">
        <h1 class="franchise-start_title" data-aos="fade-up">{{getTranslate('franchise.start.main-title', 'Этапы запуска франшизы')}}</h1>
        <div class="franchise-start_wrap">
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="0">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.step1', 'ШАГ 1')}}</div>
                <div class="franchise-start_item_title">
                    <h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.title1', 'Знакомство')}}</h1>
                    <img class="franchis-start_item_title_arrow" src="{{ asset("/img/ar-1.svg") }}" alt="next" />
                </div>
                <div class="franchise-start_item_text">
                    {{getTranslate('franchise.start.text1', 'Вы оставляете заявку, заполняете анкету потенциального партнера и мы знакомимся с Вами по телефону или в ZOOM. Обсуждаем ожидания от                    сотрудничества')}}
                </div>
            </div>
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="200">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.step2', 'ШАГ 2')}}</div>
                <div class="franchise-start_item_title">
                    <h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.title2', 'Знакомство с условиями')}}</h1>
                    <img class="franchis-start_item_title_arrow" src={{ asset("/img/ar-2.svg") }} alt="next" />
                </div>
                <div class="franchise-start_item_text">
                    {{getTranslate('franchise.start.text2', ' Вы изучаете договор, финансовую модель, узнаете о том, как происходит взаимодействие франчайзи с управляющей компанией')}}
                </div>
            </div>
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="400">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.step3', 'ШАГ 3')}}</div>
                <div class="franchise-start_item_title">
                    <h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.title3', 'Договор и пакет франшизы')}}</h1>
                    <img class="franchis-start_item_title_arrow" src={{ asset("/img/ar-3.svg") }} alt="next" />
                </div>
                <div class="franchise-start_item_text">
                    {{getTranslate('franchise.start.text3', 'Подписываем документы. Вы оплачиваете паушальный взнос. Передаем вам пакет франшизы и назначаем дату обучения')}}
                </div>
            </div>
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="600">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.step4', 'ШАГ 4')}}</div>
                <div class="franchise-start_item_title">
                    <h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.title4', 'Обучение в Одессе')}}</h1>
                    <img class="franchis-start_item_title_arrow" src={{ asset("/img/ar-4.svg") }} alt="next" />
                </div>
                <div class="franchise-start_item_text">
                    {{getTranslate('franchise.start.text4', 'Основатели и команда «Квадрата» расскажут все о франшизе, научат вести бизнес, управлять и приводить клиентов и главное покажут все на примере                    нашей работающей академии.')}}
                </div>
            </div>
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="800">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.step5', 'ШАГ 5')}}</div>
                <div class="franchise-start_item_title">
                    <h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.title5', 'Договор и пакет франшизы')}}</h1>
                    <img class="franchis-start_item_title_arrow" src={{ asset("/img/ar-5.svg") }} alt="next" />
                </div>
                <div class="franchise-start_item_text">
                    {{getTranslate('franchise.start.text5', 'Запуск и поддержка.. Руководитель выезжает к вам. И помогает открыть вашу собственную академию "Квадрат"')}}
                </div>
            </div>
            <div class="franchise-start_item" data-aos="fade-up" data-aos-delay="1000">
                <div class="franchise-start_item_step">{{getTranslate('franchise.start.start-step', 'Время действовать!')}}</div>
                <div class="franchise-start_item_title"><h1 class="franchise-start_item_title_text">{{getTranslate('franchise.start.start-step.text', 'Свяжись прямо сейчас!')}}</h1></div>
                @include('public.layout.includes.buttons.franchise-open-modal-yellow')
            </div>
        </div>
    </div>
</section>