<section class="title_block investments">
    <div class="container">
        <div class="title_block_wrap">
            <div class="title_block_col">
                <h1 class="title_block_title" data-aos="fade-up-right" data-aos-delay="0">{{getTranslate('franchise.invest.title', 'Инвестиции в франшизу')}}</h1>
                <h2 class="tilte_block_subtitle" data-aos="fade-up-right" data-aos-delay="0">{{getTranslate('franchise.invest.subtitle', 'Стоимость пашульного взноса:')}}</h2>
            </div>
            <div class="title_block_col">
                <p class="title_block_text" data-aos="fade-up-left" data-aos-delay="0">
                    {{getTranslate('franchise.invest.text', 'Готовый пошаговый план запуска и развития бизнеса (полные пошаговые инструкции открытия, начиная от поиска и найма педагогов, настройки рекламных компаний и получения первых лидов, методологии и сценарии проведения занятий, скрипты и система продаж, CRM система и автоматизация)')}}
                </p>
            </div>
        </div>
    </div>
</section>
<section class="investments-table-wrap">
    <div class="container">
        <table class="investments-table" data-aos="clip-right">
            <tr class="title">
                <td class="th">{{getTranslate('franchise.invest.item1.title', 'Показатели')}}</td>
                <td>Пакет 1</td>
                <td>Пакет 2</td>
                <td>Пакет 3</td>
            </tr>
            <tr class="subtitle">
                <th>{{getTranslate('franchise.invest.item2.title', 'Население города')}}</th>
                <td>{{getTranslate('franchise.invest.item2.value1', 'до 400 тыс. чел.')}}</td>
                <td>{{getTranslate('franchise.invest.item2.value2', 'от 400 тыс. до 900 тыс. чел.')}}</td>
                <td>{{getTranslate('franchise.invest.item2.value3', 'от 900 тыс')}}</td>
            </tr>
            <tr>
                <th>{{getTranslate('franchise.invest.item3.title', 'Стоимость франшизы')}}</th>
                <td>140 000 грн</td>
                <td>190 000 грн</td>
                <td>270 000 грн</td>
            </tr>
            <tr>
                <th>{{getTranslate('franchise.invest.item4.title', 'Общая сумма')}}</th>
                <td>160 000 грн</td>
                <td>210 000 грн</td>
                <td>290 000 грн</td>
            </tr>
            <tr>
                <th>{{getTranslate('franchise.invest.item5.title', 'Срок окупаемости')}}</th>
                <td>{{getTranslate('franchise.invest.item5.value1', 'от 3-4 месяца')}}</td>
                <td>{{getTranslate('franchise.invest.item5.value2', 'от 3-4 месяца')}}</td>
                <td>{{getTranslate('franchise.invest.item5.value3', 'от 5-6 месяца')}}</td>
            </tr>
            <tr>
                <th style="text-align: left">{{getTranslate('franchise.invest.item6.title', 'Планируемая чистая прибыль партнера')}}</th>
                <td>25 000 - 30 000 грн</td>
                <td>40 000 - 60 000 грн</td>
                <td>70 000 - 100 000 грн</td>
            </tr>
            <tr>
                <th>{{getTranslate('franchise.invest.item7.title', 'Роялти (от выручки)')}}</th>
                <td>7%</td>
                <td>7%</td>
                <td>7%</td>
            </tr>
{{--            <tr class="buttons">--}}
{{--                <td></td>--}}
{{--                <td><button class="btn_type_1">Выбрать пакет</button></td>--}}
{{--                <td><button class="btn_type_1">Выбрать пакет</button></td>--}}
{{--                <td><button class="btn_type_1">Выбрать пакетs</button></td>--}}
{{--            </tr>--}}
        </table>
    </div>
</section>