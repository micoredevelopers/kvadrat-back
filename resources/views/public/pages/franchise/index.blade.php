@extends('public.layout.app')

@section('content')
<div class="wrapper">
    <main id="main-page">
        @include('public.pages.franchise.includes.main')
        @include('public.pages.franchise.includes.story')
        @include('public.pages.franchise.includes.achievements')
        @include('public.pages.franchise.includes.user-benefits')
        @include('public.pages.franchise.includes.audience')
        @include('public.pages.franchise.includes.franchise-trial')
        @include('public.pages.franchise.includes.benefits')
        @include('public.pages.franchise.includes.invest')
        @include('public.pages.franchise.includes.start')
        @include('public.pages.franchise.includes.reviews')
        @include('public.pages.franchise.includes.founder-word')
    </main>
</div>
@endsection