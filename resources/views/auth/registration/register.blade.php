<main class="main-container container">
    <div class="d-flex justify-content-center">

        <form action="{{ route('register') }}" class="w-50" method="POST">
            @csrf
            <h4>{{ getTranslate('registration.title', 'Регистрация') }}</h4>
            <div class="mb-4">{{ getTranslate('registration.1-step-title', 'На следующем этапе мы отправим Вам смс с кодом подтверждения') }}</div>

            <div class="form-group">
                {{ errorDisplay('fio') }}
                <input class="form-control" minlength="3" name="fio"
                       placeholder="{{ getTranslate('forms.fio', 'ФИО') }}"
                       value="{{ old('fio') }}"
                       required>
            </div>
            <div class="form-group text-center">
                {!! errorDisplay('phone') !!}
                <input class="form-control" minlength="3" name="phone"
                       placeholder="{{ getTranslate('forms.phone', 'Номер телефона') }}"
                       value="{{ old('phone') }}"
                       required>
            </div>
            <div class="form-group text-center">
                {!! errorDisplay('email') !!}
                <input class="form-control" minlength="3" name="email" type="email"
                       placeholder="{{ getTranslate('forms.email') }}"
                       value="{{ old('email') }}"
                       required>
            </div>
            <div class="d-flex justify-content-between">

                <button class="btn btn-info" type="submit">
                    <span>{{ getTranslate('registration.1-step-button', 'Начать регистрацию') }}</span>
                </button>
                <a href="{{ route('login') }}"
                   class="registration-back">{{ getTranslate('registration.back-to-auth', 'Вернуться к авторизации') }}</a>
            </div>
        </form>
    </div>
</main>