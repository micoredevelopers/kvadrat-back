<main id="registration-page">
    <section class="main-section">
        <div class="container">
            <div class="breadcrumbs">
                <p class="breadcrumbs__item">
                    <span>{{ getTranslate('registration.confirm.phone', 'Подтверждение телефона')}}</span>
                    <span>>></span>
                </p>
                <p class="breadcrumbs__item active"><span>{{ getTranslate('registration.create.questionnaire', 'Заполнение анкеты')}}</span></p>
            </div>
            <h1 class="section-title">{{getTranslate('registration.title', 'Регистрация')}}</h1>
            <p class="section-desc">{{getTranslate('order.customer.info', 'Заполните информацию о себе')}}</p>
            <div class="form-group form-file-group m-w-120 m-w-xl-150">
                <div class="form-img"><img src="{{ getPathToImage($userAvatar ?? '', 'images/ProfileDefault.svg') }}" alt="Profile"/></div>
                <label class="form-input file-input excluded mb-0">
                    <input class="d-none" type="file" name="avatar" accept="image/jpeg,image/png,image/jpg"/>
                </label>
            </div>
            <div class="registration-credentials">
                <p>{{ $userPhone ?? '' }}</p>
                <p>{{ $userEmail ?? '' }}</p>
            </div>
            <div class="tabs-container">
                <ul class="nav nav-tabs" id="profile-type-tab" role="tablist">
                    <div class="tab-indicator"></div>
                    <li class="nav-item">
                        <a class="nav-link active tab-active" id="performer-tab" data-toggle="tab" href="#performer" role="tab"
                           aria-controls="performer">
                            <span>{{getTranslate('performers.singe', 'Исполнитель')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="company-tab" data-toggle="tab" href="#company" role="tab" aria-controls="company">
                            <span>{{getTranslate('header.company', 'Компания')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="customer-tab" data-toggle="tab" href="#customer" role="tab" aria-controls="customer">
                            <span>{{getTranslate('header.сustomer', 'Заказчик')}}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Исполнитель--}}
                    <div class="tab-pane show active" id="performer" role="tabpanel" aria-labelledby="performer-tab">
                        <form class="form-container m-w-300 m-w-xl-380" action="{{ route('registration.type.performer.send') }}"
                              method="POST" data-form-ajax="1" data-form-file="1">
                            @csrf
                            <input type="hidden" name="type" value="performer">
                            @includeIf('auth.registration.includes.name')
                            @includeIf('auth.registration.includes.surname')
                            @includeIf('auth.registration.includes.city')
                            @includeIf('components.password')
                            @includeIf('auth.registration.includes.send-btn')
                            @includeIf('auth.registration.includes.confirm-terms')
                        </form>
                    </div>
                    {{-- Компания--}}
                    <div class="tab-pane" id="company" role="tabpanel" aria-labelledby="company-tab">
                        <form class="form-container m-w-300 m-w-xl-380" action="{{ route('registration.type.company.send') }}"
                              method="POST" data-form-ajax="1" data-form-file="1">
                            @csrf
                            <input type="hidden" name="type" value="company">
                            <div class="form-group">
                                <input class="form-input full-width" name="name" placeholder="{{getTranslate('header.company.name', 'Название компании')}}" required/>
                            </div>
                            @includeIf('auth.registration.includes.employments')
                            @includeIf('auth.registration.includes.city')
                            @includeIf('components.password')
                            @includeIf('auth.registration.includes.send-btn')
                            @includeIf('auth.registration.includes.confirm-terms')
                        </form>
                    </div>
                    {{-- Заказчик --}}
                    <div class="tab-pane" id="customer" role="tabpanel" aria-labelledby="customer-tab">
                        <form class="form-container m-w-300 m-w-xl-380" action="{{ route('registration.type.customer.send') }}"
                              method="POST" data-form-ajax="1" data-form-file="1">
                            @csrf
                            <input type="hidden" name="type" value="customer">
                            @includeIf('auth.registration.includes.name')
                            @includeIf('auth.registration.includes.surname')
                            @includeIf('auth.registration.includes.city')
                            @includeIf('components.password')
                            @includeIf('auth.registration.includes.send-btn')
                            @includeIf('auth.registration.includes.confirm-terms')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>