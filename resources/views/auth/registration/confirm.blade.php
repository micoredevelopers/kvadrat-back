<main id="phone-recovery-page">
    <section class="main-section">
        <div class="container">
            <div class="breadcrumbs">
                <p class="breadcrumbs__item active">
                    <span>{{ getTranslate('registration.confirm.phone', 'Подтверждение телефона')}}</span>
                    <span>>></span>
                </p>
                <p class="breadcrumbs__item"><span>{{ getTranslate('registration.create.questionnaire', 'Заполнение анкеты')}}</span></p>
            </div>
            <h1 class="section-title">{{ getTranslate('sms.code-from-sms', 'Код из смс') }}</h1>
            <p class="section-desc">{{ translateFormat('sms.code-send', ['%phone%' => $phone, ]) }}</p>

            @includeIf('public.partials.sms.admin-show-code')
            <form class="form-container" id="code-send-form" method="POST" action="{{ route('registration.confirm') }}"
                  id="codeSubmit" data-form-ajax="1">
                @csrf
                <div class="form-group form-group-code">
                    @includeIf('public.partials.sms.four-fields-code')
                </div>
                <button class="custom-btn full-width" type="submit" disabled="disabled"><span>{{ getTranslate('buttons.confrim.button', 'Подтвердить')}}</span></button>
            </form>

            <form action="{{ route('verification.resend') }}" method="post">
                @csrf
                @includeIf('public.partials.sms.resend-code')
            </form>
        </div>
    </section>
</main>
