<main id="phone-recovery-page">
    <section class="main-section">
        <div class="container">
            <h1 class="section-title">{{ getTranslate('auth.phone.number', 'Введите номер телефона')}}</h1>
            <p class="section-desc">{{ getTranslate('auth.not.phone.number', 'К вашей почте не привязан номер телефона или мы не смогли его найти')}}</p>
            <form class="form-container" method="POST" action="{{ route('registration.phone.check') }}" data-form-ajax="1">
                <div class="form-group"><input class="form-input full-width" type="tel" name="phone" value="{{ old('phone', '+38') }}" placeholder="{{getTranslate('footer.contacts.phone', 'Телефон')}}" required=""></div>
                <button class="custom-btn full-width" type="submit"><span>{{ getTranslate('buttons.confrim.button', 'Подтвердить')}}</span></button>
            </form>
        </div>
    </section>
</main>