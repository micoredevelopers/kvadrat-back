<div class="form-group">
    <input class="form-input full-width" name="surname" placeholder="{{ getTranslate('forms.surname', 'Фамилия')}}" required value="{{ old('surname', $userSurname ?: getTestField('surname')) }}"/>
</div>