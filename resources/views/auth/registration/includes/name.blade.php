<div class="form-group">
    <input class="form-input full-width" name="name" placeholder="{{getTranslate('forms.name', 'Имя')}}" value="{{ old('name', $userName ?: getTestField('name')) }}" required/>
</div>