<label class="form-checkbox-label">
    <span class="agreement-text">
        <span>{{getTranslate('auth.agree.with', 'Я согласен с')}}</span>
        <a href="javascript:void(0)" data-toggle="modal" data-target="#terms"> {{getTranslate('auth.terms.use', 'условиями использования')}}</a>
    </span>
    <input class="form-checkbox" type="checkbox" data-agreement="1"  name="agreement"/>
    <span class="form-checkbox-checkmark"></span>
</label>
