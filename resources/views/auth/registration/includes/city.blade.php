<?php /** @var $cities \Illuminate\Support\Collection | \App\Models\City[] */ ?>
@if(isset($cities) && $cities->isNotEmpty())
    <div class="form-group form-select-group">
        <select name="city_id" required title="Город" data-live-search="true" data-form-select>
            @foreach($cities as $city)
                @php
                    $isSelected = ($city->getKey() === old('city_id', $userCity ?? null));
                @endphp
                <option value="{{ $city->getKey() }}" {!! selectedIfTrue($isSelected) !!}>{{ $city->getName() }}</option>
            @endforeach
        </select>
    </div>
@endif