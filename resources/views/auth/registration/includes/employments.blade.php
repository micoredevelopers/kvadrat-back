<div class="form-group form-select-group">
    <div class="form-icon to-right">
        <img src="{{ asset('images/SelectArrow.svg') }}" alt="Arrow"/>
    </div>
    <select name="company_workers" id="employments" title="Количество сотрудников" required data-form-select>
        @foreach($employersQuantites ?? [] as $key => $employerQty)
            <option value="{{ $key }}">{{ $employerQty }}</option>
        @endforeach
    </select>
</div>