<main id="registration-page">
    <section class="main-section">
        <div class="container">
            <div class="breadcrumbs">
                <p class="breadcrumbs__item">
                    <span>{{ getTranslate('registration.confirm.phone', 'Подтверждение телефона')}}</span>
                    <span>>></span>
                </p>
                <p class="breadcrumbs__item active"><span>{{ getTranslate('registration.create.questionnaire', 'Заполнение анкеты')}}</span></p>
            </div>
            <h1 class="section-title">{{getTranslate('registration.title', 'Регистрация')}}</h1>
            <p class="section-desc">{{getTranslate('order.customer.info', 'Заполните информацию о себе')}}</p>
            <div class="form-group form-file-group m-w-120 m-w-xl-150">
                <div class="form-img"><img src="{{ getPathToImage($userAvatar ?? '', 'images/ProfileDefault.svg') }}" alt="Profile"/></div>
                <input class="form-input file-input" type="file" name="avatar" accept="image/jpeg,image/png,image/jpg"/>
            </div>
            <div class="registration-credentials">
                <p>{{ $userPhone ?? '' }}</p>
                <p>{{ $userEmail ?? '' }}</p>
            </div>
            <div class="tabs-container">
                <form class="form-container m-w-300 m-w-xl-380" action="{{ route('registration.type.customer.send') }}"
                      method="POST" data-form-ajax="1">
                    @csrf
                    <input type="hidden" name="type" value="customer">
                    @includeIf('auth.registration.includes.name')
                    @includeIf('auth.registration.includes.surname')
                    @includeIf('auth.registration.includes.city')
                    @includeIf('components.password')
                    @includeIf('auth.registration.includes.send-btn')
                    @includeIf('auth.registration.includes.confirm-terms')
                </form>
            </div>
        </div>
    </section>
</main>