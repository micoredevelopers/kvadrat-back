<main id="confirm-phone-page" class="footerNone">
    <div class="container">
        <h2 class="section-title">{{ getTranslate('sms.code-from-sms', 'Код из смс') }}</h2>
        <h5 class="section-sub-title">{{ translateFormat('sms.code-send', ['%phone%' => $phone, ]) }}</h5>

        @includeIf('public.partials.sms.admin-show-code')
        <form class="form-wrap" method="POST" action="{{ route('password.reset.confirm.send') }}" id="codeSubmit">
            @csrf
            <div class="form-group code-wrap">
                @includeIf('public.partials.sms.four-fields-code')
            </div>
            <div class="form-group d-flex justify-content-center">
                <button class="custom-button">
                    <span>{{ getTranslate('auth.proceed', 'Продолжить') }}</span>
                    <span>{{ getTranslate('auth.proceed', 'Продолжить') }}</span>
                </button>
            </div>
        </form>

        <form action="{{ route('verification.resend') }}" method="post">
            @csrf
            @includeIf('public.partials.sms.resend-code')
        </form>
    </div>
</main>
