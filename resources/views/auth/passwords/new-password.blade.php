<main id="new-password-page">
    <form id="new-password-form" class="new-password-form">
        <div class="new-password_title">{{ getTranslate('registration.new.password.title', 'Новый пароль') }}</div>
        <div class="new-password_description">getTranslate('registration.password.rule.title', 'Придумайте новый пароль минимум из 8 символов, минимум с 1 цифрой и 1 буквой')</div>
        <input class="input-style password" minlength="8" name="password" placeholder="{{ getTranslate('registration.new.password.title', 'Новый пароль') }}"
               required type="password">
        <input class="input-style password" minlength="8" name="confirmPassword" placeholder="{{ getTranslate('registration.confirm.password', 'Подтверждение пароля')}}"
               required type="password">
        <button class="btn custom-button" type="submit">
            <span>{{ getTranslate('buttons.create.button', 'Создать')}}</span>
            <span>{{ getTranslate('buttons.create.button', 'Создать')}}</span>
        </button>
    </form>
</main>