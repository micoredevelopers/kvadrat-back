<main class="main-container">
        <div class="container d-flex justify-content-center">
            <form class="password_container" method="POST" action="{{ route('password.email') }}">
                @if (session('status'))
                    <div class="alert alert-success b-radius-none" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @csrf
                <h1 class="password_container_title">{!! showMeta('Восстановление пароля') !!}</h1>
                <div class="form-group">
                    <div class="password_container_input">
                        {!! errorDisplay('email', 'alert alert-danger b-radius-none') !!}
                        <input type="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="{{ getTranslate('registration.enter.email', 'Введите ваш Email') }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">{{ getTranslate('buttons.send.button', 'Отправить') }}</button>
            </form>
        </div>
</main>

