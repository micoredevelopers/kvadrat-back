

<main id="registration-page">
    <form action="{{ route('password.reset.phone') }}" class="registration-form" id="registration-form" method="POST">
        @csrf
        <div class="registration-form_title">{{ getTranslate('registration.recovery.password', 'Восстановление пароля')}}</div>
        <div class="registration-form_description">{{ getTranslate('registration.1-step-title', 'На следующем этапе мы отправим Вам смс с кодом подтверждения') }}</div>
        <div class="form-group text-center">
            {!! errorDisplay('phone') !!}
            <input class="input-style phone mb-0" minlength="3" name="phone"
                   placeholder="{{ getTranslate('forms.phone', 'Номер телефона') }}"
                   value="{{ old('phone') }}"
                   required>
        </div>
        <button class="btn custom-button" type="submit">
            <span>{{ getTranslate('buttons.recovery.button', 'Восстановить')}}</span>
            <span>{{ getTranslate('buttons.recovery.button', 'Восстановить')}}</span>
        </button>
        <a href="{{ route('login') }}"
           class="registration-back">{{ getTranslate('registration.back-to-auth', 'Вернуться к авторизации') }}</a>
    </form>
</main>