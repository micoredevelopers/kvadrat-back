<main id="phone-recovery-page">
    <section class="main-section">
        <div class="container">
            <h4 class="text-center"><span>{{ getTranslate('registration.recovery.password', 'Восстановление пароля')}}</span></h4>
            <form class="form-container" id="phone-recovery-form" method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    {!! errorDisplay('email', 'alert alert-danger b-radius-none') !!}
                    <input class="form-input full-width" type="email" value="{{ old('email', $email) }}" name="email"
                           placeholder="{{getTranslate('forms.email', 'E-mail')}}">
                </div>
                <div class="form-group">
                    {!! errorDisplay('password', 'alert alert-danger b-radius-none') !!}
                    <input class="form-input full-width" type="password" value="{{ old('password') }}" minlength="8"
                           name="password" placeholder="{{ getTranslate('forms.password', 'Пароль') }}" required>
                </div>
                <div class="form-group">
                    {!! errorDisplay('password_confirmation', 'alert alert-danger b-radius-none') !!}
                    <input class="form-input full-width" type="password" value="{{ old('password_confirmation') }}"
                           name="password_confirmation" minlength="8" required
                           placeholder="{{ getTranslate('registration.confirm.to.password', 'Подтвердите пароль') }}">
                </div>
                <button type="submit" class="custom-btn full-width"><span>{{ getTranslate('registration.change.to.password', 'Изменить пароль') }}</span></button>
            </form>
        </div>
    </section>
</main>
