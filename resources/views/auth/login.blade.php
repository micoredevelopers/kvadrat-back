<main class="main-container">
    <div class="container d-flex justify-content-center">

        <form id="login-form" class="login-form w-50" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group">
                <a href="{{ url('login/facebook') }}" class="btn btn-primary">{{ getTranslate('auth.enter.facebook', 'Войти через Facebook') }}</a>
                <a href="{{ url('login/google') }}" class="btn btn-secondary">{{ getTranslate('auth.enter.google', 'Войти через Google') }}</a>
{{--                <a href="#" class="btn btn-secondary disabled">Войти через Apple</a>--}}

            </div>

            <h4 class="mt-5">{{ getTranslate('auth.title', 'Авторизация') }}</h4>
            <div class="mb-5">
                <span>{{ getTranslate('auth.description', 'Введите номер телефона и пароль, который вы вводили при регистрации.') }}</span>
                <span>{{ getTranslate('auth.recover.forget', 'Забыли пароль?') }}</span>
                <a href="{{ route('password.request') }}" class="restore-password">{{ getTranslate('buttons.press.this', 'Нажмите сюда') }}</a>
            </div>
            <div class="form-group text-center">
                <input class="form-control" minlength="8" name="login" value="{{ old('login') }}"
                       placeholder="{{ getTranslate('forms.login', 'Email или номер телефона') }}" type="text"
                       required >
            </div>
            <div class="form-group text-center">
                <input class="form-control" minlength="3" name="password" type="password"
                       placeholder="{{ getTranslate('forms.password', 'Пароль') }}"
                       required>
            </div>
            <div class="d-flex justify-content-between">

                <button class="btn btn-success" type="submit">
                    <span>{{ getTranslate('auth.login.enter', 'Войти') }}</span>
                </button>
                <a href="{{ route('register') }}"
                   class="login-registration">{{ getTranslate('registration.do.registration', 'Зарегистрироваться') }}</a>
            </div>
        </form>
    </div>
</main>