<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models\Admin{
/**
 * App\Models\Admin\Admin
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $login
 * @property string|null $authenticated_at
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $locale
 * @property string|null $last_login_ip
 * @property string|null $user_agent
 * @property int $active
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Staff\DeletedBy|null $deletable
 * @property-read \App\Models\Staff\ModifiedBy|null $modifiable
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Admin active($active = 1)
 * @method static \Database\Factories\AdminFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAuthenticatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUserAgent($value)
 * @mixin \Eloquent
 */
	class Admin extends \Eloquent {}
}

namespace App\Models\Admin{
/**
 * App\Models\Admin\AdminMenu
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $active
 * @property mixed $name
 * @property string|null $url
 * @property string|null $gate_rule
 * @property string|null $route
 * @property string|null $image
 * @property string|null $icon_font
 * @property string|null $content_provider
 * @property string|null $option
 * @property string|null $description
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\AdminMenu[] $childrens
 * @property-read int|null $childrens_count
 * @property-read \App\Models\Admin\AdminMenu|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereContentProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereGateRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereIconFont($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\AdminMenu whereUrl($value)
 * @mixin \Eloquent
 */
	class AdminMenu extends \Eloquent {}
}

namespace App\Models\Admin{
/**
 * App\Models\Admin\Photo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @mixin \Eloquent
 */
	class Photo extends \Eloquent {}
}

namespace App\Models\Category{
/**
 * App\Models\Category\Category
 *
 * @property int $id
 * @property string|null $image
 * @property int|null $sort
 * @property int $active
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @property-read Category|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models\Category{
/**
 * App\Models\Category\CategoryLang
 *
 * @property int $category_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property-read \App\Models\Category\Category $category
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereName($value)
 */
	class CategoryLang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\City
 *
 * @property int $id
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @method static \Illuminate\Database\Eloquent\Builder|City whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereUpdatedAt($value)
 */
	class City extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models{
/**
 * App\Models\CityLang
 *
 * @property int $city_id
 * @property string|null $name
 * @property int $language_id
 * @property-read \App\Models\City $city
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CityLang whereName($value)
 */
	class CityLang extends \Eloquent {}
}

namespace App\Models\Faq{
/**
 * App\Models\Faq\Faq
 *
 * @property int $id
 * @property int|null $sort
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Faq extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models\Faq{
/**
 * App\Models\Faq\FaqLang
 *
 * @property int $faq_id
 * @property string|null $question
 * @property string|null $answer
 * @property int $language_id
 * @property-read \App\Models\Faq\Faq $faq
 * @property-read \App\Models\Language $language
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereFaqId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereLanguageId($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqLang whereQuestion($value)
 * @mixin \Eloquent
 */
	class FaqLang extends \Eloquent {}
}

namespace App\Models\Feedback{
/**
 * App\Models\Feedback\Feedback
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $type
 * @property string|null $fio
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $message
 * @property string|null $ip
 * @property string|null $referer
 * @property array|null $data
 * @property array|null $files
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereFiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereIp($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Feedback extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FranchisePackages
 *
 * @property int $id
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|FranchisePackages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FranchisePackages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FranchisePackages query()
 * @method static \Illuminate\Database\Eloquent\Builder|FranchisePackages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|FranchisePackages whereSort($value)
 */
	class FranchisePackages extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $imageable_type
 * @property int $imageable_id
 * @property int $active
 * @property string|null $name
 * @property string|null $image
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imageable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereImageableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereImageableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereType($value)
 */
	class Image extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Language
 *
 * @property int $id
 * @property int $active
 * @property int $default
 * @property string|null $name
 * @property string $key
 * @property string|null $icon
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereSort($value)
 */
	class Language extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Menu
 *
 * @property int $id
 * @property int $menu_group_id
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $type
 * @property string|null $url
 * @property string|null $icon Шрифтовые изображения
 * @property string|null $image обычная картинка, впринципе любого типа
 * @property int $sort
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\MenuGroup $menuGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|Menu[] $menus
 * @property-read int|null $menus_count
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereImage($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereMenuGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereUrl($value)
 * @mixin \Eloquent
 */
	class Menu extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models{
/**
 * App\Models\MenuGroup
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class MenuGroup extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MenuLang
 *
 * @property int $menu_id
 * @property string|null $name
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Menu $menu
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereName($value)
 * @mixin \Eloquent
 */
	class MenuLang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Meta
 *
 * @property int                             $id
 * @property string                          $url
 * @property int                             $type
 * @property int                             $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null                     $header html/js etc code for header
 * @property string|null                     $footer html/js etc code for footer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereFooter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereUrl($value)
 * @mixin \Eloquent
 */
	class Meta extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models{
/**
 * App\Models\MetaLang
 *
 * @property int $meta_id
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $text_top
 * @property string|null $text_bottom
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Meta $meta
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereKeywords($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereLanguageId($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTextBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTextTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaLang whereTitle($value)
 * @mixin \Eloquent
 */
	class MetaLang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @mixin \Eloquent
 */
	class Model extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ModelLang
 *
 * @property-read \App\Models\Language $language
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelLang query()
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @mixin \Eloquent
 */
	class ModelLang extends \Eloquent {}
}

namespace App\Models\Order{
/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $performer_id
 * @property int|null $customer_id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $description
 * @property int $budget
 * @property string|null $address
 * @property bool $is_urgent
 * @property int $views
 * @property int $city_id
 * @property int $active
 * @property int $is_blank
 * @property string|null $slug
 * @property-read int|null $bids_count
 * @property bool $is_closed
 * @property \Illuminate\Support\Carbon|null $closes_at
 * @property string|null $available_at feature, if will be need to publish at the specific time
 * @property \Illuminate\Support\Carbon|null $published_at for display date publish, because publishing can diff with date create
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $show_phone
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrderBid[] $bids
 * @property-read Category|null $category
 * @property-read City $city
 * @property-read User|null $customer
 * @property-read User|null $performer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrderUpload[] $uploads
 * @property-read int|null $uploads_count
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAvailableAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBidsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereClosesAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsBlank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsUrgent($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePerformerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShowPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereViews($value)
 * @mixin \Eloquent
 */
	class Order extends \Eloquent {}
}

namespace App\Models\Order{
/**
 * App\Models\Order\OrderBid
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property int $active
 * @property string|null $text
 * @property int $bid
 * @property int $days
 * @property int $available_edits
 * @property \Illuminate\Support\Carbon|null $modified_at
 * @property \Illuminate\Support\Carbon|null $canceled_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereAvailableEdits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereBid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereCanceledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereModifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderBid whereUserId($value)
 * @mixin \Eloquent
 */
	class OrderBid extends \Eloquent {}
}

namespace App\Models\Order{
/**
 * App\Models\Order\OrderUpload
 *
 * @property int $id
 * @property int $order_id
 * @property string|null $file
 * @property string|null $original_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderUpload whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class OrderUpload extends \Eloquent {}
}

namespace App\Models\Order{
/**
 * App\Models\Order\OrderView
 *
 * @property int $order_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order\Order $order
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereCreatedAt($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderView whereUserId($value)
 * @mixin \Eloquent
 */
	class OrderView extends \Eloquent {}
}

namespace App\Models\Order{
/**
 * App\Models\Order\Review
 *
 * @property int $id
 * @property float $rating
 * @property int $adequate_rating Адекватный
 * @property int $legibility_rating Чётко ставит задачу
 * @property int $suggestions_reacts
 * @property int $punctuality_rating
 * @property int $recommend_rating
 * @property int $from_id
 * @property int $to_id
 * @property int $is_published
 * @property string|null $published_at
 * @property string|null $comment
 * @property int $active
 * @property int $order_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $about Commented about performer or about customer
 * @property int $coast_rating Стоимость
 * @property int $quality_rating Качество
 * @property int $professionalism_rating Профессионализм
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\Order\Order $order
 * @property-read User $reviewFrom
 * @property-read User $reviewTo
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereAdequateRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCoastRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereIsPublished($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereLegibilityRating($value)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereProfessionalismRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePunctualityRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereQualityRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRecommendRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereSuggestionsReacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Review extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Page
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int|null $sort
 * @property mixed $url
 * @property bool $manual
 * @property int $active
 * @property string|null $image
 * @property string|null $page_type
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageLang[] $langs
 * @property-read int|null $langs_count
 * @property-read \App\Models\Menu|null $menu
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereManual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $sub_image
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSubImage($value)
 */
	class Page extends \Eloquent implements \App\Contracts\HasImagesContract, \App\Contracts\HasLocalized {}
}

namespace App\Models{
/**
 * App\Models\PageLang
 *
 * @property int $page_id
 * @property int $language_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @property string|null $excerpt
 * @property string|null $sub_title
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubTitle($value)
 */
	class PageLang extends \Eloquent {}
}

namespace App\Models\Page{
/**
 * App\Models\Page
 *
 * @property int                                                               $id
 * @property int|null                                                          $parent_id
 * @property int|null                                                          $sort
 * @property mixed                                                             $url
 * @property bool                                                              $manual
 * @property int                                                               $active
 * @property string|null                                                       $image
 * @property string|null                                                       $page_type
 * @property string|null                                                       $options
 * @property \Illuminate\Support\Carbon|null                                   $created_at
 * @property \Illuminate\Support\Carbon|null                                   $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @mixin \Eloquent
 * @property string|null $sub_image
 * @property-read int|null $images_count
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereImage($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereManual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page wherePageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSubImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUrl($value)
 */
	class Page extends \Eloquent implements \App\Contracts\HasImagesContract, \App\Contracts\HasLocalized {}
}

namespace App\Models\Page{
/**
 * App\Models\Page\PageLang
 *
 * @property int                       $page_id
 * @property int                       $language_id
 * @property string|null               $title
 * @property string|null               $description
 * @property string|null               $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page     $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page\PageLang whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @property string|null $excerpt
 * @property string|null $sub_title
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubTitle($value)
 */
	class PageLang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $group
 * @property string $guard_name
 * @property string $display_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\Admin[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Redirect
 *
 * @property int $id
 * @property string|null $from
 * @property string|null $to
 * @property string $code
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Redirect whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Redirect extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\Admin[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $key
 * @property string|null $display_name
 * @property string|null $value
 * @property string|null $details
 * @property string|null $type
 * @property int $sort
 * @property string|null $group
 * @property int|null $user_id Last edited by user
 * @property int|null $deleted_by Deleted by user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User|null $deletedBy
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting withoutTrashed()
 * @mixin \Eloquent
 */
	class Setting extends \Eloquent {}
}

namespace App\Models\Staff{
/**
 * App\Models\Staff\DeletedBy
 *
 * @property int $id
 * @property string|null $deletable_type
 * @property int|null $deletable_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $deletable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereDeletableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereDeletableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\DeletedBy whereUserId($value)
 * @mixin \Eloquent
 */
	class DeletedBy extends \Eloquent {}
}

namespace App\Models\Staff{
/**
 * App\Models\Staff\ModifiedBy
 *
 * @property int $id
 * @property string|null $modifiable_type
 * @property int|null $modifiable_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $modifiable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereModifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereModifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy whereUserId($value)
 * @mixin \Eloquent
 */
	class ModifiedBy extends \Eloquent {}
}

namespace App\Models\Translate{
/**
 * App\Models\Translate\Translate
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $comment
 * @property int $module_id
 * @property string|null $group
 * @property string $type
 * @property array|null $variables
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $user_id Last edited by user
 * @property int|null $deleted_by User id deleted translate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $deletedBy
 * @property-read mixed $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate\Translate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\Translate whereVariables($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate\Translate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate\Translate withoutTrashed()
 * @mixin \Eloquent
 */
	class Translate extends \Eloquent implements \App\Contracts\HasLocalized {}
}

namespace App\Models\Translate{
/**
 * App\Models\Translate\TranslateLang
 *
 * @property int $translate_id
 * @property string|null $value
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Translate\Translate $translate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang whereTranslateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate\TranslateLang whereValue($value)
 * @mixin \Eloquent
 */
	class TranslateLang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrialModal
 *
 * @property int $id
 * @property string|null $name
 * @property int $phone
 * @property string $city
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrialModal whereUpdatedAt($value)
 */
	class TrialModal extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $phone_verified_at
 * @property string|null $authenticated_at
 * @property string|null $last_seen_at
 * @property string $password
 * @property int $active
 * @property string|null $last_login_ip
 * @property string|null $user_agent
 * @property string|null $remember_token
 * @property string|null $gender
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $date_birth
 * @property string|null $description
 * @property string|null $company_workers
 * @property string|null $avatar
 * @property string|null $phone_second
 * @property string|null $about
 * @property string|null $customer_about
 * @property string|null $profession
 * @property int|null $city_id
 * @property bool $profile_enabled
 * @property float $rating
 * @property int $reviews_count
 * @property int $customer_profile_enabled
 * @property string|null $customer_avatar
 * @property float $customer_rating
 * @property int $customer_reviews_count
 * @property string|null $viber
 * @property string|null $telegram
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $site
 * @property int $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\City|null $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\UserMeta[] $metas
 * @property-read int|null $metas_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Portfolio[] $portfolios
 * @property-read int|null $portfolios_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Review[] $reviewsCustomer
 * @property-read int|null $reviews_customer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Review[] $reviewsPerformer
 * @property-read int|null $reviews_performer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\Category[] $skills
 * @property-read int|null $skills_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\SocialProvider[] $socialProviders
 * @property-read int|null $social_providers_count
 * @method static \Illuminate\Database\Eloquent\Builder|User active($active = 1)
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAuthenticatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCompanyWorkers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerProfileEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCustomerReviewsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDateBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastSeenAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfileEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereReviewsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereViber($value)
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\Portfolio
 *
 * @property int $id
 * @property int|null $sort
 * @property int $active
 * @property mixed|string $description
 * @property string|null $excerpt
 * @property int $category_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio query()
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portfolio whereUserId($value)
 * @mixin \Eloquent
 */
	class Portfolio extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\Skill
 *
 * @property int $user_id
 * @property int $category_id
 * @property-read Category $category
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Skill newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill query()
 * @method static \Illuminate\Database\Eloquent\Builder|Skill whereCategoryId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Skill whereUserId($value)
 * @mixin \Eloquent
 */
	class Skill extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\SocialProvider
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $provider_id
 * @property string|null $provider
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereId($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialProvider whereUserId($value)
 * @mixin \Eloquent
 */
	class SocialProvider extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\UserMeta
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $user
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereKey($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMeta whereValue($value)
 * @mixin \Eloquent
 */
	class UserMeta extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Verification
 *
 * @property int $id
 * @property string|null $available_at
 * @property string|null $ip
 * @property string|null $code
 * @property string|null $phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newQuery()
 * @method static Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification query()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereAvailableAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereIp($value)
 * @method static Builder|Model whereLanguage($languageId)
 * @method static Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Verification extends \Eloquent {}
}

